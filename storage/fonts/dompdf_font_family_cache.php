<?php return array (
  'sans-serif' => array(
    'normal' => $rootDir . '/lib/fonts/Helvetica',
    'bold' => $rootDir . '/lib/fonts/Helvetica-Bold',
    'italic' => $rootDir . '/lib/fonts/Helvetica-Oblique',
    'bold_italic' => $rootDir . '/lib/fonts/Helvetica-BoldOblique',
  ),
  'times' => array(
    'normal' => $rootDir . '/lib/fonts/Times-Roman',
    'bold' => $rootDir . '/lib/fonts/Times-Bold',
    'italic' => $rootDir . '/lib/fonts/Times-Italic',
    'bold_italic' => $rootDir . '/lib/fonts/Times-BoldItalic',
  ),
  'times-roman' => array(
    'normal' => $rootDir . '/lib/fonts/Times-Roman',
    'bold' => $rootDir . '/lib/fonts/Times-Bold',
    'italic' => $rootDir . '/lib/fonts/Times-Italic',
    'bold_italic' => $rootDir . '/lib/fonts/Times-BoldItalic',
  ),
  'courier' => array(
    'normal' => $rootDir . '/lib/fonts/Courier',
    'bold' => $rootDir . '/lib/fonts/Courier-Bold',
    'italic' => $rootDir . '/lib/fonts/Courier-Oblique',
    'bold_italic' => $rootDir . '/lib/fonts/Courier-BoldOblique',
  ),
  'helvetica' => array(
    'normal' => $rootDir . '/lib/fonts/Helvetica',
    'bold' => $rootDir . '/lib/fonts/Helvetica-Bold',
    'italic' => $rootDir . '/lib/fonts/Helvetica-Oblique',
    'bold_italic' => $rootDir . '/lib/fonts/Helvetica-BoldOblique',
  ),
  'zapfdingbats' => array(
    'normal' => $rootDir . '/lib/fonts/ZapfDingbats',
    'bold' => $rootDir . '/lib/fonts/ZapfDingbats',
    'italic' => $rootDir . '/lib/fonts/ZapfDingbats',
    'bold_italic' => $rootDir . '/lib/fonts/ZapfDingbats',
  ),
  'symbol' => array(
    'normal' => $rootDir . '/lib/fonts/Symbol',
    'bold' => $rootDir . '/lib/fonts/Symbol',
    'italic' => $rootDir . '/lib/fonts/Symbol',
    'bold_italic' => $rootDir . '/lib/fonts/Symbol',
  ),
  'serif' => array(
    'normal' => $rootDir . '/lib/fonts/Times-Roman',
    'bold' => $rootDir . '/lib/fonts/Times-Bold',
    'italic' => $rootDir . '/lib/fonts/Times-Italic',
    'bold_italic' => $rootDir . '/lib/fonts/Times-BoldItalic',
  ),
  'monospace' => array(
    'normal' => $rootDir . '/lib/fonts/Courier',
    'bold' => $rootDir . '/lib/fonts/Courier-Bold',
    'italic' => $rootDir . '/lib/fonts/Courier-Oblique',
    'bold_italic' => $rootDir . '/lib/fonts/Courier-BoldOblique',
  ),
  'fixed' => array(
    'normal' => $rootDir . '/lib/fonts/Courier',
    'bold' => $rootDir . '/lib/fonts/Courier-Bold',
    'italic' => $rootDir . '/lib/fonts/Courier-Oblique',
    'bold_italic' => $rootDir . '/lib/fonts/Courier-BoldOblique',
  ),
  'dejavu sans' => array(
    'bold' => $rootDir . '/lib/fonts/DejaVuSans-Bold',
    'bold_italic' => $rootDir . '/lib/fonts/DejaVuSans-BoldOblique',
    'italic' => $rootDir . '/lib/fonts/DejaVuSans-Oblique',
    'normal' => $rootDir . '/lib/fonts/DejaVuSans',
  ),
  'dejavu sans mono' => array(
    'bold' => $rootDir . '/lib/fonts/DejaVuSansMono-Bold',
    'bold_italic' => $rootDir . '/lib/fonts/DejaVuSansMono-BoldOblique',
    'italic' => $rootDir . '/lib/fonts/DejaVuSansMono-Oblique',
    'normal' => $rootDir . '/lib/fonts/DejaVuSansMono',
  ),
  'dejavu serif' => array(
    'bold' => $rootDir . '/lib/fonts/DejaVuSerif-Bold',
    'bold_italic' => $rootDir . '/lib/fonts/DejaVuSerif-BoldItalic',
    'italic' => $rootDir . '/lib/fonts/DejaVuSerif-Italic',
    'normal' => $rootDir . '/lib/fonts/DejaVuSerif',
  ),
  'qanelas light italic' => array(
    '300_italic' => $fontDir . '/qanelas_light_italic_300_italic_901812787938aa5d5a0ce80a248cc229',
  ),
  'qanelas light' => array(
    '300' => $fontDir . '/qanelas_light_300_bd1afa5948c215c9c700498308b6fea9',
  ),
  'qanelas black' => array(
    '900' => $fontDir . '/qanelas_black_900_807de20bd6b26e48a9e684464b978a67',
  ),
  'qanelas regular' => array(
    'normal' => $fontDir . '/qanelas_regular_normal_4bc5dafe2e3682fb540073a105457790',
  ),
  'qanelas bold' => array(
    'bold' => $fontDir . '/qanelas_bold_bold_e0d570e155693fbcb187d885e9feb84d',
  ),
  'qanelas regular italic' => array(
    'italic' => $fontDir . '/qanelas_regular_italic_italic_8fed7ddc712de0c6c4f78e5390a93194',
  ),
  'qanelas medium' => array(
    '500' => $fontDir . '/qanelas_medium_500_5d0d284a0040f2990f1a13aa2dd1b65b',
  ),
  'qanelas semi bold' => array(
    '500' => $fontDir . '/qanelas_semi_bold_500_36709ebcac580ab2eab76dee48f472c7',
  ),
) ?>