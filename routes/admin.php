<?php

/*
 *
 *  admin panel
 *
 */

/*
Route::get('test', function(){
    app('Illuminate\Bus\Dispatcher')->dispatch(new \App\Jobs\StatusSwitching());
});
*/

Route::get('admin/login', 'Admin\AdminController@getLogin');

Route::get('logout', 'Auth\AuthController@logout');

Route::post('admin/login', 'Admin\AdminController@postLogin');

Route::group(['middleware' => ['admin'], 'prefix' => 'admin', 'as' => 'admin.'], function () {

    Route::get('/', ['uses' => 'Admin\AdminController@index', 'as' => 'admin.index']);

    Route::get('json/changevisibility', 'Admin\JsonController@getChangeVisibility');

    Route::get('json/delete', 'Admin\JsonController@getDeleteModel');

    Route::resource('lists', 'Admin\ListsController');

    Route::get('get-users', 'Admin\UserController@ajaxData');

    Route::resource('users', 'Admin\UserController');

    Route::resource('content', 'Admin\ContentController');

    Route::get('clear-cache', 'Admin\AdminController@clearCache');

    Route::get('get-products', 'Admin\ProductsController@ajaxData');

    Route::resource('products', 'Admin\ProductsController');

    Route::get('translations', 'Admin\TranslationsController@edit');

    Route::post('translations', 'Admin\TranslationsController@save');

    Route::get('get-categories', 'Admin\CategoriesController@ajaxData');

    Route::resource('categories', 'Admin\CategoriesController');

    Route::get('get-agents', 'Admin\AgentsController@ajaxData');

    Route::resource('agents', 'Admin\AgentsController');

    Route::get('get-news', 'Admin\NewsController@ajaxData');

    Route::resource('news', 'Admin\NewsController');

    Route::resource('regions', 'Admin\RegionController');

    Route::get('get-regions', 'Admin\RegionController@ajaxData');

    Route::resource('region_groups', 'Admin\RegionGroupsController');

    Route::get('get-region-groups', 'Admin\RegionGroupsController@ajaxData');

    Route::resource('parameters', 'Admin\ParametersController');

    Route::resource('reviews', 'Admin\ReviewsController');

    Route::get('get-parameters', 'Admin\ParametersController@ajaxData');

    Route::get('json/remove-parameter', 'Admin\ParametersController@removeParameter');

    Route::get('json/remove-parameter-value', 'Admin\ParametersController@removeParameterValue');

    Route::get('json/get-category-parameters',  'Admin\ParametersController@getCategoryParameters');

    Route::get('products-agents-excel', 'Admin\ProductsController@productsAgentsPage');
    Route::post('generate-products-agents-excel', 'Admin\ProductsController@productsAgentsExcel')->name('generate-products-agents-excel');
});


/*
 *
 *  photomanager
 *
 */
// upload
Route::group(['middleware' => ['admin']], function () {
    Route::any('photos/upload', 'Admin\PhotosController@upload');
    Route::get('photos/getphotos', 'Admin\PhotosController@getJSONPhotos');
    Route::get('photos/changesort', 'Admin\PhotosController@changesort');
    Route::get('photos/delete/{id}', 'Admin\PhotosController@destroy');
    Route::post('photos/crop', 'Admin\PhotosController@postCrop');
});



