<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', 'CommonController@index')->name('index');

Route::get('category/{slug}', 'ProductsController@getCategory')->name('get-category');

Route::get('rent', 'ProductsController@getRent')->name('rent');

Route::get('rent/{slug}', 'ProductsController@getRentCategory')->name('rent-category');

Route::get('sale', 'ProductsController@getSale')->name('sale');

Route::get('sale/{slug}', 'ProductsController@getSaleCategory')->name('sale-category');

Route::get('products/{slug}', 'ProductsController@getProduct')->name('product');

Route::get('get-category-filters/{id}', 'ProductsController@getCategoryFilters')->name('get-category-filters');

Route::get('get-objects/{type}', 'ProductsController@getObjects')->name('get-objects');

Route::get('get-top-objects', 'ProductsController@getTopObjects')->name('get-top-objects');

Route::get('get-related-objects/{id}', 'ProductsController@getRelatedObjects')->name('get-related-objects');

Route::get('get-agent-objects/{id}', 'ProductsController@getAgentObjects')->name('get-agent-objects');

Route::get('news', 'NewsController@getAllNews')->name('news');

Route::get('news/{slug}', 'NewsController@getOneNews')->name('get-one-news');

Route::get('search', 'ProductsController@search')->name('search');

Route::get('get-search-results', 'ProductsController@getSearchResults')->name('get-search-results');

Route::post('send-contact-form', 'CommonController@sendContactForm')->name('send-contact-form');


/*Route::get('elements', function () {
    return view('elements');
})->name('elements');*/

Route::get('team1200522', 'CommonController@getTeam')->name('team');

/*Route::get('team-test', 'CommonController@getTeamTest')->name('team-test');*/

Route::get('objects-list', function () {
    return view('objects-list');
})->name('objects-list');

Route::post('amo/new-lead', 'RealtorController@newLead')->name('amo.new-lead');
Route::get('amo/updateToken', 'RealtorController@updateToken')->name('amo.updateToken');

Route::get('agent/{id}', 'CommonController@getAgent')->name('get-agent');

/*Route::get('object', function () {
    return view('object');
})->name('object');*/

Route::get('experts', function () {
    return view('experts');
})->name('experts');

/*Route::get('projects', function () {
    return view('projects');
})->name('projects');*/

Route::get('about', function () {
    return view('about');
})->name('about');

Route::get('our-team', 'CommonController@getOurTeam')->name('our-team');

Route::get('rieltor-objects', function () {
    return view('rieltor-objects');
})->name('rieltor-objects');

Route::get('contacts', function () {
    return view('contacts');
})->name('contacts');

Route::get('careers', function () {
    return view('careers');
})->name('careers');

Route::get('investors', function () {
    return view('investors');
})->name('investors');

Route::get('developers', function () {
    return view('developers');
})->name('developers');

/*Route::get('philosophy-page', function () {
    return view('philosophy');
})->name('philosophy-page');*/

Route::get('mission-page', function () {
    return view('mission');
})->name('mission-page');


Route::get('advices', function () {
    return view('advices');
})->name('advices');

Route::get('services', function () {
    return view('services');
})->name('services');

/*Route::get('content-page', function () {
    return view('content-page');
})->name('content-page');*/

Route::get('generate-pdf/{id}', 'ProductsController@generatePdf')->name('generate-pdf');

Route::post('send-contact-form', 'CommonController@sendContactForm')->name('send-contact-form');
Route::post('send-booking-form', 'CommonController@sendBookingForm')->name('send-booking-form');
Route::post('send-pdf-form', 'CommonController@sendPDFForm')->name('send-pdf-form');
Route::post('send-pdf-form', 'CommonController@sendPDFForm')->name('send-pdf-form');

Route::post('send-order-call-form', 'CommonController@sendOrderCall')->name('send-order-call-form');
Route::post('send-newsletter-form', 'CommonController@sendNewsletterForm')->name('send-newsletter-form');
Route::post('send-consultation-form', 'CommonController@sendConsultationForm')->name('send-consultation-form');
Route::post('send-agent-call-form', 'CommonController@sendAgentCallForm')->name('send-agent-call-form');
Route::post('send-mobile-call-form', 'CommonController@sendMobileCallForm')->name('send-mobile-call-form');
Route::post('send-agent-mortgage-form', 'CommonController@sendAgentMortgageForm')->name('send-agent-mortgage-form');

// Localization
Route::get('/js/lang.js', function () {
    $lang = app()->getLocale();

    $files   = glob(resource_path('lang/' . $lang . '/vue.php'));
    $strings = [];

    foreach ($files as $file) {
        $name           = basename($file, '.php');
        $strings[$name] = require $file;
    }

    header('Content-Type: text/javascript');
    echo('window.i18n = ' . json_encode($strings) . ';');
    exit();
})->name('assets.lang');

// ---- AmoRealtor 25/09/2022
Route::post('/realtor/webhook', 'RealtorController@webhook')->name('realtor/webhook');
Route::post('/realtor/update-label', 'RealtorController@updateLabel')->name('realtor/updateLabel');
// ---- AmoRealtor end

Route::get('privacy-policy', 'CommonController@getPrivacyPolicy')->name('privacy-policy');

Route::post('/simpals/upload-999', 'SimpalsController@addAdvert')->name('add-advert');

//!!!!этот роут должен быть всегда последним!!!!!
//!!!!этот роут должен быть всегда последним!!!!!
//!!!!этот роут должен быть всегда последним!!!!!
//!!!!этот роут должен быть всегда последним!!!!!
//!!!!этот роут должен быть всегда последним!!!!!
Route::get('{slug}', ['uses'=>'CommonController@getBySlug', 'as' => 'content']);


/*
 * Route patterns
 */
Route::pattern('id', '\d+');
Route::pattern('hash', '[a-z0-9]+');
Route::pattern('hex', '[a-f0-9]+');
Route::pattern('uuid', '[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}');
Route::pattern('base', '[a-zA-Z0-9]+');
Route::pattern('slug', '[a-z0-9-]+');
Route::pattern('username', '[a-z0-9_-]{3,16}');