<?php

use Illuminate\Foundation\Inspiring;

/*
|--------------------------------------------------------------------------
| Console Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of your Closure based console
| commands. Each Closure is bound to a command instance allowing a
| simple approach to interacting with each command's IO methods.
|
*/

Artisan::command('photos-lowercase-fix', function () {
    app('Illuminate\Bus\Dispatcher')->dispatch(new \App\Jobs\PhotosLowercaseFix());
})->describe('Change photos to lowercase in DB and uploaded folder');

Artisan::command('delete-old-products', function () {
    app('Illuminate\Bus\Dispatcher')->dispatch(new \App\Jobs\DeleteOldProducts());
});

Artisan::command('remove-unused-photos', function () {
    app('Illuminate\Bus\Dispatcher')->dispatch(new \App\Jobs\RemoveUnusedPhotos());
});

Artisan::command('refactor-categories', function () {
    app('Illuminate\Bus\Dispatcher')->dispatch(new \App\Jobs\RefactorCategories());
});

Artisan::command('refresh-amo-crm-token', function () {
    app('Illuminate\Bus\Dispatcher')->dispatch(new \App\Jobs\RefreshAmoCrmToken());
});

Artisan::command('convert-images {--productId=?}', function ($productId) {
    app('Illuminate\Bus\Dispatcher')->dispatch(new \App\Jobs\ConvertImages($productId));
});
