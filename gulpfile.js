const gulp = require('gulp');
const babel = require('gulp-babel');
const minify = require('gulp-minify');




gulp.task('fix', () =>
    gulp.src('public/js/app.js')
        .pipe(babel({
            "presets": [
                [ "@babel/preset-env", {
                    "targets": {
                        "browsers": [ "defaults" ]
                    }
                }]
            ]
        }))
        .pipe(gulp.dest('public/js/'))
);


gulp.task('compress', function() {
    gulp.src('public/js/app.js')
        .pipe(minify())
});


