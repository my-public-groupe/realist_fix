$(document).ready(function () {

    const body = document.body,
        html = document.documentElement;

    const siteHeight = Math.max(body.scrollHeight, body.offsetHeight,
        html.clientHeight, html.scrollHeight, html.offsetHeight);

    const footerHeight = document.getElementById('footer').clientHeight;
    const contentWithoutFooter = siteHeight - 896 - footerHeight


    const pathname = window.location.pathname;
    if(!pathname.includes('rent') && !pathname.includes('sale') && !pathname.includes('agent') && !pathname.includes('search')){
        window.onscroll = function () {
            const scrollTop = window.pageYOffset ? window.pageYOffset : (document.documentElement.scrollTop ? document.documentElement.scrollTop : document.body.scrollTop);

            if (scrollTop >= contentWithoutFooter) {
                scrollTopBottom = scrollTop - contentWithoutFooter;
                $('.rieltor-info-for-mobile').css("bottom", scrollTop);
            } else {
                $('.rieltor-info-for-mobile').css("bottom", "0");
            }
        }
    }

    $(".tooltip-custom").hover(function () {
        const topHeight = $(".tooltiptext-custom").height();
        $(".tooltiptext-custom").css("top", -topHeight - 14)
    });

    $(".navbar-toggler").click(function () {
   
    var isClass = $(".navbar-toggler").hasClass('navbar-collapse');
        if (!isClass) {
            $(".mobile-search").removeClass("show-mobile-nav");
            $(".mobile-languages-submenu").addClass("show-mobile-nav");

            $(".mobile-phone-container").addClass("hide-header-number");
            $(".header-menu").addClass("h-100");

            $(".navbar-brand img").removeClass("show-logo");
            $(".logo-mobile").addClass("show-logo");

            $(".navbar-toggler span").removeClass("show-icon");
            $(".close-mobile-menu").addClass("show-icon");

            $(".navbar-brand").addClass("navbar-brands-menu");
            $(".navbar-toggler").addClass("navbar-collapse");
        } 
        else {
            $(".mobile-search").addClass("show-mobile-nav");
            $(".mobile-languages-submenu").removeClass("show-mobile-nav");

            $(".mobile-phone-container").removeClass("hide-header-number");
            $(".header-menu").removeClass("h-100");

            $(".navbar-brand img").addClass("show-logo");
            $(".logo-mobile").removeClass("show-logo");

            $(".navbar-toggler span").addClass("show-icon");
            $(".close-mobile-menu").removeClass("show-icon");

            $(".navbar-brand").removeClass("navbar-brands-menu");
            $(".navbar-toggler").removeClass("navbar-collapse");

        }
        console.log("isClass", isClass)
    });



    $("details").click(function () {
        const isOpen = $(this).hasClass("open-details")
        if (!isOpen) {
            $("details").removeAttr("open");
            $("details").removeClass("open-details");
        }
        else {
            $("details").addClass("open-details");
        }

        $(this).toggleClass("open-details");
    })

    $(".has-submenu").click(function () {
        const isClass = $(this).hasClass("show-submenu");

        if (isClass) {
            $(this).removeClass("show-submenu");
        }

        else {
            $(".nav-item").removeClass("show-submenu").removeClass("arrow-up");
        }



        $(this).toggleClass("show-submenu");
        $(this).toggleClass("arrow-up");
    })
    $(".contacts-tab-content details").click(function () {
        const parentId = $(this).attr("id");
        $(".map-social-info iframe").removeClass("show-map");

        $(`#${parentId}Child`).addClass("show-map");

    })

    $('.sub-menu').on('click', function (e) {
        e.stopPropagation();
    });
    $(".languages-container").click(function () {
        $(this).toggleClass("show-submenu-languages");
    });


    $(".schedule-meet img").click(function () {
        $(".rieltor-social-media").toggleClass("show-social");
    });
    
    $(".filter-btn-mobile").click(function () {
        $(".filter-container").toggleClass("show-filter-menu");
        $(".offers-content").toggleClass("remove-position-relative");
        $(".filter-container-overlay-only-for-mobile").toggleClass("show-overlay");
        $(".reset-or-check-filter-button-container").toggleClass("show-check-filter-buttons");

    });
    $(".close-for-mobile").click(function () {
        $(".filter-container").removeClass("show-filter-menu");
        $(".offers-content").removeClass("remove-position-relative");
        $(".filter-container-overlay-only-for-mobile").removeClass("show-overlay");
        $(".reset-or-check-filter-button-container").removeClass("show-check-filter-buttons");
    });

    $(".tab-item").click(function () {
        $(".tab-item").removeClass("active");
        $(this).addClass("active");

        const parentId = $(this).attr("id");
        $(".contacts-tab-content .tab-content-item").removeClass("show-content");
        $("#" + parentId + "TabContent").addClass("show-content");

        if (parentId == "mail") {
            $(".adaptive-widget").addClass("hide-widget");
        } else {
            $(".adaptive-widget").removeClass("hide-widget");
        }
    });

    $(".tab-content-item details").click(function () {
        // $("details").removeClass("active-details");
        // $(this).toggleClass("active-details");


    });

    $('.languages-submenu').on('click', function (e) {
        e.stopPropagation();
    });

    $(".close-btn").click(function () {
        $(".custom-modal-content").removeClass("show-modal");
    });

    /*$(".read-more").click(function () {

        $(".custom-modal-content").addClass("show-modal");
        $(".has-bg").addClass("show-modal-content");
        $(".form-success ").removeClass("show-modal");

    });*/
    $(".read-more-success-form").click(function () {
        $(".has-bg").removeClass("show-modal-content");
        $(".form-success").addClass("show-modal");
    });

    jQuery(function ($) {
        $(document).mouseup(function (e) {
            var div = $(".languages-container");
            var divMenu = $(".has-submenu");
            if (!div.is(e.target)
                && div.has(e.target).length === 0) {
                div.removeClass("show-submenu-languages");
            }
            if (!divMenu.is(e.target)
                && divMenu.has(e.target).length === 0) {
                divMenu.removeClass("arrow-up");
            }
        });


    });


    $("[id^=carousel-thumbs]").carousel({
        interval: false
    });
    
    /** Pause/Play Button **/
    $(".carousel-pause").click(function () {
        var id = $(this).attr("href");
        if ($(this).hasClass("pause")) {
            $(this).removeClass("pause").toggleClass("play");
            $(this).children(".sr-only").text("Play");
            $(id).carousel("pause");
        } else {
            $(this).removeClass("play").toggleClass("pause");
            $(this).children(".sr-only").text("Pause");
            $(id).carousel("cycle");
        }
        $(id).carousel;
    });
    
    /** Fullscreen Buttun **/
    $(".carousel-fullscreen").click(function () {
        var id = $(this).attr("href");
        $(id).find(".active").ekkoLightbox({
            type: "image"
        });
    });
    
    if ($("[id^=carousel-thumbs] .carousel-item").length < 2) {
        $("#carousel-thumbs [class^=carousel-control-]").remove();
        $("#carousel-thumbs").css("padding", "0 5px");
    }
    
    $("#carousel").on("slide.bs.carousel", function (e) {
        var id = parseInt($(e.relatedTarget).attr("data-slide-number"));
        var thumbNum = parseInt(
            $("[id=carousel-selector-" + id + "]")
                .parent()
                .parent()
                .attr("data-slide-number")
        );
        $("[id^=carousel-selector-]").removeClass("selected");
        $("[id=carousel-selector-" + id + "]").addClass("selected");
        $("#carousel-thumbs").carousel(thumbNum);
    });
    

})



$(document).ready(function () {

    //Show carousel-control

    $("#myCarousel").mouseover(function () {
        $("#myCarousel .carousel-control").show();
    });

    $("#myCarousel").mouseleave(function () {
        $("#myCarousel .carousel-control").hide();
    });

    $("#myCarousel-responsive").mouseover(function () {
        $("#myCarousel .carousel-control").show();
    });

    $("#myCarousel-responsive").mouseleave(function () {
        $("#myCarousel .carousel-control").hide();
    });

    //Active thumbnail

    $("#thumbCarousel-responsive .thumb").on("click", function () {
        $(this).addClass("active");
        $(this).siblings().removeClass("active");
    });

    //When the carousel slides, auto update

    $('#myCarousel-responsive').on('slid.bs.carousel', function () {
        var index = $('.carousel-inner .item.active').index();
        //console.log(index);
        var thumbnailActive = $('#thumbCarousel .thumb[data-slide-to="' + index + '"]');
        thumbnailActive.addClass('active');
        $(thumbnailActive).siblings().removeClass("active");
        //console.log($(thumbnailActive).siblings()); 
    });

    $("#thumbCarousel .thumb").on("click", function () {
        $(this).addClass("active");
        $(this).siblings().removeClass("active");
    });

    //When the carousel slides, auto update

    $('#myCarousel').on('slid.bs.carousel', function () {
        var index = $('.carousel-inner .item.active').index();
        //console.log(index);
        var thumbnailActive = $('#thumbCarousel .thumb[data-slide-to="' + index + '"]');
        thumbnailActive.addClass('active');
        $(thumbnailActive).siblings().removeClass("active");
        //console.log($(thumbnailActive).siblings()); 
    });

    $("#thumbCarousel .additional-photos").on("click", function () {
        if ($('#thumbCarousel .thumb').length % 4 == 0) {
            var count = Math.trunc($('#thumbCarousel .thumb').length / 4);
        }
        else {
            var count = Math.trunc($('#thumbCarousel .thumb').length / 4) + 1;
        }
        $("#thumbCarousel").css( "height", function( index, value ){
            return parseFloat( value ) * count;
        });
        $("#thumbCarousel .additional-photos").removeClass("d-flex");
        $("#thumbCarousel .additional-photos").addClass("d-none");
        $("#thumbCarousel .thumb").removeClass("d-none");
        $("#thumbCarousel .thumb").addClass("d-flex");
    });

    $("#thumbCarousel-responsive .additional-photos").on("click", function () {
        if ($('#thumbCarousel-responsive .thumb').length % 3 == 0) {
            var count = Math.trunc($('#thumbCarousel-responsive .thumb').length / 3);
        }
        else {
            var count = Math.trunc($('#thumbCarousel-responsive .thumb').length / 3) + 1;
        }
        $("#thumbCarousel-responsive").css( "height", function( index, value ){
            return parseFloat( value ) * count;
        });
        $("#thumbCarousel-responsive .additional-photos").removeClass("d-flex");
        $("#thumbCarousel-responsive .additional-photos").addClass("d-none");
        $("#thumbCarousel-responsive .thumb").removeClass("d-none");
        $("#thumbCarousel-responsive .thumb").addClass("d-flex");
    });
});

let old_btn_text;
function disableButton(button) {
    old_btn_text = $(button).text();
    $(button).text(window.i18n.vue.wait);
    $(button).css("opacity", 0.5);
    $(button).attr("disabled", true);
    $(button).css("pointer-events", "none");
}

function enableButton(button) {
    $(button).text(old_btn_text);
    $(button).css("opacity", 1.0);
    $(button).attr("disabled", false);
    $(button).css("pointer-events", "all");
}





