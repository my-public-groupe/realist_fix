(function ($) {
    $(function () {
        var jcarousel = $('.jcarousel-works');
        var scrollIntervalId;

        jcarousel
            .on('jcarousel-works:reload jcarousel:create', function () {
                var carousel = $(this),
                    width = carousel.innerWidth();

                if (width >= 992) {
                    width = width / 4;
                } else if (width <= 991 && width > 768) {
                    width = width / 3;
                } else if (width <= 768 && width > 480) {
                    width = width / 2;
                } else if (width <= 480) {
                    width = width / 1;
                }

                carousel.jcarousel('items').css('width', Math.ceil(width) + 'px');
            })
            .jcarousel({
                wrap: 'circular'
            });

        $('.jcarousel-works-control-prev').jcarouselControl({
            target: '-=1'
        });

        $('.jcarousel-works-control-next').jcarouselControl({
            target: '+=1'
        });

        $('.jcarousel-works-pagination')
            .on('jcarouselpagination:active', 'a', function () {
                $(this).addClass('active');
            })
            .on('jcarouselpagination:inactive', 'a', function () {
                $(this).removeClass('active');
            })
            .on('click', function (e) {
                e.preventDefault();
            })
            .jcarouselPagination({
                perPage: 2,
                item: function (page) {
                    return '<a href="#' + page + '">' + page + '</a>';
                }
            });

        function startScrollInterval() {
            if (jcarousel )
            scrollIntervalId = setInterval(function () {
                jcarousel.jcarousel('scroll', '+=1');
            }, 2500);
        }

        startScrollInterval();

        jcarousel.on('mouseenter', function () {
            clearInterval(scrollIntervalId);
        });

        jcarousel.on('mouseleave', function () {
            startScrollInterval();
        });
    });
})(jQuery);


(function ($) {
    $(function () {
        var jcarousel = $('.jcarousel-testimonials');

        jcarousel
            .on('jcarousel-testimonials:reload jcarousel:create', function () {
                var carousel = $(this),
                    width = carousel.innerWidth();

                if (width >= 600) {
                    width = width / 1;
                } else if (width < 500) {
                    width = width / 1;
                }
                carousel.jcarousel('items').css('width', Math.ceil(width) + 'px');
            })
            .jcarousel({
                wrap: 'circular'
            });

        $('.jcarousel-testimonials-control-prev')
            .jcarouselControl({
                target: '-=1'
            });

        $('.jcarousel-testimonials-control-next')
            .jcarouselControl({
                target: '+=1'
            });

        $('.jcarousel-testimonials-pagination')
            .on('jcarouselpagination:active', 'a', function () {
                $(this).addClass('active');
            })
            .on('jcarouselpagination:inactive', 'a', function () {
                $(this).removeClass('active');
            })
            .on('click', function (e) {
                e.preventDefault();
            })
            .jcarouselPagination({
                perPage: 2,
                item: function (page) {
                    return '<a href="#' + page + '">' + page + '</a>';
                }
            });

        function startScrollInterval() {
            scrollIntervalId = setInterval(function () {
                jcarousel.jcarousel('scroll', '+=1');
            }, 2500);
        }

        startScrollInterval();

        jcarousel.on('mouseenter', function () {
            clearInterval(scrollIntervalId);
        });

        jcarousel.on('mouseleave', function () {
            startScrollInterval();
        });
    });
})(jQuery);

(function ($) {
    $(function () {
        var jcarousel = $('.jcarousel');

        jcarousel
            .on('jcarousel:reload jcarousel:create', function () {
                var carousel = $(this),
                    width = carousel.innerWidth();

                if (width >= 600) {
                    width = width / 8;
                } else if (width < 500) {
                    width = width / 2;
                }
                // alert(width);
                carousel.jcarousel('items').css('width', Math.ceil(width) + 'px');
            })
            .jcarousel({
                wrap: 'circular'
            });

        $('.jcarousel-control-prev')
            .jcarouselControl({
                target: '-=1'
            });

        $('.jcarousel-control-next')
            .jcarouselControl({
                target: '+=1'
            });

        $('.jcarousel-pagination')
            .on('jcarouselpagination:active', 'a', function () {
                $(this).addClass('active');
            })
            .on('jcarouselpagination:inactive', 'a', function () {
                $(this).removeClass('active');
            })
            .on('click', function (e) {
                e.preventDefault();
            })
            .jcarouselPagination({
                perPage: 2,
                item: function (page) {
                    return '<a href="#' + page + '">' + page + '</a>';
                }
            });
    });
})(jQuery);
