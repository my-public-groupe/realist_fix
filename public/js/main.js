(function ($) {

	"use strict";

	$('nav .dropdown').hover(function () {
		var $this = $(this);
		$this.addClass('show');
		$this.find('> a').attr('aria-expanded', true);
		$this.find('.dropdown-menu').addClass('show');
	}, function () {
		var $this = $(this);
		$this.removeClass('show');
		$this.find('> a').attr('aria-expanded', false);
		$this.find('.dropdown-menu').removeClass('show');
	});

	$('#call-order-form').submit(function (e) {
		e.preventDefault();
		let $this = $(this);

		disableButton($this.find("button[type=submit]"));

		$.post(window.lang + '/send-order-call-form', $this.serialize(), function (response) {
			if (response.success) {
				$(".success-modal").find('.order-call-title').html(window.i18n.vue.call_form_success_text);
				$(".success-modal").addClass("show-modal");
				$(".success-modal").find(".has-bg").addClass("show-modal-content");

				enableButton($this.find("button[type=submit]"));

				$this.trigger('reset');
			}
		});
	});

	$('#newsletter-form').submit(function (e) {
		e.preventDefault();
		let $this = $(this);

		disableButton($this.find("button[type=submit]"));

		$.post(window.lang + '/send-newsletter-form', $this.serialize(), function (response) {
			if (response.success) {
				$(".success-modal").find('.order-call-title').html(window.i18n.vue.newsletter_form_success_text);
				$(".success-modal").addClass("show-modal");
				$(".success-modal").find(".has-bg").addClass("show-modal-content");

				enableButton($this.find("button[type=submit]"));

				$this.trigger('reset');
			}
		});
	});

	$('#consultation-form').submit(function (e) {
		e.preventDefault();
		let $this = $(this);

		disableButton($this.find("button[type=submit]"));

		$.post(window.lang + '/send-consultation-form', $this.serialize(), function (response) {
			if (response.success) {
				$(".success-modal").find('.order-call-title').html(window.i18n.vue.consultation_form_success_text);
				$(".success-modal").addClass("show-modal");
				$(".success-modal").find(".has-bg").addClass("show-modal-content");

				enableButton($this.find("button[type=submit]"));

				$this.trigger('reset');
			}
		});
	});

	$('.rieltor-order-date').click(function () {
		$(".agent-call-modal").addClass("show-modal");
		$(".agent-call-modal").find(".has-bg").addClass("show-modal-content");
	});

	$("#orderCall").click(function () {
		$(".mobile-call-modal").addClass("show-modal");
		$(".mobile-call-modal").find(".has-bg").addClass("show-modal-content");
	});

	$('#agent-call-form').submit(function (e) {
		e.preventDefault();
		let $this = $(this);

		disableButton($this.find("button[type=submit]"));

		$.post(window.lang + '/send-agent-call-form', $this.serialize(), function (response) {
			if (response.success) {
				$(".agent-call-modal").removeClass("show-modal");
				$(".agent-call-modal").find(".has-bg").removeClass("show-modal-content");

				$(".success-modal").find('.order-call-title').html(window.i18n.vue.agent_form_success_text);
				$(".success-modal").addClass("show-modal");
				$(".success-modal").find(".has-bg").addClass("show-modal-content");

				enableButton($this.find("button[type=submit]"));

				$this.trigger('reset');
			}
		});
	});

	$('#mobile-call-form').submit(function (e) {
		e.preventDefault();
		let $this = $(this);

		disableButton($this.find("button[type=submit]"));

		$.post(window.lang + '/send-mobile-call-form', $this.serialize(), function (response) {
			if (response.success) {
				$(".mobile-call-modal").removeClass("show-modal");
				$(".mobile-call-modal").find(".has-bg").removeClass("show-modal-content");

				$(".success-modal").find('.order-call-title').html(window.i18n.vue.mobile_form_success_text);
				$(".success-modal").addClass("show-modal");
				$(".success-modal").find(".has-bg").addClass("show-modal-content");

				enableButton($this.find("button[type=submit]"));

				$this.trigger('reset');
			}
		});
	});

	$('#send-contact-form').submit(function (e) {
		e.preventDefault();
		let $this = $(this);

		disableButton($this.find("button[type=submit]"));

		$.post(window.lang + '/send-contact-form', $this.serialize(), function (response) {
			if (response.success) {
				$(".success-modal").find('.order-call-title').html(window.i18n.vue.contact_form_success_text);
				$(".success-modal").addClass("show-modal");
				$(".success-modal").find(".has-bg").addClass("show-modal-content");

				enableButton($this.find("button[type=submit]"));

				$this.trigger('reset');
			}
		});
	});

})(jQuery);

function toggleSearch() {
	$('.search').toggleClass('hide');
	$('body').toggleClass('search-active');
}

function switchLang(radio, prefix, cur_link) {
	$('.languages-submenu').find('input[type=radio]').prop('checked', false);
	$(radio).prop('checked', true);

	if (prefix === 'ro') {
		window.location.href = `/${cur_link}`;
	} else {
		window.location.href = `/${prefix}/${cur_link}`;
	}
}