<?php

namespace App\Jobs;

use App\Models\Photos;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class RemoveUnusedPhotos implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public function handle()
    {
        $photos = Photos::where('table', 'products')
            ->whereRaw('table_id NOT IN (SELECT id FROM products)')
            ->get();

        $count = $photos->count();

        foreach ($photos as $photo) {
            $photo->delete();
        }

        echo "Removed {$count} unused photos";
    }
}
