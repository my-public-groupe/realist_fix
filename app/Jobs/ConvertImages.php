<?php

namespace App\Jobs;

use App\Models\Photos;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Log;
use Intervention\Image\Facades\Image;

class ConvertImages implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $images_dir;
    private $thumbs_dir;
    private $productId;

    public function __construct($productId = null)
    {
        $this->images_dir = base_path('public/uploaded/');
        $this->thumbs_dir = base_path('public/uploaded/thumbs/');

        if($productId){
            $this->productId = $productId;
        }
    }

    public function handle()
    {
        $this->convert($this->productId);
    }

    private function convert($productId = null)
    {
        $product_photos = Photos::where('table', 'products');

        if($productId){
            $product_photos = $product_photos->where('table_id', $productId);
        }

        $product_photos = $product_photos
            ->latest('id')
            ->get();

        if($product_photos->isNotEmpty()){
            for ($i = 0; $i < $product_photos->count(); $i++){
                $photo = $product_photos[$i];

                $source = $photo->source;

                $source_path = $this->images_dir . DIRECTORY_SEPARATOR . $source;
                $source_webp = File::name($source) . '.webp';

                try{
                    $image_file = File::get($source_path);

                    //webp thumb
                    $thumbPath = $this->images_dir . DIRECTORY_SEPARATOR . 'thumbs';

                    $image_webp_thumb = Image::make($image_file);
                    $image_webp_thumb->fit(640, 640);
                    $image_webp_thumb->save($thumbPath . DIRECTORY_SEPARATOR . $source_webp, 80, 'webp');

                    Log::channel('webp_log')->debug("Converted $i of {$product_photos->count()} ({$photo->source})");
                }catch (\Exception $e){
                    continue;
                }
            }
        }
    }
}