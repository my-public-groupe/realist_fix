<?php

namespace App\Jobs;

use App\Models\Photos;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;

class PhotosLowercaseFix implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $photosPath = public_path() . config('photos.images_url');
        $thumbsPath = public_path() . config('photos.images_url') . 'thumbs/';

        foreach (Photos::all() as $item) {
            $photo = $item->source;
            try {
                Storage::move($photosPath . $photo, $photosPath . strtolower($photo));
                Storage::move($thumbsPath . $photo, $thumbsPath . strtolower($photo));
            } catch (\Exception $e) {
                echo $e->getMessage() . PHP_EOL;
            }
            $item->source = strtolower($photo);
            $item->save();
        }

    }
}
