<?php

namespace App\Jobs;

use App\Models\Product;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class DeleteOldProducts implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public function handle()
    {
        // Товары, у которых установлены лейблы SOLD и RENTED старше 2 недель
        $products = Product::whereIn('photo_label', [
            Product::PHOTO_LABEL_SOLD,
            Product::PHOTO_LABEL_RENTED,
        ])
        ->where('updated_at', '<', Carbon::now()->addWeeks(-2)->toDateTimeString())
        ->get();

        $count = $products->count();

        foreach ($products as $product) {
            $product->delete();
        }

        echo "Deleted {$count} products";
    }
}
