<?php

namespace App\Services\AmoCrm;

use AmoCRM\Client\AmoCRMApiClient;
use AmoCRM\Collections\CatalogElementsCollection;
use AmoCRM\Collections\CustomFields\CustomFieldNestedCollection;
use AmoCRM\Collections\CustomFieldsValuesCollection;
use AmoCRM\Collections\Leads\LeadsCollection;
use AmoCRM\Collections\LinksCollection;
use AmoCRM\Collections\NotesCollection;
use AmoCRM\Collections\TagsCollection;
use AmoCRM\Exceptions\AmoCRMApiException;
use AmoCRM\Exceptions\AmoCRMMissedTokenException;
use AmoCRM\Exceptions\AmoCRMoAuthApiException;
use AmoCRM\Filters\CatalogElementsFilter;
use AmoCRM\Filters\CatalogsFilter;
use AmoCRM\Filters\ContactsFilter;
use AmoCRM\Helpers\EntityTypesInterface;
use AmoCRM\Models\AccountModel;
use AmoCRM\Models\CatalogElementModel;
use AmoCRM\Models\ContactModel;
use AmoCRM\Models\Customers\CustomerModel;
use AmoCRM\Models\CustomFields\CategoryCustomFieldModel;
use AmoCRM\Models\CustomFields\NestedModel;
use AmoCRM\Models\CustomFieldsValues\MultiselectCustomFieldValuesModel;
use AmoCRM\Models\CustomFieldsValues\MultitextCustomFieldValuesModel;
use AmoCRM\Models\CustomFieldsValues\NumericCustomFieldValuesModel;
use AmoCRM\Models\CustomFieldsValues\PriceCustomFieldValuesModel;
use AmoCRM\Models\CustomFieldsValues\SelectCustomFieldValuesModel;
use AmoCRM\Models\CustomFieldsValues\TextareaCustomFieldValuesModel;
use AmoCRM\Models\CustomFieldsValues\TextCustomFieldValuesModel;
use AmoCRM\Models\CustomFieldsValues\ValueCollections\MultiselectCustomFieldValueCollection;
use AmoCRM\Models\CustomFieldsValues\ValueCollections\MultitextCustomFieldValueCollection;
use AmoCRM\Models\CustomFieldsValues\ValueCollections\NumericCustomFieldValueCollection;
use AmoCRM\Models\CustomFieldsValues\ValueCollections\TextareaCustomFieldValueCollection;
use AmoCRM\Models\CustomFieldsValues\ValueCollections\TextCustomFieldValueCollection;
use AmoCRM\Models\CustomFieldsValues\ValueModels\MultiselectCustomFieldValueModel;
use AmoCRM\Models\CustomFieldsValues\ValueModels\MultitextCustomFieldValueModel;
use AmoCRM\Models\CustomFieldsValues\ValueModels\NumericCustomFieldValueModel;
use AmoCRM\Models\CustomFieldsValues\ValueModels\PriceCustomFieldValueModel;
use AmoCRM\Models\CustomFieldsValues\ValueModels\TextareaCustomFieldValueModel;
use AmoCRM\Models\CustomFieldsValues\ValueModels\TextCustomFieldValueModel;
use AmoCRM\Models\LeadModel;
use AmoCRM\Models\NoteType\CommonNote;
use AmoCRM\Models\TagModel;
use Carbon\Carbon;
use Illuminate\Support\Facades\Cache;
use \League\OAuth2\Client\Token\AccessToken;
use League\OAuth2\Client\Token\AccessTokenInterface;

class AmoCrm
{
    protected $tokenFile;
    protected $client_id;
    protected $client_secret;
    protected $redirect_uri;

    public function __construct()
    {
        $this->tokenFile = storage_path('/amo/token_info.json');
        $this->client_id = config('amocrm.client_id');
        $this->client_secret = config('amocrm.client_secret');
        $this->redirect_uri = config('amocrm.redirect_uri');
    }


// AmoCrm

    function saveToken($accessToken)
    {
        if (
            isset($accessToken)
            && isset($accessToken['accessToken'])
            && isset($accessToken['refreshToken'])
            && isset($accessToken['expires'])
            && isset($accessToken['baseDomain'])
        ) {
            $data = [
                'accessToken' => $accessToken['accessToken'],
                'expires' => $accessToken['expires'],
                'refreshToken' => $accessToken['refreshToken'],
                'baseDomain' => $accessToken['baseDomain'],
            ];
            file_put_contents($this->tokenFile, json_encode($data));
        } else {
            exit('Invalid access token ' . var_export($accessToken, true));
        }
    }


    function curlRefreshToken()
    {

        $accessToken = json_decode(file_get_contents($this->tokenFile), true);
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => 'https://realistmd.amocrm.ru/oauth2/access_token',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => '{
            "client_id": "' . $this->client_id . '",
            "client_secret": "' . $this->client_secret . '",
            "grant_type": "refresh_token",
            "refresh_token":"' . $accessToken['refreshToken'] . '",

            "redirect_uri": "' . $this->redirect_uri . '"
        }',
            CURLOPT_HTTPHEADER => array(
                'Content-Type: application/json'
            ),
        ));

        $response = curl_exec($curl);

        curl_close($curl);

        return json_decode($response);
    }

    /**
     * @throws AmoCRMoAuthApiException
     */
    function getToken(): AccessToken
    {
        $accessToken = json_decode(file_get_contents($this->tokenFile), true);
        if (
            isset($accessToken)
            && isset($accessToken['accessToken'])
            && isset($accessToken['refreshToken'])
            && isset($accessToken['expires'])
            && isset($accessToken['baseDomain'])
        ) {
            $accessToken = new AccessToken([
                'access_token' => $accessToken['accessToken'],
                'refresh_token' => $accessToken['refreshToken'],
                'expires' => $accessToken['expires'],
                'baseDomain' => $accessToken['baseDomain'],
            ]);
            if ($accessToken->hasExpired()) {
//                if (true) {
                $apiClient = new AmoCRMApiClient($this->client_id, $this->client_secret, $this->redirect_uri);

//                $new_token = $this->curlRefreshToken();


                 $newAccessToken = $apiClient->getOAuthClient()->getAccessTokenByRefreshToken($accessToken);
                 $this->saveToken([
                     'accessToken' => $newAccessToken->getToken(),
                     'refreshToken' => $newAccessToken->getRefreshToken(),
                     'expires' => $newAccessToken->getExpires(),
                     'baseDomain' => $accessToken->getValues()['baseDomain']
                 ]);
//                 dd($newAccessToken);

//                $this->saveToken([
//                    'accessToken' => $new_token->access_token,
//                    'refreshToken' => $new_token->refresh_token,
//                    'expires' => (int)$new_token->expires_in,
//                    'baseDomain' => 'realistmd.amocrm.ru'
//                ]);
//
//                $accessToken2 = json_decode(file_get_contents($this->tokenFile), true);
//
//                $accessToken2 = new AccessToken([
//                    'access_token' => $accessToken2['accessToken'],
//                    'refresh_token' => $accessToken2['refreshToken'],
//                    'expires' => (int)$accessToken2['expires'],
//                    'baseDomain' => $accessToken2['baseDomain'],
//                ]);

                return $newAccessToken;
            } else {
                return $accessToken;
            }
        } else {
            exit('Invalid access token ' . var_export($accessToken, true));
        }
    }

    function getApiClient($accessToken): AmoCRMApiClient
    {
        $apiClient = new AmoCRMApiClient($this->client_id, $this->client_secret, $this->redirect_uri);

        $apiClient->setAccessToken($accessToken)
            ->setAccountBaseDomain('realistmd.amocrm.ru')
            ->onAccessTokenRefresh(
                function (AccessTokenInterface $accessToken, string $baseDomain) {
                    saveToken(
                        [
                            'accessToken' => $accessToken->getToken(),
                            'refreshToken' => $accessToken->getRefreshToken(),
                            'expires' => $accessToken->getExpires(),
                            'baseDomain' => $baseDomain,
                        ]
                    );
                }
            );

        return $apiClient;
    }

    /**
     * @param $data
     * @return ContactModel|null
     * @throws AmoCRMoAuthApiException
     */
    function createAmoCustomer($data): ?ContactModel
    {
        $accessToken = $this->getToken();

        $apiClient = $this->getApiClient($accessToken);

        $customer = new CustomerModel();
        $customer->setName($data['name']);

        $contact = new ContactModel();
        $contact->setName($data['name']);

        try {
            $customer = $apiClient->customers()->addOne($customer);
        } catch (AmoCRMApiException $e) {
            return null;
        }

        try {
            $contact = $apiClient->contacts()->addOne($contact);
        } catch (AmoCRMApiException $e) {
            return null;
        }


        $values = new CustomFieldsValuesCollection();

        $emailField = (new MultitextCustomFieldValuesModel())->setFieldCode('EMAIL');
        $emailField->setValues(
            (new MultitextCustomFieldValueCollection())
                ->add(
                    (new MultitextCustomFieldValueModel())
                        ->setEnum('WORK')
                        ->setValue($data['email'])
                )
        );
        $values->add($emailField);

        $phoneField = (new MultitextCustomFieldValuesModel())->setFieldCode('PHONE');
        $phoneField->setValues(
            (new MultitextCustomFieldValueCollection())
                ->add(
                    (new MultitextCustomFieldValueModel())
                        ->setEnum('WORK')
                        ->setValue($data['phone'])
                )
        );
        $values->add($phoneField);

        $contact->setCustomFieldsValues($values);

        try {
            $apiClient->contacts()->updateOne($contact);
        } catch (AmoCRMApiException $e) {
            printError($e);
        }

        $links = new LinksCollection();
        $links->add($contact);

        try {
            $apiClient->customers()->link($customer, $links);
        } catch (AmoCRMApiException $e) {
            printError($e);
        }

        return $contact;
    }

    /**
     * @param $data
     * @return LeadModel
     * @throws AmoCRMoAuthApiException
     */
    function updateToken()
    {
        $accessToken = $this->getToken();

        return $accessToken;

    }

    /**
     * @param $data
     * @return LeadModel
     * @throws AmoCRMoAuthApiException
     */
    function createAmoLead($data)
    {
        $accessToken = $this->getToken();
        $apiClient = $this->getApiClient($accessToken);

        $lead = new LeadModel();
        $leadCustomFieldsValues = new CustomFieldsValuesCollection();


        $paymentCustomFieldValueModel = $this->createCustomField(28788844, 'test send', 'text');
        $leadCustomFieldsValues->add($paymentCustomFieldValueModel);


        $lead->setResponsibleUserId(5822335);

        $lead->setName('Contact form')
            ->setPrice(0);

        $lead->setTags((new TagsCollection())
            ->add(
                (new TagModel())
                    ->setName("#FORM")
            ));


        try {
            $lead = $apiClient->leads()->addOne($lead);
        } catch (AmoCRMApiException $e) {
            printError($e);
        }
        $name = $data['name'];
        $phone = $data['phone'];
        $email = $data['email'];
        $url = $data['link'];

        $contact = $this->createAmoCustomer(array('name' => $name, 'phone' => $phone, 'email' => $email));

        $this->setAmoNote($lead->getId(), $url);

        $links = new LinksCollection();
        $links->add($contact);

        try {
            $apiClient->leads()->link($lead, $links);
        } catch (AmoCRMApiException $e) {
            printError($e);
        }
        return $lead;

    }

    /**
     * @param $order
     * @param $ids
     * @param $contact
     * @throws AmoCRMoAuthApiException
     * @throws \AmoCRM\Exceptions\InvalidArgumentException
     * @throws AmoCRMMissedTokenException|AmoCRMApiException
     */
    function createAmoOrder($order, $products, $contact)
    {
        $accessToken = getToken();
        $apiClient = getApiClient($accessToken);

        $lead = new LeadModel();
        $leadCustomFieldsValues = new CustomFieldsValuesCollection();

        $idCustomFieldValueModel = createCustomField(103309, (string)$order->id, 'text');
        $leadCustomFieldsValues->add($idCustomFieldValueModel);

        $paymentCustomFieldValueModel = createCustomField(103311, $order->payment_gateway, 'text');
        $leadCustomFieldsValues->add($paymentCustomFieldValueModel);

        $deliveryCustomFieldValueModel = createCustomField(900759, $order->billing_tax, 'numeric');
        $leadCustomFieldsValues->add($deliveryCustomFieldValueModel);

        $productsCustomFieldValueModel = createCustomField(103397, implode(',', array_column($products, 'name')), 'textarea');
        $leadCustomFieldsValues->add($productsCustomFieldValueModel);

        $address = $order->billing_city . ', str. ' . $order->billing_address . ', scara ' . $order->billing_entrance . ', et. ' . $order->billing_floor . ', ap. ' . $order->billing_apartment;
        $addressCustomFieldValueModel = createCustomField(151603, $address, 'text');
        $leadCustomFieldsValues->add($addressCustomFieldValueModel);

        $selectCustomFieldValueModel = createCustomField(151549, $order->payment_gateway, 'select');
        $leadCustomFieldsValues->add($selectCustomFieldValueModel);

        $lead->setCustomFieldsValues($leadCustomFieldsValues);

        $lead->setResponsibleUserId(6370246);

        $lead->setName('Order ' . $order->id . ' on site ggg.md')
            ->setPrice($order->billing_total);

        $lead->setTags((new TagsCollection())
            ->add(
                (new TagModel())
                    ->setName("#FORM EN")
            ));


        try {
            $lead = $apiClient->leads()->addOne($lead);
        } catch (AmoCRMApiException $e) {
            printError($e);
        }

        $links = new LinksCollection();
        $links->add($contact);

        try {
            $apiClient->leads()->link($lead, $links);
        } catch (AmoCRMApiException $e) {
            printError($e);
        }

        if ($products) {
            $catalogsCollection = $apiClient->catalogs()->get();
            $catalog = $catalogsCollection->getBy('name', 'Товары');
            $catalogElementsService = $apiClient->catalogElements($catalog->getId());

            foreach ($products as $id => $product) {
                $element = $catalogElementsService->getOne($id);
                if ($element) {
                    $element->setQuantity((float)$product['qty']);

                    $links = new LinksCollection();
                    $links->add($element);
                    try {
                        $apiClient->leads()->link($lead, $links);
                    } catch (AmoCRMApiException $e) {
                        printError($e);
                    }
                }
            }
        }
    }

    function createCustomField($id, $value, $type)
    {
        switch (true) {
            case ($type == 'text'):
            default:
                $textCustomFieldValueModel = new TextCustomFieldValuesModel();
                $textCustomFieldValueModel->setFieldId($id);
                $textCustomFieldValueModel->setValues(
                    (new TextCustomFieldValueCollection())
                        ->add((new TextCustomFieldValueModel())->setValue($value))
                );

                return $textCustomFieldValueModel;
                break;
            case ($type == 'multiselect'):
                $multiselectCustomFieldValueModel = new MultiselectCustomFieldValuesModel();
                $multiselectCustomFieldValueModel->setFieldId($id);
                $values = (new MultiselectCustomFieldValueCollection());
                foreach ($value as $row) {
                    $values->add((new MultiselectCustomFieldValueModel())->setValue($row));
                }
                $multiselectCustomFieldValueModel->setValues($values);

                return $multiselectCustomFieldValueModel;
                break;
            case ($type == 'textarea'):
                $textareaCustomFieldValueModel = new TextareaCustomFieldValuesModel();
                $textareaCustomFieldValueModel->setFieldId($id);
                $textareaCustomFieldValueModel->setValues(
                    (new TextareaCustomFieldValueCollection())
                        ->add((new TextareaCustomFieldValueModel())->setValue($value))
                );

                return $textareaCustomFieldValueModel;
                break;
            case ($type == 'numeric'):
                $numericCustomFieldValueModel = new NumericCustomFieldValuesModel();
                $numericCustomFieldValueModel->setFieldId($id);
                $numericCustomFieldValueModel->setValues(
                    (new NumericCustomFieldValueCollection())
                        ->add((new NumericCustomFieldValueModel())->setValue($value))
                );

                return $numericCustomFieldValueModel;
                break;
            case ($type == 'price'):
                $priceCustomFieldValueModel = new PriceCustomFieldValuesModel();
                $priceCustomFieldValueModel->setFieldId($id);
                $priceCustomFieldValueModel->setValues(
                    (new TextCustomFieldValueCollection())
                        ->add((new PriceCustomFieldValueModel())->setValue($value))
                );

                return $priceCustomFieldValueModel;
                break;
            case ($type == 'select'):
                $selectCustomFieldValueModel = new SelectCustomFieldValuesModel();
                $selectCustomFieldValueModel->setFieldId($id);
                $selectCustomFieldValueModel->setValues(
                    (new TextCustomFieldValueCollection())
                        ->add((new TextCustomFieldValueModel())->setValue($value))
                );

                return $selectCustomFieldValueModel;
                break;
        }
    }

    function getContact($id = false, $query = false): ?ContactModel
    {

        $accessToken = getToken();
        $apiClient = getApiClient($accessToken);

        $contact = null;

        if ($id) {
            try {
                $contact = $apiClient->contacts()->getOne($id);
            } catch (AmoCRMApiException $e) {
                $contact = null;
            }
        }
        if ($query) {
            $filter = new ContactsFilter();
            $filter->setQuery($query);

            try {
                $contact = $apiClient->contacts()->get($filter)->first();
            } catch (AmoCRMApiException $e) {
                $contact = null;
            }
        }

        return $contact;
    }

    /**
     * @throws AmoCRMoAuthApiException
     */
    function getTokenByCode($code)
    {
        $referer = "realistmd.amocrm.ru";
        $client_id = "fe18489e-b7da-4d3c-bc8b-cbff073c85a3";
        $client_secret = "5xBx1yUQAPRRoOVbzLGHoz5BafrIIfiBquQa2tqpbro6EhM9B2AlmT0Wq5nUhCtj";
        $redirect_uri = "https://www.realist.md/";
        $apiClient = new AmoCRMApiClient($client_id, $client_secret, $redirect_uri);

        $apiClient->setAccountBaseDomain($referer);
        $newToken = $apiClient->getOAuthClient()->getAccessTokenByCode($code);

        saveToken([
            'accessToken' => $newToken->getToken(),
            'refreshToken' => $newToken->getRefreshToken(),
            'expires' => $newToken->getExpires(),
            'baseDomain' => $apiClient->getAccountBaseDomain()
        ]);
    }


    /**
     * @throws AmoCRMoAuthApiException
     */
    function setAmoNote($entityId, $text)
    {
        $accessToken = $this->getToken();
        $apiClient = $this->getApiClient($accessToken);

        $notesCollection = new NotesCollection();
        $commonMessageNote = new CommonNote();
        $commonMessageNote->setEntityId($entityId)
            ->setText($text)
            ->setCreatedBy(0);

        $notesCollection->add($commonMessageNote);

        try {
            $leadNotesService = $apiClient->notes(EntityTypesInterface::LEADS);
            $notesCollection = $leadNotesService->add($notesCollection);
        } catch (AmoCRMApiException $e) {
            printError($e);
        }
    }


    /**
     * @throws InvalidArgumentException
     * @throws AmoCRMApiException
     * @throws AmoCRMMissedTokenException
     * @throws AmoCRMoAuthApiException
     */
    function createAmoProduct($product): ?int
    {
        $accessToken = getToken();
        $apiClient = getApiClient($accessToken);

        $catalogsCollection = $apiClient->catalogs()->get();
        $catalog = $catalogsCollection->getBy('name', 'Товары');

        $catalogElementsCollection = new CatalogElementsCollection();
        $catalogElement = new CatalogElementModel();
        $catalogElement->setName($product['name']);

        $productCustomFieldsValues = new CustomFieldsValuesCollection();

        $priceCustomFieldValueModel = createCustomField(285049, $product['price'], 'price');
        $productCustomFieldsValues->add($priceCustomFieldValueModel);

        $skuCustomFieldValueModel = createCustomField(285045, $product['sku'], 'text');
        $productCustomFieldsValues->add($skuCustomFieldValueModel);

        $catalogElement->setCustomFieldsValues($productCustomFieldsValues);

        $catalogElementsCollection->add($catalogElement);
        $catalogElementsService = $apiClient->catalogElements($catalog->getId());
        try {
            $catalogElementsService->add($catalogElementsCollection);
            return $catalogElement->getId();
        } catch (AmoCRMApiException $e) {
            return null;
        }
    }

    /**
     * @throws \AmoCRM\Exceptions\InvalidArgumentException
     * @throws AmoCRMApiException
     * @throws AmoCRMMissedTokenException
     * @throws AmoCRMoAuthApiException
     */
    function updateAmoProduct($product): ?int
    {
        $accessToken = getToken();
        $apiClient = getApiClient($accessToken);

        $catalogsCollection = $apiClient->catalogs()->get();
        $catalog = $catalogsCollection->getBy('name', 'Товары');

        $catalogElementsCollection = new CatalogElementsCollection();
        $catalogElementsService = $apiClient->catalogElements($catalog->getId());
        $catalogElementsFilter = new CatalogElementsFilter();
        $catalogElementsFilter->setQuery($product['sku']);

        try {
            $catalogElementsCollection = $catalogElementsService->get($catalogElementsFilter);
        } catch (AmoCRMApiException $e) {
            printError($e);
            die;
        }

        $element = $catalogElementsCollection->first();

        $element->setName($product['name']);

        $productCustomFieldsValues = new CustomFieldsValuesCollection();

        $priceCustomFieldValueModel = createCustomField(285049, $product['price'], 'price');
        $productCustomFieldsValues->add($priceCustomFieldValueModel);

        $skuCustomFieldValueModel = createCustomField(285045, $product['sku'], 'text');
        $productCustomFieldsValues->add($skuCustomFieldValueModel);

        if ($product['desc']) {
            $descCustomFieldValueModel = createCustomField(285047, $product['desc'], 'textarea');
            $productCustomFieldsValues->add($descCustomFieldValueModel);
        }
        if ($product['categories']) {
            $categoryCustomFieldValueModel = createCustomField(900757, $product['categories'], 'multiselect');
            $productCustomFieldsValues->add($categoryCustomFieldValueModel);
        }

        $element->setCustomFieldsValues($productCustomFieldsValues);

        $catalogElementsService = $apiClient->catalogElements($catalog->getId());
        try {
            $catalogElementsService->update($catalogElementsCollection);
            return $element->getId();
        } catch (AmoCRMApiException $e) {
            printError($e);
            die;
        }
    }

    function printError(AmoCRMApiException $e): void
    {
        $errorTitle = $e->getTitle();
        $code = $e->getCode();
        $debugInfo = var_export($e->getLastRequestInfo(), true);

        //echo '<pre>' . $error . '</pre>';
    }
}