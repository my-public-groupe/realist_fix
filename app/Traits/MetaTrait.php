<?php

namespace App\Traits;

use App\Models\Meta;

trait MetaTrait
{
    public function meta()
    {
        return $this->morphOne(\App\Models\Meta::class, 'table');
    }

    public function saveMeta($request)
    {
        $fields = $request->only('meta_description', 'meta_keywords', 'title');

        $meta = $this->meta;

        if (!isset($meta)) {
            $meta = new Meta();
        }

        foreach ($fields as $field => $values) {

            foreach ($values as $key => $value) {

                if ($key == config('app.base_locale')) {
                    $meta->attributes[$field] = $value;
                    continue;
                }

                $meta->attributes[$field . '_' . $key] = $value;
            }
        }

        $this->meta()->save($meta);
    }

    // Realtor 13/10/21: need raw save (w/o request)
    public function saveRawMeta($fields)
    {
        $meta = $this->meta;

        if (!isset($meta)) {
            $meta = new Meta();
        }

        foreach ($fields as $key => $value) {
            $meta->attributes[$key] = $value;
        }

        $this->meta()->save($meta);
    }
}
