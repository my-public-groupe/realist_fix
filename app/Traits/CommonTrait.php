<?php

namespace App\Traits;

use App\Http\Controllers\Controller;
use App\Models\Meta;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Str;

trait CommonTrait
{
    /**
     * Create the slug from the title
     */
    public function setSlugAttribute($value)
    {
        if ($value == "") {
            // grab the title and slugify it
            $this->attributes['slug'] = Str::slug($this->name);
        } else {
            $this->attributes['slug'] = Str::slug($value);
        }
    }

    public function setNameAttribute($values)
    {
        if (!is_array($values)) {
            $this->attributes['name'] = $values;
        } else {
            foreach ($values as $key => $value) {
                if ($key == config('app.base_locale')) {
                    $this->attributes['name'] = $value;
                    continue;
                }
                $this->attributes['name_' . $key] = $value;
            }
        }
    }

    public function setDescriptionAttribute($values)
    {
        if (!is_array($values)) {
            $this->attributes['description'] = $values;
        } else {
            foreach ($values as $key => $value) {
                if ($key == config('app.base_locale')) {
                    $this->attributes['description'] = $value;
                    continue;
                }
                $this->attributes['description_' . $key] = $value;
            }
        }
    }

    public function setDescriptionShortAttribute($values) {

        if (!is_array($values)) {
            $this->attributes['description'] = $values;
        } else {
            foreach ($values as $key => $value) {
                if ($key == config('app.base_locale')) {
                    $this->attributes['description_short'] = $value;
                    continue;
                }
                $this->attributes['description_short_' . $key] = $value;
            }
        }
    }

    public function getDescriptionAttribute()
    {
        $locale = Lang::locale();
        if ($locale != config('app.base_locale') && isset($this->attributes['description_' . $locale])) {
            return $this->attributes['description_' . $locale];
        }
        return $this->attributes['description'];
    }

    public function getDescriptionShortAttribute()
    {
        $locale = Lang::locale();
        if ($locale != config('app.base_locale') && isset($this->attributes['description_short_' . $locale])) {
            return $this->attributes['description_short_' . $locale];
        }

        return $this->attributes['description_short'];
    }

    public function getNameAttribute()
    {
        $locale = Lang::locale();
        if ($locale != config('app.base_locale') && isset($this->attributes['name_' . $locale])) {
            return $this->attributes['name_' . $locale];
        }

        return  $this->attributes['name'];
    }

    public function photos()
    {
        return $this->hasMany('App\Models\Photos','table_id')->where('table', $this->getTable())->orderBy('sort');
    }

    public function scopeEnabled($query)
    {
        return $query->where('enabled', true);
    }

    public function scopeDisabled($query)
    {
        return $query->where('enabled', false);
    }

    public function mainPhoto($index = 0)
    {
        $fileName = "no_photo.png";

        if (isset($this->photos[$index])) {
            $fileName = $this->photos[$index]->source;

            if(!File::exists(public_path('uploaded/' . $fileName))){
                $fileName = "no_photo.png";
            }

            if(Controller::hasWebp()){
                if(File::exists(public_path('uploaded/' . File::name($fileName) . '.webp'))){
                    $fileName = File::name($fileName) . '.webp';
                }
            }
        }

        return $fileName;
    }

    public function mainPhotoThumb($index = 0)
    {
        $fileName = "no_photo.png";

        if (isset($this->photos[$index])) {
            $fileName = $this->photos[$index]->source;

            if(!File::exists(public_path('uploaded/thumbs/' . $fileName))){
                $fileName = "no_photo.png";
            }

            if(Controller::hasWebp()){
                if(File::exists(public_path('uploaded/thumbs/' . File::name($fileName) . '.webp'))){
                    $fileName = File::name($fileName) . '.webp';
                }
            }
        }

        return $fileName;
    }
}