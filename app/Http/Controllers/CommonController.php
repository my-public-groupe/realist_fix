<?php

namespace App\Http\Controllers;

use AmoCRM\Exceptions\AmoCRMoAuthApiException;
use App\Helpers\Helper;
use App\Models\Agent;
use App\Models\Content;
use App\Models\Lists;
use App\Models\News;
use App\Models\Product;
use App\Models\Reviews;
use Carbon\Carbon;
use Dotzero\LaravelAmoCrm\Facades\AmoCrm;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;

class CommonController extends Controller
{
    public function index()
    {
        $slider = Lists::getBySlug('projects');
        $agents = Agent::enabled()
            ->has('photos')
            ->oldest('sort')
            ->limit(4)
            ->get();

        return view('index')->with(compact(
            'slider',
            'agents')
        );
    }

    private function amoCRM_addContact()
    {
        $amo = new AmoCRMController();
        if ($amo->Auth()) {
            $array = array(
                'name' => request()->get('name'),
                'phone' => request()->get('phone'),
                'email' => request()->get('email'),
                'lang' => config('app.locales')[app()->getLocale()],
                'question' => request()->get('question'),
                'utm_source' => session()->get('utm_tags.utm_source', ''),
                'utm_medium' => session()->get('utm_tags.utm_medium', ''),
                'utm_campaign' => session()->get('utm_tags.utm_campaign', ''),
                'utm_term' => session()->get('utm_tags.utm_term', ''),
                'utm_content' => session()->get('utm_tags.utm_content', ''),
                'pass' => 'realistocrm'

            );

            $ch = curl_init('https://glucose.md/clients/realist/handler.php');
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $array);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_HEADER, false);
            $html = curl_exec($ch);
            curl_close($ch);

            session()->forget('utm_tags');
        }
    }

    private function amoCRM_addLead()
    {
        $amo = new AmoCRMController();
        if ($amo->Auth()) {

            $array = array(
                'name' => request()->get('name'),
                'phone' => request()->get('phone'),
                'email' => request()->get('email'),
                'lang' => config('app.locales')[app()->getLocale()],
                'price' => request()->get('price'),
                'question' => request()->get('link'),
                'utm_source' => session()->get('utm_tags.utm_source', ''),
                'utm_medium' => session()->get('utm_tags.utm_medium', ''),
                'utm_campaign' => session()->get('utm_tags.utm_campaign', ''),
                'utm_term' => session()->get('utm_tags.utm_term', ''),
                'utm_content' => session()->get('utm_tags.utm_content', ''),
                'pass' => 'realistocrm'

            );

            $ch = curl_init('https://glucose.md/clients/realist/handler.php');
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $array);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_HEADER, false);
            $html = curl_exec($ch);
            curl_close($ch);

            session()->forget('utm_tags');
        }
    }

    private function getResponsibleByPhone($phone)
    {
        $phone = str_replace('+373', '', $phone);

        $client = AmoCrm::getClient();
        $account = $client->account->apiCurrent();
        $responsibles = $account['users'];

        $key = array_search($phone, array_column($responsibles, "phone_number"));

        if ($key) {
            return $responsibles[$key]['id'];
        }

        return -1;
    }

    public function sendContactForm()
    {
        // Sending email to admin
        Mail::send('emails.contact_form', ['data' => request()], function ($message) {
            $message->to(config('custom.admin_email'))
                ->subject(trans('common.contact_form_email_title'));
        });

        if (Mail::failures()) {
            return response()->Fail('Sorry! Please try again later');
        }

        try {
            //$this->amoCRM_addContact();
        } catch (\Exception $e) {
        }

        if (request()->ajax()) {
            return response()->json(['success' => true]);
        }
    }

    public function sendBookingForm()
    {
        // Sending email to admin
        Mail::send('emails.booking_form', ['data' => request()], function ($message) {
            $message->to(config('custom.admin_email'))
                ->subject('Booking Form');
        });

        if (Mail::failures()) {
            return response()->Fail('Sorry! Please try again later');
        }

        try {
            $this->amoCRM_addLead();
        } catch (\Exception $e) {
        }

        if (request()->ajax()) {
            return response()->json(['success' => true]);
        }

        return redirect()->route('index');
    }

    public function sendPDFForm()
    {
        // Sending email to admin
        Mail::send('emails.pdf_form', ['data' => request()], function ($message) {
            $message->to(config('custom.admin_email'))
                ->subject('PDF Form');
        });

        if (Mail::failures()) {
            return response()->Fail('Sorry! Please try again later');
        }

        if (request()->ajax()) {
            return response()->json(['success' => true]);
        }

        return redirect()->route('index');
    }

    public function sendOrderCall()
    {
        // Sending email to admin
        Mail::send('emails.order_form', ['data' => request()], function ($message) {
            $message->to(config('custom.admin_email'))
                ->subject(trans('common.call_form_email_title'));
        });

        if (Mail::failures()) {
            return response()->Fail('Sorry! Please try again later');
        }

        if (request()->ajax()) {
            return response()->json(['success' => true]);
        }
    }

    public function sendNewsletterForm()
    {
        $email = request()->email;

        DB::table('subscribers')->updateOrInsert(
            ['email' => $email],
            ['email' => $email, 'updated_at' => Carbon::now()->format('Y-m-d H:i:s')]
        );

        return response()->json(['success' => true]);
    }

    public function sendConsultationForm()
    {
        // Sending email to admin
        Mail::send('emails.consultation_form', ['data' => request()], function ($message) {
            $message->to(config('custom.admin_email'))
                ->subject(trans('common.consultation_form_email_title'));
        });

        if (Mail::failures()) {
            return response()->Fail('Sorry! Please try again later');
        }

        if (request()->ajax()) {
            return response()->json(['success' => true]);
        }
    }

    public function sendAgentCallForm()
    {
        $data = request()->only(['name', 'email', 'phone', 'link']);

        try {
            (new \App\Services\AmoCrm\AmoCrm())->createAmoLead($data);
        } catch (AmoCRMoAuthApiException $e) {
            Log::error($e->getMessage());
        }

        // Sending email to admin
        Mail::send('emails.agent_call_form', ['data' => request()], function ($message) {
            $message->to(config('custom.admin_email'))
                ->subject(trans('common.agent_call_form_email_title'));
        });

        if (Mail::failures()) {
            return response()->Fail('Sorry! Please try again later');
        }

        if (request()->ajax()) {
            return response()->json(['success' => true]);
        }
    }

    public function sendAgentMortgageForm()
    {
        $data = request()->only(['name', 'email', 'phone', 'link']);

        try {
            (new \App\Services\AmoCrm\AmoCrm())->createAmoLead($data);
        } catch (AmoCRMoAuthApiException $e) {
            Log::error($e->getMessage());
        }

        // Sending email to admin
        Mail::send('emails.mortgage', ['data' => request()], function ($message) {
            $message->to(config('custom.admin_email'))
                ->subject(trans('common.agent_call_mortgage'));
        });

        if (Mail::failures()) {
            return response()->Fail('Sorry! Please try again later');
        }

        if (request()->ajax()) {
            return response()->json(['success' => true]);
        }
    }


    public function sendMobileCallForm()
    {
        // Sending email to admin
        Mail::send('emails.mobile_call_form', ['data' => request()], function ($message) {
            $message->to(config('custom.admin_email'))
                ->subject(trans('common.mobile_call_form_email_title'));
        });

        if (Mail::failures()) {
            return response()->Fail('Sorry! Please try again later');
        }

        if (request()->ajax()) {
            return response()->json(['success' => true]);
        }
    }

    public function getBySlug($slug)
    {
        //show content if slug exists
        $content_lists = Lists::where('slug', $slug)->enabled()->first();
        $content = Content::where('slug', $slug)->enabled()->first();

        if(!$content_lists){
            $page = $content;
        }
        if(!$content){
            $page = $content_lists;
        }
        if(!$content_lists && !$content){
            abort(404);
        }

        return view('content')->with('data', $page);
    }

    public function getTeam()
    {
        if (! Helper::isAllowedIp()) {
            abort(403);
        }

        $agents = Agent::enabled()
            ->orderBy('sort')
            ->get();

        return view('team', compact('agents'));
    }

    public function getAgent($id)
    {
        $agent = Agent::enabled()
            ->where('id', $id)
            ->firstOrFail();

        return view('agent', compact('agent'));
    }

    public function getTeamTest()
    {
        $agents = Agent::enabled()
            ->orderBy('sort')
            ->get();

        return view('team-test', compact('agents'));
    }

    public function getPrivacyPolicy()
    {
        $data = \App\Models\Content::where('slug', 'privacy-policy')->firstOrFail();

        return view('content')->with(compact('data'));
    }

    public function getOurTeam()
    {
        $agents = Agent::enabled()
            ->oldest('sort')
            ->get();

        return view('our-team')->with(compact('agents'));
    }
}
