<?php

namespace App\Http\Controllers;

use App\Models\Product;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Log;
use Intervention\Image\Facades\Image;

class SimpalsController
{
    private $api_key;
    private $offer_types;
    private $category;
    private $subcategories;
    private $building_types;
    private $parameters_features;
    private $rooms;
    private $floor;
    private $total_floors;

    public function __construct()
    {
        ini_set('max_execution_time', 300);

        $this->api_key = base64_encode(config('custom.simpals_999_api_key') . ':');

        $this->offer_types = [
            0 => '776', //продажа
            1 => '912', //аренда
        ];

        $this->building_types = [
            1 => '19108',
            2 => '19109'
        ];

        $this->rooms = [
            3 => '893',
            4 => '894',
            5 => '895',
            6 => '902',
            7 => '20442'
        ];

        $this->floor = [
            11 => '977',
            12 => '918',
            13 => '935',
            14 => '905',
            15 => '929',
            16 => '909',
            17 => '955',
            18 => '895',
            19 => '921',
            20 => '934',
            21 => '947',
            22 => '970',
            23 => '965',
            24 => '958',
            25 => '913',
            26 => '1016',
            27 => '1019',
            28 => '940'
        ];

        $this->total_floors = [
            30 => '956',
            31 => '964',
            32 => '906',
            33 => '936',
            34 => '910',
            35 => '919',
            36 => '971',
            37 => '975',
            38 => '896',
            39 => '951',
            40 => '948',
            41 => '954',
            42 => '966',
            43 => '959',
            44 => '979',
            45 => '914',
            46 => '1018'
        ];

        $this->category = '270'; //Недвижимость

        $this->subcategories = [
            1 => '1404',
            2 => '1406',
            3 => '1405',
            4 => '1405',
            5 => '1405',
            6 => '1407',
            7 => '6655',
            8 => '1410',
            9 => '1405'
        ];

        $this->parameters_features = [
            1 => '852'
        ];
    }

    public function addAdvert()
    {
        $product_id = request()->product_id;

        $product = Product::find($product_id);

        if(empty($product->building_type)){
            return ['success' => false, 'error' => 'Заполните Тип строительства'];
        }

        if(!$product->agent){
            return ['success' => false, 'error' => 'Выберите ответственного агента'];
        }

        if(empty($product->price) && empty($product->price_square)){
            return ['success' => false, 'error' => 'Заполните одну из цен для объекта'];
        }

        $product_price = $product->price;

        if(empty($product->price)){
            $product_price = $product->price_square;
        }

        $offer_type = $this->offer_types[$product->type];
        $building_type = $this->building_types[$product->building_type];
        $floor = $this->floor[$product->floor_value];
        $total_floors = $this->total_floors[$product->total_floors_value];
        $subcategory = $this->subcategories[$product->categories[0]->id];
        $rooms = $this->getTotalRooms($product, $this->category, $subcategory, $offer_type);
        sleep(1);
        $region_group = $this->getRegionGroup($product, $this->category, $subcategory, $offer_type);
        sleep(1);
        $region = $this->getRegion($product, $region_group, $subcategory);
        sleep(1);
        $sector = $this->getSector($product, $region, $subcategory);

        $photos = $product->photos()->limit(5)->get();

        $image_ids = [];

        if($photos->isNotEmpty()) {
            foreach ($photos as $photo) {
                try {
                    $image_ids[] = $this->uploadImageOptimized($photo->source);
                } catch (\Exception $e) {
                    continue;
                }

                sleep(1);
            }
        }

        $link = 'https://partners-api.999.md/adverts';

        $headers = [
            'Authorization: Basic ' . $this->api_key,
            'Content-Type: application/json'
        ];

        $data = [
            'category_id' => $this->category,
            'subcategory_id' => $subcategory,
            'offer_type' => $offer_type,
            'features' => [
                ['id' => '3', 'value' => ''], //?
                ['id' => '4', 'value' => ''], //?
                ['id' => '5', 'value' => '12869'], //Страна
                ['id' => '6', 'value' => ''], //?
                ['id' => '2', 'value' => $product_price, 'unit' => 'eur'], //Цена
                ['id' => '7', 'value' => $region_group], //Регион
                ['id' => '8', 'value' => $region], //Нас. пункт
                ['id' => '9', 'value' => $sector], //Сектор
                ['id' => '16', 'value' => $this->getAgentPhone($product)], //Контакты
                ['id' => '10', 'value' => $product->name], //Улица
                ['id' => '852', 'value' => $building_type], //Жилой фонд
                ['id' => '12', 'value' => $product->name], //Заголовок
                ['id' => '13', 'value' => $this->getDescription($product->description)], //Описание
                ['id' => '14', 'value' => $image_ids], //Фото?
                ['id' => '241', 'value' => $rooms], //Кол-во комнат
                ['id' => '244', 'value' => (int)$product->area, 'unit' => 'm2'], //Площадь
                ['id' => '248', 'value' => $floor], //Этаж
                ['id' => '249', 'value' => $total_floors], //Кол-во этажей
                ['id' => '795', 'value' => '18894'] //Автор
            ]
        ];

        if($offer_type == "912"){
            $data['features'][] = ['id' => '1372', 'value' => '24346']; //Дети
            $data['features'][] = ['id' => '1382', 'value' => '24439']; //Животные
        }

        $curl = curl_init();

        curl_setopt($curl,CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($data));
        curl_setopt($curl,CURLOPT_URL, $link);
        curl_setopt($curl,CURLOPT_HTTPHEADER, $headers);
        curl_setopt($curl,CURLOPT_HEADER, false);
        curl_setopt($curl,CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($curl,CURLOPT_SSL_VERIFYHOST, 0);
        $out = curl_exec($curl);

        curl_close($curl);

        $Response = json_decode($out, true);
        Log::debug('=== ADVERT ADD ===');
        Log::debug('=== Request info begin ===');
        Log::debug(print_r(request()->all(), true));
        Log::debug('=== Request info end ===');
        Log::debug(print_r($Response, true));

        if(isset($Response['error'])){
            return ['success' => false, 'error' => $Response['error']['errors'][0]['message']];
        }

        if(isset($Response['advert'])){
            $product->simpals_id = $Response['advert']['id'];
            $product->save();
        }

        return [
            'success' => true,
            'url' => $this->getAdvertURL($Response['advert']['id'])
        ];
    }

    public function updateAdvertPhotos($product)
    {
        $link = "https://partners-api.999.md/adverts/$product->simpals_id";

        $headers = [
            'Authorization: Basic ' . $this->api_key,
            'Content-Type: application/json'
        ];

        $photos = $product->photos()->limit(5)->get();

        $image_ids = [];

        if($photos->isNotEmpty()) {
            foreach ($photos as $photo) {
                try {
                    $image_ids[] = $this->uploadImageOptimized($photo->source);
                } catch (\Exception $e) {
                    continue;
                }

                sleep(1);
            }
        }

        $data = [
            'features' => [
                ['id' => '14', 'value' => $image_ids]
            ]
        ];

        $curl = curl_init();

        curl_setopt($curl,CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "PATCH");
        curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($data));
        curl_setopt($curl,CURLOPT_URL, $link);
        curl_setopt($curl,CURLOPT_HTTPHEADER, $headers);
        curl_setopt($curl,CURLOPT_HEADER, false);
        curl_setopt($curl,CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($curl,CURLOPT_SSL_VERIFYHOST, 0);
        $out = curl_exec($curl);

        curl_close($curl);

        $Response = json_decode($out, true);
        Log::debug('=== ADVERT UPDATE ===');
        Log::debug(print_r($Response, true));

        return $Response;
    }

    public function uploadImage($image_name)
    {
        $file_path = public_path('uploaded/' . $image_name);
        $file_mime = File::mimeType($file_path);
        $file_name = File::name($file_path);

        $link = 'https://partners-api.999.md/images';

        $headers = [
            'Authorization: Basic ' . $this->api_key,
            'Content-Type: multipart/form-data'
        ];

        $data = [
            'file' => new \CurlFile($file_path, $file_mime, $file_name)
        ];

        $curl = curl_init();

        curl_setopt($curl,CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
        curl_setopt($curl,CURLOPT_URL, $link);
        curl_setopt($curl,CURLOPT_HTTPHEADER, $headers);
        curl_setopt($curl,CURLOPT_HEADER, false);
        curl_setopt($curl,CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($curl,CURLOPT_SSL_VERIFYHOST, 0);
        $out = curl_exec($curl);

        curl_close($curl);

        $Response = json_decode($out, true);
        Log::debug(print_r($Response, true));

        return $Response['image_id'];
    }

    public function uploadImageOptimized($image_name)
    {
        $file_path = public_path('uploaded/' . $image_name);

        $new_img = Image::make($file_path);
        $new_img->fit(1280, 1280, function ($constraint) {
            //$constraint->aspectRatio();
            //$constraint->upsize();
        });

        $new_name = $new_img->filename . '_resized.jpg';
        $new_img->save(public_path('uploaded/' . $new_name), 95, 'jpg');

        $file_path = public_path('uploaded/' . $new_name);
        $file_mime = File::mimeType($file_path);
        $file_name = File::name($file_path);

        $link = 'https://partners-api.999.md/images';

        $headers = [
            'Authorization: Basic ' . $this->api_key,
            'Content-Type: multipart/form-data'
        ];

        $data = [
            'file' => new \CurlFile($file_path, $file_mime, $file_name)
        ];

        $curl = curl_init();

        curl_setopt($curl,CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
        curl_setopt($curl,CURLOPT_URL, $link);
        curl_setopt($curl,CURLOPT_HTTPHEADER, $headers);
        curl_setopt($curl,CURLOPT_HEADER, false);
        curl_setopt($curl,CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($curl,CURLOPT_SSL_VERIFYHOST, 0);
        $out = curl_exec($curl);

        curl_close($curl);

        $Response = json_decode($out, true);
        Log::debug('=== ADVERT IMAGE UPDATE ===');
        Log::debug(print_r($Response, true));

        File::delete($file_path);

        return $Response['image_id'];
    }

    public function getDescription($description)
    {
        return html_entity_decode(strip_tags($description));
    }

    public function getAgentPhone($product)
    {
        $agent_phone = $product->agent->phone;

        return str_replace([' ', '+', '(', ')'], '', $agent_phone);
    }

    public function getTotalRooms($product, $category, $subcategory, $offer_type)
    {
        $product_rooms = $product->room;

        $link = "https://partners-api.999.md/features?category_id=$category&subcategory_id=$subcategory&offer_type=$offer_type";

        $headers = [
            'Authorization: Basic ' . $this->api_key,
            'Content-Type: multipart/form-data'
        ];

        $curl = curl_init();

        curl_setopt($curl,CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl,CURLOPT_URL, $link);
        curl_setopt($curl,CURLOPT_HTTPHEADER, $headers);
        curl_setopt($curl,CURLOPT_HEADER, false);
        curl_setopt($curl,CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($curl,CURLOPT_SSL_VERIFYHOST, 0);
        $out = curl_exec($curl);

        curl_close($curl);

        $fields = json_decode($out, true);

        $rooms_list = collect($fields['features_groups'][0]['features'])->firstWhere('id', '241');

        try {
            $rooms_id = collect($rooms_list['options'])->filter(function ($r) use ($product_rooms) {
                return stristr($r['title'], substr($product_rooms, 0, 1)) !== false;
            })->first()['id'];
        } catch (\Exception $e) {
            $rooms_id = $rooms_list['options'][0]['id'];
        }

        return $rooms_id;
    }

    public function getRegionGroup($product, $category, $subcategory, $offer_type)
    {
        $region_group_name = $product->region->group->getOriginal('name_ro');

        $link = "https://partners-api.999.md/features?category_id=$category&subcategory_id=$subcategory&offer_type=$offer_type";

        $headers = [
            'Authorization: Basic ' . $this->api_key,
            'Content-Type: multipart/form-data'
        ];

        $curl = curl_init();

        curl_setopt($curl,CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl,CURLOPT_URL, $link);
        curl_setopt($curl,CURLOPT_HTTPHEADER, $headers);
        curl_setopt($curl,CURLOPT_HEADER, false);
        curl_setopt($curl,CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($curl,CURLOPT_SSL_VERIFYHOST, 0);
        $out = curl_exec($curl);

        curl_close($curl);

        $fields = json_decode($out, true);

        if($offer_type == "776") {
            $region_list = $fields['features_groups'][2]['features'][6]['options'];
        }else{
            $region_list = $fields['features_groups'][3]['features'][6]['options'];
        }

        try {
            $ad_region_id = collect($region_list)->filter(function ($r) use ($region_group_name) {
                return stristr($r['title'], substr($region_group_name, 0, 3)) !== false;
            })->first()['id'];

            if(!$ad_region_id){
                $ad_region_id = $region_list[0]['id'];
            }

        } catch (\Exception $e) {
            $ad_region_id = $region_list[0]['id'];
        }

        return $ad_region_id;
    }

    public function getRegion($product, $region_group, $subcategory)
    {
        $region_name = $product->region->getOriginal('name');

        $link = "https://partners-api.999.md/dependent_options?subcategory_id=$subcategory&dependency_feature_id=7&parent_option_id=$region_group";

        $headers = [
            'Authorization: Basic ' . $this->api_key,
            'Content-Type: multipart/form-data'
        ];

        $curl = curl_init();

        curl_setopt($curl,CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl,CURLOPT_URL, $link);
        curl_setopt($curl,CURLOPT_HTTPHEADER, $headers);
        curl_setopt($curl,CURLOPT_HEADER, false);
        curl_setopt($curl,CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($curl,CURLOPT_SSL_VERIFYHOST, 0);
        $out = curl_exec($curl);

        curl_close($curl);

        $fields = json_decode($out, true);
        $region_list = $fields['options'];

        try {
            $ad_region_id = collect($region_list)->filter(function ($r) use ($region_name) {
                return stristr($r['title'], substr($region_name, 0, 4)) !== false;
            })->first()['id'];
        } catch (\Exception $e) {
            $ad_region_id = $region_list[0]['id'];
        }

        if($region_group == "12900"){ //Если Кишинев, возвращаем тоже Кишинев
            return "13859";
        }

        return $ad_region_id;
    }

    public function getSector($product, $region, $subcategory)
    {
        $region_name = $product->region->getOriginal('name_ro');

        $link = "https://partners-api.999.md/dependent_options?subcategory_id=$subcategory&dependency_feature_id=8&parent_option_id=$region";

        $headers = [
            'Authorization: Basic ' . $this->api_key,
            'Content-Type: multipart/form-data'
        ];

        $curl = curl_init();

        curl_setopt($curl,CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl,CURLOPT_URL, $link);
        curl_setopt($curl,CURLOPT_HTTPHEADER, $headers);
        curl_setopt($curl,CURLOPT_HEADER, false);
        curl_setopt($curl,CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($curl,CURLOPT_SSL_VERIFYHOST, 0);
        $out = curl_exec($curl);

        curl_close($curl);

        $fields = json_decode($out, true);
        $sector_list = $fields['options'];

        try {
            $ad_sector_id = collect($sector_list)->filter(function ($r) use ($region_name) {
                return stristr($r['title'], substr($region_name, 0, 3)) !== false;
            })->first()['id'];

            if(!$ad_sector_id){
                $ad_sector_id = $sector_list[0]['id'];
            }
        } catch (\Exception $e) {
            $ad_sector_id = $sector_list[0]['id'];
        }

        return $ad_sector_id;
    }

    public function getAdvertURL($id)
    {
        return "https://999.md/ro/$id";
    }
}