<?php

namespace App\Http\Controllers;

use App\Models\News;

class NewsController extends Controller
{
    public function getAllNews()
    {
        $news = News::enabled()
            ->latest()
            ->with('photos');

        $news = $news->paginate(12);

        return view('news.list')->with(compact('news'));
    }

    public function getOneNews($slug)
    {
        $data = News::enabled()
            ->whereSlug($slug)
            ->firstOrFail();

        return view('news.view')->with(compact('data'));
    }
}
