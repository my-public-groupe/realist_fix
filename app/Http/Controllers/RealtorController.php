<?php

namespace App\Http\Controllers;

use App\Jobs\ConvertImages;
use App\Models\Agent;
use App\Models\Product;
use App\Models\Region;
use App\Models\Photos;

use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\File;
use Intervention\Image\Facades\Image;

class RealtorController
{
  private function getArrayValue($array, $key, $default = null)
  {
    if ($key instanceof \Closure) {
      return $key($array, $default);
    }

    if (is_array($key)) {
      $lastKey = array_pop($key);
      foreach ($key as $keyPart) {
        $array = $this->getArrayValue($array, $keyPart);
      }
      $key = $lastKey;
    }

    if (is_array($array) && (isset($array[$key]) || array_key_exists($key, $array))) {
      return $array[$key];
    }

    if (($pos = strrpos($key, '.')) !== false) {
      $array = $this->getArrayValue($array, substr($key, 0, $pos), $default);
      $key = substr($key, $pos + 1);
    }

    if (is_object($array)) {
      return $array->$key;
    } elseif (is_array($array)) {
      return (isset($array[$key]) || array_key_exists($key, $array)) ? $array[$key] : $default;
    }

    return $default;
  }

  public function webhook()
  {
    ini_set('memory_limit', '1024M');
    ini_set('max_execution_time', 3600);

    Log::channel('crm')->debug('=== AMO WEBHOOK START ===');
    Log::channel('crm')->debug(print_r(request()->json(), true));
    Log::channel('crm')->debug('=== AMO WEBHOOK END ===');
      
    try {
      $getParamValue = function ($paramAlias, $key = 'values.0.value', $def = '') {
        $params = request()->json('params', []);

        $found = null;
        foreach ($params as $param) {
          if ($param['param_alias'] === $paramAlias) {
            $found = $param;
          }
        }

        if ($found) {
          return $this->getArrayValue($found, $key, $def);
        }

        return $def;
      };

      $visible = request()->json('export_website', false) === true;

      $type = request()->json('type.alias');
      $dealType = request()->json('deal_type_id') === 1 ? 'rent' : 'sale';

      $price = (float)request()->json('price');

      $region = (int)explode("_", $getParamValue('realist_region', 'values.0.enum_id'))[0];

      $employeeEmail = request()->json('employee.email', '');

      $nameRu = request()->json('paramsByAlias.realist_header_ru', request()->json('name'));
      $nameRo = request()->json('paramsByAlias.realist_header_ro');
      $nameEn = request()->json('paramsByAlias.realist_header_en');

      $descriptionRu = request()->json('paramsByAlias.description_ru');
      $descriptionRo = request()->json('paramsByAlias.description_ro');
      $descriptionEn = request()->json('paramsByAlias.description_en');

      $metaDescriptionRu = request()->json('paramsByAlias.meta_description_ru', $descriptionRu);
      $metaDescriptionRo = request()->json('paramsByAlias.meta_description_ro', $descriptionRo);
      $metaDescriptionEn = request()->json('paramsByAlias.meta_description_en', $descriptionEn);

      $metaKeywordsRu = request()->json('paramsByAlias.meta_keywords_ru');
      $metaKeywordsRo = request()->json('paramsByAlias.meta_keywords_ro');
      $metaKeywordsEn = request()->json('paramsByAlias.meta_keywords_en');

      $metaTitleRu = request()->json('paramsByAlias.meta_title_ru', $nameRu);
      $metaTitleRo = request()->json('paramsByAlias.meta_title_ro', $nameRo);
      $metaTitleEn = request()->json('paramsByAlias.meta_title_en', $nameEn);

      // later
      $isElite = request()->json('paramsByAlias.realist_region') === 'Y';

      $buildingType = (int)explode("_", $getParamValue('realist_tip_stroitelstva', 'values.0.enum_id'))[0];

      $roomsCountId = $getParamValue('rooms_count', 'values.0.enum_id');

      $roomsCountMap = ['1' => 3, '2' => 4, '3' => 5, '4' => 6, '5plus' => 7];
      $roomsCount = $roomsCountId && $roomsCountMap[$roomsCountId] ? $roomsCountMap[$roomsCountId] : null;

      // 3
      $repairType = (int)explode("_", $getParamValue('realist_repairtype', 'values.0.enum_id'))[0];

      // 11
      $repairTypeAlt = (int)explode("_", $getParamValue('realist_repairtype_alt', 'values.0.enum_id'))[0];

      $totalSquare = (int)(request()->json('paramsByAlias.total_square', ''));
      $floor = (int)explode("_", $getParamValue('realist_floor', 'values.0.enum_id'))[0];
      $floorsCount = (int)explode("_", $getParamValue('realist_floorscount', 'values.0.enum_id'))[0];
      $layout = (int)explode("_", $getParamValue('realist_planirovka', 'values.0.enum_id'))[0];
      $balcony = (int)explode("_", $getParamValue('realist_BalconyOrLoggia', 'values.0.enum_id'))[0];
      $bathrooms = (int)explode("_", $getParamValue('realist_bathrooms', 'values.0.enum_id'))[0];
      $parking = (int)explode("_", $getParamValue('realist_parking', 'values.0.enum_id'))[0];
      $cabinets = (int)explode("_", $getParamValue('realist_cabinets', 'values.0.enum_id'))[0];

      $typeProm = (int)explode("_", $getParamValue('type_prom', 'values.0.enum_id'))[0];
      $typeCommercial = (int)explode("_", $getParamValue('type_commerce', 'values.0.enum_id'))[0];
      $typeLand = (int)explode("_", $getParamValue('realist_landtype', 'values.0.enum_id'))[0];

      $m2square = (int)request()->json('paramsByAlias.area_sotka', '');
      $hectareSquare = (int)request()->json('paramsByAlias.area_hectare', '');

      $isOffice = request()->json('paramsByAlias.IsOffice', 'N') === 'Y';
      $isCommerce = request()->json('paramsByAlias.IsCommerce', 'N') === 'Y';

      $product = null;

      /*$websiteId = request()->json('website_id');
      if (!empty($websiteId)) {
        $product = Product::where('id', (int)$websiteId)->first();
      }*/

      $slug = Str::slug($nameRu . '-' . request()->json('id'));

      // если не нашли по website_id - ищем по названию
      /*if (empty($product)) {
        $product = Product::where('slug', $slug)->first();
      }*/

      $product = Product::where('slug', $slug)->first();

      // к обновлению
      $fields = [
        'type' => $dealType === 'sale' ? Product::TYPE_SALE : Product::TYPE_RENT,
        'name' => $nameRu,
        'name_ro' => $nameRo,
        'name_en' => $nameEn,
        'description' => $descriptionRu,
        'description_ro' => $descriptionRo,
        'description_en' => $descriptionEn,
        'price' => $price,
        'slug' => $slug,
        'sort' => 2,
        'photo_label' => 0,
        'enabled' => $visible ? 1 : 0
      ];

      $fields['region_id'] = !empty($region) ? $region : null;

      try {
        if (!empty($employeeEmail)) {
          $agent = Agent::where('params', 'LIKE', "%" . $employeeEmail . "%")->first();

          if (!empty($agent)) {
            $fields['agent_id'] = $agent->id;
          } else {
            $fields['agent_id'] = null;
          }
        }
      } catch (\Exception $e) {
      }

      if (!empty($product)) {
        $product->update($fields);

        $product = Product::where(['id' => $product->id])->first();
      } else {
        $product = Product::create($fields);
      }

      // если объект скрыт - остальное нет смысла обновлять
      /*if (!$visible) {
        return ['id' => $product->id, 'https://realist.md/products/' . $slug];
      }*/

      // categories
      $categoriesMap = [
        'realist_apartment' => 1,
        'realist_commerce' => 5,
        'realist_prom' => 4,
        'realist_houses' => 2,
        'realist_office' => 3,
        'realist_land' => 6,
        'realist_exclusive' => 10
      ];
      $categoryId = isset($categoriesMap[$type]) ? $categoriesMap[$type] : null;
      $categoriesIds = [];
      if (!empty($categoryId)) {
        $categoriesIds[] = $categoryId;
      }
      if ($isOffice && !in_array(3, $categoriesIds)) {
        $categoriesIds[] = 3;
      }
      if ($isCommerce && !in_array(5, $categoriesIds)) {
        $categoriesIds[] = 5;
      }

      $product->categories()->sync($categoriesIds);

      // parameters
      try {
        $parameters = [];

        if (!empty($buildingType)) {
          $parameters[1] = ['value_id' => $buildingType, 'value' => $buildingType, 'value_ro' => '', 'value_en' => ''];
        }

        if (!empty($roomsCount)) {
          $parameters[2] = ['value_id' => $roomsCount, 'value' => $roomsCount, 'value_ro' => '', 'value_en' => ''];
        }

        if (!empty($totalSquare)) {
          $parameters[4] = ['value_id' => $totalSquare, 'value' => $totalSquare, 'value_ro' => '', 'value_en' => ''];
        }

        if (!empty($floor)) {
          $parameters[5] = ['value_id' => $floor, 'value' => $floor, 'value_ro' => '', 'value_en' => ''];
        }

        if (!empty($floorsCount)) {
          $parameters[6] = ['value_id' => $floorsCount, 'value' => $floorsCount, 'value_ro' => '', 'value_en' => ''];
        }

        if (!empty($layout)) {
          $parameters[7] = ['value_id' => $layout, 'value' => $layout, 'value_ro' => '', 'value_en' => ''];
        }

        if (!empty($balcony)) {
          $parameters[8] = ['value_id' => $balcony, 'value' => $balcony, 'value_ro' => '', 'value_en' => ''];
        }

        if (!empty($bathrooms)) {
          $parameters[9] = ['value_id' => $bathrooms, 'value' => $bathrooms, 'value_ro' => '', 'value_en' => ''];
        }

        if (!empty($parking)) {
          $parameters[10] = ['value_id' => $parking, 'value' => $parking, 'value_ro' => '', 'value_en' => ''];
        }

        if (!empty($cabinets)) {
          $parameters[12] = ['value_id' => $cabinets, 'value' => $cabinets, 'value_ro' => '', 'value_en' => ''];
        }

        if (!empty($typeLand)) {
          $parameters[15] = ['value_id' => $typeLand, 'value' => $typeLand, 'value_ro' => '', 'value_en' => ''];
        }

        if (!empty($m2square)) {
          $parameters[16] = ['value_id' => 0, 'value' => $m2square, 'value_ro' => '', 'value_en' => ''];
        }

        if (!empty($hectareSquare)) {
          $parameters[17] = ['value_id' => 0, 'value' => $hectareSquare, 'value_ro' => '', 'value_en' => ''];
        }

        // print_r($categoryId);die();

        if (in_array($categoryId, [1, 3, 5])) {
          if (!empty($repairType)) {
            $parameters[3] = ['value_id' => $repairType, 'value' => $repairType, 'value_ro' => '', 'value_en' => ''];
          }
        } else if ($categoryId === 2) {
          if (!empty($repairTypeAlt)) {
            $parameters[11] = ['value_id' => $repairTypeAlt, 'value' => $repairTypeAlt, 'value_ro' => '', 'value_en' => ''];
          }
        }

        if ($categoryId === 4) {
          if (!empty($typeProm)) {
            $parameters[13] = ['value_id' => $typeProm, 'value' => $typeProm, 'value_ro' => '', 'value_en' => ''];
          }
        } else if ($categoryId === 5) {
          if (!empty($typeCommercial)) {
            $parameters[14] = ['value_id' => $typeCommercial, 'value' => $typeCommercial, 'value_ro' => '', 'value_en' => ''];
          }
        }

        $product->parameters()->sync($parameters);
      } catch (\Exception $e) {
      }

      // meta
      try {
        $product->saveRawMeta([
          'meta_description' => strip_tags($metaDescriptionRu),
          'meta_keywords' => strip_tags($metaKeywordsRu),
          'title' => strip_tags($metaTitleRu),
          'meta_description_ro' => strip_tags($metaDescriptionRo),
          'meta_keywords_ro' => strip_tags($metaKeywordsRo),
          'title_ro' => strip_tags($metaTitleRo),
          'meta_description_en' => strip_tags($metaDescriptionEn),
          'meta_keywords_en' => strip_tags($metaKeywordsEn),
          'title_en' => strip_tags($metaTitleEn)
        ]);
      } catch (\Exception $e) {
      }

      // photos
      try {
        $ch = curl_init();

        $photos = request()->json('files', []);

        $ids = [];
        foreach ($photos as $photo) {
          if (count($ids) <= 100) {
            $id = $this->uploadImage($ch, $product, $photo);
            $ids[] = $id;
          }
        }

        curl_close($ch);

        if (!empty($ids)) {
          $notExPhotos = Photos::whereNotIn('id', $ids)->where('table_id', $product->id)->where('table', 'products')->delete();
        } else {
          $notExPhotos = Photos::where('table_id', $product->id)->where('table', 'products')->delete();
        }
      } catch (\Exception $e) {
      }

      ConvertImages::dispatch($product->id);

      return [
        'id' => $product->id,
        'link' => 'https://realist.md/products/' . $slug
      ];
    } catch (\Exception $e) {
      return $e->getMessage();
    }

    // return response()->json([
    //   "type" => $type,
    //   "dealType" => $dealType,
    //   "price" => $price,
    //   "region" => $region,
    //   "nameRu" => $nameRu,
    //   "nameRo" => $nameRo,
    //   "nameEn" => $nameEn,
    //   "descriptionRu" => $descriptionRu,
    //   "descriptionRo" => $descriptionRo,
    //   "descriptionEn" => $descriptionEn,
    //   "metaDescriptionRu" => $metaDescriptionRu,
    //   "metaDescriptionRo" => $metaDescriptionRo,
    //   "metaDescriptionEn" => $metaDescriptionEn,
    //   "metaKeywordsRu" => $metaKeywordsRu,
    //   "metaKeywordsRo" => $metaKeywordsRo,
    //   "metaKeywordsEn" => $metaKeywordsEn,
    //   "metaTitleRu" => $metaTitleRu,
    //   "metaTitleRo" => $metaTitleRo,
    //   "metaTitleEn" => $metaTitleEn,
    //   "isElite" => $isElite,
    //   "buildingType" => $buildingType,
    //   "roomsCount" => $roomsCount,
    //   "repairType" => $repairType,
    //   "totalSquare" => $totalSquare,
    //   "floor" => $floor,
    //   "floorsCount" => $floorsCount,
    //   "layout" => $layout,
    //   "balcony" => $balcony,
    //   "bathrooms" => $bathrooms,
    //   "parking" => $parking,
    //   "cabinets" => $cabinets,
    //   "typeProm" => $typeProm,
    //   "typeCommercial" => $typeCommercial,
    //   "typeLand" => $typeLand,
    //   "m2square" => $m2square,
    //   "hectareSquare" => $hectareSquare
    // ]);
  }

  public function updateLabel()
  {
    try {
      $id = (int)request()->json('id');
      $photo_label = (int)request()->json('photo_label');

      if (!empty($id) && !empty($photo_label)) {
        $product = Product::where('id', (int)$id)->first();

        if (!empty($product)) {
          $product->update(['photo_label' => $photo_label]);
        }
      }
    } catch (\Exception $e) {
      return $e->getMessage();
    }

    return ['ok' => true];
  }

  public function uploadImage($ch, Product $product, $image)
  {
    $directory = base_path() . "/" . Config::get('photos.images_dir');

    if (!File::exists($directory)) {
      File::makeDirectory($directory);
    }

    $table = 'products';
    $sort = (int)$image['position'];

    $imageName = $image['id'] . '-' . $image['fileName'] . '.' . $image['ext'];
    $imageSrc = $directory . $imageName;

    curl_setopt($ch, CURLOPT_URL, $image['src']);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    $imageData = curl_exec($ch);

    if (empty($imageData)) {
      Log::channel('crm')->debug(print_r(['empty image data', $image], true));
      return;
    }

    file_put_contents($imageSrc, $imageData);

    $imageInfo = getimagesizefromstring($imageData);
    
    unset($imageData);

    $width = (int)$imageInfo[0];
    $height = (int)$imageInfo[1];

    $orig_image = Image::make($imageSrc);
    $image = $orig_image;
    $image->resize($width, $height, function ($constraint) {
      $constraint->aspectRatio();  //сохранять пропорции
      $constraint->upsize();       //если фото меньше, то не увеличивать
    });
    $image->save($imageSrc, 80, 'jpg');

    // thumb
    $all_thumbs = Config::get('photos.thumbs');
    foreach ($all_thumbs as $thumb) {
      $thumbPath = $directory . $thumb['path'] . "/";

      if (!File::exists($thumbPath)) {
        File::makeDirectory($thumbPath);
      }

      $image->fit($thumb['width'], $thumb['height']);
      $image->save($thumbPath . $imageName);
    }
    unset($image);

    // watermark
    Photos::addWatermark($imageSrc);

    $photo = Photos::where('source', $imageName)->first();
    if (empty($photo)) {
      $photo = new Photos;
      $photo->table = $table;
      $photo->table_id = $product->id;
    }

    $photo->source = $imageName;
    $photo->sort = $sort;
    $photo->save();

    return $photo->id;
  }

  public function test()
  {
    $regions = Region::enabled()
      ->orderBy('sort')
      ->get();

    $ret = [];
    foreach ($regions as $region) {
      $ret[] = ['id' => $region->id, 'name' => $region->name];
    }

    return response()->json($ret);
  }
}
