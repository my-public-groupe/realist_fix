<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Categories;
use App\Models\Parameter;
use App\Models\ParameterValue;
use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Session;

class ParametersController extends Controller
{
    public function index()
    {
        $data = Parameter::all();
        return view('admin.parameters.index')->with(compact('data'));
    }

    /**
     * Process datatables ajax request.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function ajaxData()
    {
        $query = Parameter::query();

        return self::datatablesCommon($query)
            ->make(true);
    }


    public function create()
    {
        return view('admin.parameters.edit');
    }

    public function store(Request $request)
    {
        $rules = array(
            'name'  => 'required'
        );

        $this->validate($request, $rules);

        return $this->save($request, null);
    }

    private function save(Request $request, $id)
    {
        // store
        if (!isset($id)) {
            $data = new Parameter();
            $data->type = $request->type;
        } else {
            $data = Parameter::find($id);
        }

        $data->name  = $request->name;
        $data->params  = $request->params;
        $data->is_filter = $request->has('is_filter');

        $data->save();

        if (isset($request->values)) {
            foreach ($request->values['id'] as $key => $value_id) {
                $ru = $request->values['ru'][$key];
                $ro = $request->values['ro'][$key];
                $en = $request->values['en'][$key];

                if ($ru == "" && $ro == "" && $en == "") continue;

                if ($value_id > 0) {
                    $parameter_value = ParameterValue::find($value_id);
                } else {
                    $parameter_value = new ParameterValue();
                }
                $parameter_value->value    = $ru;
                $parameter_value->value_ro = $ro;
                $parameter_value->value_en = $en;
                $parameter_value->sort     = $key;

                $data->values()->save($parameter_value);
            }
        }

        // redirect
        Session::flash('message', 'Сохранено!');
        return redirect()->route('admin.parameters.edit', $data->id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $data = Parameter::where('id', $id)->with('values')->first();

        return view('admin.parameters.edit')->with(compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $rules = array(
            'name'          => 'required'
        );

        $this->validate($request, $rules);

        return $this->save($request, $id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        Parameter::destroy($id);
        Session::flash('message', 'Удалено!');
        return back();
    }
    
    public function getCategoryParameters()
    {
        $category_id = Input::get('category_id', 0);
        $params = Categories::find($category_id)
                    ->parameters()
                    ->with(['parameter' => function($query){
                        $query->with('values');
                    }])
                    ->get();

        $data = [];
        foreach ($params as $param) {
            $data[] = [
                'id' => $param->parameter->id,
                'name' => $param->parameter->name,
                'type' => $param->parameter->type,
                'values' => $param->parameter->values->pluck('value', 'id')->toArray(),
            ];
        }

        return response()->json(['success' => 'true', 'data' => $data]);
    }

    public function removeParameter()
    {
        $table = Input::get('table', '');
        $parameters_id = Input::get('parameters_id', 0);
        $table_id = Input::get('table_id', 0);

        if ($table == 'categories') {
            $category = Categories::find($table_id);
            $category->parameters()->detach($parameters_id);
        }

        if ($table == 'products') {
            $product = Product::find($table_id);
            $product->parameters()->detach($parameters_id);
        }

        return response()->json(['success' => 'true']);
    }

    public function removeParameterValue()
    {
        $param_value_id = Input::get('param_value_id', 0);

        ParametersProducts::where('values_id', $param_value_id)->delete();

        ParameterValue::find($param_value_id)->delete();

        return response()->json(['success' => 'true']);
    }
}
