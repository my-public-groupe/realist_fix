<?php

namespace App\Http\Controllers\Admin;

use App\Models\Lists;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;
use Illuminate\Validation\Rule;

class ListsController extends Controller
{
    public function index(Request $request)
    {
        $id = $request->id;

        if ($id > 0) {
            $parent = Lists::where('id', $id)->with('children')->first();
            $data = $parent->children;
        } else {
            $data = Lists::where('parent_id', 0)->get();
        }

        $parents = $this->getParentsArray();
        $model = 'lists';
        return view('admin.lists.index')->with(compact('data', 'parents', 'id', 'model', 'parent'));
    }

    public function create(Request $request)
    {
        $parent_id = $request->id;
        $parents = Lists::all()->pluck('name', 'id')->toArray();

        if($parent_id == 3) {
            return view('admin.lists.edit_slide')->with(compact('parents', 'parent_id'));
        }

        return view('admin.lists.edit')->with(compact('parents', 'parent_id'));
    }

    public function store(Request $request)
    {
        $rules = array(
            'name.*' => 'required',
            'slug' => 'required|unique:lists'
        );

        $this->validate($request, $rules);

        return $this->save($request);
    }

    private function save(Request $request, $id = null)
    {
        // store
        if (!isset($id)) {
            $data = new Lists();
        } else {
            $data = Lists::find($id);
        }

        $data->name = $request->name;
        $data->created_at = $request->date;
        $data->slug = $request->slug;
        $data->parent_id = $request->parent_id;
        $data->sort = $request->sort;

        $data->description = $request->description;
        $data->description_short = $request->description_short;

        // params
        if(!empty($request->params)) {
            $data->params = $request->params;
        }

        $data->save();

        $this->UpdatePhotos($request, $data->id);

        $data->saveMeta($request);

        // redirect
        Session::flash('message', 'Сохранено');
        return redirect()->route('admin.lists.edit', $data->id);
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function edit($id)
    {
        $data = Lists::find($id);
        $parents = Lists::all()->pluck('name', 'id')->toArray();
        $parent_id = $data->parent_id;

        if($parent_id == 3){
            return view('admin.lists.edit_slide')->with(compact('data', 'parents', 'parent_id'));
        }

        return view('admin.lists.edit')->with(compact('data', 'parents', 'parent_id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $rules = array(
            'name.*' => 'required',
            'slug' => [
                'required',
                Rule::unique('lists')->ignore($id),
            ],
        );

        $this->validate($request, $rules);

        return $this->save($request, $id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id)
    {
        Lists::destroy($id);
        Session::flash('message', 'Удалено');
        return back();
    }

    private function getParentsArray()
    {
        $parent = Lists::where('parent_id', 0)->get();
        if ($parent->count() == 0) {
            return [];
        }
        return $parent->pluck('name', 'id')->toArray();
    }
}
