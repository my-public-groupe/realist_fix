<?php

namespace App\Http\Controllers\admin;


use App\Exports\CartProductsExport;
use App\Exports\ObjectsAgentsExport;
use App\Http\Controllers\Controller;
use App\Jobs\ConvertImages;
use App\Models\Agent;
use App\Models\Brand;
use App\Models\Categories;
use App\Models\Label;
use App\Models\Lists;
use App\Models\Parameter;
use App\Models\Product;
use App\Models\Region;
use App\Models\RegionGroup;
use App\Traits\CommonTrait;
use App\Traits\MetaTrait;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\URL;
use Illuminate\Validation\Rule;
use Maatwebsite\Excel\Facades\Excel;

class ProductsController extends Controller
{
    public function index()
    {
        return view('admin.products.index')->with($this->needs());
    }

    public function ajaxData()
    {
        $query = Product::query();

        $query->when(request('agent_id'), function($query) {
            $query->where('agent_id', request('agent_id'));
        });

        return self::datatablesCommon($query)
            ->rawColumns(['name', 'actions', 'area'])
            ->make(true);
    }

    public function create()
    {
        return view('admin.products.edit')->with($this->needs());
    }

    public function store(Request $request)
    {
        $rules = array(
            'name.*' => 'required',
            'categories' => 'required',
            'region_id' => 'required',
            'slug' => 'required|unique:products'
        );

        $this->validate($request, $rules);

        return $this->save($request, null);
    }

    private function save(Request $request, $id)
    {
        $fields = $request->except([
            '_token',
            'file_upload',
            'photos',
            'meta_description',
            'meta_keywords',
            'title',
            'parameters',
            'value_ru',
            'value_ro',
            'value_en',
            'value_id',
            'categories',
            'type_values'
        ]);

        $fields['top'] = $request->has('top');
        $fields['photo_label'] = $request->get('photo_label');

        $data = Product::updateOrCreate(['id' => $id], $fields);

        $this->UpdatePhotos($request, $data->id);

        $data->saveMeta($request);

        $data->categories()->sync($request->categories);

        // parameters
        if ($request->has('parameters')) {
            $parameters = [];
            foreach ($request->parameters as $parameter_id) {
                if($parameter_id != Parameter::ID_TYPE){
                    $parameters[$parameter_id] = [
                        'value' => isset($request->value_ru[$parameter_id]) ? $request->value_ru[$parameter_id] : '',
                        'value_ro' => isset($request->value_ro[$parameter_id]) ? $request->value_ro[$parameter_id] : '',
                        'value_en' => isset($request->value_en[$parameter_id]) ? $request->value_en[$parameter_id] : '',
                        'value_id' => isset($request->value_id[$parameter_id]) ? $request->value_id[$parameter_id] : 0,
                    ];

                    $data->parameters()->sync($parameters);
                }
            }

            if(!empty($request->type_values)){
                $data->parameters()->detach(Parameter::ID_TYPE);

                foreach ($request->type_values as $value_id){
                    if($value_id > 0){
                        $data->parameters()->attach([Parameter::ID_TYPE => ['value_id' => $value_id]]);
                    }
                }
            }
        }

        ConvertImages::dispatch($data->id);

        // redirect
        Session::flash('message', 'Сохранено');
        return redirect(route('admin.products.edit', $data->id));
    }

    public function edit($id)
    {
	    $data = Product::find($id);

        $category_parameters = new Collection();
        if ($data->categories()->count() > 0) {
            foreach ($data->categories as $category) {
                $params = $category
                    ->parameters()
                    ->get();
                foreach ($params as $param) {
                    if (! $category_parameters->contains('id', $param->id)) {
                        $category_parameters->push($param);
                    }
                }
            }
        }

        $product_parameters = $data->parameters;

        // собираем коллекцию из параметров категории,
        // а если есть параметр у продукта, заменяем на него
        $parameters = new Collection();
        foreach ($category_parameters as $cp) {
            if ($product_parameters->contains('id', $cp->id)) {
                $pp = $product_parameters->filter(function($item) use ($cp) {
                    return $item->id == $cp->id;
                })->first();
                $parameters->push($pp);
            } else {
                $parameters->push($cp);
            }
        }

        return view('admin.products.edit')->with(compact('data', 'parameters'))->with($this->needs());
    }

    public function update(Request $request, $id)
    {
        $rules = array(
            'name.*' => 'required',
            'categories' => 'required',
            'region_id' => 'required',
            'slug' => [
                'required',
                Rule::unique('products')->ignore($id),
            ],
        );

        $this->validate($request, $rules);

        return $this->save($request, $id);
    }

    public function show(Request $request)
    {
        $id = (int) filter_var($request->getRequestUri(), FILTER_SANITIZE_NUMBER_INT);

        return redirect()->route('admin.products.edit', $id);
    }

    private function needs()
    {
        $categories = Categories::pluck('name', 'id')->toArray();

        $agents = Agent::pluck('name', 'id')->toArray();

        $regions = Region::getSelectArray();

        $nearby_places = Lists::where('slug', 'nearby-places')
            ->first()
            ->children
            ->pluck('name', 'id')
            ->toArray();

        $features = Lists::where('slug', 'object-features')
            ->first()
            ->children
            ->pluck('name', 'id')
            ->toArray();

        return compact('categories', 'agents', 'regions', 'nearby_places', 'features');
    }

    public function productsAgentsPage()
    {
        $categories = Categories::enabled()
            ->where('id', '!=', 7)
            ->pluck('name', 'id');

        $agents = Agent::orderBy('name')->get();

        return view('admin.excel.products_agents_page')->with(compact('categories', 'agents'));
    }

    public function productsAgentsExcel(Request $request)
    {
        $date = date('d_m_Y');

        return Excel::download(new ObjectsAgentsExport($request->type, $request->category, $request->region_group, $request->agent_id),
            "realist.md_objects_agents_$date.xlsx", \Maatwebsite\Excel\Excel::XLSX);
    }
}
