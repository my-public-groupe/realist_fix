<?php

namespace App\Http\Controllers\Admin;

use App\Models\Reviews;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class ReviewsController extends Controller
{
    public function index()
    {
        $data = Reviews::orderBy('created_at', 'desc')->get();
        return view('admin.reviews.index')->with(compact('data'));
    }

    public function create()
    {
        return response()->view('admin.reviews.edit');
    }

    public function store(Request $request)
    {
        $rules = [
            'review_name.*' => 'required',
            'review_desc.*' => 'required',
        ];

        $this->validate($request, $rules);

        return $this->save($request, null);
    }

    private function save(Request $request, $id = null)
    {
        if(isset($id)) {
            $data = Reviews::find($id);
        }else{
            $data = new Reviews();
            $data->enabled = true;
        }

        $data->updated_at = Carbon::now();
        $data->name = $request->review_name;
        $data->description = $request->review_desc;
        $data->sort = $request->sort ?? 0;
        $data->save();

        // redirect
        Session::flash('message', trans('common.saved'));
        return redirect()->route('admin.reviews.edit', $data->id);
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        $data = Reviews::find($id);

        return response()->view('admin.reviews.show', compact('data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     */
    public function edit($id)
    {
        $data = Reviews::find($id);

        return view('admin.reviews.edit')->with(compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $rules = [
            'review_name.*' => 'required',
            'review_desc.*' => 'required',
        ];

        $this->validate($request, $rules);

        return $this->save($request, $id);
    }

    public function destroy($id)
    {
        Reviews::destroy($id);
        Session::flash('message', trans('common.deleted'));

        return redirect()->route('admin.reviews.index');
    }
}
