<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Models\Categories;
use App\Models\Parameter;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Session;

class CategoriesController extends Controller
{
    public function index()
    {
        return view('admin.categories.index');
    }

    /**
     * Process datatables ajax request.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function ajaxData()
    {
        $query = Categories::orderBy('sort');

        return self::datatablesCommon($query)
            ->make(true);
    }

    public function create()
    {
        return view('admin.categories.edit')->with($this->needs());
    }

    public function store(Request $request)
    {
        $rules = array(
            'name' => 'required',
            'slug' => 'required|unique:categories'
        );

        $this->validate($request, $rules);

        return $this->save($request, null);
    }

    private function save(Request $request, $id)
    {
        // store
        if (!isset($id)) {
            $data = new Categories();
        } else {
            $data = Categories::find($id);
        }

        $data->name = $request->name;
        $data->slug = $request->slug;
        $data->description = $request->description;
        $data->description_short = $request->description_short;
        $data->save();

        $this->UpdatePhotos($request, $data->id);

        $data->saveMeta($request);

        // parameters
        if ($request->has('parameters')) {
            $parameters = [];
            foreach ($request->parameters as $key => $parameter_id) {
                $parameters[$parameter_id] = ['sort' => $key];
            }
            $data->parameters()->sync($parameters);
        }

        // redirect
        Session::flash('message', 'Сохранено');
        return redirect('admin/categories');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $data = Categories::find($id);
        $category_parameters = $data->parameters()->orderBy('sort')->get();
        return view('admin.categories.edit')->with(compact('data', 'category_parameters'))->with($this->needs());;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $rules = array(
            'name' => 'required',
            'slug' => 'required|unique:categories,id,{$id}'
        );

        $this->validate($request, $rules);

        return $this->save($request, $id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        Categories::destroy($id);
        Session::flash('message', 'Удалено');
        return back();
    }

    private function needs()
    {
        $parameters = Parameter::orderBy('name')->pluck('name','id')->toArray();
        return compact('parameters');
    }
}
