<?php

namespace App\Http\Controllers\Admin;

use App\Models\News;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;
use Validator;

class NewsController extends Controller
{
    public function index()
    {
        return view('admin.news.index');
    }

    /**
     * Process datatables ajax request.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function ajaxData()
    {
        $query = News::latest();

        return self::datatablesCommon($query)
            ->make(true);
    }

    public function create()
    {
        return view('admin.news.edit');
    }

    public function store(Request $request)
    {
        $rules = [
            'name'          => 'required',
            'slug'          => 'required|unique:news'
        ];

        $this->validate($request, $rules);

        return $this->save($request, null);
    }

    private function save(Request $request, $id = null){
        // store
        $fields = $request->except([
            '_token',
            'file_upload',
            'photos',
            'meta_description',
            'meta_keywords',
            'title',
        ]);

        $data = News::updateOrCreate(['id' => $id], $fields);

        $this->UpdatePhotos($request, $data->id);

        $data->saveMeta($request);

        // redirect
        Session::flash('message', 'Сохранено!');
        return redirect()->route('admin.news.edit', $data->id);
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $data     = News::find($id);
        return view('admin.news.edit')->with(compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $rules = [
            'name'          => 'required',
            'slug'          => 'required|unique:news,id,{$id}',
        ];

        $validator = Validator::make($request->all(), $rules);

        $this->validate($request, $rules);

        return $this->save($request, $id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        News::destroy($id);
        Session::flash('message', 'Удалено');
        return back();
    }

}
