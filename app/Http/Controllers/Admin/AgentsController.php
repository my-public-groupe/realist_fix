<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Agent;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Session;

class AgentsController extends Controller
{
    public function index()
    {
        return view('admin.agents.index');
    }

    /**
     * Process datatables ajax request.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function ajaxData()
    {
        $query = Agent::orderBy('sort');

        return self::datatablesCommon($query)
            ->make(true);
    }

    public function create()
    {
        return view('admin.agents.edit');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {
        $rules = array(
            'name' => 'required',
        );

        $this->validate($request, $rules);

        return $this->save($request, null);
    }

    private function save(Request $request, $id)
    {
        // store
        if (!isset($id)) {
            $data = new Agent();
        } else {
            $data = Agent::find($id);
        }

        $data->name = $request->name;
        $data->phone = $request->phone;
        $data->sort = $request->sort ?? 0;
        $data->params = $request->params;
        $data->save();

        $this->UpdatePhotos($request, $data->id);

        // redirect
        Session::flash('message', 'Сохранено');
        return redirect('admin/agents');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $data = Agent::find($id);
        return view('admin.agents.edit')->with(compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @return Response
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(Request $request, $id)
    {
        $rules = array(
            'name' => 'required',
        );

        $this->validate($request, $rules);

        return $this->save($request, $id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        Agent::destroy($id);
        Session::flash('message', 'Удалено');
        return back();
    }
}
