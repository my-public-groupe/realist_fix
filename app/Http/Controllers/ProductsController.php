<?php

namespace App\Http\Controllers;

use App\Models\Agent;
use App\Models\Categories;
use App\Models\Lists;
use App\Models\Product;
use App\Models\RegionGroup;
use PDF;

class ProductsController extends Controller
{
    public function getRent()
    {
        $type = 'rent';

        $categories = Categories::enabled()->get()->map(function ($item){
           $item->url = route('rent-category', $item->slug);
           return $item;
        });

        $breadcrumbs = [trans('common.rent') => route('rent')];

        return view('objects-list')->with(compact('categories', 'breadcrumbs', 'type'));
    }

    public function getRentCategory($slug)
    {
        $category = Categories::enabled()
            ->whereSlug($slug)
            ->firstOrFail();

        $breadcrumbs = [trans('common.rent') => route('rent'), $category->name => '#'];

        $type = 'rent';

        return view('objects-list')->with(compact('category', 'breadcrumbs', 'type'));
    }

    public function getSale()
    {
        $type = 'sale';

        $categories = Categories::enabled()->get()->map(function ($item){
            $item->url = route('sale-category', $item->slug);
            return $item;
        });

        $breadcrumbs = [trans('common.sale') => route('sale')];

        return view('objects-list')->with(compact('categories', 'breadcrumbs', 'type'));
    }

    public function getSaleCategory($slug)
    {
        $category = Categories::enabled()
            ->whereSlug($slug)
            ->firstOrFail();

        $breadcrumbs = [trans('common.sale') => route('sale'), $category->name => '#'];

        $type = 'sale';

        return view('objects-list')->with(compact('category', 'breadcrumbs', 'type'));
    }

    public function getProduct($slug)
    {
        $product = Product::enabled()
            ->with('parameters')
            ->whereSlug($slug)
            ->firstOrFail();

        $breadcrumbs = [];

        if ($product->type == Product::TYPE_RENT) {
            $breadcrumbs[trans('common.rent')] = route('rent-category', $product->categories[0]->slug);
            //$breadcrumbs[$product->categories[0]->name] = route('rent-category', $product->categories[0]->slug);
        } else {
            $breadcrumbs[trans('common.sale')] = route('sale-category', $product->categories[0]->slug);
            //$breadcrumbs[$product->categories[0]->name] = route('sale-category', $product->categories[0]->slug);
        }

        $breadcrumbs[$product->name] = "#";

        return view('object')->with(compact('breadcrumbs', 'product'));
    }

    public function getCategoryFilters($id)
    {
        $category = Categories::find($id);

        $regions_sort = [
            1 => 0,
            2 => 1,
            4 => 2,
            5 => 3,
            6 => 4,
            3 => 5,
            8 => 6
        ];
        $regions = RegionGroup::whereIn('id', [1,2,3,4,5,6,8])
            ->with(['regions' => function ($q) {
                $q->enabled();
                $q->where('id', '!=', 12); //без кишинева
                $q->oldest('regions.sort');
            }])
            ->get()
            ->map(function ($item) use ($regions_sort) {
                $item->sort = $regions_sort[$item->id];

                //костыль замены ID группы на ID регионов
                if($item->id == 4){
                    $item->id = 122;
                }else if($item->id == 6){
                    $item->id = 7;
                }else if($item->id == 5){
                    $item->id = 6;
                }
                return $item;
            });

        $regions = $regions->sortBy('sort')->values();

        $params = $category->parameters;

        if ($id == 1) {
            $params = $params->whereIn('id', [1, 2, 3, 4]);
        } elseif ($id == 3) {
            $params = $params->whereNotIn('id', [1, 12]);
        } elseif ($id == 5) {
            foreach ($params as $param) {
                if ($param->id == 14) {
                    $param->setRelation('values', $param->noHotels()); //без отелей
                }
            }
        }

        return response()->json([
            'params' => $params,
            'regions' => $regions,
        ]);
    }

    public function getObjects($type)
    {
        $sort = request('sort', 'new');

        $query = Product::enabled();

        switch ($sort){
            case 'old':
                $query = $query->oldest('updated_at');
                break;
            case 'price_asc':
                $query = $query->oldest('price');
                break;
            case 'price_desc':
                $query = $query->latest('price');
                break;
            default:
                $query = $query->latest();
                break;
        }

        if ($type == 'rent') {
            $query->rent();
        } else {
            $query->sale();
        }

        $category_id = request('category_id', 0);

        if ($category_id != 0) {
            $query->whereHas('categories', function ($query) use ($category_id) {
                $query->where('categories_id', $category_id);
            });
        }

        $query->whereHas('agent', function ($query){
           $query->enabled();
        });

        $filters = json_decode(request('filters'), true);

        if (!empty($filters)) {
            $query->filters($filters);
        }

        $products = $query->paginate();

        $products->getCollection()->transform(function ($p) {
            $p->mainphotothumb = $p->mainPhotoThumb();
            $p->timestamp = $p->getTimestamp($p->mainphotothumb);

            return $p;
        });

        return response()->json($products);
    }

    public function getTopObjects()
    {
        $products = Product::where('top', true)
            ->enabled()
            ->get();

        return response()->json($products);
    }

    public function getRelatedObjects($id)
    {
        $product = Product::find($id);

        return response()->json($product->getRelated());
    }

    public function getAgentObjects($id)
    {
        $agent = Agent::find($id);

        $products = $agent->products()
            ->enabled()
            ->paginate();

        $products->getCollection()->transform(function ($p) {
            $p->mainphotothumb = $p->mainPhotoThumb();
            $p->timestamp = $p->getTimestamp($p->mainphotothumb);

            return $p;
        });

        return response()->json($products);
    }

    public function search()
    {
        return view('search');
    }

    public function getSearchResults()
    {
        $products = Product::searchByKeyword(request('searchword'))
            ->enabled()
            ->paginate();

        $products->getCollection()->transform(function ($p) {
            $p->mainphotothumb = $p->mainPhotoThumb();
            $p->timestamp = $p->getTimestamp($p->mainphotothumb);

            return $p;
        });

        return response()->json($products);
    }

    public function generatePdf($id)
    {
        $product = Product::enabled()
            ->with('parameters')
            ->whereId($id)
            ->firstOrFail();

        $product->description = str_replace("<br />", "<p></p>", $product->description);
        $product->description_ro = str_replace("<br />", "<p></p>", $product->description_ro);
        $product->description_en = str_replace("<br />", "<p></p>", $product->description_en);

        $pdf = PDF::loadView('pdf.index', compact('product'));

        return $pdf->download('realist.pdf');
    }
}
