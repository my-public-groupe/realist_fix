<?php

namespace App\Http\Controllers;

//$root = config_path();
//require $root.'Amo/prepare.php'; #Здесь будут производиться подготовительные действия, объявления функций и т.д.
//require $root . 'Amo/auth.php'; #Здесь будет происходить авторизация пользователя
//require $root . 'Amo/account_current.php'; #Здесь мы будем получать информацию об аккаунте
//require $root . 'Amo/fields_info.php'; #Получим информацию о полях
//require $root . 'Amo/contacts_list.php';
//require $root . 'Amo/lead_add.php'; # There will be adding a Lead
//require $root . 'Amo/contact_add.php'; #Здесь будет происходить добавление контакта

// Добавление примечаний
//require_once($root.'Amo/notes.php');

class AmoCRMController
{
    private $login;
    private $subdomain;
    private $api_key;
    private $account;
    private $custom_fields;
    private $user;
    private $lead_name;
    private $lead_number;
    private $lead_id = 0;

    public static $scope_info = [
        'it' => 'IT, телекоммуникации, связь, электроника',
        'auto' => 'Автосервис, автобизнес',
        'bookkeeping' => 'Бухгалтерия, аудит',
        'restaurants' => 'Рестораны, фастфуд',
        'economy' => 'Экономика, финансы'
    ];

    public function __construct()
    {
        $this->login = env('AMO_LOGIN');
        $this->subdomain = env('AMO_SUBDOMAIN');
        $this->api_key = env('AMO_API_KEY');

        $this->user = [
            'USER_LOGIN' => $this->login, #Ваш логин (электронная почта)
            'USER_HASH' => $this->api_key #Хэш для доступа к API (смотрите в профиле пользователя)
        ];

        $this->lead_number = time();
        $this->lead_name = 'Solicitare site #' . $this->lead_number;
    }

    public function Auth()
    {
        #Массив с параметрами, которые нужно передать методом POST к API системы
        $user = array(
            'USER_LOGIN' => $this->login, #Ваш логин (электронная почта)
            'USER_HASH' => $this->api_key #Хэш для доступа к API (смотрите в профиле пользователя)
        );

        #Формируем ссылку для запроса
        $link = 'https://' . $this->subdomain . '.amocrm.ru/private/api/auth.php?type=json';
        $curl = curl_init(); #Сохраняем дескриптор сеанса cURL

        #Устанавливаем необходимые опции для сеанса cURL
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_USERAGENT, 'amoCRM-API-client/1.0');
        curl_setopt($curl, CURLOPT_URL, $link);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($user));
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_COOKIEFILE, config_path() . '/amo/cookie.txt'); #PHP>5.3.6 dirname(__FILE__) -> __DIR__
        curl_setopt($curl, CURLOPT_COOKIEJAR, config_path() . '/amo/cookie.txt'); #PHP>5.3.6 dirname(__FILE__) -> __DIR__
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);

        $out = curl_exec($curl); #Инициируем запрос к API и сохраняем ответ в переменную
        $code = curl_getinfo($curl, CURLINFO_HTTP_CODE); #Получим HTTP-код ответа сервера
        curl_close($curl); #Заверашем сеанс cURL

        $this->CheckCurlResponse($code);

        /**
         * Данные получаем в формате JSON, поэтому, для получения читаемых данных,
         * нам придётся перевести ответ в формат, понятный PHP
         */
        $Response = json_decode($out, true);
        $Response = $Response['response'];
        if (isset($Response['auth'])) { #Флаг авторизации доступен в свойстве "auth"
            return true;
        } else {
            return false;
        }
    }

    public function getAccount()
    {
        $link = 'https://' . $this->subdomain . '.amocrm.ru/private/api/v2/json/accounts/current'; #$subdomain уже объявляли выше
        $curl = curl_init(); #Сохраняем дескриптор сеанса cURL
#Устанавливаем необходимые опции для сеанса cURL
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_USERAGENT, 'amoCRM-API-client/1.0');
        curl_setopt($curl, CURLOPT_URL, $link);
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_COOKIEFILE, config_path() . '/amo/cookie.txt'); #PHP>5.3.6 dirname(__FILE__) -> __DIR__
        curl_setopt($curl, CURLOPT_COOKIEJAR, config_path() . '/amo/cookie.txt'); #PHP>5.3.6 dirname(__FILE__) -> __DIR__
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);

        $out = curl_exec($curl); #Инициируем запрос к API и сохраняем ответ в переменную
        $code = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        curl_close($curl);

        $this->CheckCurlResponse($code);

        /**
         * Данные получаем в формате JSON, поэтому, для получения читаемых данных,
         * нам придётся перевести ответ в формат, понятный PHP
         */
        $Response = json_decode($out, true);

        $this->account = $Response['response']['account'];

        return $this;
    }

    public function getCustomFields()
    {
        $need = array_flip(array('POSITION', 'PHONE', 'EMAIL'));
        if (isset($this->account['custom_fields'], $this->account['custom_fields']['contacts']))
            do {
                foreach ($this->account['custom_fields']['contacts'] as $field)
                    if (is_array($field) && isset($field['id'])) {
                        if (isset($field['code']) && isset($need[$field['code']]))
                            $fields[$field['code']] = (int)$field['id'];
                        #SCOPE - нестандартное поле, поэтому обрабатываем его отдельно
                        elseif (isset($field['name']) && $field['name'] == 'Сфера деятельности')
                            $fields['SCOPE'] = $field;

                        $diff = array_diff_key($need, $fields);
                        if (empty($diff))
                            break 2;
                    }
                if (isset($diff))
                    die('В amoCRM отсутствуют следующие поля' . ': ' . join(', ', $diff));
                else
                    die('Невозможно получить дополнительные поля');
            } while (false);
        else
            die('Невозможно получить дополнительные поля');
        $custom_fields = isset($fields) ? $fields : false;
        $this->custom_fields = $custom_fields;

        return $this->custom_fields;
    }

    public function getContactByPhone($phone)
    {
        $link = 'https://' . $this->subdomain . '.amocrm.ru/private/api/v2/json/contacts/list?query=' . $phone;
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_USERAGENT, 'amoCRM-API-client/1.0');
        curl_setopt($curl, CURLOPT_URL, $link);
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_COOKIEFILE, config_path() . '/amo/cookie.txt'); #PHP>5.3.6 dirname(__FILE__) -> __DIR__
        curl_setopt($curl, CURLOPT_COOKIEJAR, config_path() . '/amo/cookie.txt'); #PHP>5.3.6 dirname(__FILE__) -> __DIR__
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);

        $out = curl_exec($curl);
        $code = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        curl_close($curl);

        $this->CheckCurlResponse($code);

        $Response = json_decode($out, true);
        $Response = $Response['response']['contacts'];

        return $Response[0];
    }

    public function addLead($data = [])
    {
        $day_month = date('d') - 1;
        $hur = date('H');
        $min = date('i');
        $sec = date('s');
        $previous_week = strtotime("-7 day -" . $hur . " hours -" . $min . " minutes -" . $sec . " seconds");
        $date_create = $previous_week;
        $data_w = date(DATE_RFC2822, $previous_week);

        $link = 'https://' . $this->subdomain . '.amocrm.ru/private/api/v2/json/leads/list';
        $curl = curl_init(); #Сохраняем дескриптор сеанса cURL
#Устанавливаем необходимые опции для сеанса cURL
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_USERAGENT, 'amoCRM-API-client/1.0');
        curl_setopt($curl, CURLOPT_URL, $link);
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_COOKIEFILE, config_path() . '/amo/cookie.txt'); #PHP>5.3.6 dirname(__FILE__) -> __DIR__
        curl_setopt($curl, CURLOPT_COOKIEJAR, config_path() . '/amo/cookie.txt'); #PHP>5.3.6 dirname(__FILE__) -> __DIR__
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);

        curl_setopt($curl, CURLOPT_HTTPHEADER, array('IF-MODIFIED-SINCE: ' . $data_w));

        $out = curl_exec($curl); #Инициируем запрос к API и сохраняем ответ в переменную
        $code = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        curl_close($curl);


        $lead = array(
            'name' => $this->lead_name,
            'tags' => '',
            'price' => $data['price'],
            'status_id' => 31673785,
            'responsible_user_id' => 5822515,
            'custom_fields' => array(

                array(
                    'id' => 265951,
                    'values' => array(
                        array(
                            'value' => (isset($data['source']) ? $data['source'] : '')
                        )
                    )
                ),
                array(
                    'id' => 265953,
                    'values' => array(
                        array(
                            'value' => (isset($data['medium']) ? $data['medium'] : '')
                        )
                    )
                ),
                array(
                    'id' => 265955,
                    'values' => array(
                        array(
                            'value' => (isset($data['campaign']) ? $data['campaign'] : '')
                        )
                    )
                ),
                array(
                    'id' => 265957,
                    'values' => array(
                        array(
                            'value' => (isset($data['term']) ? $data['term'] : '')
                        )
                    )
                ),
                array(
                    'id' => 265959,
                    'values' => array(
                        array(
                            'value' => (isset($data['content']) ? $data['content'] : '')
                        )
                    )
                ),
                array(
                    'id' => 265969,
                    'values' => array(
                        array(
                            'value' => 520145
                        )
                    )
                )
            )
        );

        $set['request']['leads']['add'][] = $lead;
        $link = 'https://' . $this->subdomain . '.amocrm.ru/private/api/v2/json/leads/set';
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_USERAGENT, 'amoCRM-API-client/1.0');
        curl_setopt($curl, CURLOPT_URL, $link);
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($set));
        curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_COOKIEFILE, config_path() . '/amo/cookie.txt'); #PHP>5.3.6 dirname(__FILE__) -> __DIR__
        curl_setopt($curl, CURLOPT_COOKIEJAR, config_path() . '/amo/cookie.txt'); #PHP>5.3.6 dirname(__FILE__) -> __DIR__
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
        $out = curl_exec($curl);
        $code = curl_getinfo($curl, CURLINFO_HTTP_CODE);

        $this->CheckCurlResponse($code);

        $Response = json_decode($out, true);
        $Response = $Response['response']['leads']['add'];

        $output = 'Added leads IDs:' . PHP_EOL;
        foreach ($Response as $v)
            if (is_array($v)) {
                $output .= $v['id'] . PHP_EOL;
                $this->lead_id = $v['id'];
            }

        $this->addNote($this->lead_id, $data['link']);

        return $output;
    }

    public function addContact($phone, $data = [])
    {
        $this->getAccount()->getCustomFields();

        $phone = $this->getContactByPhone($phone);

        if (empty($contact['id'])) {
            $contact = array(
                'name' => 'Test Dima',
                'linked_leads_id' => 11909569,
                'custom_fields' => array(
                    array(
                        'id' => $this->custom_fields['EMAIL'],
                        'values' => array(
                            array(
                                'value' => 'test@asa.as',
                                'enum' => 'WORK'
                            )
                        )
                    ),
                    array(
                        'id' => 265973,
                        'values' => array(
                            array(
                                'value' => $c_lang,
                            )
                        )
                    )
                )
            );

            if (!empty($data['phone']))
                $contact['custom_fields'][] = array(
                    'id' => $this->custom_fields['PHONE'],
                    'values' => array(
                        array(
                            'value' => '23131321',
                            'enum' => 'WORK'
                        )
                    )
                );


            $set['request']['contacts']['add'][] = $contact;

            $link = 'https://' . $this->subdomain . '.amocrm.ru/private/api/v2/json/contacts/set';

            $curl = curl_init();
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($curl, CURLOPT_USERAGENT, 'amoCRM-API-client/1.0');
            curl_setopt($curl, CURLOPT_URL, $link);
            curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'POST');
            curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($set));
            curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
            curl_setopt($curl, CURLOPT_HEADER, false);
            curl_setopt($curl, CURLOPT_COOKIEFILE, config_path() . '/amo/cookie.txt'); #PHP>5.3.6 dirname(__FILE__) -> __DIR__
            curl_setopt($curl, CURLOPT_COOKIEJAR, config_path() . '/amo/cookie.txt'); #PHP>5.3.6 dirname(__FILE__) -> __DIR__
            curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
            curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);

            $out = curl_exec($curl);
            $code = curl_getinfo($curl, CURLINFO_HTTP_CODE);

            $this->CheckCurlResponse($code);

            $Response = json_decode($out, true);
            $Response = $Response['response']['contacts']['add'];

            //echo 'New order successfully added.';

            $output = 'Added contacts IDs:' . PHP_EOL;

            foreach ($Response as $v)
                if (is_array($v))
                    $output .= $v['id'] . PHP_EOL;
            return $output;
        } else {
            $leads = $contact['linked_leads_id'];
            $leads[] = $this->lead_id;
            $contact4update = array(
                'id' => $contact['id'],
                'linked_leads_id' => $leads,
                'last_modified' => time()
//  'name' => $data['name']
            );

            $set['request']['contacts']['update'][] = $contact4update;

            $link = 'https://' . $this->subdomain . '.amocrm.ru/private/api/v2/json/contacts/set';
            $curl = curl_init();
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($curl, CURLOPT_USERAGENT, 'amoCRM-API-client/1.0');
            curl_setopt($curl, CURLOPT_URL, $link);
            curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'POST');
            curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($set));
            curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
            curl_setopt($curl, CURLOPT_HEADER, false);
            curl_setopt($curl, CURLOPT_COOKIEFILE, config_path() . '/amo/cookie.txt'); #PHP>5.3.6 dirname(__FILE__) -> __DIR__
            curl_setopt($curl, CURLOPT_COOKIEJAR, config_path() . '/amo/cookie.txt'); #PHP>5.3.6 dirname(__FILE__) -> __DIR__
            curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
            curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);

            $out = curl_exec($curl);
            $code = curl_getinfo($curl, CURLINFO_HTTP_CODE);
            $Response = json_decode($out, true);
            $Response = $Response['response']['contacts']['update'];

            return $Response;
        }
    }

    public function addNote($id, $text)
    {
        $data = array(
            'add' =>
                array(
                    0 =>
                        array(
                            'element_id' => $id,
                            'element_type' => '2',
                            'text' => $text,
                            'note_type' => '4',
                        ),
                ),
        );

        $link = 'https://' . $this->subdomain . '.amocrm.ru/api/v2/notes';

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_USERAGENT, 'amoCRM-API-client/1.0');
        curl_setopt($curl, CURLOPT_URL, $link);
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($data));
        curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_COOKIEFILE, config_path() . '/amo/cookie.txt');
        curl_setopt($curl, CURLOPT_COOKIEJAR, config_path() . '/amo/cookie.txt');
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
        $out = curl_exec($curl);
        $code = curl_getinfo($curl, CURLINFO_HTTP_CODE);

        $code = (int)$code;
        $errors = array(
            301 => 'Moved permanently',
            400 => 'Bad request',
            401 => 'Unauthorized',
            403 => 'Forbidden',
            404 => 'Not found',
            500 => 'Internal server error',
            502 => 'Bad gateway',
            503 => 'Service unavailable'
        );
        try {
            if ($code != 200 && $code != 204)
                throw new Exception(isset($errors[$code]) ? $errors[$code] : 'Undescribed error', $code);
        } catch (Exception $E) {
            die('Ошибка: ' . $E->getMessage() . PHP_EOL . 'Код ошибки: ' . $E->getCode());
        }
    }

    public function CheckCurlResponse($code)
    {
        $code = (int)$code;
        $errors = array(
            301 => 'Moved permanently',
            400 => 'Bad request',
            401 => 'Unauthorized',
            403 => 'Forbidden',
            404 => 'Not found',
            500 => 'Internal server error',
            502 => 'Bad gateway',
            503 => 'Service unavailable'
        );
        try {
            #Если код ответа не равен 200 или 204 - возвращаем сообщение об ошибке
            if ($code != 200 && $code != 204)
                throw new \Exception(isset($errors[$code]) ? $errors[$code] : 'Undescribed error', $code);
        } catch (\Exception $E) {
            die('Ошибка: ' . $E->getMessage() . PHP_EOL . 'Код ошибки: ' . $E->getCode());
        }
    }
}