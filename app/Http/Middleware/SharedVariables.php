<?php

namespace App\Http\Middleware;

use App\Models\Categories;
use App\Models\Lists;
use App\Models\News;
use App\Models\Product;
use App\Models\Reviews;
use App\Models\TecDoc;
use Closure;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;

class SharedVariables
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $categories = Categories::enabled()
            ->orderBy('sort')
            ->with('photos')
            ->get();

        $news = News::enabled()
            ->with('photos')
            ->latest()
            ->limit(3)
            ->get();

        $partners = Lists::where('parent_id', '65')->orderBy('sort')->get();

        $rent_offers = Product::enabled()
            ->whereHas('categories', function ($query){
                $query->whereIn('id', [Categories::EXCLUSIVE]);
            })
            ->rent()
            ->where('top', true)
            ->where('price', '>', 0)
            ->oldest('sort')
            ->limit(12)
            ->get();

        $sale_offers = Product::enabled()
            ->whereHas('categories', function ($query){
                $query->whereIn('id', [Categories::EXCLUSIVE]);
            })
            ->sale()
            ->where('top', true)
            ->where('price', '>', 23)
            ->oldest('sort')
            ->limit(12)
            ->get();

        $services = Lists::getBySlug('services');

        $reviews = Reviews::getReviews();

        view()->share('categories', $categories);
        view()->share('news', $news);
        view()->share('partners', $partners);
        view()->share('rent_offers', $rent_offers);
        view()->share('sale_offers', $sale_offers);
        view()->share('services', $services);
        view()->share('reviews', $reviews);

        return $next($request);
    }
}
