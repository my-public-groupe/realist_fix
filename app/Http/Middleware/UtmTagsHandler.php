<?php

namespace App\Http\Middleware;

use App;
use Closure;
use Illuminate\Support\Facades\Route;
use Request;

class UtmTagsHandler
{
    private $utm_tags = [
        'utm_source',
        'utm_medium',
        'utm_campaign',
        'utm_term',
        'utm_content'
    ];

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if($request->hasAny($this->utm_tags)){
            session()->put('utm_tags', $request->only($this->utm_tags));
        }

        return $next($request);
    }
}