<?php


namespace App\Helpers;


class Helper
{
    public static function isAllowedIp()
    {
        $allowedIps = explode(',', env('TEAM_PAGE_ALLOWED_IP'));
        return in_array(request()->ip(), $allowedIps);
    }
}