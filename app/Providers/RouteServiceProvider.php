<?php

namespace App\Providers;

use Illuminate\Support\Facades\Route;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * This namespace is applied to your controller routes.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     */
    protected $namespace = 'App\Http\Controllers';

    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @return void
     */
    public function boot()
    {
        //

        parent::boot();
    }

    /**
     * Define the routes for the application.
     *
     * @return void
     */
    public function map()
    {
//        $this->mapApiRoutes();

        $this->mapAdminRoutes();

        $this->mapLangRoutes();

    }


    protected function mapAdminRoutes()
    {
        Route::middleware(['web'])
            ->namespace($this->namespace)
            ->group(base_path('routes/admin.php'));
    }

    /**
     * Define the "web" routes for the application.
     *
     * These routes all receive session state, CSRF protection, etc.
     *
     * @param string $prefix
     * @return void
     */
    protected function mapWebRoutes($prefix = '')
    {
        Route::middleware(['web', 'shared'])
            ->namespace($this->namespace)
            ->prefix($prefix)
            ->group(base_path('routes/web.php'));
    }

    /**
     * Define the routes for the application.
     *
     * @return void
     */
    protected function mapLangRoutes()
    {
        // localization prefix
        $segment = request()->segment(1);

        if (array_key_exists($segment, config('app.locales'))) {
            $this->mapWebRoutes($segment);
        } else {
            $segment = config('app.fallback_locale');
            $this->mapWebRoutes();
        }

        app()->setLocale($segment);
    }


    /**
     * Define the "api" routes for the application.
     *
     * These routes are typically stateless.
     *
     * @return void
     */
    protected function mapApiRoutes()
    {
        Route::prefix('api')
            ->middleware('api')
            ->namespace($this->namespace)
            ->group(base_path('routes/api.php'));
    }
}
