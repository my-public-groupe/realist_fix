<?php

namespace App\Models;

use App\Traits\CommonTrait;
use Illuminate\Database\Eloquent\Model;

class Region extends Model
{
    use CommonTrait;

    protected $guarded = [];

    public function group()
    {
        return $this->hasOne('App\Models\RegionGroup', 'id', 'group_id');
    }

    /**
     * @return array
     */
    public static function getSelectArray()
    {
        $region_groups = RegionGroup::with('regions')->get();

        $regions = [];

        foreach ($region_groups as $group) {
            $regions[$group->name] = $group->regions->pluck('name', 'id')->toArray();
        }

        return $regions;
    }
}
