<?php

namespace App\Models;

use App\Traits\CommonTrait;
use App\Traits\MetaTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class Product extends Model
{
    use CommonTrait, MetaTrait;

    protected $perPage = 9;

    protected $guarded = ['id'];

    protected $casts = [
        'params' => 'collection',
    ];

    protected $with = [
        'photos',
        'region',
        'agent',
        'parameters.values',
    ];

    protected $appends = [
        'type_text',
        'url',
        'price_text',
        'area',
        'old_price_text',
        'price_square_text'
    ];

    const TYPE_SALE = 0;
    const TYPE_RENT = 1;

    const PHOTO_LABEL_BEST_CHOICE = 1;
    const PHOTO_LABEL_SPECIAL_OFFER = 2;
    const PHOTO_LABEL_HOT_PRICE = 3;
    const PHOTO_LABEL_SOLD = 4;
    const PHOTO_LABEL_RENTED = 5;
    const PHOTO_LABEL_RESERVED = 6;
    const PHOTO_LABEL_PROMO = 7;

    protected static function boot()
    {
        parent::boot();

        static::updated(function ($product) {
            $fields = request()->all();

            Log::channel('objects_log')->debug("=== UPDATED START ===");
            Log::channel('objects_log')->debug("User id: " . Auth::id());
            Log::channel('objects_log')->debug(print_r($_SERVER['HTTP_USER_AGENT'], true));
            if(isset($_SERVER['HTTP_REFERER'])) {
                Log::channel('objects_log')->debug(print_r($_SERVER['HTTP_REFERER'], true));
            }
            Log::channel('objects_log')->debug(print_r($_SERVER['REQUEST_URI'], true));
            Log::channel('objects_log')->debug(print_r($_SERVER['REMOTE_ADDR'], true));
            if(isset($fields['lead'])) {
                Log::channel('objects_log')->debug('Amo ID: ' . $fields['id']);
            }
            Log::channel('objects_log')->debug(print_r($fields, true));
            Log::channel('objects_log')->debug("=== UPDATED END ===");
        });

        static::deleted(function ($product) {
            Log::channel('objects_log')->debug("=== DELETED START ===");
            Log::channel('objects_log')->debug("User id: " . Auth::id());
            Log::channel('objects_log')->debug(print_r($_SERVER['HTTP_USER_AGENT'], true));
            if(isset($_SERVER['HTTP_REFERER'])) {
                Log::channel('objects_log')->debug(print_r($_SERVER['HTTP_REFERER'], true));
            }
            Log::channel('objects_log')->debug(print_r($_SERVER['REQUEST_URI'], true));
            Log::channel('objects_log')->debug(print_r($_SERVER['REMOTE_ADDR'], true));
            Log::channel('objects_log')->debug(print_r($product->toArray(), true));
            Log::channel('objects_log')->debug("=== DELETED END ===");
        });
    }

    public function categories()
    {
        return $this->belongsToMany('App\Models\Categories');
    }

    public function region()
    {
        return $this->hasOne('App\Models\Region', 'id', 'region_id');
    }

    public function agent()
    {
        return $this->hasOne('App\Models\Agent', 'id', 'agent_id');
    }

    public function parameters()
    {
        return $this->belongsToMany(Parameter::class, 'parameters_products', 'product_id', 'parameter_id')
            ->orderBy('sort')
            ->withPivot('value', 'value_ro', 'value_en', 'value_id');
    }

    public function getTypeTextAttribute()
    {
        $product_types = config('custom.product_types');

        return $product_types[$this->type] ?? '';
    }

    public function scopeRent($query)
    {
        return $query->where('type', self::TYPE_RENT);
    }

    public function scopeSale($query)
    {
        return $query->where('type', self::TYPE_SALE);
    }

    public function getUrlAttribute()
    {
        return route('product', $this->slug);
    }

    public function getPriceTextAttribute()
    {
        if ($this->price == 0) return '';

        return number_format($this->price, 0, '.', ' ') . $this->valute_symbol;
    }

    public function getOldPriceTextAttribute()
    {
        if ($this->old_price == 0) return '';

        return number_format($this->old_price, 0, '.', ' ') . $this->valute_symbol;
    }

    public function getPriceSquareTextAttribute()
    {
        if (empty($this->price_square)) return '';

        return number_format($this->price_square, 0, '.', ' ') . $this->valute_symbol . '/' . trans('common.meter_short') . '<sup>2</sup>';
    }

    public function scopeFilters($query, $filters)
    {
        if (isset($filters['regions']) && count($filters['regions']) > 0) {
            $out = $filters['regions'];
            if(in_array(122, $filters['regions'])){
                $r = Region::where('group_id', 4)->pluck('id')->toArray();
                $out = array_merge($out, $r);
            }
            if (in_array(6, $filters['regions'])){
                $r = Region::where('group_id', 5)->pluck('id')->toArray();
                $out = array_merge($out, $r);
            }
            if (in_array(7, $filters['regions'])){
                $r = Region::where('group_id', 6)->pluck('id')->toArray();
                $out = array_merge($out, $r);
            }

            $query->whereIn('region_id', $out);
        }

        if (isset($filters['price'])) {
            $from = ((integer) $filters['price'][0]) ?? 0;
            $to = ((integer) $filters['price'][1]) ?? 0;
            if ($from != 0) $query->where('price', '>=', $from);
            if ($to != 0) $query->where('price', '<=', $to);
        }

        if (isset($filters['params']) && count($filters['params']) > 0) {
            $values = $filters['params'];
            $paramsValues = ParameterValue::whereIn('id', $values)
                ->get();

            $params = [];
            foreach ($paramsValues as $item) {
                $params[$item->parameter_id][] = $item->id;
            }

            $productIds = [];
            foreach ($params as $values) {
                $ids = DB::table('parameters_products')
                    ->select('products.id')
                    ->whereIn('value_id', $values)
                    ->select('product_id')
                    ->get(['product_id'])
                    ->pluck('product_id')
                    ->toArray();

                if (count($productIds) == 0) {
                    $productIds = $ids;
                } else {
                    // пересечение отфильтрованных ID
                    $productIds = array_intersect($productIds, $ids);
                }
            }

            $query->whereIn('id', $productIds);
        }

        if (isset($filters['intervals'])) {
            foreach ($filters['intervals'] as $item) {

                $parameter_id = key($item);
                $from = ((integer) $item[$parameter_id][0]) ?? 0;
                $to = ((integer) $item[$parameter_id][1]) ?? 0;

                $q = DB::table('parameters_products');

                if ($from != 0) $q->whereRaw("REPLACE(REPLACE(REPLACE(REPLACE(value, UNHEX('C2A0'), ''), UNHEX('A0C2'), ''), ' ', ''), ',', '.') >= ?", $from);
                if ($to != 0) $q->whereRaw("REPLACE(REPLACE(REPLACE(REPLACE(value, UNHEX('C2A0'), ''), UNHEX('A0C2'), ''), ' ', ''), ',', '.') <= ?", $to);

                $products_ids = $q->distinct()
                    ->where('parameter_id', $parameter_id)
                    ->select('product_id')
                    ->get(['product_id'])
                    ->pluck('product_id')
                    ->toArray();

                $query->whereIn('id', $products_ids);
            }
        }

        return $query;
    }

    /**
     * @param int $limit
     * @return
     */
    public function getRelated($limit = 4)
    {
        $price_min = $this->price - ((10 / 100) * $this->price);
        $price_max = $this->price + ((10 / 100) * $this->price);
        $product_categories = $this->categories->pluck('id')->toArray();

        return Product::enabled()
            ->where('type', $this->type)
            ->whereHas('categories', function ($query) use ($product_categories) {
                $query->whereIn('id', $product_categories);
            })
            ->where('region_id', $this->region_id)
            ->where('id', '!=', $this->id)
            ->whereBetween('price', [$price_min, $price_max])
            ->whereNotIn('photo_label', [
                Product::PHOTO_LABEL_RESERVED,
                Product::PHOTO_LABEL_SOLD,
            ])
            ->orderBy('top', 'desc')
            ->limit($limit)
            ->get();
    }

    /**
     * Получаем площадь из параметров объекта
     */
    public function getAreaAttribute()
    {
        // квадратные метры
        $param = $this->parameters->filter(function($item) {
            return $item->id === Parameter::ID_AREA;
        })->first();
        if (isset($param->pivot->value) && $param->pivot->value != '') {
            return $param->pivot->value . ' ' . trans('common.meter_short') . '<sup>2</sup>';
        }

        // сотки
        $param = $this->parameters->filter(function($item) {
            return $item->id === Parameter::ID_AREA_AR;
        })->first();
        if (isset($param->pivot->value) && $param->pivot->value != '') {
            return $param->pivot->value . ' ' . trans('vue.ar');
        }

        // гектары
        $param = $this->parameters->filter(function($item) {
            return $item->id === Parameter::ID_AREA_HA;
        })->first();
        if (isset($param->pivot->value) && $param->pivot->value != '') {
            return $param->pivot->value . ' ' . trans('vue.ha');
        }

        return '';
    }

    public function getRoomAttribute()
    {
        $param = $this->parameters->filter(function($item) {
            return $item->id === Parameter::ID_ROOM || $item->id === Parameter::ID_CABINET;
        })->first();

        if (isset($param->pivot->value_id) && $param->pivot->value_id != '') {
            return $param->values->find($param->pivot->value_id)->value;
        }

        if (isset($param->pivot->value) && $param->pivot->value != '') {
            return $param->pivot->value;
        }

        return '';
    }

    public function getRoomValueAttribute()
    {
        $param = $this->parameters->filter(function($item) {
            return $item->id === Parameter::ID_ROOM || $item->id === Parameter::ID_CABINET;
        })->first();

        if (isset($param->pivot->value_id) && $param->pivot->value_id != '') {
            return $param->pivot->value_id;
        }

        return '';
    }

    public function getRoomQuantityText()
    {
        $rooms = $this->room;

        if(intval($rooms) == 1){
            return trans('common.rooms_quantity1');
        }
        if(intval($rooms) > 1){
            return trans('common.rooms_quantity2');
        }
        if(intval($rooms) > 4){
            return trans('common.rooms_quantity3');
        }

        return '';
    }

    public function getBathroomAttribute()
    {
        $param = $this->parameters->filter(function($item) {
            return $item->id === Parameter::ID_BATHROOM;
        })->first();

        if (isset($param->pivot->value_id) && $param->pivot->value_id != '') {
            return $param->values->find($param->pivot->value_id)->value;
        }

        if (isset($param->pivot->value) && $param->pivot->value != '') {
            return $param->pivot->value;
        }

        return '';
    }

    public function getBathroomQuantityText()
    {
        $bathrooms = $this->bathroom;

        if(intval($bathrooms) == 1){
            return trans('common.bathrooms_quantity1');
        }
        if(intval($bathrooms) >= 2){
            return trans('common.bathrooms_quantity2');
        }

        return '';
    }

    public function getBuildingTypeAttribute()
    {
        $param = $this->parameters->filter(function($item) {
            return $item->id === 1;
        })->first();

        if (isset($param->pivot->value_id) && $param->pivot->value_id != '') {
            return $param->pivot->value_id;
        }
    }

    public function getFloorValueAttribute()
    {
        $param = $this->parameters->filter(function($item) {
            return $item->id === Parameter::ID_FLOOR;
        })->first();

        if (isset($param->pivot->value_id) && $param->pivot->value_id != '') {
            return $param->pivot->value_id;
        }

        return '';
    }

    public function getTotalFloorsValueAttribute()
    {
        $param = $this->parameters->filter(function($item) {
            return $item->id === Parameter::ID_TOTAL_FLOORS;
        })->first();

        if (isset($param->pivot->value_id) && $param->pivot->value_id != '') {
            return $param->pivot->value_id;
        }

        return '';
    }

    public function scopeSearchByKeyword($query, $keyword)
    {
        if ($keyword != '') {

            $searchValues = preg_split('/\s+/', $keyword, -1, PREG_SPLIT_NO_EMPTY);

            foreach ($searchValues as $keyword) {
                $query->where(function ($query) use ($keyword) {
                    $query->where('name', 'LIKE', '%' . $keyword . '%')
                        ->orWhere('name_ro', 'LIKE', '%' . $keyword . '%')
                        ->orWhere('name_en', 'LIKE', '%' . $keyword . '%')
                        ->orWhere('description', 'LIKE', '%' . $keyword . '%')
                        ->orWhere('description_ro', 'LIKE', '%' . $keyword . '%')
                        ->orWhere('description_en', 'LIKE', '%' . $keyword . '%')
                        ->orWhere('id', 'LIKE', '%' . $keyword . '%');
                });
            }
        }
        return $query;
    }

    public function scopeOrderBySort($query)
    {
        $query->select(['*', DB::raw('IF(`sort` != 0, `sort`, 1000000) `sortOrder`')]);
        $query->orderBy('sortOrder');

        return $query;
    }

    public function getNearByPlaces()
    {
        return array_filter($this->params['nearby_places'] ?? []);
    }

    public function getFeatures()
    {
        return array_filter($this->params['features'] ?? []);
    }

    public function getValuteAttribute($value)
    {
        return $value == "" ? "EUR" : $value;
    }

    public function getValuteSymbolAttribute()
    {
        return ($this->valute == "EUR" || "") ? "€" : "$";
    }

    public function getTypeValues()
    {
        $types = $this->parameters->where('id', Parameter::ID_TYPE);
        $values = [];

        if($types->isNotEmpty()){
            foreach ($types as $type){
                $values[] = $type->pivot->value_id;
            }
        }

        return $values;
    }

    public function getTimestamp($path)
    {
        try {
            $filemtime = filemtime(public_path("uploaded/thumbs/$path"));
        } catch (\Exception $e) {
            $filemtime = date('YmdHis');
        }

        return $filemtime;
    }

    public function delete()
    {
        // удаляем фото
        foreach ($this->photos as $photo) {
            $photo->delete();
        }

        // удаляем связи
        $this->photos()->delete();

        parent::delete();
    }
}
