<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;
use Intervention\Image\Facades\Image;
use function foo\func;

class Photos extends Model
{
    public $timestamps  =   false;

    protected static function boot()
    {
        parent::boot();

        static::updated(function ($photo){
            Log::channel('objects_log')->debug("=== PHOTO UPDATED START ===");
            Log::channel('objects_log')->debug(print_r($photo->toArray(), true));
            Log::channel('objects_log')->debug("User id: " . Auth::id());
            Log::channel('objects_log')->debug(print_r($_SERVER['HTTP_USER_AGENT'], true));
            if(isset($_SERVER['HTTP_REFERER'])) {
                Log::channel('objects_log')->debug(print_r($_SERVER['HTTP_REFERER'], true));
            }
            Log::channel('objects_log')->debug(print_r($_SERVER['REQUEST_URI'], true));
            Log::channel('objects_log')->debug(print_r($_SERVER['REMOTE_ADDR'], true));
            Log::channel('objects_log')->debug("=== PHOTO UPDATED END ===");
        });

        static::deleted(function ($photo){
            Log::channel('objects_log')->debug("=== PHOTO DELETED START ===");
            Log::channel('objects_log')->debug(print_r($photo->toArray(), true));
            Log::channel('objects_log')->debug("User id: " . Auth::id());
            Log::channel('objects_log')->debug(print_r($_SERVER['HTTP_USER_AGENT'], true));
            if(isset($_SERVER['HTTP_REFERER'])) {
                Log::channel('objects_log')->debug(print_r($_SERVER['HTTP_REFERER'], true));
            }
            Log::channel('objects_log')->debug(print_r($_SERVER['REQUEST_URI'], true));
            Log::channel('objects_log')->debug(print_r($_SERVER['REMOTE_ADDR'], true));
            Log::channel('objects_log')->debug("=== PHOTO DELETED END ===");
        });
    }

    public static function addWatermark($inputFile, $outputFile = '')
    {
        //check if file exists
        if (!File::exists($inputFile)) {
            return false;
        }

        if ($outputFile == '') {
            $outputFile = $inputFile;
        }

        //get from custom configuration file
        $watermarkFile = config('photos.watermark.path');

        // open an image file
        $img = Image::make($inputFile);

        //init watermark source
        $watermark = Image::make($watermarkFile);

        //if watermark width is bigger than half of image width
        if ($watermark->width() > $img->width() / 2) {
            // resize only the width of the watermark
            $watermark->resize($img->width() / 2, null, function ($constraint) {
                $constraint->aspectRatio();  //proportional
            });
        }

        // insert a watermark
        $img->insert($watermark, config('photos.watermark.position'));

        // finally we save the image as a new file
        $img->save($outputFile, 80, 'jpg');
    }

    public function get()
    {
        return '/uploaded/' . $this->source;
    }

    public function delete()
    {
        $filesLocation = base_path() . DIRECTORY_SEPARATOR . config('photos.images_dir');
        $filePath = $filesLocation . $this->source;
        $thumbPath = $filesLocation . DIRECTORY_SEPARATOR . 'thumbs' . DIRECTORY_SEPARATOR . $this->source;

        if (File::exists($filePath)) {
            File::delete($filePath);
        }

        if (File::exists($thumbPath)) {
            File::delete($thumbPath);
        }

        parent::delete();
    }
}
