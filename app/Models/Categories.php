<?php

namespace App\Models;

use App\Traits\CommonTrait;
use App\Traits\MetaTrait;
use Illuminate\Database\Eloquent\Model;

class Categories extends Model
{
    use CommonTrait, MetaTrait;

    const EXCLUSIVE = 9;

    public function children()
    {
	    return $this->belongsToMany('App\Models\Categories', 'categories_xref', 'parent_id', 'child_id');
    }

    public function products()
    {
        return $this->hasMany(Product::class, 'category_id');
    }
    
    public function parents() 
    {
	    return $this->belongsToMany('App\Models\Categories', 'categories_xref',  'child_id', 'parent_id');
    }

    public function getFullPathAttribute()
    {
        return (isset($this->parents{0}) ? $this->parents{0}->name . ' > ' : '' )  . $this->name;
    }

    public function parameters()
    {
        return $this->belongsToMany(Parameter::class, 'parameters_categories', 'category_id', 'parameter_id')->orderBy('sort');
    }

    public function parametersWithCount($category_id, $productsIds = [])
    {
        $query = $this->parameters()->withCount(['products' => function ($query) use ($category_id, $productsIds) {
            $query->where('category_id', $category_id);
            if (count($productsIds) > 0) {
                $query->whereIn('products.id', $productsIds);
            }
        }]);

        if (count($productsIds) > 0) {
            $query->whereHas('products', function($query) use ($productsIds, $category_id) {
                $query->where('category_id', $category_id);
                $query->whereIn('products.id', $productsIds);
            });
        }

        return $query;
    }
}
