<?php

namespace App\Models;

use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'open_password', 'phone', 'address',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token', 'open_password',
    ];

    protected $casts = [
        'contacts' => 'collection',
        'entity' => 'collection',
        'params' => 'collection',
    ];

    public function isManager()
    {
        // this looks if is manager in rights column in your users table
        if ($this->rights == 2) return true;
        return false;
    }

    public function isAdmin()
    {
        // this looks if is admin in rights column in your users table
        if ($this->rights == 1) return true;
        return false;
    }

    public function city()
    {
        return $this->hasOne('App\Models\City', 'id', 'city_id');
    }

    /**
     * Adding param to field
     *
     * @param $key string
     * @param $value mixed
     *
     * @return void
     */
    public function addParam($key, $value)
    {
        $params = $this->params;
        $params[$key] = $value;
        $this->params = $params;
    }

    /**
     * Adding multiple params to field
     *
     * @param $arr array
     *
     * @return void
     */
    public function addParams($arr)
    {
        $params = $this->params;

        foreach ($arr as $key => $value) {
            $params[$key] = $value;
        }

        $this->params = $params;
    }

    /**
     * Adding contact to contacts JSON field
     *
     * @param $key string
     * @param $value mixed
     *
     * @return void
     */
    public function addContact($key, $value)
    {
        $contacts = $this->contacts;
        $contacts[$key] = $value;
        $this->contacts = $contacts;
    }

    /**
     * Adding multiple contact to field
     *
     * @param $arr array
     *
     * @return void
     */
    public function addContacts($arr)
    {
        $contacts = $this->contacts;

        foreach ($arr as $key => $value) {
            $contacts[$key] = $value;
        }

        $this->contacts = $contacts;
    }

    /**
     * Adding entity param to JSON field
     *
     * @param $key string
     * @param $value mixed
     *
     * @return void
     */
    public function addEntity($key, $value)
    {
        $entity = $this->entity;
        $entity[$key] = $value;
        $this->entity = $entity;
    }

    /**
     * Adding multiple params to field
     *
     * @param $arr array
     *
     * @return void
     */
    public function addEntities($arr)
    {
        $entity = $this->entity;

        foreach ($arr as $key => $value) {
            $entity[$key] = $value;
        }

        $this->entity = $entity;
    }


}
