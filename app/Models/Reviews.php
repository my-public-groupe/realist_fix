<?php

namespace App\Models;

use App\Traits\CommonTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Str;

class Reviews extends Model
{
    use CommonTrait;

    protected $guarded = [];
    protected $dates = ['created_at', 'updated_at'];
    //protected $dateFormat = 'd.m.Y';

    public function scopeSort($query)
    {
        return $query->orderBy('created_at', 'desc');
    }

    public function getDescriptionShort()
    {
        return Str::limit($this->description);
    }

    public function hasPhotos()
    {
        $file_exists = false;

        try {
            $file_exists = File::exists(public_path("uploaded/{$this->photos[0]->source}"));
        } catch (\Exception $e) {}

        return count($this->photos) > 0 && $file_exists;
    }

    public function getPhoto()
    {
        if(isset($this->photos[0])){
            return "uploaded/{$this->photos[0]->source}";
        }

        return null;
    }

    public static function getReviews()
    {
        $out = [];
        $reviews = self::enabled()
            ->oldest('sort')
            ->latest('created_at')
            ->get();

        if($reviews->isNotEmpty()){
            foreach ($reviews as $r){
                $out[] = [
                    'name' => $r->name,
                    'text' => $r->description,
                    'date' => $r->created_at->format('d.m.Y')
                ];
            }
        }

        return $out;
    }
}
