<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Lang;

class ParameterValue extends Model
{
    public $timestamps = false;

    public function products()
    {
        return $this->belongsToMany(Product::class, 'parameters_products', 'value_id', 'product_id');
    }

    public function getValueAttribute()
    {
        $locale = Lang::locale();
        if ($locale == config('app.base_locale')) {
            return $this->attributes['value'];
        } else {
            return $this->attributes['value_' . $locale];
        }
    }
}
