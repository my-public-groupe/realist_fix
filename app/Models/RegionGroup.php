<?php

namespace App\Models;

use App\Traits\CommonTrait;
use Illuminate\Database\Eloquent\Model;

class RegionGroup extends Model
{
    use CommonTrait;

    protected $guarded = [];

    public function regions()
    {
        return $this->hasMany(Region::class, 'group_id');
    }
}
