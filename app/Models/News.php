<?php

namespace App\Models;

use App\Traits\CommonTrait;
use App\Traits\MetaTrait;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Date;

class News extends Model
{
    use CommonTrait, MetaTrait;

    protected $guarded = ['id'];

    public function setTopAttribute($value)
    {
        if (!isset($value)) $value = false;

        $this->attributes['top'] = $value;
    }

    public function getCreatedDay()
    {
        return Carbon::parse($this->created_at)->format('d');
    }

    public function setCreatedAtAttribute($value)
    {
        $this->attributes['created_at'] = Carbon::createFromFormat('d-m-Y', $value);
    }

    public function getDateLocalizedMonth()
    {
        $locale = App::getLocale();
        $months_short = config('custom.months_short');
        $created_month = Carbon::parse($this->created_at)->format('n');
        return $months_short[$locale][$created_month-1];
    }


    public function getDateLocalizedDay()
    {
        $created_day = Carbon::parse($this->created_at)->format('d');
        return $created_day;
    }
}
