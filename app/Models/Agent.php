<?php

namespace App\Models;

use App\Traits\CommonTrait;
use Illuminate\Database\Eloquent\Model;

class Agent extends Model
{
    use CommonTrait;

    protected $casts = [
        'params' => 'collection',
    ];

    public function products()
    {
        return $this->hasMany(Product::class)->orderBySort()->latest();
    }

    public function getNameAttribute()
    {
        return  $this->attributes['name'];
    }

    public function getShortNameAttribute()
    {
        $names = explode(' ', $this->attributes['name']);
        if (isset($names[0])) {
            return $names[0];
        }
        return '';
    }

    public function hasSocials()
    {
        if(!empty($this->params['viber']) ||
            !empty($this->params['messenger']) ||
            !empty($this->params['telegram']) ||
            !empty($this->params['whatsapp'])){

            return true;
        }

        return false;
    }

    public function getViber()
    {
        if(!empty($this->params['viber'])){
            return str_replace('+', '', $this->params['viber']);
        }

        return '';
    }

    public function getPhoneFormatted()
    {
        return strlen($this->phone) == 12 ? preg_replace('/(\d{3})(\d{2})(\d{3})(\d{3})/', '$1 $2 $3 $4', $this->phone) : $this->phone;
    }
}
