<?php

namespace App\Models;

use App\Traits\CommonTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Lang;

class Parameter extends Model
{
    use CommonTrait;

    public $timestamps = false;

    protected $with = ['values'];

    protected $casts = [
        'params' => 'collection',
        'type' => 'integer',
    ];

    protected $appends = [
        'active',
    ];

    const TYPE_INPUT = 0;
    const TYPE_SELECT = 1;
    const TYPE_STRING = 2;

    const ID_AREA = 4;
    const ID_AREA_AR = 16;
    const ID_AREA_HA = 17;
    const ID_BATHROOM = 9;
    const ID_ROOM = 12;
    const ID_CABINET = 2;
    const ID_FLOOR = 5;
    const ID_TOTAL_FLOORS = 6;
    const ID_BALCONY = 8;
    const ID_TYPE = 14;

    public function products()
    {
        return $this->belongsToMany(Product::class, 'parameters_products', 'parameter_id', 'product_id');
    }

    public function values()
    {
        return $this->hasMany('App\Models\ParameterValue', 'parameter_id');
    }

    public function noHotels()
    {
        return $this->values->where('id', '!=', 96);
    }

    public function valuesWithCount($category_id, $productsIds = [])
    {
        $query = $this->values()->withCount(['products' => function ($query) use ($category_id, $productsIds) {
            $query->where('category_id', $category_id);
            if (count($productsIds) > 0) {
                $query->whereIn('products.id', $productsIds);
            }
        }])->orderBy('products_count', 'desc');

        if (count($productsIds) > 0) {
            $query->whereHas('products', function($query) use ($productsIds, $category_id) {
                $query->where('category_id', $category_id);
            });
        }


        return $query;
    }

    public function getSelectedValue()
    {
        $locale = Lang::locale();
        if ($locale == config('app.base_locale')) {
            $field = 'value';
        } else {
            $field = 'value_' . $locale;
        }

        if ($this->type == Parameter::TYPE_INPUT) {
            return $this->pivot->value;
        }

        if ($this->type == Parameter::TYPE_SELECT) {
            $values = $this->values->pluck($field, 'id')->toArray();
            if (isset($values[$this->pivot->value_id])) {
                return $values[$this->pivot->value_id];
            }
        }

        if ($this->type == Parameter::TYPE_STRING) {
            return $this->pivot->$field;
        }

        return '';
    }

    public function getActiveAttribute()
    {
        return $this->params['active'] ?? false;
    }
}
