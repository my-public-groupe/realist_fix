<?php

namespace App\Models;

use App\Traits\CommonTrait;
use App\Traits\MetaTrait;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

class Lists extends Model
{
    use CommonTrait, MetaTrait;

    protected $with = [
        'children',
        'photos',
    ];

    protected $casts = [
        'params' => 'collection',
    ];

    public $timestamps  =   false;

    public function children()
    {
        return $this->hasMany('App\Models\Lists', 'parent_id')->orderBy('sort');
    }

    public function parent()
    {
        return $this->hasOne('App\Models\Lists', 'id', 'parent_id');
    }

    public function getValueAttribute()
    {
        return $this->attributes['description_short'];
    }

    public function getParamsAttribute()
    {
        return (object) json_decode($this->attributes['params'], true);
    }

    public function getParamsObj()
    {
        $params = json_decode($this->attributes['params'], true);
        $data = [];
        foreach ($params as $param) {
            if ( ! isset($param['key'])) continue;
            $data[$param['key']] = $param['value'];
        }
        return (object) $data;
    }

    // search by slug
    public static function getBySlug($slug, $withParent = false)
    {
        $list = self::where('slug', $slug)->with(['children' => function($query){
            $query->with('photos')->enabled();
        }])->first();

        if ($withParent) {
            return $list;
        }

        if ( ! isset($list) ) {
            return new Collection();
        }

        return $list->children;
    }

    public function get($slug)
    {
        return $this->children->filter(function($item) use ($slug) {
            return ($item->slug == $slug && $item->enabled == true);
        })->first();
    }

    public function getBtnLink()
    {
        $locale = app()->getLocale();

        return $this->params->{'btn_link_' . $locale} ?? '';
    }

    public function getBtnText()
    {
        $locale = app()->getLocale();

        return $this->params->{'btn_text_' . $locale} ?? '';
    }

    public function getMainPhoto()
    {
        if(app()->getLocale() == 'ru'){
            $index = 0;
        }elseif (app()->getLocale() == 'ro'){
            $index = 1;
        }elseif (app()->getLocale() == 'en'){
            $index = 2;
        }

        return $this->mainPhoto($index ?? 0);
    }
}
