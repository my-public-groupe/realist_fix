<?php

namespace App\Exports;

use App\Models\Product;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class ObjectsAgentsExport implements FromView
{
    protected $type;
    protected $category;
    protected $region_group;
    protected $agent_id;

    public function __construct($type, $category, $region_group, $agent_id)
    {
        $this->type = $type;
        $this->category = $category;
        $this->region_group = $region_group;
        $this->agent_id = $agent_id;
    }

    public function collection()
    {
        $products = Product::query();

        if (!empty($this->type)) {
            $products->whereIn('type', $this->type);
        }

        if (!empty($this->category)) {
            $products->whereHas('categories', function ($query) {
                $query->whereIn('id', $this->category);
            });
        }

        if(!empty($this->region_group)){
            $products->whereHas('region', function ($query) {
                $query->whereHas('group', function ($query2){
                   $query2->whereIn('id', $this->region_group);
                });
            });
        }

        if(!empty($this->agent_id)){
            $products->whereHas('agent', function ($query){
               $query->where('id', $this->agent_id);
            });
        }

        return $products->enabled()->get();
    }

    public function view(): View
    {
        return view('excel.objects_agents', [
            'data' => $this->collection()
        ]);
    }
}
