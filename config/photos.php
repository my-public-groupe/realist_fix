<?php
return [
    'images_dir' => 'public/uploaded/',
    'images_url' => '/uploaded/',
    'width' => 1920,
    'height' => 1920,
    'agents_width' => 640,
    'agents_height' => 640,
    'thumbs' => [
        [
            'path' => 'thumbs',
            'width' => 640,
            'height' => 640,
            'width_webp' => 640,
            'height_webp' => 640
        ],
    ],

    'watermark' => [
        'path' => public_path('img/watermark@3x.png'),
        'position' => 'center',
    ],

    'file_type_array' => [
        "pdf" => "Adobe Acrobat",
        "docx" => "Microsoft Word",
        "xls" => "Microsoft Excel",
        "zip" => 'Archive',
        "gif" => 'GIF Image',
        "jpg" => 'JPEG Image',
        "jpeg" => 'JPEG Image',
        "png" => 'PNG Image',
        "ppt" => 'Microsoft PowerPoint',
        "pptx" => 'Microsoft PowerPoint',
    ],

    'file_icon_array' => [
        "pdf" => "fa-file-pdf-o",
        "docx" => "fa-file-word-o",
        "xls" => "fa-file-excel-o",
        "zip" => 'fa-file-archive-o',
        "gif" => 'fa-file-image-o',
        "jpg" => 'fa-file-image-o',
        "jpeg" => 'fa-file-image-o',
        "png" => 'fa-file-image-o',
        "ppt" => 'fa-file-powerpoint-o',
        "pptx" => 'fa-file-powerpoint-o',
    ],
];
