<?php

return [

    'admin_email' => explode(',', env('ADMIN_EMAIL')),

    'simpals_999_api_key' => env('SIMPALS_999_API_KEY'),

    'parameter_types' => [
        0 => 'Число',
        1 => 'Список',
        2 => 'Строка',
    ],

    'product_types' => [
        0 => 'Продажа',
        1 => 'Аренда',
    ],

    'months_short' => [
        'ro' => [
            'Ianuarie',
            'Februarie',
            'Martie',
            'Aprilie',
            'Mai',
            'Iunie',
            'Iulie',
            'August',
            'Septembrie',
            'Octombrie',
            'Noiembrie',
            'Decembrie'
        ],
        'ru' => [
            'Января',
            'Февраля',
            'Марта',
            'Апреля',
            'Мая',
            'Июня',
            'Июля',
            'Августа',
            'Сентября',
            'Октября',
            'Ноября',
            'Декабря'
        ],
        'en' => [
            'January',
            'February',
            'March',
            'April',
            'May',
            'June',
            'July',
            'August',
            'September',
            'October',
            'November',
            'December'
        ]
    ],

    'object_icons' => [
        'area' => 'ico-area',
        'room' => 'ico-room',
        'bathroom' => 'ico-bathroom',
        'type' => 'ico-apartament',
        'place' => 'ico-place'
    ],

    'header_locales' => [
        'ru' => 'Ru',
        'ro' => 'Ro',
        'en' => 'En'
    ],

    'sorting' => [
        'default' => 'default',
        'price_asc' => 'price_asc',
        'price_desc' => 'price_desc'
    ],

    'nearby_icons' => [
        99 => 'pl-d-sad',
        100 => 'pl-school',
        101 => 'pl-univer',
        102 => 'pl-hospital',
        103 => 'pl-bank',
        104 => 'pl-kids-place',
        105 => 'pl-park',
        106 => 'pl-biblio',
        107 => 'pl-pharmacy',
        108 => 'pl-market',
        109 => 'pl-veterinar',
        110 => 'pl-police',
        111 => 'pl-ambulance'
    ]
];
