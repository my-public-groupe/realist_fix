<?php

/*
 * This file is part of Laravel AmoCrm.
 *
 * (c) dotzero <mail@dotzero.ru>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

return [

    /*
    |--------------------------------------------------------------------------
    | Авторизация в системе amoCRM
    |--------------------------------------------------------------------------
    |
    | Эти параметры необходимы для авторизации в системе amoCRM.
    | - Поддомен компании. Приставка в домене перед .amocrm.ru;
    | - Логин пользователя. В качестве логина в системе используется e-mail;
    | - Ключ пользователя, который можно получить на странице редактирования
    |   профиля пользователя.
    |
    */

    'domain' => env('AMO_DOMAIN', 'realistmd'),
    'login' => env('AMO_LOGIN', 'realist@amocrm.md'),
    'hash' => env('AMO_HASH', '769802b161d5103c7af03fa0cffe914eb1ae5920'),

    'client_id' => env('AMO_CLIENT_ID'),
    'client_secret' => env('AMO_CLIENT_SECRET'),
    'redirect_uri' => env('AMO_REDIRECT_URI', "https://www.realist.md/"),


/*
|--------------------------------------------------------------------------
| Авторизация в системе B2B Family
|--------------------------------------------------------------------------
|
| Эти параметры авторизации необходимо указать если будет использована
| отправка писем с привязкой к сделке в amoCRM, через сервис B2B Family.
|
*/

    'b2bfamily' => [

        'appkey' => env('B2B_APPKEY'),
        'secret' => env('B2B_SECRET'),
        'email' => env('B2B_EMAIL'),
        'password' => env('B2B_PASSWORD'),

    ]

];