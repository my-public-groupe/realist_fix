const mix = require('laravel-mix');


mix.browserSync({
    proxy: 'http://realist.local'
});

mix.js('resources/js/app.js', 'public/js').vue({
    extractStyles: false,
    globalStyles: false
}).webpackConfig((webpack) => {
    return {
        plugins: [
            new webpack.DefinePlugin({
                __VUE_OPTIONS_API__: true,
                __VUE_PROD_DEVTOOLS__: false,
            }),
        ],
    };
})
    .sass('resources/scss/features.scss', 'public/css')
    .options({
        processCssUrls: false,
    })
    .version()