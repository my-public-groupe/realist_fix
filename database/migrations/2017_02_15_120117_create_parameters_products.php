<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateParametersProducts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('parameters_products', function(Blueprint $table) {
            $table->increments('id');
            $table->integer('parameter_id')->unsigned();
            $table->integer('product_id')->unsigned();
            $table->string('value')->index();
            $table->string('value_ro')->index();
            $table->string('value_en')->index();
            $table->integer('value_id')->index();
            $table->integer('sort')->default(0);

            $table->foreign('parameter_id')->references('id')->on('parameters')
                ->onDelete('cascade')
                ->onUpdate('cascade');
            $table->foreign('product_id')->references('id')->on('products')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('parameters_products', function(Blueprint $table) {
            $table->dropForeign('parameters_products_parameter_id_foreign');
            $table->dropForeign('parameters_products_product_id_foreign');
        });
        
        Schema::drop('parameters_products');
    }
}
