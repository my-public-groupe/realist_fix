<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLists extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lists', function($t){
            $t->increments('id');
            $t->string('name')->nullable();
            $t->string('name_ro')->nullable();
            $t->string('name_en')->nullable();
            $t->text('description')->nullable();
            $t->text('description_ro')->nullable();
            $t->text('description_en')->nullable();
            $t->text('description_short')->nullable();
            $t->text('description_short_ro')->nullable();
            $t->text('description_short_en')->nullable();
            $t->integer('parent_id')->index()->default(0);
            $t->boolean('enabled')->default(true);
            $t->integer('views')->default(0);
            $t->integer('sort')->default(0);
            $t->string('slug', 100)->unique();
            $t->text('params')->nullable();
            $t->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('lists');
    }
}
