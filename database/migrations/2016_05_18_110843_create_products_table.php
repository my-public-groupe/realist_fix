<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $t) {
            $t->increments('id');
            $t->integer('category_id')->index()->nullable();
            $t->tinyInteger('type')->default(0);
            $t->string('name');
            $t->string('name_ro')->nullable();
            $t->string('name_en')->nullable();
            $t->text('description')->nullable();
            $t->text('description_ro')->nullable();
            $t->text('description_en')->nullable();
            $t->text('description_short')->nullable();
            $t->text('description_short_ro')->nullable();
            $t->text('description_short_en')->nullable();
            $t->text('params')->nullable();
            $t->float('price', 15, 2)->nullable();
            $t->float('old_price', 15, 2)->nullable();
            $t->float('sale', 15, 2)->nullable();
            $t->string('slug')->unique()->nullable();
            $t->boolean('enabled')->default(true);
            $t->integer('views')->default(0);
            $t->integer('sort')->default(0);
            $t->integer('agent_id')->nullable();
            $t->integer('region_id')->nullable();
            $t->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
