<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNews extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
       Schema::create('news', function(Blueprint $t){
			$t->increments('id');
			$t->string('name')->nullable();
			$t->string('name_ro')->nullable();
			$t->string('name_en')->nullable();
			$t->text('description')->nullable();
			$t->text('description_ro')->nullable();
			$t->text('description_en')->nullable();
			$t->text('description_short')->nullable();
			$t->text('description_short_ro')->nullable();
			$t->text('description_short_en')->nullable();
			$t->boolean('enabled')->default(true);
			$t->boolean('top')->default(false);
            $t->integer('views')->default(0);
            $t->integer('sort')->default(0);
            $t->string('slug')->unique()->nullable();
			$t->timestamps();
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('news');
    }
}
