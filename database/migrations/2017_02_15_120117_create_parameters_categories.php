<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateParametersCategories extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('parameters_categories', function(Blueprint $table) {
            $table->increments('id');
            $table->integer('parameter_id')->unsigned();
            $table->integer('category_id')->unsigned();
            $table->integer('sort');

            $table->foreign('parameter_id')->references('id')->on('parameters')
                ->onDelete('cascade')
                ->onUpdate('cascade');
            $table->foreign('category_id')->references('id')->on('categories')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('parameters_categories', function(Blueprint $table) {
            $table->dropForeign('parameters_categories_parameter_id_foreign');
            $table->dropForeign('parameters_categories_category_id_foreign');
        });
        
        Schema::drop('parameters_categories');
    }
}
