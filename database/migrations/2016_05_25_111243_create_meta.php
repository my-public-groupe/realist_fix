<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMeta extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('meta', function(Blueprint $table) {
            $table->increments('id');
            $table->string('meta_description')->nullable();
            $table->string('meta_description_ro')->nullable();
            $table->string('meta_description_en')->nullable();
            $table->string('meta_keywords')->nullable();
            $table->string('meta_keywords_ro')->nullable();
            $table->string('meta_keywords_en')->nullable();
            $table->string('title')->nullable();
            $table->string('title_ro')->nullable();
            $table->string('title_en')->nullable();
            $table->morphs('table');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('meta');
    }
}
