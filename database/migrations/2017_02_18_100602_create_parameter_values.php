<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateParameterValues extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('parameter_values', function(Blueprint $table) {
            $table->increments('id');
            $table->string('value')->nullable();
            $table->string('value_ro')->nullable();
            $table->string('value_en')->nullable();
            $table->integer('sort')->default(0);
            $table->integer('parameter_id')->unsigned();
        });

        Schema::table('parameter_values', function(Blueprint $table) {
            $table->foreign('parameter_id')->references('id')->on('parameters')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

        Schema::table('parameter_values', function(Blueprint $table) {
            $table->dropForeign('parameter_values_parameter_id_foreign');
        });
        
        Schema::drop('parameters_values');
    }
}
