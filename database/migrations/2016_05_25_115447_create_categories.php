<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCategories extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('categories', function(Blueprint $t){
			$t->increments('id');
            $t->string('name')->nullable();
            $t->string('name_ro')->nullable();
            $t->string('name_en')->nullable();
            $t->text('description')->nullable();
            $t->text('description_ro')->nullable();
            $t->text('description_en')->nullable();
            $t->text('description_short')->nullable();
            $t->text('description_short_ro')->nullable();
            $t->text('description_short_en')->nullable();
			$t->boolean('enabled')->default(true);
			$t->integer('views')->default(0);
            $t->integer('sort')->default(0);
            $t->string('slug')->unique()->nullable();
			$t->timestamps();
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('categories');
    }
}
