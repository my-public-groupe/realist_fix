<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateMetaColumns extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement('ALTER TABLE meta MODIFY meta_description TEXT;');
        DB::statement('ALTER TABLE meta MODIFY meta_description_ro TEXT;');
        DB::statement('ALTER TABLE meta MODIFY meta_description_en TEXT;');
        DB::statement('ALTER TABLE meta MODIFY meta_keywords TEXT;');
        DB::statement('ALTER TABLE meta MODIFY meta_keywords_ro TEXT;');
        DB::statement('ALTER TABLE meta MODIFY meta_keywords_en TEXT;');
        DB::statement('ALTER TABLE meta MODIFY title TEXT;');
        DB::statement('ALTER TABLE meta MODIFY title_ro TEXT;');
        DB::statement('ALTER TABLE meta MODIFY title_en TEXT;');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

    }
}
