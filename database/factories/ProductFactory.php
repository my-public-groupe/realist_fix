<?php

use Faker\Generator as Faker;

$factory->define(\App\Models\Product::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'description_short' => $faker->text(50),
        'category_id' => function () {
            $c = factory(App\Models\Categories::class)->create();
            return $c->id;
        },
    ];
});
