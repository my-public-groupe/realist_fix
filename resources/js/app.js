import InfiniteLoading from "v3-infinite-loading";

import _ from "lodash";
import { createApp } from 'vue'
import {store} from './store';

import Splide from '@splidejs/splide';
import '@splidejs/splide/css';

const splideContainer = document.querySelector('#main-carousel');
if (splideContainer) {
  document.addEventListener( 'DOMContentLoaded', function() {
      var splide = new Splide( '#main-carousel', {
          pagination: false,
        } );
        
        var thumbnails = document.getElementsByClassName( 'thumbnail' );
        var current;
        
        for ( var i = 0; i < thumbnails.length; i++ ) {
          initThumbnail( thumbnails[ i ], i );
        }
        
        function initThumbnail( thumbnail, index ) {
          thumbnail.addEventListener( 'click', function () {
            splide.go( index );
          } );
        }
        
        splide.on( 'mounted move', function () {
          var thumbnail = thumbnails[ splide.index ];
        
          if ( thumbnail ) {
            if ( current ) {
              current.classList.remove( 'is-active' );
            }
        
            thumbnail.classList.add( 'is-active' );
            current = thumbnail;
          }
        } );
        
        splide.mount();
  } );
}

import ObjectsList from './components/objects-list.vue';
import ObjectsListCategory from './components/objects-list-category.vue';
import ObjectsListAgent from './components/objects-list-agent.vue';
import ObjectsListSearch from './components/objects-list-search.vue';
import ObjectsCard from './components/objects-card.vue';
import ObjectsFilter from './components/objects-filter.vue';

const app = createApp({

    data() {
        return {
            callOpened: false
        }
    },
    components: {

    },
    computed: {
        errorObj() {
            return this.$store.getters.error
        },
    },

    watch: {
        errorObj(error) {
            if (error.response.data.errors) {
                for (const [key, value] of Object.entries(error.response.data.errors)) {
                    console.error(`${value}`);
                }
            } else {
                console.error('Server Error');
            }
        }
    },

    async mounted(){

    }

});

app.config.globalProperties.trans = (key) => {
    return _.get(window.i18n, key, key);
};

app.component("infinite-loading", InfiniteLoading);

app.component('objects-list', ObjectsList);
app.component('objects-list-category', ObjectsListCategory);
app.component('objects-list-agent', ObjectsListAgent);
app.component('objects-list-search', ObjectsListSearch);
app.component('objects-card', ObjectsCard);
app.component('objects-filter', ObjectsFilter);

app.use(store);

app.mount("#app");