import {createStore} from "vuex";
import Api from "../services/api";
import queryMixin from "../mixins/query.mixin";

const store = createStore({
    state: {
        query_params: queryMixin.methods.getQueryParams(window.location.search),
        category_id: null,
        filterParameters: {
            params: [],
            regions: [],
            filters: {
                params: []
            },
            price: {active: false},
            region: {active: false},
            filter_regions: [],
            filter_params: [],
            filter_price: [],
            filter_intervals: [],
            current_page: 1,
            current_sort: 'default'
        },
        objects: {
            data: {},
            on_each_side: 2,
            total: 0,
            last_page: 0
        },
        isLoading: null,
        mobileMenuOpen: false,
        changedSort: false,
        deletedFilter: false,
        is_mobile: window.is_mobile
    },
    getters: {
        category_id: s => s.category_id,
        objects: s => s.objects.data,
        total_objects: s => s.objects.total,
        current_page: s => parseInt(s.filterParameters.current_page),
        current_sort: s => s.filterParameters.current_sort,
        on_each_side: s => s.objects.on_each_side,
        last_page: s => s.objects.last_page,
        filterParameters: s => s.filterParameters,
        filters: s => s.filterParameters.filters,
        query_params: s => s.query_params
    },
    mutations: {
        SET_CATEGORY_ID(state, category_id){
          state.category_id = category_id;
        },
        SET_REGIONS(state, regions) {
            state.filterParameters.regions = regions;
        },
        SET_PARAMS(state, params) {
            state.filterParameters.params = params;
        },
        SET_OBJECTS(state, objects){
            if(window.is_mobile && Object.keys(state.objects.data).length && !state.mobileMenuOpen && !state.changedSort && !state.deletedFilter){
                if (Object.keys(objects).length) {
                    objects.forEach(function (object) {
                        if (!state.objects.data.find(item => item.id === object.id)) {
                            state.objects.data = [...state.objects.data, object];
                        }
                    });
                }
            }else{
                state.objects.data = objects;
                state.mobileMenuOpen = false;
                state.changedSort = false;
                state.deletedFilter = false;
            }
        },
        SET_TOTAL_OBJECTS(state, total){
            state.objects.total = total;
        },
        SET_OBJECTS_LINKS_LAST_PAGE(state, last_page){
            state.objects.last_page = last_page
        },
        SET_OBJECTS_PAGE(state, page){
            state.filterParameters.current_page = page;
        },
        OBJECTS_PAGE_INCREMENT(state){
            state.filterParameters.current_page++;
        },
        OBJECTS_PAGE_DECREMENT(state){
            state.filterParameters.current_page--;
        },
        SET_SORTING(state, sort){
            state.filterParameters.current_sort = sort;
        },
        SET_LOADING(state, loading_state){
            state.isLoading = loading_state;
        }
    },
    actions: {
        async getCategoryObjects({commit}, payload){
            commit('SET_LOADING', true);

            await Api().get(payload.url, {
                params: {
                    category_id: payload.category_id ?? this.getters.category_id,
                    filters: payload.filters ?? this.getters.filters,
                    page: payload.page ?? this.getters.current_page,
                    sort: payload.sort ?? this.getters.current_sort
                }
            }).then(function (response) {
                commit('SET_OBJECTS', response.data.data);
                commit('SET_TOTAL_OBJECTS', response.data.total);
                commit('SET_OBJECTS_LINKS_LAST_PAGE', response.data.last_page);

                commit('SET_LOADING', false);

                if(payload.state) {
                    if(Object.keys(response.data.data).length == 0){
                        payload.state.completed();
                    }else {
                        payload.state.loaded();
                    }
                }
            });
        },

        async getAgentObjects({commit}, payload){
            commit('SET_LOADING', true);

            await Api().get(payload.url, {
                params: {
                    page: payload.page ?? this.getters.current_page,
                }
            }).then(function (response) {
                commit('SET_OBJECTS', response.data.data);
                commit('SET_TOTAL_OBJECTS', response.data.total);
                commit('SET_OBJECTS_LINKS_LAST_PAGE', response.data.last_page);

                commit('SET_LOADING', false);

                if(payload.state) {
                    if(Object.keys(response.data.data).length == 0){
                        payload.state.completed();
                    }else {
                        payload.state.loaded();
                    }
                }
            });
        },

        async getSearchObjects({commit}, payload){
            commit('SET_LOADING', true);

            await Api().get(payload.url, {
                params: {
                    searchword: payload.searchword,
                    page: payload.page ?? this.getters.current_page,
                }
            }).then(function (response) {
                commit('SET_OBJECTS', response.data.data);
                commit('SET_TOTAL_OBJECTS', response.data.total);
                commit('SET_OBJECTS_LINKS_LAST_PAGE', response.data.last_page);

                commit('SET_LOADING', false);

                if(payload.state) {
                    if(Object.keys(response.data.data).length == 0){
                        payload.state.completed();
                    }else {
                        payload.state.loaded();
                    }
                }
            });
        },

        async getAllObjects({commit}, payload){
            commit('SET_LOADING', true);

            await Api().get(payload.url, {
                params: {
                    category_id: 0,
                    page: payload.page ?? this.getters.current_page,
                }
            }).then(function (response) {
                commit('SET_OBJECTS', response.data.data);
                commit('SET_TOTAL_OBJECTS', response.data.total);
                commit('SET_OBJECTS_LINKS_LAST_PAGE', response.data.last_page);

                commit('SET_LOADING', false);
            });
        },

        async getCategoryFilters({commit}, url) {
            await Api().get(url).then(function (response) {
                commit('SET_REGIONS', response.data.regions);
                commit('SET_PARAMS', response.data.params);
            });
        },
    }
});


export {store};
