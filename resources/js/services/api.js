import axios from 'axios'

export default() => {
    let headers = {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Cache-Control': 'no-cache',
        'Pragma': 'no-cache',
        'Expires': '0',
    }

    if(window.has_webp_support){
        headers['Accept'] += ', image/webp';
    }

    return axios.create({
        baseURL: window.baseUrl,
        withCredentials: true,
        headers: headers
    })
}
