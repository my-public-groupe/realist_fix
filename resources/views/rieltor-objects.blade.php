@extends('body')
@section('centerbox')

    <section class="rieltor-objects single-page">
        <div class="bread-crumbs">
            <div class="container">
                <span>Вы здесь: Главная /</span>
                <span class="active">Алла Р.</span>
            </div>
        </div>
        <div class="container">
            <div class="row">

                <div class="col-md-3">
                    <div class="employee-item">
                        <div class="rieltor-info">
                            <div class="img-rieltor-info img-rieltor-info-object">
                                <img class="rieltor-info-img" src="images/rieltor-image.png" alt="">
                            </div>
                            <div class="rieltor-contacts ">
                                <div class="rieltor-details">
                                    <div class="rieltor-name">Алла Ройтман</div>
                                    <a href="tel:+37368584828" class="rieltor-phone">
                                    <img src="./images/contacts-call.png" alt="">
                                    +373 68 584 828</a>
                                    <a href="mailto: alla.realtor@realist.md" class="rieltor-email">
                                        alla.realtor@realist.md
                                    </a>
                                    <a href="#" class="more-objects">
                                    Объекты риелтора
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="rieltor-social-media">
                            <div class="rieltor-details-title">
                                Связь с риелтором в один клик:
                            </div>
                            <ul class="rieltor-social">
                                <li class="rieltor-viber"></li>
                                <li class="rieltor-messenger"></li>
                                <li class="rieltor-telegram"></li>
                                <li class="rieltor-whatsup"></li>
                            </ul>
                        </div>
                    </div>

                </div>



                    <div class="col-md-9">
                        <div class="container">
                            <div class="about-us-title">
                                Недвижимость которую представляет данный риелтор
                            </div>
                            <div class="row">
                            <div class="col-md-4">
                                    <a href="#">
                                        <div class="proposal-container section-container">
                                            <div class="preview-image"
                                                style="background-image:url('./images/proposal-template.png')">
                                                <div class="hot-price"> Hot Price </div>
                                            </div>
                                            <div class="price-container">
                                                <div class="price">€ 35 000</div>
                                                <div class="discount">€ 40 055</div>
                                            </div>
                                            <div class="information">
                                                <div class="information-line">
                                                    <div class="ico-place"> г. Кишинёв, Чеканы, Милеску Спэтару 23 </div>
                                                </div>
                                                <div class="information-line">
                                                    <div class="ico-apartament"> Квартира </div>
                                                    <div class="ico-area"> 32м2 </div>
                                                </div>
                                                <div class="information-line">
                                                    <div class="ico-room"> 2 комнаты </div>
                                                    <div class="ico-bathroom"> 1 санузел</div>
                                                </div>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                                <div class="col-md-4">
                                    <a href="#">
                                        <div class="proposal-container section-container">
                                            <div class="preview-image"
                                                style="background-image:url('./images/proposal-template.png')">
                                                <div class="hot-price"> Hot Price </div>
                                            </div>
                                            <div class="price-container">
                                                <div class="price">€ 35 000</div>
                                                <div class="discount">€ 40 055</div>
                                            </div>
                                            <div class="information">
                                                <div class="information-line">
                                                    <div class="ico-place"> г. Кишинёв, Чеканы, Милеску Спэтару 23 </div>
                                                </div>
                                                <div class="information-line">
                                                    <div class="ico-apartament"> Квартира </div>
                                                    <div class="ico-area"> 32м2 </div>
                                                </div>
                                                <div class="information-line">
                                                    <div class="ico-room"> 2 комнаты </div>
                                                    <div class="ico-bathroom"> 1 санузел</div>
                                                </div>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                                <div class="col-md-4">
                                    <a href="#">
                                        <div class="proposal-container section-container">
                                            <div class="preview-image"
                                                style="background-image:url('./images/proposal-template.png')">
                                                <div class="hot-price"> Hot Price </div>
                                            </div>
                                            <div class="price-container">
                                                <div class="price">€ 35 000</div>
                                                <div class="discount">€ 40 055</div>
                                            </div>
                                            <div class="information">
                                                <div class="information-line">
                                                    <div class="ico-place"> г. Кишинёв, Чеканы, Милеску Спэтару 23 </div>
                                                </div>
                                                <div class="information-line">
                                                    <div class="ico-apartament"> Квартира </div>
                                                    <div class="ico-area"> 32м2 </div>
                                                </div>
                                                <div class="information-line">
                                                    <div class="ico-room"> 2 комнаты </div>
                                                    <div class="ico-bathroom"> 1 санузел</div>
                                                </div>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                                <div class="col-md-4">
                                    <a href="#">
                                        <div class="proposal-container section-container">
                                            <div class="preview-image"
                                                style="background-image:url('./images/proposal-template.png')">
                                                <div class="hot-price"> Hot Price </div>
                                            </div>
                                            <div class="price-container">
                                                <div class="price">€ 35 000</div>
                                                <div class="discount">€ 40 055</div>
                                            </div>
                                            <div class="information">
                                                <div class="information-line">
                                                    <div class="ico-place"> г. Кишинёв, Чеканы, Милеску Спэтару 23 </div>
                                                </div>
                                                <div class="information-line">
                                                    <div class="ico-apartament"> Квартира </div>
                                                    <div class="ico-area"> 32м2 </div>
                                                </div>
                                                <div class="information-line">
                                                    <div class="ico-room"> 2 комнаты </div>
                                                    <div class="ico-bathroom"> 1 санузел</div>
                                                </div>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                        @include('partials.pagination')
                    </div>
            </div>
        </div>
    </section>
@endsection