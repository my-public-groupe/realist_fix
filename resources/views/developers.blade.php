@extends('body')
@section('centerbox')
    <section class="developers single-page">
        <div class="bread-crumbs">
            <div class="container">
                <span>@lang('common.youre_here'): <a href="{{route('index')}}">@lang('common.main')</a> /</span>
                <span class="active">@lang('common.for_property_developers')</span>
            </div>
        </div>
        <div class="container">
            <div class="row">


                <div class="x-row x-row--centered">
                    <div class="x-col developers__left-col">
                        <div class="developers__img-wrap"><img src="images/zastrooika_02.jpg" alt="building"
                                                               class="developers__img-building"> <img
                                    src="images/realist-figure-blue.svg" alt="figure"
                                    class="developers__img-figure"></div>
                        <div class="developers__btns"><a href="tel:+37379 57 88 00"
                                                         class="icon-btn icon-btn--blue icon-btn--white-icon icon-btn--extra-big">
                                <svg
                                        xmlns="http://www.w3.org/2000/svg" width="16.974" height="17.006"
                                        viewBox="0 0 16.974 17.006">
                                    <path id="phone"
                                          d="M17.585,13.608v2.334a1.556,1.556,0,0,1-1.7,1.556,15.4,15.4,0,0,1-6.714-2.388,15.171,15.171,0,0,1-4.668-4.668A15.4,15.4,0,0,1,2.118,3.7,1.556,1.556,0,0,1,3.666,2H6A1.556,1.556,0,0,1,7.556,3.338,9.99,9.99,0,0,0,8.1,5.524a1.556,1.556,0,0,1-.35,1.642l-.988.988a12.448,12.448,0,0,0,4.668,4.668l.988-.988a1.556,1.556,0,0,1,1.642-.35,9.99,9.99,0,0,0,2.186.545A1.556,1.556,0,0,1,17.585,13.608Z"
                                          transform="translate(-1.361 -1.25)" fill="none" stroke="#000"
                                          stroke-linecap="round"
                                          stroke-linejoin="round" stroke-width="1.5"></path>
                                </svg>
                                <span>@lang('common.developers_phone')</span></a> <a href="mailto:@lang('common.developers_email')"
                                                                    class="icon-btn icon-btn--blue icon-btn--white-icon icon-btn--extra-big">
                                <img src="images/contact-mail.png" alt="">
                                <span class="smaller">@lang('common.developers_email')</span></a></div>
                    </div>
                    <div class="x-col developers__right-col">
                        <h2 class="title title--blue title--new-big">@lang('common.for_property_developers')</h2>
                        <p class="developers__text-bolder mb-23">@lang('common.developers_text1')</p>
                        <p class="developers__text mb-30">@lang('common.developers_text2')</p>
                        <p class="developers__text mb-15">@lang('common.developers_text3')</p>
                        <p class="developers__text-bolder">@lang('common.developers_text4')</p>
                    </div>
                </div>
                <div class="col-number big-top">
                    <div class="x-col "></div>
                    <div class="x-col ">
                        <h2 class="title title--blue title--new-big">@lang('common.developers_title1')</h2>
                    </div>
                </div>
                <div class="col-number">
                    <div class="x-col">
                        <div class="developers__num">01</div>
                    </div>
                    <div class="x-col ">
                        <h2 class="title">@lang('common.developers_title3')</h2>
                        <p class="main-big-text">@lang('common.developers_text7')</p>
                    </div>
                </div>
                <div class="col-number">
                    <div class="x-col ">
                        <div class="developers__num">02</div>
                    </div>
                    <div class="x-col">
                        <h2 class="title">@lang('common.developers_title3_1')</h2>
                        <p class="main-big-text">@lang('common.developers_text6')</p>
                    </div>
                </div>
                <div class="col-number">
                    <div class="x-col ">
                        <div class="developers__num">03</div>
                    </div>
                    <div class="x-col">
                        <h2 class="title">@lang('common.developers_title4')</h2>
                        <p class="main-big-text">
                            @lang('common.developers_text7')</p>
                    </div>
                </div>
                <div class="col-number">
                    <div class="x-col"></div>
                    <div class="x-col">
                        <h2 class="title">@lang('common.developers_title5')</h2>
                        <ul class="developers__list">
                            <li>
                                <p class="main-big-text">@lang('common.developers_text8')</p>
                            </li>
                            <li>
                                <p class="main-big-text">@lang('common.developers_text9')</p>
                            </li>
                            <li>
                                <p class="main-big-text">@lang('common.developers_text10')</p>
                            </li>
                            <li>
                                <p class="main-big-text">@lang('common.developers_text11')</p>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection