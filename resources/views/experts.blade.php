@extends('body')
@section('centerbox')
    <section class="experts single-page">
        <div class="bread-crumbs">
            <div class="container">
                <span>@lang('common.youre_here'): <a href="{{route('index')}}">@lang('common.main')</a> /</span>
                <span class="active">@lang('common.experts')</span>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <section class="experts__types">
                    <div class="experts__types-container">
                        <div class="experts__types-row align-items-center">
                            <div class="experts__col-small">
                                <div class="experts__photo">
                                    <div class="experts__photo-wrap"><img src="images/expert_01.jpg" alt="expert"></div>
                                </div>
                            </div>
                            <div class="experts__col-big">
                                <h2 class="title">@lang('common.expert_finance')</h2>
                                <p class="text text--big">@lang('common.expert_finance_text')</p>
                                <div class="experts__col-contacts"><a href="tel:@lang('common.expert_phone')"
                                                                      class="icon-btn icon-btn--blue icon-btn--white-icon icon-btn--big">
                                        <svg
                                                xmlns="http://www.w3.org/2000/svg" width="16.974" height="17.006"
                                                viewBox="0 0 16.974 17.006">
                                            <path id="phone"
                                                  d="M17.585,13.608v2.334a1.556,1.556,0,0,1-1.7,1.556,15.4,15.4,0,0,1-6.714-2.388,15.171,15.171,0,0,1-4.668-4.668A15.4,15.4,0,0,1,2.118,3.7,1.556,1.556,0,0,1,3.666,2H6A1.556,1.556,0,0,1,7.556,3.338,9.99,9.99,0,0,0,8.1,5.524a1.556,1.556,0,0,1-.35,1.642l-.988.988a12.448,12.448,0,0,0,4.668,4.668l.988-.988a1.556,1.556,0,0,1,1.642-.35,9.99,9.99,0,0,0,2.186.545A1.556,1.556,0,0,1,17.585,13.608Z"
                                                  transform="translate(-1.361 -1.25)" fill="none" stroke="#000"
                                                  stroke-linecap="round"
                                                  stroke-linejoin="round" stroke-width="1.5"></path>
                                        </svg>
                                        <span>+373 788 88 252</span></a> <a href="mailto:@lang('common.expert_email')"
                                                                            class="icon-btn icon-btn--blue icon-btn--white-icon icon-btn--big">
                                        <svg
                                                xmlns="http://www.w3.org/2000/svg" width="18.45" height="14.588"
                                                viewBox="0 0 18.45 14.588">
                                            <g id="mail" transform="translate(-0.955 -3.25)">
                                                <path id="Path_3661" data-name="Path 3661"
                                                      d="M3.636,4H16.724A1.641,1.641,0,0,1,18.36,5.636v9.816a1.641,1.641,0,0,1-1.636,1.636H3.636A1.641,1.641,0,0,1,2,15.452V5.636A1.641,1.641,0,0,1,3.636,4Z"
                                                      fill="none" stroke="#000" stroke-linecap="round"
                                                      stroke-linejoin="round"
                                                      stroke-width="1.5"></path>
                                                <path id="Path_3662" data-name="Path 3662" d="M18.36,6l-8.18,5.726L2,6"
                                                      transform="translate(0 -0.364)" fill="none" stroke="#000"
                                                      stroke-linecap="round"
                                                      stroke-linejoin="round" stroke-width="1.5"></path>
                                            </g>
                                        </svg>
                                        <span>@lang('common.expert_email')</span></a></div>
                            </div>
                        </div>
                        <div class="experts__types-row align-items-center">
                            <div class="experts__col-small">
                                <div class="experts__photo">
                                    <div class="experts__photo-wrap"><img src="images/expert_02.jpg" alt="expert"></div>
                                </div>
                            </div>
                            <div class="experts__col-big">
                                <h2 class="title">@lang('common.expert_legal')</h2>
                                <p class="text text--big">@lang('common.expert_legal_text')</p>
                                <div class="experts__col-contacts"><a href="tel:@lang('common.legal_phone')"
                                                                      class="icon-btn icon-btn--blue icon-btn--white-icon icon-btn--big">
                                        <svg
                                                xmlns="http://www.w3.org/2000/svg" width="16.974" height="17.006"
                                                viewBox="0 0 16.974 17.006">
                                            <path id="phone"
                                                  d="M17.585,13.608v2.334a1.556,1.556,0,0,1-1.7,1.556,15.4,15.4,0,0,1-6.714-2.388,15.171,15.171,0,0,1-4.668-4.668A15.4,15.4,0,0,1,2.118,3.7,1.556,1.556,0,0,1,3.666,2H6A1.556,1.556,0,0,1,7.556,3.338,9.99,9.99,0,0,0,8.1,5.524a1.556,1.556,0,0,1-.35,1.642l-.988.988a12.448,12.448,0,0,0,4.668,4.668l.988-.988a1.556,1.556,0,0,1,1.642-.35,9.99,9.99,0,0,0,2.186.545A1.556,1.556,0,0,1,17.585,13.608Z"
                                                  transform="translate(-1.361 -1.25)" fill="none" stroke="#000"
                                                  stroke-linecap="round"
                                                  stroke-linejoin="round" stroke-width="1.5"></path>
                                        </svg>
                                        <span>@lang('common.legal_phone')</span></a> <a href="mailto:@lang('common.legal_email')"
                                                                            class="icon-btn icon-btn--blue icon-btn--white-icon icon-btn--big">
                                        <svg
                                                xmlns="http://www.w3.org/2000/svg" width="18.45" height="14.588"
                                                viewBox="0 0 18.45 14.588">
                                            <g id="mail" transform="translate(-0.955 -3.25)">
                                                <path id="Path_3661" data-name="Path 3661"
                                                      d="M3.636,4H16.724A1.641,1.641,0,0,1,18.36,5.636v9.816a1.641,1.641,0,0,1-1.636,1.636H3.636A1.641,1.641,0,0,1,2,15.452V5.636A1.641,1.641,0,0,1,3.636,4Z"
                                                      fill="none" stroke="#000" stroke-linecap="round"
                                                      stroke-linejoin="round"
                                                      stroke-width="1.5"></path>
                                                <path id="Path_3662" data-name="Path 3662" d="M18.36,6l-8.18,5.726L2,6"
                                                      transform="translate(0 -0.364)" fill="none" stroke="#000"
                                                      stroke-linecap="round"
                                                      stroke-linejoin="round" stroke-width="1.5"></path>
                                            </g>
                                        </svg>
                                        <span>@lang('common.legal_email')</span></a></div>
                            </div>
                        </div>
                        <div class="experts__types-row align-items-center">
                            <div class="experts__col-small">
                                <div class="experts__photo">
                                    <div class="experts__photo-wrap"><img src="images/expert_06.jpg" alt="expert"></div>
                                </div>
                            </div>
                            <div class="experts__col-big">
                                <h2 class="title">@lang('common.expert_marketing')</h2>
                                <p class="text text--big">@lang('common.expert_marketing_text')</p>
                                <div class="experts__col-contacts"><a href="tel:@lang('common.expert_marketing_phone')"
                                                                      class="icon-btn icon-btn--blue icon-btn--white-icon icon-btn--big">
                                        <svg
                                                xmlns="http://www.w3.org/2000/svg" width="16.974" height="17.006"
                                                viewBox="0 0 16.974 17.006">
                                            <path id="phone"
                                                  d="M17.585,13.608v2.334a1.556,1.556,0,0,1-1.7,1.556,15.4,15.4,0,0,1-6.714-2.388,15.171,15.171,0,0,1-4.668-4.668A15.4,15.4,0,0,1,2.118,3.7,1.556,1.556,0,0,1,3.666,2H6A1.556,1.556,0,0,1,7.556,3.338,9.99,9.99,0,0,0,8.1,5.524a1.556,1.556,0,0,1-.35,1.642l-.988.988a12.448,12.448,0,0,0,4.668,4.668l.988-.988a1.556,1.556,0,0,1,1.642-.35,9.99,9.99,0,0,0,2.186.545A1.556,1.556,0,0,1,17.585,13.608Z"
                                                  transform="translate(-1.361 -1.25)" fill="none" stroke="#000"
                                                  stroke-linecap="round"
                                                  stroke-linejoin="round" stroke-width="1.5"></path>
                                        </svg>
                                        <span>@lang('common.expert_marketing_phone')</span></a> <a href="mailto:marketingexpert@realist.md"
                                                                                                   class="icon-btn icon-btn--blue icon-btn--white-icon icon-btn--big">
                                        <svg
                                                xmlns="http://www.w3.org/2000/svg" width="18.45" height="14.588"
                                                viewBox="0 0 18.45 14.588">
                                            <g id="mail" transform="translate(-0.955 -3.25)">
                                                <path id="Path_3661" data-name="Path 3661"
                                                      d="M3.636,4H16.724A1.641,1.641,0,0,1,18.36,5.636v9.816a1.641,1.641,0,0,1-1.636,1.636H3.636A1.641,1.641,0,0,1,2,15.452V5.636A1.641,1.641,0,0,1,3.636,4Z"
                                                      fill="none" stroke="#000" stroke-linecap="round"
                                                      stroke-linejoin="round"
                                                      stroke-width="1.5"></path>
                                                <path id="Path_3662" data-name="Path 3662" d="M18.36,6l-8.18,5.726L2,6"
                                                      transform="translate(0 -0.364)" fill="none" stroke="#000"
                                                      stroke-linecap="round"
                                                      stroke-linejoin="round" stroke-width="1.5"></path>
                                            </g>
                                        </svg>
                                        <span>@lang('common.expert_marketing_email')</span></a>
                                </div>
                            </div>
                        </div>
                        <div class="experts__types-row align-items-center">
                            <div class="experts__col-small">
                                <div class="experts__photo">
                                    <div class="experts__photo-wrap"><img src="images/expert_05.jpg" alt="expert"></div>
                                </div>
                            </div>
                            <div class="experts__col-big">
                                <h2 class="title">@lang('common.expert_price')</h2>
                                <p class="text text--big">@lang('common.expert_price_text')</p>
                                <div class="experts__col-contacts"><a href="tel:@lang('common.expert_price_phone')"
                                                                      class="icon-btn icon-btn--blue icon-btn--white-icon icon-btn--big">
                                        <svg
                                                xmlns="http://www.w3.org/2000/svg" width="16.974" height="17.006"
                                                viewBox="0 0 16.974 17.006">
                                            <path id="phone"
                                                  d="M17.585,13.608v2.334a1.556,1.556,0,0,1-1.7,1.556,15.4,15.4,0,0,1-6.714-2.388,15.171,15.171,0,0,1-4.668-4.668A15.4,15.4,0,0,1,2.118,3.7,1.556,1.556,0,0,1,3.666,2H6A1.556,1.556,0,0,1,7.556,3.338,9.99,9.99,0,0,0,8.1,5.524a1.556,1.556,0,0,1-.35,1.642l-.988.988a12.448,12.448,0,0,0,4.668,4.668l.988-.988a1.556,1.556,0,0,1,1.642-.35,9.99,9.99,0,0,0,2.186.545A1.556,1.556,0,0,1,17.585,13.608Z"
                                                  transform="translate(-1.361 -1.25)" fill="none" stroke="#000"
                                                  stroke-linecap="round"
                                                  stroke-linejoin="round" stroke-width="1.5"></path>
                                        </svg>
                                        <span>@lang('common.expert_price_phone')</span></a> <a href="mailto:@lang('common.expert_price_email')"
                                                                                               class="icon-btn icon-btn--blue icon-btn--white-icon icon-btn--big">
                                        <svg
                                                xmlns="http://www.w3.org/2000/svg" width="18.45" height="14.588"
                                                viewBox="0 0 18.45 14.588">
                                            <g id="mail" transform="translate(-0.955 -3.25)">
                                                <path id="Path_3661" data-name="Path 3661"
                                                      d="M3.636,4H16.724A1.641,1.641,0,0,1,18.36,5.636v9.816a1.641,1.641,0,0,1-1.636,1.636H3.636A1.641,1.641,0,0,1,2,15.452V5.636A1.641,1.641,0,0,1,3.636,4Z"
                                                      fill="none" stroke="#000" stroke-linecap="round"
                                                      stroke-linejoin="round"
                                                      stroke-width="1.5"></path>
                                                <path id="Path_3662" data-name="Path 3662" d="M18.36,6l-8.18,5.726L2,6"
                                                      transform="translate(0 -0.364)" fill="none" stroke="#000"
                                                      stroke-linecap="round"
                                                      stroke-linejoin="round" stroke-width="1.5"></path>
                                            </g>
                                        </svg>
                                        <span>@lang('common.expert_price_email')</span></a></div>
                            </div>
                        </div>
                        <div class="experts__types-row align-items-center">
                            <div class="experts__col-small">
                                <div class="experts__photo">
                                    <div class="experts__photo-wrap"><img src="images/expert_07.jpg" alt="expert"></div>
                                </div>
                            </div>
                            <div class="experts__col-big">
                                <h2 class="title">@lang('common.expert_insurance')</h2>
                                <p class="text text--big">@lang('common.expert_insurance_text')</p>
                                <div class="experts__col-contacts"><a href="tel:@lang('common.expert_insurance_phone')"
                                                                      class="icon-btn icon-btn--blue icon-btn--white-icon icon-btn--big">
                                        <svg
                                                xmlns="http://www.w3.org/2000/svg" width="16.974" height="17.006"
                                                viewBox="0 0 16.974 17.006">
                                            <path id="phone"
                                                  d="M17.585,13.608v2.334a1.556,1.556,0,0,1-1.7,1.556,15.4,15.4,0,0,1-6.714-2.388,15.171,15.171,0,0,1-4.668-4.668A15.4,15.4,0,0,1,2.118,3.7,1.556,1.556,0,0,1,3.666,2H6A1.556,1.556,0,0,1,7.556,3.338,9.99,9.99,0,0,0,8.1,5.524a1.556,1.556,0,0,1-.35,1.642l-.988.988a12.448,12.448,0,0,0,4.668,4.668l.988-.988a1.556,1.556,0,0,1,1.642-.35,9.99,9.99,0,0,0,2.186.545A1.556,1.556,0,0,1,17.585,13.608Z"
                                                  transform="translate(-1.361 -1.25)" fill="none" stroke="#000"
                                                  stroke-linecap="round"
                                                  stroke-linejoin="round" stroke-width="1.5"></path>
                                        </svg>
                                        <span>@lang('common.expert_insurance_phone')</span></a> <a href="mailto:insurence@realist.md"
                                                                                                   class="icon-btn icon-btn--blue icon-btn--white-icon icon-btn--big">
                                        <svg
                                                xmlns="http://www.w3.org/2000/svg" width="18.45" height="14.588"
                                                viewBox="0 0 18.45 14.588">
                                            <g id="mail" transform="translate(-0.955 -3.25)">
                                                <path id="Path_3661" data-name="Path 3661"
                                                      d="M3.636,4H16.724A1.641,1.641,0,0,1,18.36,5.636v9.816a1.641,1.641,0,0,1-1.636,1.636H3.636A1.641,1.641,0,0,1,2,15.452V5.636A1.641,1.641,0,0,1,3.636,4Z"
                                                      fill="none" stroke="#000" stroke-linecap="round"
                                                      stroke-linejoin="round"
                                                      stroke-width="1.5"></path>
                                                <path id="Path_3662" data-name="Path 3662" d="M18.36,6l-8.18,5.726L2,6"
                                                      transform="translate(0 -0.364)" fill="none" stroke="#000"
                                                      stroke-linecap="round"
                                                      stroke-linejoin="round" stroke-width="1.5"></path>
                                            </g>
                                        </svg>
                                        <span>@lang('common.expert_insurance_email')</span></a>
                                </div>
                            </div>
                        </div>
                        <div class="experts__types-row align-items-center">
                            <div class="experts__col-small">
                                <div class="experts__photo">
                                    <div class="experts__photo-wrap"><img src="images/expert_04.jpg" alt="expert"></div>
                                </div>
                            </div>
                            <div class="experts__col-big">
                                <h2 class="title">@lang('common.expert_fisc')</h2>
                                <p class="text text--big">@lang('common.expert_fisc_text')</p>
                                <div class="experts__col-contacts"><a href="tel:+373 22 22 67 49"
                                                                      class="icon-btn icon-btn--blue icon-btn--white-icon icon-btn--big">
                                        <svg
                                                xmlns="http://www.w3.org/2000/svg" width="16.974" height="17.006"
                                                viewBox="0 0 16.974 17.006">
                                            <path id="phone"
                                                  d="M17.585,13.608v2.334a1.556,1.556,0,0,1-1.7,1.556,15.4,15.4,0,0,1-6.714-2.388,15.171,15.171,0,0,1-4.668-4.668A15.4,15.4,0,0,1,2.118,3.7,1.556,1.556,0,0,1,3.666,2H6A1.556,1.556,0,0,1,7.556,3.338,9.99,9.99,0,0,0,8.1,5.524a1.556,1.556,0,0,1-.35,1.642l-.988.988a12.448,12.448,0,0,0,4.668,4.668l.988-.988a1.556,1.556,0,0,1,1.642-.35,9.99,9.99,0,0,0,2.186.545A1.556,1.556,0,0,1,17.585,13.608Z"
                                                  transform="translate(-1.361 -1.25)" fill="none" stroke="#000"
                                                  stroke-linecap="round"
                                                  stroke-linejoin="round" stroke-width="1.5"></path>
                                        </svg>
                                        <span>@lang('common.expert_fisc_phone')</span></a> <a href="mailto:@lang('common.expert_fisc_email')"
                                                                             class="icon-btn icon-btn--blue icon-btn--white-icon icon-btn--big">
                                        <svg
                                                xmlns="http://www.w3.org/2000/svg" width="18.45" height="14.588"
                                                viewBox="0 0 18.45 14.588">
                                            <g id="mail" transform="translate(-0.955 -3.25)">
                                                <path id="Path_3661" data-name="Path 3661"
                                                      d="M3.636,4H16.724A1.641,1.641,0,0,1,18.36,5.636v9.816a1.641,1.641,0,0,1-1.636,1.636H3.636A1.641,1.641,0,0,1,2,15.452V5.636A1.641,1.641,0,0,1,3.636,4Z"
                                                      fill="none" stroke="#000" stroke-linecap="round"
                                                      stroke-linejoin="round"
                                                      stroke-width="1.5"></path>
                                                <path id="Path_3662" data-name="Path 3662" d="M18.36,6l-8.18,5.726L2,6"
                                                      transform="translate(0 -0.364)" fill="none" stroke="#000"
                                                      stroke-linecap="round"
                                                      stroke-linejoin="round" stroke-width="1.5"></path>
                                            </g>
                                        </svg>
                                        <span>@lang('common.expert_fisc_email')</span></a>
                                </div>
                            </div>
                        </div>
                        <div class="experts__types-row align-items-center">
                            <div class="experts__col-small">
                                <div class="experts__photo">
                                    <div class="experts__photo-wrap"><img src="images/expert_03.jpg" alt="expert"></div>
                                </div>
                            </div>
                            <div class="experts__col-big">
                                <h2 class="title">@lang('common.expert_manager')</h2>
                                <p class="text text--big">@lang('common.expert_manager_text')</p>
                                <div class="experts__col-contacts"><a href="mailto:@lang('common.expert_manager_email')"
                                                                      class="icon-btn icon-btn--blue icon-btn--white-icon icon-btn--big">
                                        <svg
                                                xmlns="http://www.w3.org/2000/svg" width="18.45" height="14.588"
                                                viewBox="0 0 18.45 14.588">
                                            <g id="mail" transform="translate(-0.955 -3.25)">
                                                <path id="Path_3661" data-name="Path 3661"
                                                      d="M3.636,4H16.724A1.641,1.641,0,0,1,18.36,5.636v9.816a1.641,1.641,0,0,1-1.636,1.636H3.636A1.641,1.641,0,0,1,2,15.452V5.636A1.641,1.641,0,0,1,3.636,4Z"
                                                      fill="none" stroke="#000" stroke-linecap="round"
                                                      stroke-linejoin="round"
                                                      stroke-width="1.5"></path>
                                                <path id="Path_3662" data-name="Path 3662" d="M18.36,6l-8.18,5.726L2,6"
                                                      transform="translate(0 -0.364)" fill="none" stroke="#000"
                                                      stroke-linecap="round"
                                                      stroke-linejoin="round" stroke-width="1.5"></path>
                                            </g>
                                        </svg>
                                        <span>@lang('common.expert_manager_email')</span></a>
                                </div>
                            </div>
                        </div>

                    </div>
                </section>

            </div>
        </div>
    </section>
@endsection
@section('scripts')
    <script>
        $(function () {
            let scrollToIndex = window.location.search.split('service-')[1] ?? null;
            if(scrollToIndex){
                $('html, body').animate({
                    scrollTop: $(".experts__types-row").eq(scrollToIndex).offset().top - 100
                }, 2000);
            }
        });
    </script>
@endsection