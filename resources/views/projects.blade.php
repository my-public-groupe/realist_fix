@extends('body')
@section('centerbox')


    @include('partials.breadcrumbs', ['breadcrumbs' => [trans('common.projects') => '#']])

    <div class="projects-page">

        <div class="projects__header">
            <div class="x-container">
                <div class="x-row x-row--centered">
                    <div class="x-col">
                        <h2 class="title">@lang('common.projects')</h2>
                    </div>
                </div>
            </div>

        </div>


        <div class="projects__main-container">

            <div class="projects__main-row">

                <div class="projects__list">

                    <div class="project__list-row">
                        @foreach (\App\Models\Lists::getBySlug('projects') as $item)

                            <div class="project__list-item">

                                <a href="{{ $item->description_short }}" target="_blank" class="project__item-content" style="background-image: url('/uploaded/{{ $item->photos{1}->source ?? '' }}')">
                                    <h2 class="title title--white">{{ $item->name }}</h2>
                                </a>

                            </div>
                        @endforeach
                    </div>

                </div>
            </div>
        </div>
    </div>


@endsection