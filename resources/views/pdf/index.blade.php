<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <link type="text/css" media="all" rel="stylesheet" href="{{ public_path('css/app.css') }}">
        <style>
            .pdf-header h4 {
                top: 18px;
            }
            .contact-info span{
                position: relative;
                top: 0;
            }
            .price-square{
                margin-top: 15px;
                color: #0000ff;
            }
        </style>
        <title></title>
    </head>
    <body>
        <div class="pdf-header">
            <div class="logo-box">
                <img src="{{ public_path('/img/logo_blue.svg') }}" alt=""/>
            </div>
            <div class="object-info">
                <span class="object-type">{{$product->name}}</span>
            </div>
            <div class="price-box">
                <h4 class="price">{{$product->price}}&#8364;</h4>

                @if(!empty($product->price_square))
                    <div class="price-square">{{ $product->price_square }}&#8364;/{{trans('common.meter_short')}}<sup>2</sup></div>
                @endif
            </div>
        </div>
        <div class="pdf-body">
            <div class="upper-images">
                <div class="big-photo">
                    @isset($product->photos[0]->source)
                        <img src="{{ public_path('/uploaded/' . $product->photos[0]->source) }}" alt=""/>
                    @endisset
                </div>
            </div>
            <div class="offer-type">
                @if($product->type)
                <span class="offer">@lang('common.rent')</span>
                <span>www.realist.md</span>
                <span class="offer">@lang('common.rent')</span>
                @else
                    <span class="offer">@lang('common.sale')</span>
                    <span>www.realist.md</span>
                    <span class="offer">@lang('common.sale')</span>
                @endif
            </div>
        </div>
        <div class="pdf-footer">
            <div class="footer-object-info">
                <div class="upper-info">
                    <div class="location-info">
                        @isset($product->region->name)
                            <img src="{{ public_path('/img/icons/location.jpg') }}" alt="">
                            <span class="object-type">{{$product->region->name}}</span>
                        @endisset
                    </div>
                    <div class="space-info">
                        @isset($product->parameters[3]->id)
                            @if($product->parameters[3]->id == \App\Models\Parameter::ID_AREA)
                                <img src="{{ public_path('/img/icons/m2.jpg') }}" alt="">
                                <span>{{$product->parameters[3]->getSelectedValue()}}</span>
                            @endif
                        @endisset
                    </div>
                    <div class="level-info">
                        @isset($product->parameters[4])
                            <img src="{{ public_path('/img/icons/stairs.jpg') }}" alt="">
                            <span>{{$product->parameters[4]->getSelectedValue()}}</span>
                        @endisset
                    </div>
                    @if($product->agent)
                        <div class="contact-info">
                            <span>{{ $product->agent->getPhoneFormatted() ?? '' }}</span>
                            <p style="font-size: 16px;">@lang('common.call_center_num')</p>
                        </div>
                    @endif
                </div>
                <div class="bottom-info">
                    <ul>
                        @foreach(explode('<p>',$product->description) as $row)
                            @if(($loop->iteration<=15 || $loop->iteration>=18) && !empty($row))
                                <li>{!!  strip_tags($row) !!}</li>
                            @elseif($loop->iteration==16)
                                <div class="page-break"></div>
                            @elseif($loop->iteration==17)
                                <div class="divider"></div>
                            @endif
                        @endforeach
                    </ul>

                </div>


                <div class="page-break"></div>

                <ul class="rest-imgs-list">
                    @foreach ($product->photos as $photo)
                        @if ($loop->index == 0)
                            @continue
                        @endif
                        <li>
                            <img src="{{ public_path('/uploaded/' . $photo->source) }}" alt="Img">
                        </li>
                    @endforeach
                </ul>

            </div>


        </div>
    </body>
</html>