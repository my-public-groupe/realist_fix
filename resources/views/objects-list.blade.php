@extends('body')
@section('centerbox')
    <section class="bread-crumbs">
        <div class="container">
            <span>@lang('common.youre_here'): <a href="{{route('index')}}">@lang('common.main')</a> / </span>
            @if(in_array(Route::currentRouteName(), ['rent-category', 'rent']))
                <span class="active">@lang('common.rent')</span>
            @elseif(in_array(Route::currentRouteName(), ['sale-category', 'sale']))
                <span class="active">@lang('common.sale')</span>
            @endif
        </div>
    </section>

    <section class="offers-content">
        <div class="container">
            <div class="section-title">
                @if(Route::currentRouteName() == 'rent-category')
                    <h1 class="text">@lang('common.rent_offers') {{ $category->name }}</h1>
                @elseif(Route::currentRouteName() == 'rent')
                    <h1 class="text">@lang('common.rent_offers')</h1>
                @elseif(Route::currentRouteName() == 'sale-category')
                    <h1 class="text">@lang('common.sale_offers') {{ $category->name }}</h1>
                @elseif(Route::currentRouteName() == 'sale')
                    <h1 class="text">@lang('common.sale_offers')</h1>
                @endif

                <img src="images/timer.png" alt="" style="display: none;">
            </div>

            @if(Route::currentRouteName() == 'rent-category')
                <objects-filter
                        category_filters_url="{{route('get-category-filters', $category->id)}}"
                        category_objects_url="{{route('get-objects', 'rent')}}"
                        category_id="{{$category->id}}"
                        category_name="{{$category->name}}"
                        :sorting='@json(config('custom.sorting'))'
                        :is_mobile='@json(\App\Http\Controllers\Controller::isMobile())'>
                </objects-filter>
            @elseif(Route::currentRouteName() == 'rent')
                <objects-list-category
                    category_objects_url="{{route('get-objects', 'rent')}}"
                    :categories='@json($categories)'>
                </objects-list-category>
            @elseif(Route::currentRouteName() == 'sale-category')
                <objects-filter
                        category_filters_url="{{route('get-category-filters', $category->id)}}"
                        category_objects_url="{{route('get-objects', 'sale')}}"
                        category_id="{{$category->id}}"
                        category_name="{{$category->name}}"
                        :sorting='@json(config('custom.sorting'))'
                        :is_mobile='@json(\App\Http\Controllers\Controller::isMobile())'>
                </objects-filter>
            @elseif(Route::currentRouteName() == 'sale')
                <objects-list-category
                        category_objects_url="{{route('get-objects', 'sale')}}"
                        :categories='@json($categories)'>
                </objects-list-category>
            @endif
        </div>
    </section>

    @include('partials.order-call-block')

    @include('partials.news-block')
@endsection