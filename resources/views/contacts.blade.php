@extends('body')
@section('centerbox')
    <sections class="contacts single-page">
        <div class="bread-crumbs">
            <div class="container">
                <span>@lang('common.youre_here'): <a href="{{route('index')}}">@lang('common.main')</a> / </span>
                <span class="active">@lang('common.contacts')</span>
            </div>
        </div>
        <div class="container">
            <div class="about-us-title page-title">
                @lang('common.contacts')
            </div>
            <div class="row">
                <div class="contacts-tabs">
                    <div class="tab-item col-md-6 active" id="call">
                        <span class="call">@lang('common.contacts')</span>
                        <img src="images/active-tab.png" alt="" class="active-tab">
                    </div>
                    <div class="tab-item col-md-6" id="mail">
                        <span class="mail">@lang('common.leave_message')</span>
                        <img src="images/active-tab.png" alt="" class="active-tab">
                    </div>
                </div>
                <div class="contacts-tab-content">
                    <div class="tab-content-item col-md-12 show-content" id="callTabContent">
                        <div class="row">
                            <div class="col-md-5">
                                <div class="about-us-title">
                                    @lang('common.realist_offices')
                                </div>
                                <details class="open-details" open id="chisinau">
                                    <summary>
                                        <div class="summary-title">@lang('common.office_chisinau')</div>
                                        <div class="summary-subtitle">@lang('common.office_chisinau_address')</div>
                                        <img src="images/contact-active.png" alt="" class="active-contact-tab">
                                    </summary>
                                    <ul>
                                        <li class="contact-call contanct-ico">
                                            <a href="tel:@lang('common.call_center_num')">@lang('common.call_center_num')</a>
                                        </li>
                                        <li class="contact-mail contanct-ico">
                                            <a href="mailto:@lang('common.site_email')">@lang('common.site_email')</a>
                                        </li>
                                        <li class="clock contanct-ico">
                                            <a href="#">
                                                @lang('common.time_val_1')<br>
                                                @lang('common.time_val_2')
                                            </a>
                                        </li>
                                    </ul>
                                </details>

                                <details id="balti">
                                    <summary>
                                        <div class="summary-title">@lang('common.office_balti')</div>
                                        <div class="summary-subtitle">@lang('common.office_balti_address')</div>
                                        <img src="images/contact-active.png" alt="" class="active-contact-tab">
                                    </summary>
                                    <ul>
                                        <li class="contact-call contanct-ico">
                                            <a href="tel:@lang('common.office_balti_phone')">@lang('common.office_balti_phone')</a>
                                        </li>
                                        <li class="contact-mail contanct-ico">
                                            <a href="mailto:@lang('common.office_balti_email')">@lang('common.office_balti_email')</a>
                                        </li>
                                        <li class="clock contanct-ico">
                                            <a href="#">
                                                @lang('common.office_balti_time_val_1')<br>
                                                @lang('common.office_balti_time_val_2')
                                            </a>
                                        </li>
                                    </ul>
                                </details>
                            </div>
                            <div class="col-md-7 map-social-info">
                                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d894.1565496398708!2d28.822221875576687!3d47.020885665538714!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x40c97c2d4ce45acb%3A0x78067c4cd3b2247a!2zU3RyYWRhIE1paGFpbCBLb2fEg2xuaWNlYW51IDczLCBDaGnImWluxIN1LCDQnNC-0LvQtNCw0LLQuNGP!5e0!3m2!1sru!2s!4v1659707184325!5m2!1sru!2s" width=100% height=100% style="border:0;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade" id="chisinauChild" class="show-map"></iframe>
                                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2681.69763354832!2d27.93033831593463!3d47.767918979195706!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x40cb6178fb4eaa53%3A0x24a0754f1e8ebaed!2sRealist%20Estate%20Agency%20B%C4%83l%C8%9Bi!5e0!3m2!1sen!2s!4v1652781032051!5m2!1sen!2s" width=100% height=100% style="border:0;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade" id="baltiChild"></iframe>
                                    <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m12!1m3!1d3213.027140591798!2d28.81762448549864!3d47.376372880515554!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!5e0!3m2!1sen!2s!4v1652781172297!5m2!1sen!2s" width=100% height=100% style="border:0;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade" id="orheiChild" ></iframe>
                                <div class="media-container">
                                    <div class="social-media messages">
                                        <div class="social-media-title">
                                            @lang('common.our_messengers')
                                        </div>
                                        <ul>
                                            <li><a href="@lang('common.viber_link')" target="_blank" class="viber"></a></li>
                                            <li><a href="@lang('common.telegram_link')" target="_blank" class="telegram"></a></li>
                                            <li><a href="@lang('common.messenger_link')" target="_blank" class="messenger"></a></li>
                                            <li><a href="@lang('common.whatsapp_link')" target="_blank" class="whats"></a></li>
                                        </ul>
                                    </div>

                                    <div class="social-media social-networks">
                                        <div class="social-media-title">
                                            @lang('common.our_socials')
                                        </div>
                                        <ul>
                                            <li><a href="@lang('common.fb_link')" target="_blank" class="fb"></a></li>
                                            <li><a href="@lang('common.insta_link')" target="_blank" class="inst"></a></li>
                                            <li><a href="@lang('common.linkedin_link')" target="_blank" class="linkedin"></a></li>
                                            <li><a href="@lang('common.tiktok_link')" target="_blank" class="tiktok"></a></li>
                                            <li><a href="@lang('common.yt_link')" target="_blank" class="youtube"></a></li>
                                        </ul>
                                    </div>
                                </div>

                            </div>
                            <div class="clear"></div>
                        </div>
                        <div class="clear"></div>
                    </div>
                    <div class="tab-content-item col-md-12 " id="mailTabContent">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="about-us-title">
                                    @lang('common.leave_message')
                                </div>
                                <form class="contact-form" id="send-contact-form">
                                    {{ csrf_field() }}

                                    <div class="input-form-label">
                                        <input type="text" name="name" placeholder="@lang('common.name')" required>
                                    </div>

                                    <div class="input-form-label">
                                        <input type="text" name="phone" placeholder="@lang('common.phone')" required>
                                    </div>

                                    <div class="input-form-label">
                                        <input type="email" name="email" placeholder="Email" required>
                                        <div class="error-text"></div>
                                    </div>

                                    <div class="input-form-label">
                                        <textarea name="question" id="" cols="30" rows="10" placeholder="@lang('common.message')" required></textarea>
                                    </div>

                                    <div class="order-call-note">
                                        {!! trans('common.personal_details_allow', ['link' => route('privacy-policy')]) !!}
                                    </div>
                                    <button type="submit">@lang('common.send')</button>
                                </form>
                            </div>
                            <div class="col-md-6 form-info-block">
                                <img src="images/contacts-logo.png" alt="">
                                <div class="about-us-title">
                                    @lang('common.useful_info_text1')
                                </div>
                                <div class="about-us-title">
                                    @lang('common.useful_info_text2')
                                </div>
                                <div class="form-title">
                                    @lang('common.useful_info_text3')
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </sections>
    <div class="contacts-tab-content adaptive-widget">
        <div class="map-social-info">
            <div class="adaptive media-container">
                <div class="social-media messages">
                    <div class="social-media-title">
                        Мессенджеры, связь в один клик:
                    </div>
                    <ul>
                        <li><a href="#" class="viber"></a></li>
                        <li><a href="#" class="telegram"></a></li>
                        <li><a href="#" class="messenger"></a></li>
                        <li><a href="#" class="whats"></a></li>
                    </ul>
                </div>

                <div class="social-media social-networks">
                    <div class="social-media-title">
                        Следите за нами в соц. сетях:
                    </div>
                    <ul>
                        <li><a href="#" class="fb"></a></li>
                        <li><a href="#" class="inst"></a></li>
                        <li><a href="#" class="linkedin"></a></li>
                        <li><a href="#" class="tiktok"></a></li>
                        <li><a href="#" class="youtube"></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    @include('partials.news-block')
@endsection
@section('styles')
    <style>
        .contacts-tab-content .contanct-ico {
            margin: 10px 25px !important;
        }
        .contanct-ico{
            padding-left: 25px;
        }
    </style>
@endsection