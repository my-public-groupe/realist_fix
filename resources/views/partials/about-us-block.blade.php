<section class="about-us">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <div class="about-us-title">
                    @lang('common.about_us')
                </div>
                <div class="about-us-description">
                    <p>
                        @lang('common.about_us_text')
                    </p>


                    <p>
                        @lang('common.about_us_text2')
                    </p>

                    <div class="container-line-bg">
                        <img src="images/about-us-bg2.png" alt="" class="about-us-line">
                    </div>
                    <a href="{{route('about')}}">
                        <button>
                            
                            @lang('common.learn_more')
                        </button>
                    </a>

                </div>
            </div>
            <div class="col-md-6">
                <iframe height="315" src="@lang('common.about_us_youtube')" title="YouTube"
                        frameborder="0"
                        allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                        allowfullscreen></iframe>
                <div class="more-vacancies">
                    <div class="vacancies-title">@lang('common.work_with_us')</div>
                    <a href="{{route('careers')}}" class="read-more">@lang('common.careers')</a>

                    <!--@TODO взять модальное окно отсюда-->
                    <!--<span class="read-more">@lang('common.careers')</span>-->
                </div>

            </div>
        </div>
    </div>
</section>