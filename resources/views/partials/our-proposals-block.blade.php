<section class="our-proposals">
    <div class="container">
        <div class="section-title">
            <div class="text">@lang('common.our_offers')</div>
            <div class="additional-block">
                <div class="tooltip-custom">
                    <a class="zero-commision">@lang('common.zero_comission')</a>
                </div>
            </div>
        </div>
        <div class="row">
            @foreach ($categories as $category)
                @if ($category->id !== 9)
                    <div class="section-container col-md-4 col-lg-3">
                        <div class="proposal-container">
                            <div class="proposal-title"> {{$category->name}}</div>
                            <div class="lazyload proposal-image" data-bg="{{ url('uploaded/' . $category->mainphoto()) }}">
                            </div>
                            <div class="proposal-buttons">
                                <a class="btn-regular m-right"
                                href="{{ route('rent-category', $category->slug) }}">@lang('common.rent')</a>
                                <a class="btn-regular btn-blue"
                                href="{{ route('sale-category', $category->slug) }}">@lang('common.sale')</a>
                            </div>
                        </div>
                    </div>
                @endif
            @endforeach
        </div>
    </div>
</section>

