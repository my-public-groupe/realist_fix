<section class="slider">
    <div class="container">
        <div class="carousel-container">
            <div class="navigation-arrows">
                @if($slider->count() > 1)
                    <a href="#" class="jcarousel-testimonials-control-next" data-jcarouselcontrol="true"></a>
                    <a href="#" class="jcarousel-testimonials-control-prev" data-jcarouselcontrol="true"></a>
                @endif
            </div>
            <div class="jcarousel-wrapper">
                <div class="jcarousel-testimonials" data-jcarousel="true">
                    <ul>
                        @foreach($slider as $slide)
                            <li class="slide-width">
                                @if(!empty($slide->getBtnLink()))
                                    <a href="{{$slide->getBtnLink()}}">
                                        <div class="slider-container"
                                             style="background-image:url('uploaded/{{$slide->getMainPhoto()}}')">
                                        </div>
                                    </a>
                                @else
                                    <div class="slider-container"
                                         style="background-image:url('uploaded/{{$slide->getMainPhoto()}}')">
                                    </div>
                                @endif
                            </li>
                        @endforeach
                    </ul>
                </div>
            </div>

        </div>
    </div>

</section>