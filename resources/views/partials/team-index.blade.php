<section class="team-index">
	<div class="container">
		<div class="row">
			<div class="col-md-12 section-title">
				<p class="text">@lang('common.our_agents')</p>
				<a href="{{route('our-team')}}" class="additional-block"> @lang('common.all_agents') <img src="./images/arrow-right-gray.png" alt=""></a>
			</div>
		</div>
		<div class="row">
			@foreach($agents as $agent)
				@include('partials.one-employee')
			@endforeach
		</div>
	</div>
</section>