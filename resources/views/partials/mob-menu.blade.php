<vk-offcanvas flipped overlay :show.sync="show">
    <vk-offcanvas-close @click="show = false"></vk-offcanvas-close>

    <div class="mob-menu">
        <ul>
            <li>
                <a href="{{ route('rent') }}" class="link link--white">@lang('common.rent')</a>
            </li>
            <li>
                <a href="{{ route('sale') }}" class="link link--white">@lang('common.sale')</a>
            </li>
            <li>
                <a href="{{ route('experts') }}" class="link link--white">@lang('common.experts')</a>
            </li>
            <li>
                <a href="{{ route('investors') }}" class="link link--white">@lang('common.for_investors')</a>
            </li>
            <li>
                <a href="{{ route('developers') }}" class="link link--white">@lang('common.for_property_developers')</a>
            </li>



            <li class="mob-menu__separator">
                <hr>
            </li>

          {{--  <li>
                <a href="{{ route('projects') }}" class="link link--white">@lang('common.projects')</a>
            </li>--}}

            <li>
                <a href="{{ route('advices') }}" class="link link--white">@lang('common.advice_short')</a>
            </li>
            @if (\App\Helpers\Helper::isAllowedIp())
                <li>
                    <a href="{{ route('team') }}" class="link link--white">@lang('common.team')</a>
                </li>
            @endif
            <li>
                <a href="{{ route('careers') }}" class="link link--white">@lang('common.careers')</a>
            </li>
            <li>
                <a href="{{ route('mission-page') }}" class="link link--white">@lang('common.mission')</a>
            </li>
            <li>
                <a href="{{ route('content', 'contacts') }}" class="link link--white">@lang('common.contacts')</a>
            </li>



            <li class="mob-menu__lang-item">
                <a class="link link--white @if(app()->getLocale() == 'ro') current @endif" href="/ro/{{ $cur_link }}">Ro</a>
                <a class="link link--white @if(app()->getLocale() == 'ru') current @endif" href="/ru/{{ $cur_link }}">Ru</a>
                <a class="link link--white @if(app()->getLocale() == 'en') current @endif" href="/en/{{ $cur_link }}">En</a>
            </li>
        </ul>
    </div>

</vk-offcanvas>
