<div class="call @if(\Request::route()->getName() == 'product') special-order @endif">
    <transition name="slide-fade">
        <div v-show="!callOpened" @click="callOpened = !callOpened">
            <img src="images/svg/dialog-assistant-icon.svg" alt="" id="setMessages">
        </div>
    </transition>
    <transition name="bounce">
        <section class="call__content" v-show="callOpened" style="display:none;">
            <div class="call__container">
                <button class="call__inside-btn" @click="callOpened = !callOpened">
                    <img src="images/chevron-white-top.svg">
                </button>
                <div class="call-row">
                    <div class="call__img-wrap">
                        <img src="images/girl.jpg">
                    </div>
                    <div class="call__main">
                        <h3 class="call__title">
                            Call Center
                        </h3>
                        <a href="tel:@lang('common.call_center_num')" class="call__phone">@lang('common.call_center_num')</a>
                        <ul class="rieltor-social">
                            <li><a href="@lang('common.viber_link')" target="_blank" class="rieltor-viber"></a></li>
                            <li><a href="@lang('common.telegram_link')" target="_blank" class="rieltor-telegram"></a></li>
                            <li><a href="@lang('common.messenger_link')" target="_blank" class="rieltor-messenger"></a></li>
                            <li><a href="@lang('common.whatsapp_link')" target="_blank" class="rieltor-whatsup"></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </section>
    </transition>
</div>