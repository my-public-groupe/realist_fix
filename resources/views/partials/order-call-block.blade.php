<section class="order-call" id="orderCallContainer">
    <div class="container">
        <div class="row">
            <div class="order-call-container">
                <div class="order-call-title">
                    @lang('common.help_choose_house') <br /> @lang('common.help_choose_house2')
                </div>
                <div class="order-call-form">
                    <form id="call-order-form">
                        {{ csrf_field() }}
                        <input type="text" name="name" placeholder="@lang('common.name')" required/>
                        <input type="text" name="phone" placeholder="@lang('common.phone')" required/>
                        <div class="order-call-note container-for-mobile">
                            {!! trans('common.personal_details_allow', ['link' => route('privacy-policy')]) !!}
                        </div>
                        <button type="submit">@lang('common.call_now')</button>
                    </form>

                    <div class="order-call-note">
                        {!! trans('common.personal_details_allow', ['link' => route('privacy-policy')]) !!}
                    </div>
                </div>

            </div>
        </div>
    </div>
</section>