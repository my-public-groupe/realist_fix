<section class="header-menu">
    <div class="cookie-container" style="display: none;">
        <div>
            <img src="./images/svg/cookie-icon.svg" alt="Cookie Icon" class="cookie-icon">
        </div>
        <div class="cookie-text">
            <div> Для правильной работы сайта, мы используем файлы <u>cookies</u>. </div>
        </div>
        <a href="#" class="close-icon">
            <img src="./images/close-icon.png" alt="Close Icon">
        </a>
    </div>
    <div class="mobile-phone-container want-too">
        <img src="images/svg/call-mobile.svg" alt="" id="orderCall">
        <a href="tel:@lang('common.call_center_num')" class="contact-phone pouring">@lang('common.call_center_num')</a>
    </div>
    <div class="container">
        <nav class="navbar navbar-expand-lg ftco_navbar ftco-navbar-light" id="ftco-navbar">
            <div class="container">
                <div class="mobile-navigation">
                    <div class="search-container mobile-search show-mobile-nav">
                        <img src="images/svg/search-icon-header.svg" onclick="toggleSearch();">
                    </div>

                    <div class="languages-container has-submenu mobile-languages-submenu">
                        <img src="images/svg/icon-globe.svg" alt="">
                        {{ ucfirst(app()->getLocale()) }}
                        <div class="languages-submenu">
                            @php
                                $locales = config('custom.header_locales');
                            @endphp
                            @foreach($locales as $prefix => $locale)
                                @if($prefix == "ro")
                                    <label class="lang-container">
                                        <input type="radio"
                                               onchange="switchLang(this, '{{$prefix}}', '{{$cur_link}}');" class="lang-switch" @if(app()->getLocale() == $prefix)checked @endif>
                                        <span class="checkmark"></span> {{ $locale }}
                                    </label>
                                @else
                                    <label class="lang-container">
                                        <input type="radio"
                                               onchange="switchLang(this, '{{$prefix}}', '{{$cur_link}}');" @if(app()->getLocale() == $prefix)checked @endif>
                                        <span class="checkmark"></span> {{ $locale }}
                                    </label>
                                @endif
                            @endforeach
                        </div>
                    </div>

                </div>


                <a class="navbar-brand" href="{{route('index')}}">
                    <img src="./images/logo.svg" alt=""  class="logo-simple show-logo">
                    <img src="./images/svg/logo-mobile.svg" alt=""  class="logo-mobile">
                </a>
                <button class="navbar-toggler" type="button" data-target="#ftco-nav"
                        aria-controls="ftco-nav" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="burger show-icon"> 
                        <img src="./images/svg/menu-burger.svg" alt="">
                    </span>
                    <span class="close-mobile-menu">
                        <div class="close-btn">
                            <span class="close-text">Закрыть</span>
                            <img src="images/svg/icon-close.svg" alt="">
                        </div>
                    </span>

                </button>
                <div class="collapse navbar-collapse" id="ftco-nav">
                    <ul class="navbar-nav ml-auto mr-md-3">
                        <li class="nav-item has-submenu " id="">
                            <a href="javascript:void(0);" class="nav-link">@lang('common.rent')</a>
                            <ul class="sub-menu">
                                @foreach($categories as $category)
                                    <li> <a href="{{route('rent-category', $category->slug)}}">{{$category->name}}</a></li>
                                @endforeach
                            </ul>
                        </li>
                        <li class="nav-item has-submenu ">
                            <a href="javascript:void(0);" class="nav-link">@lang('common.sale')</a>
                            <ul class="sub-menu">
                                @foreach($categories as $category)
                                    <li> <a href="{{route('sale-category', $category->slug)}}">{{$category->name}}</a></li>
                                @endforeach
                            </ul>
                        </li>
                        <li class="nav-item">
                            <a href="{{route('services')}}" class="nav-link">@lang('common.services')</a>
                        </li>
                        <li class="nav-item has-submenu">
                            <a href="javascript:void(0);" class="nav-link">@lang('common.about_us')</a>
                            <ul class="sub-menu">
                                <li> <a href="{{route('about')}}">@lang('common.about_company')</a></li>
                                <li> <a href="{{route('about', 'scrollTo=vision')}}">@lang('common.our_vision')</a></li>
                                <!--@TODO временно скрыл-->
                                <!--<li> <a href="{{route('about', 'scrollTo=reviews')}}">@lang('common.reviews')</a></li>-->
                                <li> <a href="{{route('experts')}}">@lang('common.our_experts')</a></li>
                            </ul>
                        </li>
                        <li class="nav-item has-submenu ">
                            <a href="javascript:void(0);" class="nav-link">@lang('common.useful')</a>
                            <ul class="sub-menu">
                                <li> <a href="{{route('advices')}}">@lang('common.advice_short')</a></li>
                                <li> <a href="{{route('investors')}}">@lang('common.for_investors')</a></li>
                                <li> <a href="{{route('developers')}}">@lang('common.for_property_developers')</a></li>
                            </ul>
                        </li>
                        <li class="nav-item"><a href="{{route('careers')}}" class="nav-link">@lang('common.careers')</a></li>
                        <li class="nav-item active"><a href="{{route('contacts')}}" class="nav-link">@lang('common.contacts')</a></li>
                    </ul>
                </div>
                <div class="info-container">
                    <div class="search-container">
                        <img src="images/svg/search-icon-header.svg" onclick="toggleSearch();">
                    </div>
                    <div class="languages-container has-submenu">
                        <img src="images/svg/icon-globe.svg" alt="">
                        {{ ucfirst(app()->getLocale()) }}
                        <div class="languages-submenu">
                            @php
                                $locales = config('custom.header_locales');
                            @endphp
                            @foreach($locales as $prefix => $locale)
                                @if($prefix == "ro")
                                    <label class="lang-container">
                                        <input type="radio"
                                               onchange="switchLang(this, '{{$prefix}}', '{{$cur_link}}');" class="lang-switch" @if(app()->getLocale() == $prefix)checked @endif>
                                        <span class="checkmark"></span> {{ $locale }}
                                    </label>
                                @else
                                    <label class="lang-container">
                                        <input type="radio"
                                               onchange="switchLang(this, '{{$prefix}}', '{{$cur_link}}');" @if(app()->getLocale() == $prefix)checked @endif>
                                        <span class="checkmark"></span> {{ $locale }}
                                    </label>
                                @endif
                            @endforeach
                        </div>
                    </div>
                    <div class="contact-time-container">
                        <img src="images/svg/phone-icon-header.svg" alt="" id="orderCall">
                        <a href="tel:@lang('common.call_center_num')" class="contact-phone">@lang('common.call_center_num')</a>
                        <div class="contact-time">
                            <div class="online-icon"></div>
                            <span>@lang('common.main_schedule2')</span>
                        </div>
                    </div>
                </div>
            </div>
        </nav>
        <!-- END nav -->
    </div>
</section>
