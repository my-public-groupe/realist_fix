<section class="newslatter">
    <div class="container">
        <div class="row">
            <div class="newslatter-container">
                <div class="newslatter-title">
                    @lang('common.newsletter_title')
                </div>
                <div class="newslatter-subtitle">
                    @lang('common.newsletter_description')
                </div>

                <form id="newsletter-form">
                    {{ csrf_field() }}
                    <div class="newslatter-form">
                        <input type="email" name="email" placeholder="Email" required/>

                        <div class="newslatter-note container-for-mobile">
                            {!! trans('common.personal_details_allow', ['link' => route('privacy-policy')]) !!}
                        </div>

                        <button type="submit">@lang('common.subscribe')</button>
                    </div>
                </form>
                <div class="newslatter-note">
                    {!! trans('common.personal_details_allow', ['link' => route('privacy-policy')]) !!}
                </div>
            </div>
        </div>
    </div>
</section>