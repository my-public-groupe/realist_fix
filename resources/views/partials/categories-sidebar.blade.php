    <section class="objects-list__filter">
        <div class="filter-element">
            <div class="filter-element__header">
                <h2 class="filter-element__title">
                    <span>@lang('common.categories')</span>

                </h2>
            </div>

            <div class="filter__element-body toggle-body special-element">
                <ul class="filter-element__items-list">
                    @foreach($categories as $category)
                        <li class="filter-element__items-list-item">
                            <h3 class="filter-element__text filter-element__subtitle">
                                <a href="{{ route("{$type}-category", $category->slug) }}" class="filter-element__active-inside">{{ $category->name }}</a>
                            </h3>
                        </li>
                    @endforeach
                </ul>
            </div>

        </div>

    </section>