<div class="partners">
    <section class="x-container">
        <div class="x-row">
            <div class="x-col x-col--full">
                <h2 class=" title title--new-big title--blue">@lang('common.our_partners')</h2>

                <partners-slider-component inline-template :list="{{$partners}}">
                    <div class="partners__slider-wrapper">
                        <carousel :lazy-load="true"
                                  :nav-text="[
                            '<img class=\'partners__slider-arrow\' src=\'/img/icons/button-chevron.svg\'>',
                            '<img class=\'partners__slider-arrow\' src=\'/img/icons/button-chevron.svg\'>'
                            ]"
                                  :responsive="{
                                0:{
                                    items:2,
                                    loop:true,
                                    nav:false,
                                    autoplay:true,
                                    autoplaySeed:false,
                                    margin: 16
                                },
                                600:{
                                    items:4,
                                    nav:true,
                                    margin: 16

                                 },
                                 990:{
                                    items: 5,
                                    margin: 16
                                  }
                                }">
                            @foreach ($partners as $item)
                                <div class="partners__slider-item">
                                    <a href="javascript:void(0)">
                                        <img src="/uploaded/{{ $item->mainPhoto() }}" alt="">
                                    </a>
                                </div>
                            @endforeach
                        </carousel>
                    </div>
                </partners-slider-component>


            </div>
        </div>
    </section>
</div>
