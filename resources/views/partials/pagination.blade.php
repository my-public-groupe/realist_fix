@if (isset($paginator) && $paginator->hasPages())
    <div class="pagination">
        <ul class="pagination-container">
            @if ($paginator->onFirstPage())
                <li class="pag-item">
                    <img src="images/svg/pag-prev.svg">
                </li>
            @else
                <li class="pag-item prev">
                    <a href="{{ $paginator->previousPageUrl() }}"><img src="images/svg/pag-prev.svg"></a>
                </li>
            @endif
            @foreach ($elements as $element)
                @if (is_array($element))
                    @foreach ($element as $page => $url)
                        @if ($page == $paginator->currentPage())
                            <li class="pag-item active">{{$page}}</li>
                        @else
                            <li class="pag-item"><a href="{{ $url }}">{{$page}}</a></li>
                        @endif
                    @endforeach

                    @if ($paginator->hasMorePages())
                        <li class="pag-item next">
                            <a href="{{ $paginator->nextPageUrl() }}"><img src="images/svg/pag-next.svg"></a>
                        </li>
                    @endif
                @endif
            @endforeach
        </ul>
    </div>
@endif