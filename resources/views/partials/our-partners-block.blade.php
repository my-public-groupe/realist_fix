<section class="our-partners">
    <div class="container">
        <div class="section-title">
            <div class="text">@lang('common.our_partners')</div>
        </div>

        <div class="carousel-container">
            <div class="navigation-arrows">
                <a href="#" class="jcarousel-control-next" data-jcarouselcontrol="true"></a>
                <a href="#" class="jcarousel-control-prev" data-jcarouselcontrol="true"></a>
            </div>
            <div class="jcarousel-wrapper">
                <div class="jcarousel" data-jcarousel="true">
                    <ul>
                        @foreach($partners as $partner)
                            <li><img class="lazyload" data-src="uploaded/{{$partner->mainphoto()}}" alt="{{$partner->name}}"></li>
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>