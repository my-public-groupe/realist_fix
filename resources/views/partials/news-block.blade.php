<section class="news">
    <div class="container">
        <div class="section-title">
            <div class="text">@lang('common.news')</div>
            <div class="additional-block">
                <div class="additional-block"><a href="{{route('news')}}">@lang('common.all_news') <img
                                src="images/arrow-right-gray.png" alt=""></a>
                </div>
            </div>
        </div>

        <div class="row">
            @foreach($news as $n)
                <div class="col-md-4">
                    <a href="{{route('get-one-news', $n->slug)}}" class="news-item">
                        <div class="news-date">
                            {{$n->created_at->format('d.m.Y')}}
                        </div>
                        <div class="lazyload news-preview" data-bg="{{ url('uploaded/' . $n->mainphoto()) }}"></div>
                        <div class="news-title">
                            {{$n->name}}
                        </div>
                    </a>
                </div>
            @endforeach
        </div>
    </div>
</section>