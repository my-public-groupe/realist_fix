<section class="best-rent">
    <div class="container">

        <div class="section-title">
            <div class="text">@lang('common.best_rent_offers')</div>
            <a class="additional-block" href="{{ route('rent-category', 'exclusive') }}"> @lang('common.see_all_offers') <img src="./images/arrow-right-gray.png"
                                                                                    alt=""></a>
        </div>
        <div class="carousel-container">
            <div class="navigation-arrows">
                <a href="#" class="jcarousel-works-control-next" data-jcarouselcontrol="true"></a>
                <a href="#" class="jcarousel-works-control-prev" data-jcarouselcontrol="true"></a>
            </div>
            <div class="jcarousel-wrapper">
                <div class="jcarousel-works" data-jcarousel="true">
                    <ul>
                        @foreach($rent_offers as $object)
                            <li class="col-md-3">
                                <a href="{{route('product', $object->slug)}}">
                                    <div class="section-container">
                                        <div class="proposal-container">
                                            <div class="lazyload preview-image"
                                                 data-bg="{{ url('uploaded/' . $object->mainphoto()) }}">

                                                @if($object->photo_label > 0)
                                                    <img data-src="img/labels/{{$object->photo_label}}.png" class="lazyload photo-label photo-label-{{$object->photo_label}}">
                                                @endif
                                            </div>
                                            <div class="price-container">
                                                <div class="price">{{$object->price_text}}</div>

                                                @if(!empty($object->old_price_text))
                                                    <div class="discount">{{ $object->old_price_text }}</div>
                                                @endif

                                                @if(!empty($object->price_square))
                                                    <div class="price-square">{!! $object->price_square_text !!}</div>
                                                @endif
                                            </div>
                                            <div class="information">
                                                <div class="information-line">
                                                    <div class="{{config('custom.object_icons.place')}} {{$object->id}}"> {{$object->name}}</div>
                                                    <div class="@if($object->categories[0]->id <= 6)category-{{$object->categories[0]->id}} @endif"> {{$object->categories[0]->name}} </div>
                                                    <div class="{{config('custom.object_icons.area')}}"> {!! $object->area !!}</div>

                                                    @if($object->room != '')
                                                        <div class="{{config('custom.object_icons.room')}}"> {{ $object->room }} {{$object->getRoomQuantityText()}}</div>
                                                    @endif

                                                    @if($object->bathroom != '')
                                                        <div class="{{config('custom.object_icons.bathroom')}}">&nbsp;{{ $object->bathroom }} {{$object->getBathroomQuantityText()}}</div>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </li>
                        @endforeach
                    </ul>
                </div>
            </div>
            <div class="jcarousel-works-pagination"></div>
        </div>
    </div>
</section>
