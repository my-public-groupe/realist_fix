<div id="myCarousel-responsive" class="carousel slide" data-interval="false" data-touch="true" data-ride="carousel">
    <div id="myCarousel-responsive" class="carousel slide" data-interval="false" data-touch="true" data-ride="carousel">
        <div class="carousel-loading">
            <div class="spinner-border text-primary" role="status">
                <span class="visually-hidden"></span>
            </div>
        </div>
        <div class="carousel-inner" role="listbox">
            <!-- carousel items -->
        </div>
    </div>

    <div class="carousel-inner" role="listbox">
        @foreach($product->photos as $k => $photo)
            <div class="carousel-item item @if( $loop->first )active @endif">
                <div class="img-carousel-large-preview proposal-container preview-image" style="background-image: url('uploaded/thumbs/{{ $product->mainphotothumb($k) }}')" data-bs-img-index="{{ $loop->index }}">
                @if($product->photo_label > 0)
                        <img data-src="img/labels/{{ $product->photo_label }}.png" class="lazyload photo-label photo-label-{{ $product->photo_label }}" style="bottom: 0; right: 0; position: absolute;">
                    @endif 
                    <a data-fancybox="gallery" data-fancybox-index="{{ $loop->index }}" class="w-100 h-100 position-absolute" id="productMainImg" href="/uploaded/{{ $photo->source }}"></a>
                </div>
            </div>
        @endforeach

        <a class="left carousel-control" href="#myCarousel-responsive" role="button" data-slide="prev">
            <span class="glyphicon glyphicon-chevron-left jcarousel-control-prev-custom" aria-hidden="true">

            </span>
        </a>
        <a class="right carousel-control" href="#myCarousel-responsive" role="button" data-slide="next">
            <span class="glyphicon glyphicon-chevron-right jcarousel-control-next-custom" aria-hidden="true">
            </span>
        </a>
    </div>

    <div id="thumbCarousel-responsive">
        @foreach($product->photos as $k => $photo)

            @if($loop->index < 2) 
                <div data-target="#myCarousel-responsive" data-slide-to="{{ $loop->index }}" class="thumb">
                    <div class="lazyload img-carousel-thumb" data-bg="{{ url('uploaded/' . $photo->source) }}"></div>
                </div>
            @endif
            
            @if($loop->index == 2)
                @if ($loop->count > 3)
                <div data-target="#myCarousel-responsive" data-slide-to="{{ $loop->index }}" class="thumb">
                    <div class="additional-photos d-flex">{{ $loop->count - ($loop->index + 1) }}+</div>
                    <div class="lazyload img-carousel-thumb" data-bg="{{ url('uploaded/' . $photo->source) }}"></div>
                </div>
                @else
                <div data-target="#myCarousel-responsive" data-slide-to="{{ $loop->index }}" class="thumb">
                    <div class="lazyload img-carousel-thumb" data-bg="{{ url('uploaded/' . $photo->source) }}"></div>
                </div>
                @endif
            
            @endif
            
            @if($loop->index > 2)
                <div data-target="#myCarousel-responsive" data-slide-to="{{ $loop->index }}" class="thumb thumb-hide d-none">
                    <div class="lazyload img-carousel-thumb" data-bg="{{ url('uploaded/thumbs/' . $photo->source) }}"></div>
                </div>
            @endif

        @endforeach
    </div>
</div>