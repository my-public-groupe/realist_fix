<footer id="footer">
    <div class="container">
        <div class="row">
            <div class="col-lg-2 col-md-6 text-center w-100">
                <img src="./images/svg/footer-logo.svg" alt="">
            </div>
            <div class="col-xl-8 col-lg-7 d-lg-flex d-none justify-content-around flex-nowrap">
                <div class="footer-menu-container text-center">
                    <ul>
                        <li>
                            <a href="{{route('rent')}}">@lang('common.rent')</a>
                        </li>
                        <li>
                            <a href="{{route('sale')}}">@lang('common.sale')</a>
                        </li>
                        <li>
                            <a href="{{route('services')}}">@lang('common.services')</a>
                        </li>
                    </ul>
                </div>

                <div class="footer-menu-container text-center">
                    <ul>
                        <li>
                            <a href="{{route('about')}}">@lang('common.about_us')</a>
                        </li>
                        <li>
                            <a href="{{route('experts')}}">@lang('common.experts')</a>
                        </li>
                    </ul>
                </div>

                <div class="footer-menu-container text-center">
                    <ul>
                        <li>
                            <a href="{{route('advices')}}">@lang('common.advice_short')</a>
                        </li>
                        <li>
                            <a href="{{route('investors')}}">@lang('common.for_investors')</a>
                        </li>
                        <li>
                            <a href="{{route('developers')}}">@lang('common.for_property_developers')</a>
                        </li>
                    </ul>
                </div>

                <div class="footer-menu-container text-center">
                    <ul>
                        <li>
                            <a href="{{route('careers')}}">@lang('common.careers')</a>
                        </li>
                        <li>
                            <a href="{{route('news')}}">@lang('common.news')</a>
                        </li>
                    </ul>
                </div>

                <div class="footer-menu-container footer-contacts d-flex text-center justify-content-center">

                    <ul>
                        <li>
                            <a href="javascript:void(0);">@lang('common.contacts')</a>
                        </li>
                        <li class="contact-adress contanct-ico">
                            <a href="javascript:void(0);">{!! trans('common.main_address_value') !!}</a>
                        </li>
                        <li class="contact-mail contanct-ico">
                            <a href="mailto:@lang('common.site_email')">@lang('common.site_email')</a>
                        </li>
                        <li class="contact-call contanct-ico">
                            <a href="tel:@lang('common.call_center_num')">@lang('common.call_center_num')</a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="col-xl-2 col-lg-3 col-md-6 text-center">

<!--                <div class="collapse navbar-collapse" id="ftco-nav">
                    <ul class="navbar-nav ml-auto mr-md-3">
                        <li class="nav-item has-submenu " id="">
                            <span class="nav-link">Аренда</span>
                            <ul class="sub-menu">
                                <li><a href="{{route('rent')}}">Квартиры</a></li>
                                <li><a href="{{route('rent')}}">Дома</a></li>
                                <li><a href="{{route('rent')}}">Офисы</a></li>
                                <li><a href="{{route('rent')}}">Коммерческое</a></li>
                                <li><a href="{{route('rent')}}">Промышленное</a></li>
                                <li><a href="{{route('rent')}}">Участки</a></li>
                            </ul>
                        </li>
                        <li class="nav-item has-submenu ">
                            <span class="nav-link">Продажа</span>
                            <ul class="sub-menu">
                                <li><a href="{{route('sale')}}">Квартиры</a></li>
                                <li><a href="{{route('sale')}}">Дома</a></li>
                                <li><a href="{{route('sale')}}">Офисы</a></li>
                                <li><a href="{{route('sale')}}">Коммерческое</a></li>
                                <li><a href="{{route('sale')}}">Промышленное</a></li>
                                <li><a href="{{route('sale')}}">Участки</a></li>
                            </ul>
                        </li>
                        <li class="nav-item">
                            <span class="nav-link">Услуги</span>
                        </li>
                        <li class="nav-item has-submenu ">
                            <span class="nav-link">О нас</span>
                            <ul class="sub-menu">
                                <li><a href="#">Виденье</a></li>
                                <li><a href="#">Наша Команда</a></li>
                                <li><a href="#">Отзывы</a></li>
                                <li><a href="#">Наши эксперты</a></li>
                            </ul>
                        </li>
                        <li class="nav-item has-submenu ">
                            <span class="nav-link">Полезное</span>
                            <ul class="sub-menu">
                                <li><a href="{{route('advices')}}">Советы</a></li>
                                <li><a href="{{route('investors')}}">Инвесторам</a></li>
                                <li><a href="{{route('developers')}}">Застройщикам</a></li>
                            </ul>
                        </li>
                        <li class="nav-item"><a href="#" class="nav-link">Карьера</a></li>
                        <li class="nav-item active"><a href="#" class="nav-link">Контакты</a></li>
                    </ul>
                </div>-->

                <div class="social-media messages">
                    <div class="social-media-title">
                        @lang('common.our_messengers')
                    </div>
                    <ul class="social-block-style">
                        <li><a href="@lang('common.viber_link')" target="_blank" class="viber"></a></li>
                        <li><a href="@lang('common.telegram_link')" target="_blank" class="telegram"></a></li>
                        <li><a href="@lang('common.messenger_link')" target="_blank" class="messenger"></a></li>
                        <li><a href="@lang('common.whatsapp_link')" target="_blank" class="whats"></a></li>
                    </ul>
                </div>

                <div class="social-media social-networks">
                    <div class="social-media-title">
                        @lang('common.our_socials')
                    </div>
                    <ul class="social-block-style">
                        <li><a href="@lang('common.fb_link')" target="_blank" class="fb"></a></li>
                        <li><a href="@lang('common.insta_link')" target="_blank" class="inst"></a></li>
                        <li><a href="@lang('common.linkedin_link')" target="_blank" class="linkedin"></a></li>
                        <li><a href="@lang('common.tiktok_link')" target="_blank" class="tiktok"></a></li>
                        <li><a href="@lang('common.yt_link')" target="_blank" class="youtube"></a></li>
                    </ul>
                </div>
            </div>
        </div>


        <div class="copyright">
            <div class="all-rights">
                © 2018 - {{date('Y')}} Realist Estate Agency. @lang('common.copyright_value')
            </div>
            <div class="private-policy">
                <a href="{{ route('privacy-policy') }}">@lang('common.privacy')</a>
            </div>
        </div>
    </div>
    <div class="powered-by">
        <a href="https://xsort.md">Developed by Xsort Web Studio</a>
    </div>
</footer>