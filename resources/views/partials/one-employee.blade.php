<a class="d-block" href="{{ route('get-agent', $agent->id) }}">
    <div class="col-md-6 col-lg-4 col-xl-3 pe-auto">
        <div class="employee-item pe-auto">
            <div class="rieltor-info pe-auto">
                <div class="img-rieltor-info img-rieltor-info-main">
                    <img class="lazyload rieltor-info-img" data-src="uploaded/{{$agent->mainphoto()}}" alt="{{$agent->name}}">
                </div>
                <div class="block-contacte-agent">
                    <div class="rieltor-contacts">
                        <div class="rieltor-details rieltor-details-main">
                            <div class="rieltor-name">{{ $agent->name }}</div>
                            @if(!empty($agent->phone))
                            <a href="tel:{{$agent->phone}}" class="rieltor-phone">
                                <img class="rieltor-phone-img" src="images/contacts-call.png" alt="">
                                {{$agent->phone}}
                            </a>
                            @endif

                            @if(!empty($agent->params['email']))
                            <a href="mailto:{{ $agent->params['email'] }}" class="rieltor-email">
                                <img class="rieltor-phone-img" src="images/mail-icon.png" alt="">
                                {{ $agent->params['email'] }}
                            </a>
                            @endif
                        </div>
                    </div>
                    @if($agent->hasSocials())
                    <div class="rieltor-social-media rieltor-social-media-main">
                        <ul class="rieltor-social">
                            @if(!empty($agent->getViber()))
                            <a href="{{$agent->getViber()}}">
                                <li class="rieltor-viber d-flex align-items-center">
                                    <img class="rieltor-icon-img" src="images/rieltor-viber.png" alt="">
                                </li>
                            </a>
                            @endif

                            @if(!empty($agent->params['messenger']))
                            <a href="{{$agent->params['messenger']}}">
                                <li class="rieltor-messenger d-flex align-items-center">
                                    <img class="rieltor-icon-img" src="images/rieltor-messenger.png" alt="">
                                </li>
                            </a>
                            @endif

                            @if(!empty($agent->params['telegram']))
                            <a href="{{$agent->params['telegram']}}">
                                <li class="rieltor-telegram d-flex align-items-center">
                                    <img class="rieltor-icon-img" src="images/rieltor-telegram.png" alt="">
                                </li>
                            </a>
                            @endif

                            @if(!empty($agent->params['whatsapp']))
                            <a href="{{$agent->params['whatsapp']}}">
                                <li class="rieltor-whatsup d-flex align-items-center">
                                    <img class="rieltor-icon-img" src="images/rieltor-whatsup.png" alt="">
                                </li>
                            </a>
                            @endif
                        </ul>
                    </div>
                    @else
                    <div class="rieltor-social-media rieltor-social-media-main rieltor-social-media-none"></div>
                    @endif
                </div>
            </div>
        </div>
    </div>
</a>