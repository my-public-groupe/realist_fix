<div id="myCarousel" class="carousel slide" data-interval="false">
    <div class="carousel-inner" role="listbox">
        <div class="item active">
            <div class="img-carousel-large-preview" style="background-image: url('./images/about-us-carousel-1.png'); min-height: 320px;">
            </div>
        </div>
        <div class="item">
            <div class="img-carousel-large-preview" style="background-image: url('./images/about-us-carousel-2.png'); min-height: 320px; ">
            </div>
        </div>
        <div class="item">
            <div class="img-carousel-large-preview" style="background-image: url('./images/about-us-carousel-1.png'); min-height: 320px; ">
            </div>
        </div>
        <div class="item">
            <div class="img-carousel-large-preview" style="background-image: url('./images/about-us-carousel-2.png'); min-height: 320px; ">
            </div>
        </div>

        <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
            <span class="glyphicon glyphicon-chevron-left jcarousel-control-prev-custom" aria-hidden="true">

            </span>
        </a>
        <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
            <span class="glyphicon glyphicon-chevron-right jcarousel-control-next-custom" aria-hidden="true">
            </span>
        </a>
    </div>

    <div id="thumbCarousel">
        <div data-target="#myCarousel" data-slide-to="0" class="thumb">
            <div class="img-carousel-thumb" style="background-image: url('./images/about-us-carousel-1.png')"> </div>
        </div>
        <div data-target="#myCarousel" data-slide-to="1" class="thumb">
            <div class="img-carousel-thumb" style="background-image: url('./images/about-us-carousel-2.png')"> </div>
        </div>
        <div data-target="#myCarousel" data-slide-to="2" class="thumb">
            <div class="img-carousel-thumb" style="background-image: url('./images/about-us-carousel-1.png')"> </div>
        </div>
        <div data-target="#myCarousel" data-slide-to="3" class="thumb">
            <div class="additional-photos">4+</div>
            <div class="img-carousel-thumb" style="background-image: url('./images/about-us-carousel-2.png')"> </div>
        </div>
    </div>
</div>