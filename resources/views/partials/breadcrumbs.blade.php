<div class="bread-crumbs">
    <div class="container">
        <span>@lang('common.youre_here'): <a href="{{route('index')}}">@lang('common.main')</a> / </span>
        @if (isset($breadcrumbs))
            @foreach ($breadcrumbs as $name => $url)
                @if($loop->last)
                    <span class="active">{{$name}}</span>
                @else
                    <span><a href="{{$url}}">{{$name}}</a> / </span>
                @endif
            @endforeach
        @endif
    </div>
</div>