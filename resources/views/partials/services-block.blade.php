<section class="services">
    <div class="container">
        <div class="section-title">
            <div class="text">@lang('common.services')</div>
        </div>

        <div class="row">
            @foreach($services as $k => $service)
                @php
                    switch ($k){
                        case 1:
                            $route = route("experts", "scrollTo=service-0");
                            break;
                        case 2:
                            $route = route("investors");
                            break;
                        case 3:
                            $route = route("experts", "scrollTo=service-1");
                            break;
                        case 4:
                            $route = route("experts", "scrollTo=service-2");
                            break;
                        case 5:
                            $route = route("experts", "scrollTo=service-3");
                            break;
                        default:
                            $route = route("experts");
                            break;
                    }
                @endphp

            <div class="col-md-6 col-lg-3">
                @if(!$loop->last)
                    <a class="service-one-item" href="{{ $route }}">
                        <img src="images/svg/logo-long-for-bg-of-popup.svg" alt="{{$service->name}}">
                        <div class="service-title">{{$service->name}}</div>
                        <div class="service-description">{{$service->description_short}}
                        </div>

                        <span class="read-more">@lang('common.learn_more')</span>
                    </a>
                @else
                    <a class="service-one-item" href="javascript:void(0);">
                        <img src="images/svg/logo-long-for-bg-of-popup.svg" alt="{{$service->name}}">
                        <div class="service-title">{{$service->name}}</div>
                        <div class="service-description">{{$service->description_short}}
                        </div>
                    </a>
                @endif
            </div>
            @endforeach

            <div class="col-md-6 col-lg-3">
                <div class="service-one-item fill-color">
                    <img src="images/svg/logo-long-for-bg-of-popup.svg" alt="Realist">
                    <div class="service-title">
                        @lang('common.call_realist') <br />
                        @lang('common.call_realist2')
                    </div>

                    <a href="tel:@lang('common.call_center_num')" class="call-service">@lang('common.call_center_num')</a>
                </div>
            </div>

        </div>
    </div>

</section>