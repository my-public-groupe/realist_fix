<section class="consulting">
    <div class="container">
        <div class="row">
            <div class="consulting-container">
                <div class="consulting-title">
                    @lang('common.free_consultation')
                </div>
                <div class="consulting-subtitle">
                    @lang('common.consultation_text')
                    <a href="tel:+37378206206">+373 78 206 206</a>
                </div>
                <form id="consultation-form">
                    <div class="consulting-form">
                        {{ csrf_field() }}

                        <input type="text" name="name" placeholder="@lang('common.name')" required/>
                        <input type="text" name="phone" placeholder="@lang('common.phone')" required/>
                        <div class="consulting-note container-for-mobile">
                            {!! trans('common.personal_details_allow', ['link' => route('privacy-policy')]) !!}
                        </div>

                        <button type="submit">@lang('common.send')</button>
                    </div>
                </form>
                <div class="consulting-note">
                    {!! trans('common.personal_details_allow', ['link' => route('privacy-policy')]) !!}
                </div>
            </div>
        </div>
    </div>
</section>