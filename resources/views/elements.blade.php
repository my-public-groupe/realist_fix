@extends('body')

@section('centerbox')

    <div class="elements">
        <div class="x-container" style="margin-top: 50px;">
            <h1 style="text-align: center; font-size: 50px;"><mark>TEXT</mark></h1>
            <div class="x-row">
                <div class="x-col" style="margin-bottom: 30px;">
                    <mark>.text :</mark>
                    <p class="text">
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Magni nihil possimus praesentium qui reprehenderit. Ab aperiam dolorum eaque sequi sint! Aliquam doloribus est eveniet fugit laborum natus nobis sapiente sit.
                    </p>
                </div>

                <div class="x-col" style="margin-bottom: 30px; background: #0000ff">
                    <mark>.text--white :</mark>
                    <p class="text--white">
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Magni nihil possimus praesentium qui reprehenderit. Ab aperiam dolorum eaque sequi sint! Aliquam doloribus est eveniet fugit laborum natus nobis sapiente sit.
                    </p>
                </div>

                <div class="x-col" style="margin-bottom: 30px;">
                    <mark>.text--light :</mark>
                    <p class="text--light">
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Magni nihil possimus praesentium qui reprehenderit. Ab aperiam dolorum eaque sequi sint! Aliquam doloribus est eveniet fugit laborum natus nobis sapiente sit.
                    </p>
                </div>

                <div class="x-col" style="margin-bottom: 30px;">
                    <mark>.text--medium-font :</mark>
                    <p class="text--medium-font">
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Magni nihil possimus praesentium qui reprehenderit. Ab aperiam dolorum eaque sequi sint! Aliquam doloribus est eveniet fugit laborum natus nobis sapiente sit.
                    </p>
                </div>


                <div class="x-col" style="margin-bottom: 30px;">
                    <mark>.text--bold :</mark>
                    <p class="text--bold">
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Magni nihil possimus praesentium qui reprehenderit. Ab aperiam dolorum eaque sequi sint! Aliquam doloribus est eveniet fugit laborum natus nobis sapiente sit.
                    </p>
                </div>




                <div class="x-col" style="margin-bottom: 30px;">
                    <mark>.text--big :</mark>
                    <p class="text--big">
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Magni nihil possimus praesentium qui reprehenderit. Ab aperiam dolorum eaque sequi sint! Aliquam doloribus est eveniet fugit laborum natus nobis sapiente sit.
                    </p>
                </div>

                <div class="x-col" style="margin-bottom: 30px;">
                    <mark>.text--extra-big :</mark>
                    <p class="text--extra-big">
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Magni nihil possimus praesentium qui reprehenderit. Ab aperiam dolorum eaque sequi sint! Aliquam doloribus est eveniet fugit laborum natus nobis sapiente sit.
                    </p>
                </div>

                <div class="x-col" style="margin-bottom: 30px;">
                    <mark>.text--medium-size :</mark>
                    <p class="text--medium-size">
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Magni nihil possimus praesentium qui reprehenderit. Ab aperiam dolorum eaque sequi sint! Aliquam doloribus est eveniet fugit laborum natus nobis sapiente sit.
                    </p>
                </div>

                <div class="x-col" style="margin-bottom: 30px;">
                    <mark>.text--small :</mark>
                    <p class="text--small">
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Magni nihil possimus praesentium qui reprehenderit. Ab aperiam dolorum eaque sequi sint! Aliquam doloribus est eveniet fugit laborum natus nobis sapiente sit.
                    </p>
                </div>

                <div class="x-col" style="margin-bottom: 30px;">
                    <mark>.text--smaller :</mark>
                    <p class="text--smaller">
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Magni nihil possimus praesentium qui reprehenderit. Ab aperiam dolorum eaque sequi sint! Aliquam doloribus est eveniet fugit laborum natus nobis sapiente sit.
                    </p>
                </div>

                <div class="x-col" style="margin-bottom: 30px;">
                    <mark>.text--extra-small :</mark>
                    <p class="text--extra-small">
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Magni nihil possimus praesentium qui reprehenderit. Ab aperiam dolorum eaque sequi sint! Aliquam doloribus est eveniet fugit laborum natus nobis sapiente sit.
                    </p>
                </div>

            </div>
        </div>


        <hr>

        <div class="x-container" style="margin-top: 50px;">
            <h1 style="text-align: center; font-size: 50px;"><mark>Titles</mark></h1>
            <div class="x-row">
                <div class="x-col" style="margin-bottom: 30px;">
                    <mark>.title :</mark>
                    <h2 class="title">Lorem ipsum</h2>
                </div>
                <div class="x-col" style="margin-bottom: 30px; background-color: blue">
                    <mark>.title--white :</mark>
                    <h2 class="title title--white">Lorem ipsum</h2>
                </div>

                <div class="x-col" style="margin-bottom: 30px;">
                    <mark>.title--medium :</mark>
                    <h2 class="title--medium">Lorem ipsum</h2>
                </div>

                <div class="x-col" style="margin-bottom: 30px;">
                    <mark>.title--small :</mark>
                    <h2 class="title--small">Lorem ipsum</h2>
                </div>



            </div>
        </div>


        <hr>

        <div class="x-container" style="margin-top: 50px;">
            <h1 style="text-align: center; font-size: 50px;"><mark>Links</mark></h1>
            <div class="x-row">
                <div class="x-col" style="margin-bottom: 30px;">
                    <mark>.link :</mark>
                    <br>
                    <a href="#" class="link">Lorem ipsum</a>
                </div>
                <div class="x-col" style="margin-bottom: 30px; background-color: blue">
                    <mark>.link--white :</mark>
                    <br>
                    <a href="#" class="link link--white">Lorem ipsum</a>
                </div>
            </div>
        </div>



        <hr>

        <div class="x-container" style="margin-top: 50px;">
            <h1 style="text-align: center; font-size: 50px;"><mark>Buttons</mark></h1>
            <div class="x-row">

                <div class="x-col" style="margin-bottom: 30px; padding-bottom: 10px;">
                    <mark>button .button--blue :</mark>
                    <br>
                    <button class="button button--blue">Заказать обрантый звонок</button>
                </div>

                <div class="x-col" style="margin-bottom: 30px; background: blue; padding-bottom: 10px;">
                    <mark>.button :</mark>
                    <br>
                    <button class="button">Аренда</button>
                </div>

                <div class="x-col" style="margin-bottom: 30px; padding-bottom: 10px;">
                    <mark>.button .button--blue .button--big-paddigns :</mark>
                    <br>
                    <button class="button button--blue button--big-paddigns">Заказать обрантый звонок</button>
                </div>

                <div class="x-col" style="margin-bottom: 30px; background: blue; padding-bottom: 10px;">
                    <mark>.button .button-full-width :</mark>
                    <br>
                    <button class="button button--full-width">Аренда</button>
                </div>
            </div>
        </div>



        <div class="x-container" style="margin-top: 50px;">
            <h1 style="text-align: center; font-size: 50px;"><mark>wysiwing-content</mark></h1>
            <div class="x-row">

            </div>
        </div>

    </div>







@endsection



