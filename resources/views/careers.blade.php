@extends('body')
@section('centerbox')
    <section class="carier single-page">
        <div class="bread-crumbs">
            <div class="container">
                <span>@lang('common.youre_here'): <a href="{{route('index')}}">@lang('common.main')</a> / </span>
                <span class="active">@lang('common.careers')</span>
            </div>
        </div>
        <div class="container">

            <div class="row">
                <div class="welcome-carier">
                    <div class="about-us-title">
                        @lang('common.careers')
                    </div>
                    <div class="carier-description">
                        @lang('common.careers_top_text')
                    </div>
                </div>

                <div class="cariers-teams row">
                    <div class="col-md-6 left-block-description">
                        <div class="carier-team-item">
                            <div class="carier-line-title">
                                @lang('common.careers_block_1_title')
                            </div>
                            <div class="carier-line-description ">
                                @lang('common.careers_block_1_text')
                            </div>

                        </div>

                        <div class="carier-team-item">
                            <div class="carier-line-title">
                                @lang('common.careers_block_2_title')
                            </div>
                            <div class="carier-line-description ">
                                @lang('common.careers_block_2_text')
                            </div>
                        </div>

                        <div class="carier-team-item">
                            <div class="carier-line-title">
                                @lang('common.careers_block_3_title')
                            </div>
                            <div class="carier-line-description ">
                                @lang('common.careers_block_3_text')
                            </div>
                        </div>

                    </div>
                    <div class="col-md-6">
                        <img src="images/realist-carier.png" alt="">
                    </div>
                </div>
            </div>

            <div class="offers-work-conditions">
                <div class="about-us-title">
                    @lang('common.careers_what_we_offer')
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <div class="offer-line">@lang('common.what_we_offer_1')</div>
                        <div class="offer-line">@lang('common.what_we_offer_2')</div>
                        <div class="offer-line">@lang('common.what_we_offer_3')</div>
                    </div>
                    <div class="col-md-6">
                        <div class="offer-line">@lang('common.what_we_offer_4')</div>
                        <div class="offer-line">@lang('common.what_we_offer_5')</div>
                        <div class="offer-line">@lang('common.what_we_offer_6')</div>
                    </div>
                </div>
            </div>

            <div class="carier-accordion-container">
                <div class="about-us-title">
                    @lang('common.vacant_posts')
                </div>
                <div class="carier-description">
                    @lang('common.careers_email')<a href="mailto:hr@realist.md"> hr@realist.md. </a>
                </div>

                @foreach (\App\Models\Lists::getBySlug('vacancies') as $item)
                    <details class="details">
                        <summary><span>{{$item->name}}</span></summary>

                        <div class="carier-details">
                            <div class="carier-details-text">
                                {!! $item->description !!}
                            </div>
                        </div>
                    </details>
                @endforeach
            </div>

        </div>
    </section>
@endsection