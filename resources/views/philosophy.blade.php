@extends('body')
@section('centerbox')

    <div class="breadcrumbs">
        <div class="x-container">
            <div class="x-row">
                <div class="x-col">
                    <a href="{{ route('index') }}" class="breadcrumbs__link text--small link">@lang('common.main')</a>
                    <img src="/img/icons/chevron-right.svg" alt="chevron" class="breadcrumb__chevron">
                    <a href="javascript:void(0)" class="breadcrumbs__current-link text--medium-font link">@lang('common.philosophy')</a>

                </div>
            </div>
        </div>
    </div>

    <div class="content-page">
        <div class="content-page__container">
            <div class="content-page__row">

                <div class="content-page__col">

                    <div class="content-page__img-wrapper">
                        <img class="content-page__main-img" src="/img/content/filoosfy_01.jpg" alt="Mission">
                        <img class="content-page__figure-img" src="/img/content/realist-figure-filled-blue.svg" alt="figure">
                    </div>



                    <h2 class="content__title title title--blue title--new-big">@lang('common.philosophy')</h2>


                    <p>
                       @lang('common.philosophy_text')
                    </p>



                </div>

            </div>
        </div>
    </div>

@endsection





