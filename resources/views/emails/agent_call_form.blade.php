<h1>@lang('common.agent_call_form_email_text')</h1>
<b>@lang('common.name'):</b> {{ $data->name ?? ''}}<br/>
<b>@lang('common.email'):</b> {{ $data->email ?? ''}}<br/>
<b>@lang('common.phone'):</b> {{ $data->phone ?? ''}}<br/>
<br><br>
--------<br>
<b>@lang('common.agent'):</b> {{$data->agent ?? ''}}<br>
<b>URL:</b> <a href="{{$data->link ?? ''}}">{{$data->link ?? ''}}</a>