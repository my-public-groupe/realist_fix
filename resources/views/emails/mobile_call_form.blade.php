<h1>@lang('common.mobile_call_form_email_text')</h1>
<b>@lang('common.name'):</b> {{ $data->name ?? ''}}<br/>
<b>@lang('common.phone'):</b> {{ $data->phone ?? ''}}<br/>