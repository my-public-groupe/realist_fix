<h1>@lang('common.contact_form_email_text')</h1>

<b>@lang('common.name'):</b> {{ $data->name ?? ''}}<br/>
<b>Email:</b> {{ $data->email ?? ''}}<br/>
<b>@lang('common.phone'):</b> {{ $data->phone ?? ''}}<br/>
<b>@lang('common.message'):</b> {{ $data->question ?? ''}}<br/>