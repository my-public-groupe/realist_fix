@extends('body')
@section('centerbox')
    <section class="advices single-page">
        <div class="bread-crumbs">
            <div class="container">
                <span>@lang('common.youre_here'): <a href="{{route('index')}}">@lang('common.main')</a> /</span>
                <span class="active">@lang('common.advice_short')</span>
            </div>
        </div>
        <div class="container">
            <div class="row">

                <div class="x-row x-row--centered">
                    <div class="x-col developers__left-col developers__left-col--smaller">
                        <div class="developers__img-wrap developers__img-wrap--big-mt"><img
                                    srcset="images/new.jpg, images/new_x2.jpg" alt="building"
                                    class="developers__img-building">
                        </div>
                        <h2 class="title developers__right-title title--caps title--blue">@lang('common.be_realist')
                        </h2>
                        <div class="developers__btns"><a href="tel:@lang('common.call_center_num')"
                                                         class="icon-btn icon-btn--blue icon-btn--white-icon icon-btn--extra-big">
                                <svg
                                        xmlns="http://www.w3.org/2000/svg" width="16.974" height="17.006"
                                        viewBox="0 0 16.974 17.006">
                                    <path id="phone"
                                          d="M17.585,13.608v2.334a1.556,1.556,0,0,1-1.7,1.556,15.4,15.4,0,0,1-6.714-2.388,15.171,15.171,0,0,1-4.668-4.668A15.4,15.4,0,0,1,2.118,3.7,1.556,1.556,0,0,1,3.666,2H6A1.556,1.556,0,0,1,7.556,3.338,9.99,9.99,0,0,0,8.1,5.524a1.556,1.556,0,0,1-.35,1.642l-.988.988a12.448,12.448,0,0,0,4.668,4.668l.988-.988a1.556,1.556,0,0,1,1.642-.35,9.99,9.99,0,0,0,2.186.545A1.556,1.556,0,0,1,17.585,13.608Z"
                                          transform="translate(-1.361 -1.25)" fill="none" stroke="#000"
                                          stroke-linecap="round"
                                          stroke-linejoin="round" stroke-width="1.5"></path>
                                </svg>
                                <span>@lang('common.advice_phone')</span></a>

                            <a href="mailto:@lang('common.advice_email')"
                               class="icon-btn icon-btn--blue icon-btn--white-icon icon-btn--big">
                                <svg
                                        xmlns="http://www.w3.org/2000/svg" width="18.45" height="14.588"
                                        viewBox="0 0 18.45 14.588">
                                    <g id="mail" transform="translate(-0.955 -3.25)">
                                        <path id="Path_3661" data-name="Path 3661"
                                              d="M3.636,4H16.724A1.641,1.641,0,0,1,18.36,5.636v9.816a1.641,1.641,0,0,1-1.636,1.636H3.636A1.641,1.641,0,0,1,2,15.452V5.636A1.641,1.641,0,0,1,3.636,4Z"
                                              fill="none" stroke="#000" stroke-linecap="round" stroke-linejoin="round"
                                              stroke-width="1.5"></path>
                                        <path id="Path_3662" data-name="Path 3662" d="M18.36,6l-8.18,5.726L2,6"
                                              transform="translate(0 -0.364)" fill="none" stroke="#000"
                                              stroke-linecap="round"
                                              stroke-linejoin="round" stroke-width="1.5"></path>
                                    </g>
                                </svg>
                                <span>@lang('common.advice_email')</span></a>


                        </div>
                    </div>
                    <div class="x-col developers__right-col developers__right-col--bigger">
                        <h2 class="title title--blue title--new-big">@lang('common.advice')</h2>
                        <p class="developers__text-bolder mb-23">@lang('common.buying_real')</p>
                        <h2 class="title  title--semi title--big title--blue-bg title--big-mb">@lang('common.inform_and_get')</h2>
                        <h2 class="title title--semi-big">@lang('common.here_you_ll_find')</h2>
                        <div class="developers__pdfs">
                            <details>
                                <summary class="pdf-element__text-content">
                                    <div class="pdf-element__img">
                                        <img src="images/sfaturi-pentru-cumprtor_23083.jpg"
                                             alt="pdf"></div>
                                    <div class="pdf-element__text-content">
                                        <h3 class="pdf-element__title">@lang('common.advices_for_buyer')</h3>
                                        <p class="pdf-element__description">@lang('common.useful_advices')
                                        </p>
                                    </div>
                                </summary>
                                <div class="pdf-element__bottom">
                                    <p>
                                        <a
                                           href="https://realist.md/storage/files/shares/Realist_tips_{{app()->getLocale()}}.pdf"
                                           download="">@lang('common.download')</a></p>
                                </div>
                            </details>

                            <details>
                                <summary class="pdf-element__text-content">
                                    <div class="pdf-element__img">
                                        <img
                                             src="images/lovushki-nedvizhimosti_23084.jpg"
                                             alt="pdf"></div>
                                    <div class="pdf-element__text-content">
                                        <h3 class="pdf-element__title">@lang('common.real_estate_traps')</h3>
                                        <p class="pdf-element__description">@lang('common.deal_attention')</p>
                                    </div>
                                </summary>
                                <div class="pdf-element__bottom">
                                    <p>
                                        <a
                                           href="https://realist.md/storage/files/shares/Realist_tips_{{app()->getLocale()}}.pdf"
                                           download="">@lang('common.download')</a></p>
                                </div>
                            </details>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </section>
@endsection