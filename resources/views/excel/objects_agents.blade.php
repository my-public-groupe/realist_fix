<table>
    <thead>
    <tr>
        <th><strong>Название и ссылка</strong></th>
        <th><strong>Ответственный агент</strong></th>
    </tr>
    </thead>
    <tbody>
    @if($data->isNotEmpty())
        @foreach($data as $d)
            <tr>
                <td><a href="{{ route('product', $d->slug) }}">{{$d->name}}</a></td>
                <td>{{ $d->agent->name ?? '' }}</td>
            </tr>
        @endforeach
    @else
        <tr>
            <td colspan="2">Нет объектов по выбранным критериям</td>
        </tr>
    @endif
    </tbody>
</table>