@extends('body')
@section('meta_title', $product->meta->title ?? null)
@section('meta_keywords', $product->meta->meta_keywords ?? null)
@section('meta_description', $product->meta->meta_description ?? null)
@section('og_image', url('/uploaded/' . $product->mainphoto()))
@section('twitter_image', url('/uploaded/' . $product->mainphoto()))
@section('centerbox')
    <!-- agent call -->
    <div class="custom-modal-content agent-call-modal">
        <div class="form-container form-modal has-bg ">
            <img src="images/modal-logo.png" alt="" class="modal-logo">
            <div class="close-btn">
                <span class="close-text">@lang('common.close')</span>
                <img src="images/svg/icon-close.svg" alt="">
            </div>

            <div class="modal-order-call-container">
                <div class="order-call-title">
                    @lang('common.leave_number')
                </div>
                <form id="agent-call-form">
                    {{ csrf_field() }}
                    <input type="hidden" name="agent" value="{{ $product->agent->name ?? '' }}">
                    <input type="hidden" name="link" value="{{ URL::current() }}">
                    <div class="modal-order-call-form">
                        <input type="text" name="name" placeholder="@lang('common.name')" required />
                        <input type="email" name="email" placeholder="@lang('common.email')" required />
                        <input type="text" name="phone" placeholder="@lang('common.phone')" required />
                        <div class="modal-order-call-note">
                            @lang('common.personal_details_allow')
                        </div>
                        <button type="submit">@lang('common.agent_call')</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="custom-modal-content agent-call-modal" id="mortgage">
        <div class="form-container form-modal has-bg" id="mortgage-content">
            <img src="images/modal-logo.png" alt="" class="modal-logo">
            <div class="close-btn">
                <span class="close-text">@lang('common.close')</span>
                <img src="images/svg/icon-close.svg" alt="">
            </div>

            <div class="modal-order-call-container">
                <div class="order-call-title">
                    @lang('common.leave_number')
                </div>
                <form id="agent-mortgage-form" method="POST" action="{{ route('send-agent-mortgage-form') }}">
                    {{ csrf_field() }}
                    <input type="hidden" name="agent" value="{{ $product->agent->name ?? '' }}">
                    <input type="hidden" name="link" value="{{ URL::current() }}">
                    <div class="modal-order-call-form">
                        <input type="text" name="name" placeholder="@lang('common.name')" required />
                        <input type="email" name="email" placeholder="@lang('common.email')" required />
                        <input type="text" name="phone" placeholder="@lang('common.phone')" required />
                        <div class="modal-order-call-note">
                            @lang('common.personal_details_allow')
                        </div>
                        <button type="submit">@lang('common.agent_call')</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <section class="one-item single-page">
        @include('partials.breadcrumbs')

        @if ($product->agent)
            <div class="rieltor-info-for-mobile">
                <div class="rieltor-details">
                    <div class="rieltor-details-title">
                        @lang('common.object_agent_repr'):
                    </div>
                    <div class="employee-item employee-item-mobile">
                        <div class="rieltor-info pe-auto">
                            <div class="img-rieltor-info img-rieltor-info-main img-rieltor-info-mobile">
                                <img class="lazyload rieltor-info-img"
                                    data-src="uploaded/{{ $product->agent->mainphoto() }}"
                                    alt="{{ $product->agent->name }}">
                            </div>
                            <div class="block-contacte-agent block-contacte-agent-object block-contacte-agent-mobile">
                                <div class="rieltor-contacts rieltor-contacts-mobile">
                                    <div class="rieltor-details rieltor-details-main">
                                        <a href="{{ route('get-agent', $product->agent->id) }}"
                                            class="rieltor-name">{{ $product->agent->name }}</a>
                                        @if (!empty($product->agent->phone))
                                            <a href="tel:{{ $product->agent->phone }}" class="rieltor-phone">
                                                <img class="rieltor-phone-img" src="images/contacts-call.png"
                                                    alt="">
                                                {{ $product->agent->phone }}
                                            </a>
                                        @endif

                                        @if (!empty($product->agent->params['email']))
                                            <a href="mailto:{{ $product->agent->params['email'] }}" class="rieltor-email">
                                                <img class="rieltor-phone-img" src="images/mail-icon.png" alt="">
                                                {{ $product->agent->params['email'] }}
                                            </a>
                                        @endif
                                    </div>
                                </div>
                                @if ($product->agent->hasSocials())
                                    <div class="rieltor-social-media rieltor-social-media-main">
                                        <ul class="rieltor-social">
                                            @if (!empty($product->agent->getViber()))
                                                <a href="{{ $product->agent->getViber() }}">
                                                    <li class="rieltor-viber d-flex align-items-center">
                                                        <img class="rieltor-icon-img" src="images/rieltor-viber.png"
                                                            alt="">
                                                    </li>
                                                </a>
                                            @endif

                                            @if (!empty($product->agent->params['messenger']))
                                                <a href="{{ $product->agent->params['messenger'] }}">
                                                    <li class="rieltor-messenger d-flex align-items-center">
                                                        <img class="rieltor-icon-img" src="images/rieltor-messenger.png"
                                                            alt="">
                                                    </li>
                                                </a>
                                            @endif

                                            @if (!empty($product->agent->params['telegram']))
                                                <a href="{{ $product->agent->params['telegram'] }}">
                                                    <li class="rieltor-telegram d-flex align-items-center">
                                                        <img class="rieltor-icon-img" src="images/rieltor-telegram.png"
                                                            alt="">
                                                    </li>
                                                </a>
                                            @endif

                                            @if (!empty($product->agent->params['whatsapp']))
                                                <a href="{{ $product->agent->params['whatsapp'] }}">
                                                    <li class="rieltor-whatsup d-flex align-items-center">
                                                        <img class="rieltor-icon-img" src="images/rieltor-whatsup.png"
                                                            alt="">
                                                    </li>
                                                </a>
                                            @endif
                                        </ul>
                                    </div>
                                @else
                                    <div class="rieltor-social-media rieltor-social-media-main rieltor-social-media-none">
                                    </div>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="schedule-meet">
                        <button class="rieltor-order-date">
                            @lang('common.book_apartment')
                            <img class="rieltor-order-date-img" src="../images/svg/calendar-icon.svg" />
                        </button>
                    </div>
                </div>
            </div>
        @endif

        <div class="container obj-page">

            <div class="row">
                <div class="col-md-12 col-lg-4 col-xl-4">

                    @if(\App\Http\Controllers\Controller::isMobile() && !empty($product->params['tour_link']))
                        <div class="iframeInstall mb-2">
                            <iframe src="{{ $product->params['tour_link'] }}" frameborder="0" allowfullscreen id="tourIframe"></iframe>
                        </div>
                    
                        <div class="d-flex justify-content-center">
                            <button id="openModalButton-iframe" >@lang('common.fullscreen')</button>
                        </div>

                        <div id="myModal-iframe" class="modal-iframe">
                            <div class="modal-content-iframe">
                                <span class="close-iframe">&times;</span>
                                <div class="iframeInstall">
                                    <iframe class="iframe" src="{{ $product->params['tour_link'] }}" frameborder="0" allowfullscreen id="tourIframe"></iframe>
                                </div>
                            </div>
                        </div>
                    @endif

                    @if (\App\Http\Controllers\Controller::isMobile() && $product->photos->isNotEmpty())
                        <div class="slider-carousel-for-mobile">
                            @include('partials.carousel-mobile-block')
                        </div>
                    @endif

                    <div class="obj-id">
                        @lang('common.id_object'): {{ $product->id }}
                    </div>
                    <div class="proposal-name">
                        {{ $product->name }}
                    </div>

                    <div class="price-container">
                        <div class="price-label">@lang('common.price'):</div>
                        @if (!empty($product->price))
                            <div class="price pt-0">{{ $product->price_text }}</div>
                        @endif

                        @if (!empty($product->old_price_text))
                            <div class="discount">{{ $product->old_price_text }}</div>
                        @endif

                        @if (!empty($product->price_square))
                            @if (!empty($product->price))
                                <div class="price-square">({!! $product->price_square_text !!})</div>
                            @else
                                <div class="price">{!! $product->price_square_text !!}</div>
                            @endif
                        @endif
                    </div>

                    <div class="block-rieltor-mortgage-call" id="btn-mortgage">
                        <button class="rieltor-mortgage-call">
                            <img class="rieltor-mortgage-call-img" src="../images/svg/mortgage.svg" />
                            @lang('common.mortgage')
                        </button>
                    </div>

                    <div class="proposal-details">
                        <div class="proposal-details-title">
                            @lang('common.characteristics')
                        </div>
                        <div class="proposal-details-container">
                            <div class="information-line">
                                @foreach ($product->parameters->chunk(3) as $parameter2)
                                    @foreach ($parameter2 as $k => $parameter)
                                        @if ($parameter->getSelectedValue() == '')
                                            @continue
                                        @endif

                                        @if ($k == 0)
                                            <div class="category-{{ $product->categories[0]->id }} categories-object">
                                                {{ $product->categories[0]->name }}
                                            </div>
                                        @endif

                                        <div class="chars-{{ $parameter->id }} categories-object">
                                            {{ $parameter->getSelectedValue() }}

                                            @if ($parameter->id == \App\Models\Parameter::ID_AREA)
                                                {{ trans('common.meter_short') }}<sup>2</sup>
                                            @endif

                                            @if ($parameter->id == \App\Models\Parameter::ID_CABINET || $parameter->id == \App\Models\Parameter::ID_ROOM)
                                                @if ($parameter->getSelectedValue() == 1)
                                                    {{ trans('common.rooms_quantity1') }}
                                                @elseif($parameter->getSelectedValue() > 1)
                                                    {{ trans('common.rooms_quantity2') }}
                                                @elseif($parameter->getSelectedValue() > 4)
                                                    {{ trans('common.rooms_quantity3') }}
                                                @endif
                                            @endif

                                            @if ($parameter->id == \App\Models\Parameter::ID_BATHROOM)
                                                @if ($parameter->getSelectedValue() == 1)
                                                    {{ trans('common.bathrooms_quantity1') }}
                                                @elseif($parameter->getSelectedValue() >= 2)
                                                    {{ trans('common.bathrooms_quantity2') }}
                                                @endif
                                            @endif

                                            @if ($parameter->id == \App\Models\Parameter::ID_FLOOR)
                                                {{ trans('common.floor') }}
                                            @endif

                                            @if ($parameter->id == \App\Models\Parameter::ID_TOTAL_FLOORS)
                                                {{ trans('common.total_floors') }}
                                            @endif

                                            @if ($parameter->id == \App\Models\Parameter::ID_BALCONY)
                                                {{ trans('common.balcony') }}
                                            @endif
                                        </div>
                                    @endforeach
                                @endforeach
                            </div>
                            <div class="clear"></div>
                        </div>
                    </div>

                    <div class="employee-item d-sm-block d-none">
                        <div class="rieltor-info rieltor-info-object rieltor-info-object-product pe-auto">
                            <a href="{{ route('get-agent', $product->agent->id) }}"
                                class="img-rieltor-info img-rieltor-info-main img-rieltor-info-adaptive">
                                <img class="lazyload rieltor-info-img"
                                    data-src="uploaded/{{ $product->agent->mainphoto() }}"
                                    alt="{{ $product->agent->name }}">

                            </a>
                            <div class="block-contacte-agent block-contacte-agent-object block-contacte-agent-adaptive">
                                <div class="rieltor-contacts">
                                    <div class="rieltor-details rieltor-details-main">
                                        <div class="rieltor-name">{{ $product->agent->name }}</div>
                                        @if (!empty($product->agent->phone))
                                            <a href="tel:{{ $product->agent->phone }}" class="rieltor-phone">
                                                <img class="rieltor-phone-img" src="images/contacts-call.png"
                                                    alt="">
                                                {{ $product->agent->phone }}
                                            </a>
                                        @endif

                                        @if (!empty($product->agent->params['email']))
                                            <a href="mailto:{{ $product->agent->params['email'] }}"
                                                class="rieltor-email">
                                                <img class="rieltor-phone-img" src="images/mail-icon.png" alt="">
                                                {{ $product->agent->params['email'] }}
                                            </a>
                                        @endif
                                    </div>
                                </div>
                                @if ($product->agent->hasSocials())
                                    <div class="rieltor-social-media rieltor-social-media-main">
                                        <ul class="rieltor-social">
                                            @if (!empty($product->agent->getViber()))
                                                <a href="{{ $product->agent->getViber() }}">
                                                    <li class="rieltor-viber d-flex align-items-center">
                                                        <img class="rieltor-icon-img" src="images/rieltor-viber.png"
                                                            alt="">
                                                    </li>
                                                </a>
                                            @endif

                                            @if (!empty($product->agent->params['messenger']))
                                                <a href="{{ $product->agent->params['messenger'] }}">
                                                    <li class="rieltor-messenger d-flex align-items-center">
                                                        <img class="rieltor-icon-img" src="images/rieltor-messenger.png"
                                                            alt="">
                                                    </li>
                                                </a>
                                            @endif

                                            @if (!empty($product->agent->params['telegram']))
                                                <a href="{{ $product->agent->params['telegram'] }}">
                                                    <li class="rieltor-telegram d-flex align-items-center">
                                                        <img class="rieltor-icon-img" src="images/rieltor-telegram.png"
                                                            alt="">
                                                    </li>
                                                </a>
                                            @endif

                                            @if (!empty($product->agent->params['whatsapp']))
                                                <a href="{{ $product->agent->params['whatsapp'] }}">
                                                    <li class="rieltor-whatsup d-flex align-items-center">
                                                        <img class="rieltor-icon-img" src="images/rieltor-whatsup.png"
                                                            alt="">
                                                    </li>
                                                </a>
                                            @endif
                                        </ul>
                                    </div>
                                @else
                                    <div class="rieltor-social-media rieltor-social-media-main rieltor-social-media-none">
                                    </div>
                                @endif
                            </div>
                        </div>
                    </div>

                    <div class="block-rieltor-order-date">
                        <button class="rieltor-order-date">
                            @lang('common.book_apartment')
                            <img class="rieltor-order-date-img" src="../images/svg/calendar-icon.svg" />
                        </button>
                    </div>

                    <div class="rieltor-other-proposals">
                        <div class="rieltor-other-text">
                            @lang('common.agent_objects')
                        </div>
                        <div class="rieltor-other-button">
                            <a href="{{ route('get-agent', $product->agent->id) }}"
                                class="read-more">@lang('common.see_all_offers')</a>
                        </div>
                    </div>


                    <div class="download-pdf">
                        <a href="{{ route('generate-pdf', $product->id) }}"
                            class="download-pdf-link">@lang('common.download_pdf')</a>
                    </div>
                </div>

                <div class="col-md-12 col-lg-8 col-xl-8">

                    @if(!\App\Http\Controllers\Controller::isMobile() && !empty($product->params['tour_link']))
                        <div class="iframeInstall mb-2">
                            <iframe src="{{ $product->params['tour_link'] }}" frameborder="0" allowfullscreen id="tourIframe"></iframe>
                        </div>
                    @endif

                    @if (!\App\Http\Controllers\Controller::isMobile() && $product->photos->isNotEmpty())
                        <div class="slider-carousel-for-desktop">
                            @include('partials.carousel-desktop-block')
                        </div>
                    @endif

                    <div class="placement-container">
                        <div class="proposal-description">
                            {!! $product->description !!}
                        </div>

                        @if (!empty($product->getNearByPlaces()))
                            <div class="left-block-title">
                                @lang('common.nearby')
                            </div>
                            <div class="placement-items row">
                                @foreach (collect($product->getNearByPlaces())->chunk(3) as $nearby_place)
                                    <div class="col-md-4">
                                        @foreach ($nearby_place as $place)
                                            <div class="placement-line {{ config('custom.nearby_icons')[$place] }}">
                                                {{ \App\Models\Lists::find($place)->name }}
                                            </div>
                                        @endforeach
                                    </div>
                                @endforeach
                            </div>
                        @endif

                        @if (!empty($product->getFeatures()))
                            <div class="peculiarity-container">
                                <div class="left-block-title">
                                    @lang('common.object_chars')
                                </div>
                                <div class="peculiarity-lines-container row">
                                    @foreach (collect($product->getFeatures())->chunk(2) as $features)
                                        <div class="col-md-6">
                                            @foreach ($features as $feature)
                                                <div class="peculiarity-line">
                                                    {{ \App\Models\Lists::find($feature)->name }}</div>
                                            @endforeach
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        @endif
                        <div class="col-4 col-sm-3 mb-3"><img src="{{ asset('images/maib_ID_new_artwork_colour.svg') }}"
                                alt="logo maib"></div>
                        <div>@include('partials.credit-calculator')</div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    @if ($product->getRelated()->isNotEmpty())
        @include('partials.related-objects')
    @endif
    @include('partials.order-call-block')
    @include('partials.news-block')
@endsection
@section('styles')
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/uikit@3.2.6/dist/css/uikit.min.css" />
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.css" />

    <style>
        .fancybox-container {
            z-index: 999999999;
        }

        .fancybox-image,
        .fancybox-spaceball {
            height: 100% !important;
        }
    </style>
@endsection
@section('scripts')
    <script>
        $(window).on('load', function() {
            $('.carousel-loading').remove();
            $('.carousel-inner').css('visibility', 'visible');
            $('.proposal-details-container').css('height', 392 - $('.proposal-name').height());
        });

        $(function() {
            $('.download-pdf-link').click(function(e) {
                e.preventDefault();
                let self = $(this);

                self.text('{{ trans('common.wait') }}');
                window.location.href = self.attr('href');

                setTimeout(function() {
                    self.text('{{ trans('common.download_pdf') }}');
                }, 4000);
            });
        });
    </script>

    <script src="https://cdn.jsdelivr.net/npm/uikit@3.2.6/dist/js/uikit.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/uikit@3.2.6/dist/js/uikit-icons.min.js"></script>
    <script src="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.js"></script>

    <script>
        $.fancybox.defaults.buttons = [
            "zoom",
            "slideShow",
            "fullScreen",
            "download",
            "thumbs",
            "close"
        ];

        var visibleImg = 0;

        $(function() {
            window.setTimeout(function() {
                var iframe = document.getElementById('credit_frame');
                iframe.setAttribute('src', 'https://creditemaibune.md/iframe/Promimobil');
            }, 3000);
        });

       
    </script>

    <script type="application/ld+json">
        {
          "@context": "https://schema.org/",
          "@type": "Product",
          "name": "{{ $product->name }}",
          "image": "{{ url($product->mainphoto()) }}",
          "offers": {
            "@type": "Offer",
            "priceCurrency": "{{ $product->valute_symbol == "$" ? 'USD' : 'EUR' }}",
            "price": "{{ $product->price }}",
            "availability": "https://schema.org/InStock",
            "url": "{{ Request::url() }}",
            "priceValidUntil": "{{ \Carbon\Carbon::parse($product->updated_at)->addYears(10)->format('Y-m-d H:i:s') }}",
          },
        @if(!empty($product->description))
        "description": "{{ strip_tags(str_replace(["\r\n", "\r", "\n", "\t"], " ", $product->description)) }}",
        @endif
        "sku": "{{$product->id}}",
        "mpn": "{{$product->id}}",
        "productID": "{{$product->id}}"
    }
    </script>

    <script>
        window.onmessage = (e) => {
            if (e.data.hasOwnProperty("frameHeight")) {
                document.getElementById("credit_frame").style.height = `${e.data.frameHeight}px`;
            }
        };
    </script>

    <script>
        document.getElementById('btn-mortgage').addEventListener('click', function() {
            document.getElementById('mortgage').classList.add('show-modal')
            document.getElementById('mortgage-content').classList.add('show-modal-content')
        });
        document.getElementById("openModalButton-iframe").addEventListener("click", function() {
            document.getElementById("myModal-iframe").style.display = "block";
        });

        document.getElementsByClassName("close-iframe")[0].addEventListener("click", function() {
            document.getElementById("myModal-iframe").style.display = "none";
        });
    </script>
@endsection
