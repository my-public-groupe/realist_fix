@extends('body')
@section('centerbox')
    <section class="about-us single-page">
        <div class="bread-crumbs">
            <div class="container">
                <span>@lang('common.youre_here'): <a href="{{route('index')}}">@lang('common.main')</a> / </span>
                <span class="active">@lang('common.about_us')</span>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <div class="about-us-title">
                        @lang('common.about_us')
                    </div>
                    <div class="about-us-description">
                        <p>{!! trans('common.about_us_text') !!}</p>

                        <p>{!! trans('common.about_us_text2') !!}</p>
                    </div>

                </div>
                <div class="col-md-6">
                    <iframe height="315" src="@lang('common.about_us_youtube')" title="YouTube"
                            frameborder="0"
                            allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                            allowfullscreen></iframe>

                </div>
            </div>
        </div>
    </section>

    <section class="our-seen">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <div class="about-us-title" id="vision">
                        @lang('common.about_us_vision')
                    </div>
                    <div class="about-us-description">
                        <p>@lang('common.mission_text')</p>

                        <p>@lang('common.mission_bottom')</p>
                    </div>

                </div>
                <div class="col-md-6">
                    <img src="images/about-white-block-photo.png" alt="">
                </div>
            </div>
        </div>
    </section>

    <section class="our-seen container-with-carousel">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <div class="about-us-title text-center">
                        @lang('common.our_office')
                    </div>
                    <div class="about-us-description">

                    @include('partials.realist_office')
                         {{-- @include('partials.carousel-about-us')  --}}
                    </div>

                </div>
                <div class="col-md-6">
                    {{-- <div class="more-vacancies">
                        <div class="vacancies-title">Наша команда</div>
                        <div class="vacancies-text">
                            Команда экспертов по продаже недвижимости, всегда готова прийти к вам на помощь! Узнайте
                            больше о членах нашей команды.
                        </div>
                        <a href="#" class="read-more">Узнать больше</a>
                    </div> --}}
                    <div class="more-vacancies">
                        <div class="vacancies-title">@lang('common.work_with_us')</div>
                        <div class="vacancies-text">
                            @lang('common.work_with_us_vacancies')
                        </div>
                        <a href="{{route('careers')}}" class="read-more">@lang('common.careers')</a>
                    </div>

                </div>
            </div>
        </div>
    </section>
    @include('partials.testimonials-block')
    @include('partials.our-partners-block')
    @include('partials.consulting-block')
@endsection
@section('scripts')
    <script>
        $(function () {
           let scrollToBlock = window.location.search.split('=')[1] ?? null;
           if(scrollToBlock){
               $('html, body').animate({
                   scrollTop: $("#" + scrollToBlock).offset().top - 100
               }, 2000);
           }
        });
    </script>
@endsection