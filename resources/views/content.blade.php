@extends('body')
@section('centerbox')

    @include('partials.breadcrumbs', ['breadcrumbs' => [$data->name => 'javascript:void(0)']])

    <div class="{{ Route::currentRouteName() == 'privacy-policy' ? 'content-page privacy-policy' : 'content-page' }}">
        <div class="content-page__container">
            <div class="content-page__row">

                <div class="content-page__col">
                    @if($data->photos->count() > 0)
                        <img class="content-page__main-img" src="/uploaded/{{$data->mainPhoto()}}" alt="{{ $data->name }}">
                    @else
                  @endif


                    <h1 class="mt-4 pt-2">{{ $data->name }}</h1>


                    {!! $data->description !!}


                    @if($data->id == 8)


                            <a href="javascript: void(0)" download class="icon-btn" @click="showPdfModal()">
                                <svg xmlns="http://www.w3.org/2000/svg" width="12.609" height="13.055" viewBox="0 0 12.609 13.055">
                                    <g id="download" transform="translate(0.75 0.75)">
                                        <path id="Path_3659" data-name="Path 3659" d="M14.109,15v2.469A1.234,1.234,0,0,1,12.875,18.7H4.234A1.234,1.234,0,0,1,3,17.469V15" transform="translate(-3 -7.148)" fill="none" stroke="blue" stroke-linecap="round" stroke-linejoin="round" stroke-width="1.5"/>
                                        <path id="Path_3660" data-name="Path 3660" d="M7,10l3.086,3.086L13.172,10" transform="translate(-4.531 -5.234)" fill="none" stroke="blue" stroke-linecap="round" stroke-linejoin="round" stroke-width="1.5"/>
                                        <line id="Line_13" data-name="Line 13" y1="7" transform="translate(5.555)" fill="none" stroke="blue" stroke-linecap="round" stroke-linejoin="round" stroke-width="1.5"/>
                                    </g>
                                </svg>

                                <span>@lang('common.download_instruction') <b>PDF</b></span>
                            </a>


                            <vk-modal center :show.sync="pdfModal">
                                <vk-modal-close @click="pdfModal = false; isBlur = false" ></vk-modal-close>
                                <form action="" @submit="pdfFormSubmit" ref="pdfForm">
                                    <div class="">
                                        <div class="form-element" :class="{'error': $v.pdfForm.name.$error}">
                                            <input
                                                    type="text"
                                                    name="name"
                                                    id="name1"
                                                    data-empty="true"
                                                    @blur="blur($event)"
                                                    v-model="pdfForm.name">
                                            <label for="name1">@lang('common.name')</label>
                                        </div>
                                        <div class="form-element" :class="{'error': $v.pdfForm.phone.$error}">
                                            <input
                                                    type="tel"
                                                    name="phone"
                                                    id="phone1"
                                                    data-empty="true"
                                                    @blur="blur($event)"
                                                    v-model="pdfForm.phone">
                                            <label for="phone1">@lang('common.phone')</label>
                                        </div>


                                        <button v-if="!downloadCatalog" type="submit" class="button button--blue button--full-width">@lang('common.send')</button>

                                        <div>
                                            <a href="/pdf/Realist.pdf" download  v-if="downloadCatalog" class="button button-blue button--full-width"><span>@lang('common.download') <b>PDF</b></span></a>
                                        </div>
                                    </div>
                                </form>

                            </vk-modal>

                    @endif

                </div>

            </div>
        </div>
    </div>

@endsection




@section('scripts')



    <!-- UIkit JS -->
    <script src="https://cdn.jsdelivr.net/npm/uikit@3.2.6/dist/js/uikit.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/uikit@3.2.6/dist/js/uikit-icons.min.js"></script>



@endsection

@section('styles')
    <!-- UIkit CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/uikit@3.2.6/dist/css/uikit.min.css" />
    <style>
        ol,ul{
            padding: 0;

        }
        .footer__interactive-contact-text p{
            margin-bottom: 0;
        }

        .form-element{
            margin-bottom: 26px !important;
        }


        h3{
            margin-bottom: 0;
            line-height: 100%;
        }

        p{
            margin-bottom: 0;
        }

    </style>
@endsection


