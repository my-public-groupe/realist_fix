@extends('body')
@section('centerbox')

    @if($slider->isNotEmpty())
        @include('partials.slider-block')
    @endif

    @include('partials.our-proposals-block')

    @if($rent_offers->isNotEmpty())
        @include('partials.best-rent-block')
    @endif

    @if($sale_offers->isNotEmpty())
        @include('partials.best-sale-block')
    @endif

    @include('partials.team-index')

    @include('partials.order-call-block')

    @if($services->isNotEmpty())
        @include('partials.services-block')
    @endif

    @include('partials.newsletter-block')

    @include('partials.about-us-block')

    @if(!empty($reviews))
        @include('partials.testimonials-block')
    @endif

    @if($partners->isNotEmpty())
        @include('partials.our-partners-block')
    @endif

    @if($news->isNotEmpty())
        @include('partials.news-block')
    @endif

    @include('partials.consulting-block')
@endsection