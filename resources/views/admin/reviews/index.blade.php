@extends('admin.body')
@section('title', 'Отзывы')
@section('centerbox')
    <div class="page-header">
        <h1> Отзывы <small><i class="ace-icon fa fa-angle-double-right"></i> Список отзывов на сайте </small></h1>
    </div>

    <div class="row">
        <div class="col-sm-3">
            <a class="btn btn-success" href="{{ URL::to('admin/reviews/create') }}">
                <i class="ace-icon fa fa-plus-square-o  bigger-120"></i>
                Добавить
            </a>
        </div>
        <div class="col-xs-12">
            <div class="clearfix">
                <div class="pull-right tableTools-container"></div>
            </div>
            <div class="table-header"> Список отзывов на сайте</div>

            <table id="dynamic-table" class="table table-striped table-bordered table-hover dataTable">
                <thead>
                <tr>
                    <th align="center">№</th>
                    <th align="center">Имя</th>
                    <th align="center">Отзыв</th>
                    <th align="center">Создан</th>
                    <th align="center"><i class="ace-icon fa fa-eye-slash bigger-130"></i></th>
                    <th align="center"><i class="menu-icon fa fa-cogs"></i></th>
                </tr>
                </thead>
                <tbody bgcolor="white">
                @foreach($data as $d)
                    <tr class="">
                        <td align="center">
                            {{ $d->id }}
                        </td>
                        <td align="center">
                            {{ $d->name }}
                        </td>
                        <td align="center">
                            <a href="{{ route('admin.reviews.edit', $d->id) }}">
                                {!! $d->getDescriptionShort() !!}
                            </a>
                        </td>
                        <td align="center">{{ $d->created_at }}</td>
                        <td align="center">
                            <div class="action-buttons">
                                <a href="javascript:void(0);" class="{{ $d->enabled ? 'visible' : 'unvisible' }}"
                                   data-id="{{ $d->id }}" data-model="reviews">
                                    <i class="ace-icon fa fa-eye bigger-130"></i>
                                </a>
                            </div>
                        </td>
                        <td align="center">
                            <div class="action-buttons">
                                <a href="{{ route('admin.reviews.edit', $d->id) }}" class="yellow"><i
                                        class="ace-icon fa fa-pencil bigger-130"></i></a>

                                {{ Form::open(array('url' => 'admin/reviews/' . $d->id, 'class' => 'pull-right')) }}
                                {{ Form::hidden('_method', 'DELETE') }}
                                {{ Form::button('<i class="ace-icon fa fa-trash-o bigger-130"></i>', ['type' => 'submit', 'class' => 'red deletebutton']) }}
                                {{ Form::close() }}
                            </div>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div><!-- /.col-xs-12 -->
    </div><!-- /.row -->
@endsection

@section('styles')
    {{HTML::style('ace/assets/css/dataTables.responsive.css')}}
@endsection

@section('scripts')

    @include('admin.partials.datatable-init')

    @include('admin.partials.visibility')

    <script>
        $(function () {
            $(".deletebutton").click(function (e) {
                e.preventDefault();

                if (confirm("Удалить запись?")) {
                    $(this).parents("tr").remove();
                    var url = $(this).parents("form").attr("action");
                    $.post(url, $(this).parents("form").serialize());
                }
            });
        });
    </script>
@append
