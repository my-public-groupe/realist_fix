@extends('admin.body')
@section('title', 'Отзыв')


@section('centerbox')
    <div class="page-header">
        <h1><a href="{{ URL::to('admin/reviews') }}">Отзывы</a> <small><i class="ace-icon fa fa-angle-double-right"></i>
                Редактирование</small></h1>
    </div>

    @if(!isset($data))
        {{ Form::open(['url' => 'admin/reviews', 'class' => 'form-horizontal']) }}
    @else
        {{ Form::open(['url' => 'admin/reviews/' . $data->id, 'method' => 'put', 'class' => 'form-horizontal']) }}
    @endif

    <div class="form-actions">
        <div class="row center">
            <div class="col-sm-2">
                <button id="submit_button1" type="submit" class="btn  btn-success btn-block btn-responsive"><i
                        class="ace-icon fa fa-floppy-o  bigger-120"></i> Сохранить
                </button>
            </div>
            <div class="col-sm-4">
                <div class="profile-contact-info">
                    <div class="profile-links align-left">

                        @if (isset($data))
                            <div class="btn btn-link">
                                <i class="ace-icon fa fa- bigger-120 green"></i>
                                ID: {{ $data->id }}
                            </div>
                        @endif
                    </div>
                </div>
            </div>

        </div><!-- /.row -->
    </div><!-- /.form-actions -->


    <div class="row">
        <div class="col-sm-6">
            <div class="tabbable">
                <ul class="nav nav-tabs padding-12 tab-color-blue background-blue" id="myTab1">
                    <li class="active">
                        <a data-toggle="tab" href="#name_ru" aria-expanded="true">RU</a>
                    </li>
                    <li class="">
                        <a data-toggle="tab" href="#name_ro" aria-expanded="false">RO</a>
                    </li>
                    <li>
                        <a data-toggle="tab" href="#name_en" aria-expanded="false">EN</a>
                    </li>
                    <div class="center">
                        <span class="label label-xlg label-purple">Имя</span>
                    </div>
                </ul>
                <div class="tab-content">
                    <div id="name_ru" class="tab-pane active">
                        {{ Form::textarea('review_name[ru]', (isset($data->name) ? $data->name : old('name')), array('class' => 'form-control name_ru', 'rows' => 3, 'cols' => 50)) }}
                    </div>
                    <div id="name_ro" class="tab-pane">
                        {{ Form::textarea('review_name[ro]', (isset($data->name_ro) ? $data->name_ro : old('name_ro')), array('class' => 'form-control name_ro', 'rows' => 3, 'cols' => 50)) }}
                    </div>
                    <div id="name_en" class="tab-pane">
                        {{ Form::textarea('review_name[en]', (isset($data->name_en) ? $data->name_en : old('name_en')), array('class' => 'form-control name_en', 'rows' => 3, 'cols' => 50)) }}
                    </div>
                </div>
            </div>
        </div><!-- /.col-sm-6 -->
        <div class="col-sm-6">
            <div class="form-group">
                {{ Form::label('sort', 'Сортировка', ['class'=>'showTip L_sort col-sm-3 control-label no-padding-right']) }}
                <div class="col-sm-9">
                    {{ Form::text('sort', (isset($data->sort) ? $data->sort : old('sort')), array('class' => 'showTip L_sort col-sm-11 col-xs-12 showTip L1')) }}
                </div>
            </div>
        </div>
    </div><!-- /.row -->
    <div class="space"></div>
    <div class="tabbable">
        <ul id="myTab4" class="nav nav-tabs padding-12 tab-color-blue background-blue">
            <li class="active">
                <a href="#description" data-toggle="tab">Отзыв</a>
            </li>
        </ul>

        <div class="tab-content">
            <div class="tab-pane active" id="description">
                <div class="tabbable tabs-left">
                    <ul id="myTab" class="nav nav-tabs">
                        <li class="active">
                            <a href="#descRu" data-toggle="tab">RU</a>
                        </li>
                        <li>
                            <a href="#descRo" data-toggle="tab">RO</a>
                        </li>
                        <li>
                            <a href="#descEn" data-toggle="tab">EN</a>
                        </li>
                    </ul>

                    <div class="tab-content">
                        <div class="tab-pane in active" id="descRu">
                            {{ Form::textarea('review_desc[ru]', (isset($data->description) ? $data->description : old('description')), array('class' => 'form-control')) }}
                        </div>
                        <div class="tab-pane" id="descRo">
                            {{ Form::textarea('review_desc[ro]', (isset($data->description_ro) ? $data->description_ro : old('description_ro')), array('class' => 'form-control')) }}
                        </div>
                        <div class="tab-pane" id="descEn">
                            {{ Form::textarea('review_desc[en]', (isset($data->description_en) ? $data->description_en : old('description_en')), array('class' => 'form-control')) }}
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>

    <div class="form-actions">
        {{ Form::submit('Сохранить', array('class' => 'btn btn-success')) }}
    </div>

    {{ Form::close() }}
@endsection

@section('scripts')

@endsection
