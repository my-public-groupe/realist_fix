@extends('admin.body')
@section('centerbox')
    <div class="main">
        <div class="page-header">
            <h1><a href="{{route('admin.reviews.index')}}">Отзывы</a>
                <small><i class="ace-icon fa fa-angle-double-right"></i> Отзыв № {{$data->id}}</small>
            </h1>
        </div>
        <div class="row">
            <div class="col-sm-7">
                <div class="profile-user-info profile-user-info-striped">
                    <div class="profile-info-row">
                        <div class="profile-info-name">@lang('common.date'):</div>
                        <div class="profile-info-value">
                            {{ $data->created_at }}
                        </div>
                    </div>
                    <div class="profile-info-row">
                        <div class="profile-info-name">Автор:</div>
                        <div class="profile-info-value">
                            <a href="{{route('admin.users.edit', $data->user->id)}}" target="_blank">
                                <i class="ace-icon fa fa-user"></i> {{ $data->user->getFullName() }}
                            </a>
                        </div>
                    </div>
                    <div class="profile-info-row">
                        <div class="profile-info-name">Товар:</div>
                        <div class="profile-info-value">
                            <a href="{{route('admin.products.edit', $data->product->id)}}" target="_blank">
                                {{ $data->product->name }}
                            </a>
                        </div>
                    </div>
                    <div class="profile-info-row">
                        <div class="profile-info-name">Отзыв:</div>
                        <div class="profile-info-value">
                            {!! $data->description !!}
                        </div>
                    </div>
                    <div class="profile-info-row">
                        <div class="profile-info-name">Рейтинг:</div>
                        <div class="profile-info-value">
                            @for($i=0;$i<$data->rating;$i++)
                                <i class="ace-icon fa fa-star orange"></i>
                            @endfor
                        </div>
                    </div>
                </div>
                <div class="form-horizontal">
                    <div class="form-actions">
                        <div class="row">
                            <div class="col-sm-6 col-md-4">
                                {{ Form::open(array('url' => 'admin/reviews/' . $data->id)) }}
                                {{ Form::hidden('_method', 'DELETE') }}
                                {{ Form::button('Удалить', ['type' => 'submit', 'class' => 'btn btn-danger btn-block']) }}
                                {{ Form::close() }}
                            </div>
                            <div class="col-sm-6 col-md-4">
                                <a href="{{route('admin.reviews.index')}}" class="btn btn-cancel btn-block"><i
                                        class="ace-icon fa fa-times-circle bigger-120"></i> Закрыть</a>
                            </div>
                        </div><!-- /.row -->
                    </div>
                </div>
            </div><!-- /.col-sm-6 -->
        </div><!-- /.row -->
    </div>
@endsection
