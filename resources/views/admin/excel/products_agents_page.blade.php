@extends('admin.body')
@section('title', 'Экспорт объектов и агентов')

@section('centerbox')
    <div class="page-header">
        <h1> Администрирование <small><i class="ace-icon fa fa-angle-double-right"></i> Экспорт объектов и агентов
            </small></h1>
    </div>

    {{ Form::open(['url' => route('admin.generate-products-agents-excel'), 'class' => 'form-horizontal']) }}
    <div class="row">
        <div class="col-xs-12">
            <div class="alert alert-info">Экспортируются только <b>видимые</b> (активные) объекты.</div>

            <div class="col-xs-12 col-sm-4 col-lg-3">
                <div class="control-group">
                    <label class="control-label bolder blue">ТИП СДЕЛКИ</label>

                    <div class="checkbox">
                        <label class="block">
                            <input name="type[]" type="checkbox" class="ace input-lg" value="0">
                            <span class="lbl bigger-120"> Продажа</span>
                        </label>
                    </div>
                    <div class="checkbox">
                        <label class="block">
                            <input name="type[]" type="checkbox" class="ace input-lg" value="1">
                            <span class="lbl bigger-120"> Аренда</span>
                        </label>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-4 col-lg-3">
                <div class="control-group">
                    <label class="control-label bolder blue">КАТЕГОРИИ</label>

                    @foreach($categories as $id => $name)
                        <div class="checkbox">
                            <label class="block">
                                <input name="category[]" type="checkbox" class="ace input-lg" value="{{$id}}">
                                <span class="lbl bigger-120"> {{$name}}</span>
                            </label>
                        </div>
                    @endforeach
                </div>
            </div>
            <div class="col-xs-12 col-sm-4 col-lg-3">
                <div class="control-group">
                    <label class="control-label bolder blue">РЕГИОН</label>
                    <div class="checkbox">
                        <label class="block">
                            <input name="region_group[]" type="checkbox" class="ace input-lg" value="1">
                            <span class="lbl bigger-120"> Кишинев</span>
                        </label>
                    </div>
                    <div class="checkbox">
                        <label class="block">
                            <input name="region_group[]" type="checkbox" class="ace input-lg" value="2">
                            <span class="lbl bigger-120"> Пригород Кишинева</span>
                        </label>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-4 col-lg-3">
                <div class="control-group">
                    <label class="control-label bolder blue">АГЕНТ</label>
                    <div class="form-group">
                        <select name="agent_id" id="chosencat">
                            <option value="">Все</option>
                            @foreach($agents as $agent)
                                <option value="{{$agent->id}}">{{ $agent->name }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>
        </div><!-- /.col-xs-12 -->
    </div><!-- /.row -->
    <div class="form-actions">
        {{ Form::submit('Экспорт в Excel', array('class' => 'btn btn-success')) }}
    </div>
@stop

@section('scripts')
    @include('admin.partials.chosen')
@endsection