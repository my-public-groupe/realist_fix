@extends('admin.body')
@section('title', 'Новости')

@section('centerbox')
    <div class="page-header">
        <h1> Администрирование <small><i class="ace-icon fa fa-angle-double-right"></i> Новости </small> </h1>
    </div>

    <div class="row">
        <div class="col-xs-12">
            <a class="btn  btn-success" href="{{ URL::to('admin/news/create') }}">
                <i class="ace-icon fa fa-plus-square-o  bigger-120"></i>
                Создать
            </a>
            <div class="clearfix"><div class="pull-right tableTools-container"></div></div>
            <div class="table-header"> Список новостей </div>

            <table id="dynamic-table" class="table table-striped table-bordered table-hover dataTable">
                <thead>
                <tr>
                    <th align="center">ID</th>
                    <th align="center">Наименование</th>
                    <th align="center">Дата</th>
                    <th align="center"><i class="fa fa-cogs"></i></th>
                </tr>
                </thead>
                <tbody bgcolor="white"></tbody>
            </table>
        </div><!-- /.col-xs-12 -->
    </div><!-- /.row -->
@stop

@section('scripts')

    <script>
        var columns = [
            { data : 'id' },
            { data : 'name' },
            { data : 'created_at' },
            { data : 'actions', className : 'center' }
        ];
    </script>

    @include('admin.partials.datatable-init-ajax', ['ajax' => 'admin/get-news'])

@endsection