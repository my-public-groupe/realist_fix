@extends('admin.body')
@section('title', 'Контент')


@section('centerbox')
<div class="page-header">
    <h1> <a href="{{ URL::to('admin/news') }}">Новости</a> <small><i class="ace-icon fa fa-angle-double-right"></i> Редактирование статьи </small> </h1>
</div>

@if(!isset($data))
{{ Form::open(['url' => 'admin/news', 'class' => 'form-horizontal']) }}
@else
{{ Form::open(['url' => 'admin/news/' . $data->id, 'method' => 'put', 'class' => 'form-horizontal']) }}
@endif

    <div class="form-actions">
        <div class="row center">
            <div class="col-sm-2">
                <button id="submit_button1" type="submit" class="btn  btn-success btn-block btn-responsive" ><i class="ace-icon fa fa-floppy-o  bigger-120"></i> Сохранить </button>
            </div>
            <div class="col-sm-2 ">
                <label>
                    {{ Form::checkbox('enabled',  1, (isset($data) && $data->enabled == 1 ? true : false), ['class' => 'ace']) }}
                    <span class="lbl hide"> Включить </span>
                </label>
            </div>
            <div class="col-sm-4">
                <div class="profile-contact-info">
                    <div class="profile-links align-left">
                        
                        @if (isset($data))
                        <div class="btn btn-link">
                            <i class="ace-icon fa fa- bigger-120 green"></i>
                            ID: {{ $data->id }}
                        </div>
                        
                        <div class="btn btn-link">
                            <i class="ace-icon fa fa-calendar bigger-120 green"></i>
                             Изменен: {{ $data->updated_at }}
                        </div>
                        @endif
                    </div>
                </div>
            </div>

        </div><!-- /.row -->
    </div><!-- /.form-actions -->


    <div class="row">
        <div class="col-sm-6">
            <div class="form-group">
                {{ Form::label('name[ru]', 'Заголовок (ru)', ['class'=>'col-sm-3 control-label no-padding-right']) }}
                <div class="col-sm-9">
                    {{ Form::text('name[ru]', (isset($data->name) ? $data->name : old('name.ru')), array('class' => 'col-sm-11 col-xs-12 name_ru')) }}
                </div>
            </div>
            <div class="form-group lang-ro" >
                {{ Form::label('name[ro]', 'Заголовок (ro)', ['class'=>'col-sm-3 control-label no-padding-right']) }}
                <div class="col-sm-9">
                    {{ Form::text('name[ro]', (isset($data->name_ro) ? $data->name_ro : old('name.ro')), array('class' => 'col-sm-11 col-xs-12')) }}
                </div>
            </div>
            <div class="form-group lang-en">
                {{ Form::label('name[en]', 'Заголовок (en)', ['class'=>'col-sm-3 control-label no-padding-right']) }}
                <div class="col-sm-9">
                    {{ Form::text('name[en]', (isset($data->name_en) ? $data->name_en : old('name.en')), array('class' => 'col-sm-11 col-xs-12')) }}
                </div>
            </div>

        </div><!-- /.col-sm-6 -->

        <div class="col-sm-6">
            <div class="form-group">
                <label for="mydate" class="col-sm-3 control-label no-padding-right"> Дата:</label>
                <div class="col-sm-5">
                    <div class="input-group">
                        <input type="text" name="created_at" id="mydate" class="form-control date-picker"
                               data-date-format="dd-mm-yyyy"
                               value="{{ (isset($data->created_at) ? $data->created_at->format('d-m-Y') : old('created_at', date('d-m-Y'))) }}" />
                        <span class="input-group-addon">
                            <i class="fa fa-calendar bigger-110"></i>
                        </span>
                    </div>
                </div>
            </div>
            <div class="form-group showTip L1">
                 {{ Form::label('slug', 'URL', ['class'=>'col-sm-3 control-label no-padding-right showTip L1']) }}
                <div class="col-sm-9">
                    {{ Form::text('slug', (isset($data->slug) ? $data->slug : old('slug')), array('class' => 'col-sm-11 col-xs-12 showTip L1')) }}
                </div>
            </div>

        </div><!-- /.col-sm-6 -->
    </div><!-- /.row -->


<div class="row">


        <div class="col-xs-12 mt-5">

            <div class="form-group">
                <div class="col-xs-6">
                    <div class="tabbable">
                        <ul id="myTab1" class="nav nav-tabs padding-12 tab-color-blue background-blue">
                            <li class="active">
                                <a href="#short_ru" data-toggle="tab" aria-expanded="true">Значение (RU)</a>
                            </li>

                            <li class="lang-ro">
                                <a href="#short_ro" data-toggle="tab" aria-expanded="false">Значение (RO)</a>
                            </li>

                            <li class="lang-en">
                                <a href="#short_en" data-toggle="tab" aria-expanded="false">Значение (EN)</a>
                            </li>

                            <div class="center"> <span class="label label-xlg label-purple">Краткий текст</span></div>
                        </ul>

                        <div class="tab-content">
                            <div class="tab-pane in active" id="short_ru">
                                {{ Form::textarea('description_short[ru]', (isset($data->description_short) ? $data->description_short : old('description_short')), array('style'=>'width:100%', 'rows'=>'3')) }}
                            </div>
                            <div class="tab-pane lang-ro" id="short_ro">
                                {{ Form::textarea('description_short[ro]', (isset($data->description_short_ro) ? $data->description_short_ro : old('description_short_ro')), array('style'=>'width:100%', 'rows'=>'3')) }}
                            </div>
                            <div class="tab-pane lang-en" id="short_en">
                                {{ Form::textarea('description_short[en]', (isset($data->description_short_en) ? $data->description_short_en : old('description_short_en')), array('style'=>'width:100%', 'rows'=>'3')) }}
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>

</div>

    <div class="space"></div>
    <div class="tabbable">
        <ul id="myTab4" class="nav nav-tabs padding-12 tab-color-blue background-blue">
            <li class="active">
                <a href="#ru" data-toggle="tab">Описание</a>
            </li>
            <li>
                <a href="#newsPhotos" data-toggle="tab">Фото</a>
            </li>
            <li class="hide">
                <a href="#meta" data-toggle="tab">META</a>
            </li>
        </ul>

        <div class="tab-content">
             <div class="tab-pane active" id="ru">

                <div class="tabbable  tabs-left">

                 <ul id="myTab" class="nav nav-tabs">
                   <li class="active">
                      <a href="#descRu" data-toggle="tab">Описание (ru)</a>
                   </li>
                   <li class="lang-ro">
                      <a href="#descRo" data-toggle="tab">Описание (ro)</a>
                   </li>
                   <li class="lang-en">
                      <a href="#descEn" data-toggle="tab">Описание (en)</a>
                   </li>
                 </ul>

                 <div class="tab-content">
                   <div class="tab-pane in active" id="descRu">
                     {{ Form::textarea('description[ru]', (isset($data->description) ? $data->description : old('description.ru')), array('class' => 'ckeditor', 'id' => 'editor')) }}
                   </div>
                   <div class="tab-pane" id="descRo" class="lang-ro">
                     {{ Form::textarea('description[ro]', (isset($data->description_ro) ? $data->description_ro : old('description.ro')), array('class' => 'ckeditor', 'id' => 'editor_ro')) }}
                   </div>
                   <div class="tab-pane" id="descEn" class="lang-en">
                     {{ Form::textarea('description[en]', (isset($data->description_en) ? $data->description_en : old('description.en')), array('class' => 'ckeditor', 'id' => 'editor_en')) }}
                   </div>

                 </div>

                </div>
             </div>

            @include('admin.partials.photos', ['label_text' => '1 фото - превью в списке новостей<br>2 фото - превью на странице одной новости', 'table' => 'news', 'div_id' => 'newsPhotos', 'table_id' => isset($data->id) ? $data->id : 0, 'class' => 'showTip L8'])

            @include('admin.partials.meta')

        </div>
    </div>

    <div class="form-actions">
        {{ Form::submit('Сохранить', array('class' => 'btn btn-success')) }}
    </div>

{{ Form::close() }}
@endsection

@section('styles')

<style>
    div#tipDiv {
    font-size:12px; line-height:1.2; letter-spacing: .2px;
    color:#000; background-color:white; padding: 2px;
    
    padding:4px;
    width:320px; 
    box-shadow: 0 1px 5px 0 rgba(0,0,0,.2), 0 2px 2px 0 rgba(0,0,0,.14), 0 3px 1px -2px rgba(0,0,0,.12);
}
</style>

@endsection

@section('scripts')

    @include('admin.partials.ckeditor')

    @include('admin.partials.slug',['input_name'=>'name[ru]'])

    @include('admin.partials.datepicker')

    
    

<script type="text/javascript">

dw_Tooltip.defaultProps = {
}
dw_Tooltip.content_vars = {
    L1: '<b>URL</b> новости должен быть латинскими маленькими символами и вместо пробела должен быть симол нижего подчеркивания <b>"_"</b>',    

    L8: 'Изображение для новости должно быть с минимальной шириной в <b>1280 px</b>',

    L10: '<b>Title</b> это название страницы, которое отображается в самом <b>верхнем поле</b> браузера. Также содержание title отображается в выдаче поисковых систем по запросам пользователей',
    L11: '<b>Meta Description</b> должен содержать <b>краткое описание</b> страницы. Достаточно добавить одно-два небольших предложения, в которых указать о чём и для кого эта страница',
    L12: '<b>Meta Keywords</b> - Те слова, которые наиболее полно характеризуют содержимое страницы и будут для нее ключевыми. Это могут быть как <b>отдельные слова</b>, так и <b>словосочетания</b>, но они <b>обязательно</b> должны встречаться в тексте на странице.',  

}

</script>

@endsection


