@extends('admin.body')
@section('title', 'Главная')

@section('centerbox')
<div class="page-header">
    <h1> Главная </h1>
</div>

<div class="alert alert-success">
    <button class="close" data-dismiss="alert"><i class="ace-icon fa fa-times"></i></button>
    Здравствуйте, @if (auth()->check()) {{ auth()->user()->name }} @endif! Управление сайтом в панели слева.
</div>

<div class="row main-page">
    <div class="col-xs-12">
        <h3 class="header smaller lighter green"></h3>
        <div class="row">
            <div class="col-md-3 col-sm-6 col-xs-12 center">
                <a href="{{ url('admin/products') }}" class="btn btn-danger btn-app radius-4">
                    <i class="ace-icon fa fa-star bigger-230"></i>
                    Объекты
                </a>
            </div>
            <div class="col-md-3 col-sm-6 col-xs-12 center hide">
                <a href="{{ url('admin/categories') }}" class="btn btn-warning btn-app radius-4">
                    <i class="ace-icon fa fa-book bigger-230"></i>
                    Категории
                </a>
            </div>
            <div class="col-md-3 col-sm-6 col-xs-12 center">
                <a href="{{ url('admin/lists') }}" class="btn btn-success btn-app radius-4">
                    <i class="ace-icon fa fa-file-text-o bigger-230"></i>
                    Контент
                </a>
            </div>
            <div class="col-md-3 col-sm-6 col-xs-12 center">
                <a href="{{ url('admin/translations') }}" class="btn btn-primary btn-app radius-4">
                    <i class="ace-icon fa fa-globe bigger-230"></i>
                    Константы
                </a>
            </div>
            <div class="col-md-3 col-sm-6 col-xs-12 center">
                <a href="{{ url('admin/products-agents-excel') }}" class="btn btn-success btn-app radius-4">
                    <i class="ace-icon fa fa-file-excel-o bigger-230"></i>
                    Экспорт объектов и агентов
                </a>
            </div>
        </div><!-- /.row -->
        <!--<div class="row">
            <div class="col-md-3 col-sm-6 col-xs-12 center">
                <a href="{{ url('admin/reviews') }}" class="btn btn-purple btn-app radius-4">
                    <i class="ace-icon fa fa-comments bigger-230"></i>
                    Отзывы
                </a>
            </div>
        </div>-->
        <div class="space"></div>
    </div><!-- /.col-xs-12 -->
</div>
@stop
