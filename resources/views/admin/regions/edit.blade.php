@extends('admin.body')
@section('title', 'Районы')

@section('centerbox')
    <div class="page-header">
        <h1> Администрирование <small><i class="ace-icon fa fa-angle-double-right"></i> Редактирование </small> </h1>
    </div>

    @if(!isset($data))
        {{ Form::open(['url' => 'admin/regions', 'class' => 'form-horizontal']) }}
    @else
        {{ Form::open(['url' => 'admin/regions/' . $data->id, 'method' => 'put', 'class' => 'form-horizontal']) }}
    @endif

    @include('admin.regions.partials.fields')

    <div class="form-actions">
        {{ Form::submit('Сохранить', array('class' => 'btn btn-success')) }}
    </div>

    {{ Form::close() }}

@endsection
