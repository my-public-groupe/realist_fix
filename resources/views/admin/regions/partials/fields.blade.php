<div class="row">
    <div class="form-group">
        {{ Form::label('name[ru]', 'Название (ru)', ['class'=>'col-sm-3 control-label no-padding-right']) }}
        <div class="col-sm-9">
            {{ Form::text('name[ru]', (isset($data->name) ? $data->name : old('name.ru')), array('class' => 'col-sm-11 col-xs-12 name_ru', 'required')) }}
        </div>
    </div>
    <div class="form-group lang-ro" >
        {{ Form::label('name[ro]', 'Название (ro)', ['class'=>'col-sm-3 control-label no-padding-right']) }}
        <div class="col-sm-9">
            {{ Form::text('name[ro]', (isset($data->name_ro) ? $data->name_ro : old('name.ro')), array('class' => 'col-sm-11 col-xs-12', 'required')) }}
        </div>
    </div>
    <div class="form-group lang-en">
        {{ Form::label('name[en]', 'Название (en)', ['class'=>'col-sm-3 control-label no-padding-right']) }}
        <div class="col-sm-9">
            {{ Form::text('name[en]', (isset($data->name_en) ? $data->name_en : old('name.en')), array('class' => 'col-sm-11 col-xs-12', 'required')) }}
        </div>
    </div>
    <div class="form-group">
        {{ Form::label('group_id', 'Группа', ['class'=>'col-sm-3 control-label no-padding-right']) }}
        <div class="col-sm-9">
            {{ Form::select('group_id', \App\Models\RegionGroup::all()->pluck('name', 'id')->toArray(), $data->group_id ?? old('group_id') , array('class' => 'col-sm-11 col-xs-12')) }}
        </div>
    </div>
    <div class="form-group">
        {{ Form::label('sort', 'Сортировка', ['class'=>'col-sm-3 control-label no-padding-right']) }}
        <div class="col-sm-9">
            {{ Form::text('sort', (isset($data->sort) ? $data->sort : 0), array('class' => 'col-sm-11 col-xs-12')) }}
        </div>
    </div>
</div><!-- /.row -->