@extends('admin.body')
@section('title', 'Группы')


@section('centerbox')
    <div class="page-header">
        <h1> <a href="{{ route('admin.region_groups.index')  }}">Группы</a> <small><i class="ace-icon fa fa-angle-double-right"></i> Редактирование </small> </h1>
    </div>

    @if(!isset($data))
        {{ Form::open(['url' => 'admin/region_groups', 'class' => 'form-horizontal']) }}
    @else
        {{ Form::open(['url' => 'admin/region_groups/' . $data->id, 'method' => 'put', 'class' => 'form-horizontal']) }}
    @endif

    <div class="row">
        <div class="form-group">
            {{ Form::label('name[ru]', 'Название (ru)', ['class'=>'col-sm-3 control-label no-padding-right']) }}
            <div class="col-sm-9">
                {{ Form::text('name[ru]', (isset($data->name) ? $data->name : old('name.ru')), array('class' => 'col-sm-11 col-xs-12 name_ru')) }}
            </div>
        </div>
        <div class="form-group lang-ro" >
            {{ Form::label('name[ro]', 'Название (ro)', ['class'=>'col-sm-3 control-label no-padding-right']) }}
            <div class="col-sm-9">
                {{ Form::text('name[ro]', (isset($data->name_ro) ? $data->name_ro : old('name.ro')), array('class' => 'col-sm-11 col-xs-12')) }}
            </div>
        </div>
        <div class="form-group lang-en">
            {{ Form::label('name[en]', 'Название (en)', ['class'=>'col-sm-3 control-label no-padding-right']) }}
            <div class="col-sm-9">
                {{ Form::text('name[en]', (isset($data->name_en) ? $data->name_en : old('name.en')), array('class' => 'col-sm-11 col-xs-12')) }}
            </div>
        </div>
    </div><!-- /.row -->

    <div class="form-actions">
        {{ Form::submit('Сохранить', array('class' => 'btn btn-success')) }}
    </div>

    {{ Form::close() }}

@endsection
