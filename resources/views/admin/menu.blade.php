
<ul class="nav nav-list">
    <li class="">
        <a href="{{ url('admin') }}">
            <i class="menu-icon fa fa-home"></i>
            <span class="menu-text"> Главная </span>
        </a>
    </li>
    <li>
        <a href="{{ url('admin/products') }}">
            <i class="menu-icon fa fa-star"></i>
            <span class="menu-text"> Объекты </span>
        </a>
    </li>
    <li>
        <a href="{{ url('admin/categories') }}" class="hide">
            <i class="menu-icon fa fa-book"></i>
            <span class="menu-text"> Категории </span>
        </a>
    </li>
    <li>
        <a href="{{ url('admin/lists') }}">
            <i class="menu-icon fa fa-file-text-o"></i>
            <span class="menu-text"> Контент </span>
        </a>
    </li>
    <li>
        <a href="{{ url('admin/translations') }}">
            <i class="menu-icon fa fa-globe"></i>
            <span class="menu-text"> Константы </span>
        </a>
    </li>
    <li>
        <a href="{{ url('admin/agents') }}">
            <i class="menu-icon fa fa-users"></i>
            <span class="menu-text"> Агенты </span>
        </a>
    </li>
    <li>
        <a href="{{ url('admin/news') }}">
            <i class="menu-icon fa fa-newspaper-o"></i>
            <span class="menu-text"> Новости </span>
        </a>
    </li>
    <li class="">
        <a href="#" class="dropdown-toggle">
            <i class="menu-icon fa fa-cogs"></i>
            <span class="menu-text"> Администр.</span>
            <b class="arrow fa fa-angle-down"></b>
        </a>
        <b class="arrow"></b>
        <ul class="submenu">
            <li class="">
                <a href="{{ url('admin/users') }}" class="hide">
                    <i class="menu-icon fa fa-users"></i>
                    Пользователи
                </a>
                <b class="arrow"></b>
            </li>
            <li class="">
                <a href="{{ url('admin/parameters') }}" class="hide">
                    Характеристики
                </a>
                <b class="arrow"></b>
            </li>
            <li class="">
                <a href="{{ url('admin/regions') }}">
                    Районы
                </a>
                <b class="arrow"></b>
            </li>
            <li class="">
                <a href="{{ url('admin/products-agents-excel') }}">
                    Экспорт объектов и агентов
                </a>
                <b class="arrow"></b>
            </li>
            <li class="">
                <a href="{{ url('admin/clear-cache') }}">
                    <i class="menu-icon fa fa-trash-o"></i>
                    <span class="menu-text"> Очистить кеш </span>
                </a>
            </li>
        </ul>
    </li>
    <li class="">
        <a href="{{ url('logout') }}">
            <i class="menu-icon fa fa-sign-out"></i>
            <span class="menu-text"> Выход </span>
        </a>
    </li>
</ul><!-- /.nav-list -->
