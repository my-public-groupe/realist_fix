@extends('admin.body')
@section('title', 'Редактирование пользователя')
@section('centerbox')
    <div class="page-header">
        <h1> <a href="{{ url('admin/users') }}"> Пользователи</a>
            <small><i class="ace-icon fa fa-angle-double-right"></i> Редактирование</small> </h1>
    </div>

    @include('admin.partials.errors')

    @if(!isset($data))
        {{ Form::open(['url' => 'admin/users', 'class' => 'form-horizontal']) }}
    @else
        {{ Form::open(['url' => 'admin/users/' . $data->id, 'method' => 'put', 'class' => 'form-horizontal']) }}
    @endif

    <div class="form-actions">
        <div class="row center">
            <div class="col-sm-2">
                <button id="submit_button1" type="submit" class="btn  btn-success btn-block btn-responsive" ><i class="ace-icon fa fa-floppy-o  bigger-120"></i> Сохранить </button>
            </div>
        </div><!-- /.row -->
    </div><!-- /.form-actions -->

    <div class="row">
        <div class="col-sm-6">
            <div class="form-group">
                {{ Form::label('name', 'Имя', ['class'=>'col-sm-3 control-label no-padding-right']) }}
                <div class="col-sm-9">
                    {{ Form::text('name', (isset($data->name) ? $data->name : old('name')), array('class' => 'col-sm-11 col-xs-12')) }}
                </div>
            </div>

            <div class="form-group">
                {{ Form::label('email', 'Email', ['class'=>'col-sm-3 control-label no-padding-right']) }}
                <div class="col-sm-9">
                    {{ Form::text('email', (isset($data->email) ? $data->email : old('email')), array('class' =>
                    'col-sm-11 col-xs-12')) }}
                </div>
            </div>
            <div class="form-group">
                {{ Form::label('open_password', 'Пароль', ['class'=>'col-sm-3 control-label no-padding-right']) }}
                <div class="col-sm-9">
                    {{ Form::text('open_password', (isset($data->open_password) ? $data->open_password : old
                    ('open_password')), array('class' => 'col-sm-11 col-xs-12')) }}
                </div>
            </div>
            <div class="form-group">
                {{ Form::label('comment', 'Комментарий', ['class'=>'col-sm-3 control-label no-padding-right']) }}
                <div class="col-sm-9">
                    {{ Form::textarea('params[comment]', (isset($data->params['comment']) ? $data->params['comment'] : old
                    ('params.comment')), array('class' => 'col-sm-11 col-xs-12')) }}
                </div>
            </div>
        </div>
        <div class="col-sm-6">
            <div class="form-group">
                {{ Form::label('rights', 'Роль', ['class'=>'col-sm-3 control-label no-padding-right']) }}
                <div class="col-sm-9">
                    {{ Form::select('rights', ['1' => 'Администратор'], (isset($data->rights) ? $data->rights : old('rights')), ['class' => 'col-sm-9']) }}
                </div>
            </div>
            <div class="form-group">
                {{ Form::label('enabled', 'Активный', ['class'=>'col-sm-3 control-label no-padding-right']) }}
                <div class="col-sm-9">
                    {{ Form::select('enabled', ['1' => 'Да', '0' => 'Нет'], (isset($data->enabled) ? $data->enabled : old('enabled')), ['class' => 'col-sm-9']) }}
                </div>
            </div>
        </div>
    </div><!-- row -->

    {{ Form::close() }}

@endsection

@section('scripts')

    @include('admin.partials.chosen')

@endsection