@extends('admin.body')
@section('title', 'Агенты')


@section('centerbox')

    <div class="page-header">
        <h1> <a href="{{ URL::to('admin/agents') }}">Агенты</a> <small><i class="ace-icon fa fa-angle-double-right"></i> Редактирование </small> </h1>
    </div>

    @if(!isset($data))
    {{ Form::open(['url' => 'admin/agents', 'class' => 'form-horizontal']) }}
    @else
    {{ Form::open(['url' => 'admin/agents/' . $data->id, 'method' => 'put', 'class' => 'form-horizontal']) }}
    @endif

    <div class="row">
        <div class="col-sm-6">
            <div class="form-group">
                {{ Form::label('name', 'ФИО', ['class'=>'col-sm-3 control-label no-padding-right']) }}
                <div class="col-sm-9">
                    {{ Form::text('name', (isset($data->name) ? $data->name : old('name')), array('class' => 'col-sm-11 col-xs-12 name_ru')) }}
                </div>
            </div>
            <div class="form-group">
                {{ Form::label('phone', 'Телефон', ['class' => 'col-sm-3 control-label no-padding-right']) }}
                <div class="col-sm-9">
                    {{ Form::text('phone', (isset($data->phone) ? $data->phone : old('phone')), array('class' => 'col-sm-11 col-xs-12')) }}
                </div>
            </div>
            <div class="form-group">
                {{ Form::label('params[viber]', 'Viber', ['class' => 'col-sm-3 control-label no-padding-right']) }}
                <div class="col-sm-9">
                    {{ Form::text('params[viber]', (isset($data->params['viber']) ? $data->params['viber'] : old('params.viber')), array('class' => 'col-sm-11 col-xs-12')) }}
                </div>
            </div>
            <div class="form-group">
                {{ Form::label('params[messenger]', 'Messenger', ['class' => 'col-sm-3 control-label no-padding-right']) }}
                <div class="col-sm-9">
                    {{ Form::text('params[messenger]', (isset($data->params['messenger']) ? $data->params['messenger'] : old('params.messenger')), array('class' => 'col-sm-11 col-xs-12')) }}
                </div>
            </div>
            <div class="form-group">
                {{ Form::label('params[telegram]', 'Telegram', ['class' => 'col-sm-3 control-label no-padding-right']) }}
                <div class="col-sm-9">
                    {{ Form::text('params[telegram]', (isset($data->params['telegram']) ? $data->params['telegram'] : old('params.telegram')), array('class' => 'col-sm-11 col-xs-12')) }}
                </div>
            </div>
            <div class="form-group">
                {{ Form::label('params[whatsapp]', 'WhatsApp', ['class' => 'col-sm-3 control-label no-padding-right']) }}
                <div class="col-sm-9">
                    {{ Form::text('params[whatsapp]', (isset($data->params['whatsapp']) ? $data->params['whatsapp'] : old('params.whatsapp')), array('class' => 'col-sm-11 col-xs-12')) }}
                </div>
            </div>
            <div class="form-group">
                {{ Form::label('params[function]', 'Должность', ['class' => 'col-sm-3 control-label no-padding-right']) }}
                <div class="col-sm-9">
                    {{ Form::text('params[function]', (isset($data->params['function']) ? $data->params['function'] : old('params.function')), array('class' => 'col-sm-11 col-xs-12')) }}
                </div>
            </div>
            <div class="form-group">
                {{ Form::label('params[email]', 'Email', ['class' => 'col-sm-3 control-label no-padding-right']) }}
                <div class="col-sm-9">
                    {{ Form::text('params[email]', (isset($data->params['email']) ? $data->params['email'] : old('params.email')), array('class' => 'col-sm-11 col-xs-12')) }}
                </div>
            </div>
            <div class="form-group">
                {{ Form::label('sort', 'Сортировка', ['class' => 'col-sm-3 control-label no-padding-right']) }}
                <div class="col-sm-9">
                    {{ Form::number('sort', (isset($data->sort) ? $data->sort : old('sort')), array('class' => 'col-sm-11 col-xs-12')) }}
                </div>
            </div>
        </div>
    </div><!-- /.row -->

    <div class="tabbable">
        <ul id="myTab4" class="nav nav-tabs padding-12 tab-color-blue background-blue">
            <li class="active">
                <a href="#photos" data-toggle="tab">Фото</a>
            </li>
        </ul>
    </div>

    <div class="tab-content">

        @include('admin.partials.photos', ['table' => 'agents', 'table_id' => isset($data->id) ? $data->id : 0, 'class' => 'active', 'crop' => [600, 645]] )

    </div>



    <div class="form-actions">

        {{ Form::submit('Сохранить', array('class' => 'btn btn-success')) }}

    </div>

{{ Form::close() }}

@endsection