@extends('admin.body')
@section('title', 'Константы')
@section('centerbox')
    <div class="page-header">
        <h1> <a href="{{ url('/admin') }}">Главная</a> <small><i class="ace-icon fa fa-angle-double-right"></i> Список констант </small> </h1>
    </div>

    {{ Form::open(['url' => 'admin/translations', 'class' => 'form-horizontal']) }}

    <div class="form-actions">
        <div class="row center">
            <div class="col-sm-2">
                <button id="submit_button1" type="submit" class="btn  btn-success btn-block btn-responsive" ><i class="ace-icon fa fa-floppy-o  bigger-120"></i> Сохранить </button>
            </div>
        </div><!-- /.row -->
    </div><!-- /.form-actions -->

    <div class="tabbable">
        <ul class="nav nav-tabs padding-12 tab-color-blue background-blue">
            @foreach($languages as $key => $data)
            <li @if($loop->first)class="active"@endif>
                <a href="#{{ $key }}" data-toggle="tab">{{ $data['name'] }}</a>
            </li>
            @endforeach
        </ul>
    </div>

    <div class="tab-content">
        @foreach($languages as $key => $data)
        <div class="tab-pane @if($loop->first) active @endif" id="{{ $key }}">
            {{ Form::textarea('languages[' . $key . ']', $data['content'], ['id' => 'textarea_' . $key, 'class' => 'hide']) }}
            <div id="editor_{{ $key }}">{{ $data['content'] }}</div>
        </div>
        @endforeach
    </div>
    {{ Form::close() }}
@endsection

@section('styles')
    <style>
        .ace_editor {
            height: 500px;
        }
    </style>
@endsection

@section('scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/ace/1.2.6/ace.js" type="text/javascript" charset="utf-8"></script>

    @foreach ($languages as $key => $data)
    <script>
        var editor_{{ $key }} = ace.edit("editor_{{ $key }}");
        editor_{{ $key }}.setTheme("ace/theme/twilight");
        editor_{{ $key }}.getSession().setMode("ace/mode/yaml");
        editor_{{ $key }}.getSession().on('change', function(){
            $('#textarea_{{ $key }}').val(editor_{{ $key }}.getSession().getValue());
        });
    </script>
    @endforeach

@endsection

