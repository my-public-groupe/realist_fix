@extends('admin.body')
@section('title', 'Объекты')


@section('centerbox')
    <div class="page-header">
        <h1> <a href="{{ URL::to('admin/products') }}">Объекты</a> <small><i class="ace-icon fa fa-angle-double-right"></i>
                Редактирование </small> </h1>
    </div>

    @if (!isset($data))
        {{ Form::open(['url' => 'admin/products', 'class' => 'form-horizontal', 'id' => 'edit-product']) }}
    @else
        {{ Form::open(['url' => 'admin/products/' . $data->id, 'method' => 'put', 'class' => 'form-horizontal', 'id' => 'edit-product']) }}
    @endif

    <div class="form-actions">
        <div class="row center">
            <div class="col-sm-2">
                <button id="submit_button1" type="submit" class="btn  btn-success btn-block btn-responsive"><i
                        class="ace-icon fa fa-floppy-o  bigger-120"></i> Сохранить </button>
            </div>
            @if (isset($data) && $data->categories->firstWhere('id', 1))
                <!--Квартиры-->
                <div class="col-sm-2">
                    <button type="button" class="btn btn-purple btnPublish"><i class="ace-icon fa fa-upload"></i>
                        Опубликовать на 999.md</button>
                </div>
            @endif
            <div class="col-sm-8">
                @if (isset($data))
                    <div class="btn btn-link">
                        <i class="ace-icon fa fa- bigger-120 green"></i>
                        ID: {{ $data->id }}
                    </div>

                    <div class="btn btn-link">
                        <i class="ace-icon fa fa-calendar bigger-120 green"></i>
                        Изменен: {{ $data->updated_at }}
                    </div>

                    <div class="btn-group">
                        <button data-toggle="dropdown" class="btn btn-info btn-sm dropdown-toggle" aria-expanded="true">
                            Посмотреть на сайте
                            <span class="ace-icon fa fa-caret-down icon-on-right"></span>
                        </button>
                        <ul class="dropdown-menu dropdown-info dropdown-menu-right">
                            <li>
                                <a href="{{ url("/ru/products/{$data->slug}?t=" . date('His')) }}" target="_blank">RU</a>
                            </li>

                            <li>
                                <a href="{{ url("/products/{$data->slug}?t=" . date('His')) }}" target="_blank">RO</a>
                            </li>

                            <li>
                                <a href="{{ url("/en/products/{$data->slug}?t=" . date('His')) }}" target="_blank">EN</a>
                            </li>
                        </ul>
                    </div>
                @endif
            </div>
        </div><!-- /.row -->
    </div><!-- /.form-actions -->


    <div class="row">

        <div class="col-sm-6">
            <div class="form-group">
                {{ Form::label('name[ru]', 'Заголовок (ru)', ['class' => 'col-sm-3 control-label no-padding-right']) }}
                <div class="col-sm-9">
                    {{ Form::text('name[ru]', $data->name ?? old('name.ru'), ['class' => 'col-sm-11 col-xs-12 name_ru']) }}
                </div>
            </div>
            <div class="form-group lang-ro">
                {{ Form::label('name[ro]', 'Заголовок (ro)', ['class' => 'col-sm-3 control-label no-padding-right']) }}
                <div class="col-sm-9">
                    {{ Form::text('name[ro]', $data->name_ro ?? old('name.ro'), ['class' => 'col-sm-11 col-xs-12']) }}
                </div>
            </div>
            <div class="form-group lang-en">
                {{ Form::label('name[en]', 'Заголовок (en)', ['class' => 'col-sm-3 control-label no-padding-right']) }}
                <div class="col-sm-9">
                    {{ Form::text('name[en]', $data->name_en ?? old('name.en'), ['class' => 'col-sm-11 col-xs-12']) }}
                </div>
            </div>

            <div class="form-group">
                {{ Form::label('slug', 'URL', ['class' => 'col-sm-3 control-label no-padding-right']) }}
                <div class="col-sm-9">
                    {{ Form::text('slug', isset($data->slug) ? $data->slug : old('slug'), ['class' => 'col-sm-11 col-xs-12']) }}
                </div>
            </div>

            <div class="form-group">
                {{ Form::label('sort', 'Сортировка', ['class' => 'col-sm-3 control-label no-padding-right']) }}
                <div class="col-sm-9">
                    {{ Form::number('sort', isset($data->sort) ? $data->sort : old('sort', 0), ['class' => 'col-sm-11 col-xs-12']) }}
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-offset-3">
                    {{ Form::checkbox('top', 1, isset($data) && $data->top == 1 ? true : false, ['class' => 'ace']) }}
                    <span class="lbl"> Выводить на главной странице</span>
                </label>
            </div>

        </div><!-- /.col-sm-6 -->

        <div class="col-sm-6">

            <div class="form-group">
                {{ Form::label('categories', 'Категория', ['class' => 'col-sm-3 control-label no-padding-right']) }}
                <div class="col-sm-9">
                    {{ Form::select('categories[]', [null => 'не выбрана'] + $categories, $data->categories ?? [], ['multiple' => 'multiple', 'id' => 'chosencat', 'class' => 'tag-input-style col-sm-11 control-label no-padding-right']) }}
                </div>
            </div>

            <div class="form-group">
                {{ Form::label('type', 'Тип сделки', ['class' => 'col-sm-3 control-label no-padding-right']) }}
                <div class="col-sm-9">
                    {{ Form::select('type', config('custom.product_types'), isset($data->type) ? $data->type : old('type'), ['class' => 'col-sm-11']) }}
                </div>
            </div>

            <div class="form-group">
                {{ Form::label('agent_id', 'Агент', ['class' => 'col-sm-3 control-label no-padding-right']) }}
                <div class="col-sm-6">
                    {{ Form::select('agent_id', [null => 'не выбран'] + $agents, isset($data->agent_id) ? $data->agent_id : old('agent_id'), ['class' => 'col-sm-11 chosencat']) }}
                </div>
            </div>

            <div class="form-group">
                {{ Form::label('region_id', 'Регион', ['class' => 'col-sm-3 control-label no-padding-right']) }}
                <div class="col-sm-6">
                    {{ Form::select('region_id', [null => 'не выбран'] + $regions, isset($data->region_id) ? $data->region_id : old('region_id'), ['class' => 'col-sm-11 chosencat']) }}
                </div>
                <div class="col-sm-2">
                    <a href="#" data-toggle="modal" data-target="#modal-add-region"
                        class="btn btn-white btn-sm">Добавить</a>
                </div>
            </div>

            <div class="form-group">
                {{ Form::label('valute', 'Валюта', ['class' => 'col-sm-3 control-label no-padding-right']) }}
                <div class="col-sm-9">
                    <div data-toggle="buttons" class="btn-group valute">
                        <label class="btn btn-default @if (!isset($data) || $data->valute == 'EUR') active @endif">
                            <input type="radio" name="valute" value="EUR">
                            <i class="icon-only ace-icon fa fa-euro"></i>
                        </label>

                        <label class="btn btn-default @if (isset($data) && $data->valute == 'USD') active @endif">
                            <input type="radio" name="valute" value="USD">
                            <i class="icon-only ace-icon fa fa-dollar"></i>
                        </label>
                    </div>
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-3 control-label no-padding-right">Цена, <span
                        class="valute-text">{{ $data->valute_symbol ?? '€' }}</span></label>
                <div class="col-sm-9">
                    {{ Form::text('price', isset($data->price) ? $data->price : old('price'), ['class' => 'col-sm-11 col-xs-12']) }}
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-3 control-label no-padding-right">Цена за м<sup>2</sup>, <span
                        class="valute-text">{{ $data->valute_symbol ?? '€' }}</span></label>
                <div class="col-sm-9">
                    {{ Form::text('price_square', isset($data->price_square) ? $data->price_square : old('price_square'), ['class' => 'col-sm-11 col-xs-12']) }}
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-3 control-label no-padding-right">Старая цена, <span
                        class="valute-text">{{ $data->valute_symbol ?? '€' }}</span></label>
                <div class="col-sm-9">
                    {{ Form::text('old_price', isset($data->old_price) ? $data->old_price : old('old_price'), ['class' => 'col-sm-11 col-xs-12']) }}
                </div>
            </div>

            <div class="form-group">
                {{ Form::label('categories', 'Что находится рядом', ['class' => 'col-sm-3 control-label no-padding-right']) }}
                <div class="col-sm-9">
                    {{ Form::select('params[nearby_places][]', [null => 'не выбрано'] + $nearby_places, $data->params['nearby_places'] ?? [], ['multiple' => 'multiple', 'class' => 'chosencat tag-input-style col-sm-11 control-label no-padding-right']) }}
                </div>
            </div>

            <div class="form-group">
                {{ Form::label('categories', 'Особенности', ['class' => 'col-sm-3 control-label no-padding-right']) }}
                <div class="col-sm-9">
                    {{ Form::select('params[features][]', [null => 'не выбрано'] + $features, $data->params['features'] ?? [], ['multiple' => 'multiple', 'class' => 'chosencat tag-input-style col-sm-11 control-label no-padding-right']) }}
                </div>
            </div>

            <div class="form-group">
                {{ Form::label('tour_link', 'Ссылка на 3D-тур', ['class' => 'col-sm-3 control-label no-padding-right']) }}
                <div class="col-sm-9">
                    {{ Form::text('params[tour_link]', !empty($data->params['tour_link']) ? $data->params['tour_link'] : old('tour_link'), ['class' => 'col-sm-11 col-xs-12']) }}
                </div>
            </div>


        </div><!-- /.col-sm-6 -->

    </div><!-- /.row -->

    <div class="space"></div>

    <div class="tabbable">
        <ul id="myTab4" class="nav nav-tabs padding-12 tab-color-blue background-blue">

            <li class="active">
                <a href="#photos" data-toggle="tab">Фото</a>
            </li>
            <li>
                <a href="#description" data-toggle="tab">Описание</a>
            </li>
            <li>
                <a href="#characteristics" data-toggle="tab">Характеристики</a>
            </li>
            <li>
                <a href="#meta" data-toggle="tab">META</a>
            </li>

        </ul>
    </div>

    <div class="tab-content">

        @include('admin.partials.description')

        @include('admin.products.partials.characteristics')

        @include('admin.partials.meta')

        @include('admin.partials.photos', [
            'table' => 'products',
            'table_id' => isset($data->id) ? $data->id : 0,
            'class' => 'active',
            'with_watermark' => true,
        ])

    </div>

    <div class="form-actions">

        {{ Form::submit('Сохранить', ['class' => 'btn btn-success']) }}

    </div>

    {{ Form::close() }}


    @include('admin.products.partials.add-region-modal')

    @include('admin.products.partials.simpals_success')

@endsection

@section('scripts')

    @include('admin.partials.slug', ['input_name' => 'name[ru]', 'form_id' => 'edit-product'])

    @include('admin.partials.chosen')

    <script>
        $("input[name=valute]").change(function() {
            var valute = $(this).val();
            if (valute == "USD") {
                $(".valute-text").text("$");
            } else if (valute == "EUR") {
                $(".valute-text").text("€");
            }
        });
    </script>

    @if (isset($data))
        <script>
            $(function() {
                $(".btnPublish").click(function() {
                    let _this = $(this);
                    _this.attr('disabled', true);
                    _this.attr('data-old-text', _this.text()).text('Загружаем объявление, подождите...');

                    $.post("{{ route('add-advert') }}", {
                        'product_id': '{{ $data->id }}'
                    }, function(response) {
                        if (response.success) {
                            $("#modal-simpals-success").find('#simpals-link').attr('href', response
                                .url);
                            $("#modal-simpals-success").modal('show');
                        } else {
                            toastr.error(`Ошибка добавления объявления<br><b>${response.error}</b>`);
                        }

                        _this.attr('disabled', false);
                        _this.html(`<i class='ace-icon fa fa-upload'></i>${_this.data('old-text')}`);
                    });
                });
            });
    
        </script>
    @endif
@append
