<!-- Modal -->
<div class="modal fade" id="modal-add-region" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Добавить новый регион</h4>
            </div>

            {{ Form::open(['url' => 'admin/regions', 'class' => 'form-horizontal', 'id' => 'add-region-form']) }}

            <div class="modal-body">

                @include('admin.regions.partials.fields', ['data' => null])

            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary">Сохранить</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
            </div>

            {{ Form::close() }}
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->

@section('scripts')

    {!! HTML::script('ace/assets/js/jquery.form.js') !!}

    <script>

        $('#add-region-form').ajaxForm({

            success: function (response) {

                const $selector = $('select[name=region_id]');

                $selector.empty();

                $.each(response.regions, function (group, regions) {

                    const $optgroup = $('<optgroup label="' + group + '"></optgroup>');

                    $.each(regions, function (key, value) {

                         let $option = $('<option value="' + key +'">' + value + '</option>');

                        if (+response.region_id === +key) {
                             $option.attr('selected', 'selected');
                         }

                         $optgroup.append($option);

                    });

                    $selector.append($optgroup);

                });


                $selector.trigger("chosen:updated");

                $('#modal-add-region').modal('hide');

                toastr.success('Готово');
            }
        });

    </script>

@append