<!-- Modal -->
<div class="modal fade" id="modal-simpals-success" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title text-center">Объявление успешно опубликовано!</h4>
            </div>

            {{ Form::open(['url' => 'admin/regions', 'class' => 'form-horizontal', 'id' => 'add-region-form']) }}

            <div class="modal-body">
                <div class="row">
                    <div class="col-sm-12 text-center">
                        <div class="well">При необходимости, дополните объявление новыми данными и характеристиками.</div>
                        <a href="" target="_blank" id="simpals-link" class="btn btn-success btn-small">Посмотреть на 999.md</a>
                    </div>
                </div>
            </div>

            {{ Form::close() }}
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->