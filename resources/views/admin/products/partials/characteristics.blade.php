<div class="tab-pane" id="characteristics">
        @if (isset($parameters))
            <ul>
                @foreach($parameters as $item)
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="form-group">
                                <label class="col-sm-3 control-label no-padding-right"> {{ $item->name }} </label>
                                <div class="col-sm-9">
                                    {{ Form::hidden('parameters[]', $item->id) }}
                                    @if ($item->type == 0)
                                        {{ Form::text('value_ru['.$item->id.']', $item->pivot->value, ['placeholder' => 'Значение', 'class' => 'col-xs-12 col-sm-4 input-sm']) }}
                                    @elseif ($item->type == 1)
                                        @if($item->id == 14)
                                            {{ Form::select('type_values[]',
                                            [0 => 'не выбрано'] + $item->values->pluck('value', 'id')->toArray(),
                                            $data->getTypeValues(),
                                            ['class' => 'col-xs-12 col-sm-4 chosencat', 'multiple' => true])
                                        }}
                                        @else
                                            {{ Form::select('value_id[' . $item->id . ']',
                                            [0 => 'не выбрано'] + $item->values->pluck('value', 'id')->toArray(),
                                            $item->pivot->value_id,
                                            ['class' => 'col-xs-12 col-sm-4'])
                                        }}
                                        @endif
                                    @elseif ($item->type == 2)
                                        {{ Form::text('value_ru['.$item->id.']', $item->pivot->value, ['placeholder' => 'Значение', 'class' => 'col-xs-12 col-sm-4 input-sm']) }}
                                        {{ Form::text('value_ro['.$item->id.']', $item->pivot->value_ro, ['placeholder' => 'Значение RO', 'class' => 'col-xs-12 col-sm-4 input-sm lang-ro']) }}
                                        {{ Form::text('value_en['.$item->id.']', $item->pivot->value_en, ['placeholder' => 'Значение EN', 'class' => 'col-xs-12 col-sm-4 input-sm lang-en']) }}
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </ul>
        @else
            <div class="row">
                <div class="col-xs-12">
                    <div class="alert alert-info"><span>Выберите категорию с характеристиками и сохраните товар</span></div>
                </div>
            </div>
        @endif
</div>

@section('scripts')

    {!! HTML::script('ace/assets/js/jquery-ui.js') !!}

    <script>
        $(document).ready(function(){

            //$('#products_sortable').sortable();

            //getCategoryParameters();
        });

        function getCategoryParameters() {

            var category_id = $('input[name=category_id]').val();

            $.get("{{ URL::to('admin/json/get-category-parameters') }}",
                {
                    'category_id': category_id
                },
                function(response) {
                    if (response.success == "true") {
                        $.each(response.data, function() {
                            addItem(this);
                        });
                        toastr.success("Добавлены параметры категории");
                        return true;
                    }
                    toastr.error(response);
                }
            );
        }

        function addItem(item) {
            $li         = $('<li class="ui-state-default"></li>');
            $input_hidd = $('<input type="hidden" name="parameters_id[]" value="' + item.id + '" />');
            $labeltext   = $('<label class="param-label">' + item.name + '  ' + '</label>');

            if (item.type == 0) {
                $input = $('<input type="text" name="values[]" />');
            }

            if (item.type == 1) {
                $input = $('<select name="values[]"></select>');
                $.each(item, function(key, value) {
                    $option = $('<option name="' + key + '">' + value + '</option>');
                    $input.append($option);
                });
            }

            $li.append($input_hidd).append($labeltext).append($input);
            $('#products_sortable').append($li);
        }
    </script>

@append


@section('styles')

    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

    <style>
        #products_sortable {
            border: 1px solid #eee;
            min-height: 20px;
            list-style-type: none;
            margin: 0;
            padding: 5px 0 0 0;
            margin-right: 10px;
            width: 100%;
        }

        #products_sortable li {
            margin: 0 5px 5px 5px;
            padding: 5px;
            border: 1px solid #ccc;
            background-color: #cce2c1;
        }
    </style>

@append

