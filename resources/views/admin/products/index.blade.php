@extends('admin.body')
@section('title', 'Объекты')

@section('centerbox')
    <div class="page-header">
        <h1> Администрирование <small><i class="ace-icon fa fa-angle-double-right"></i> Объекты </small> </h1>
    </div>

    <div class="row">
        <div class="col-xs-12">
            <a class="btn btn-success col-sm-2 col-xs-12" href="{{ URL::to('admin/products/create') }}">
                <i class="ace-icon fa fa-plus-square-o  bigger-120"></i>
                Создать
            </a>

            <div class="col-sm-2 col-xs-12">
                <div class="form-group">
                    {{ Form::select('agent_id', [null => 'Все агенты'] + $agents, null, ['class' => 'col-xs-12 form-control chosencat']) }}
                </div>
            </div>
        </div>

        <div class="col-xs-12">
            <div class="clearfix"><div class="pull-right tableTools-container"></div></div>
            <div class="table-header"> Объекты </div>

            <table id="dynamic-table" class="table table-striped table-bordered table-hover dataTable">
                <thead>
                <tr>
                    <th align="center">ID</th>
                    <th align="center">Создан</th>
                    <th align="center">Название (ru)</th>
                    <th align="center">Название (ro)</th>
                    <th align="center">Цена, €</th>
                    <th align="center">Площадь</th>
                    <th align="center">Тип сделки</th>
                    <th align="center"><i class="fa fa-cogs"></i></th>
                </tr>
                </thead>
                <tbody bgcolor="white"></tbody>
            </table>
        </div><!-- /.col-xs-12 -->
    </div><!-- /.row -->
@stop

@section('scripts')

    <script>
        var columns = [
            { data : 'id' },
            { data : 'created_at' },
            { data : 'name' },
            { data : 'name_ro' },
            { data : 'price' },
            { data : 'area', orderable: false, searchable: false },
            { data : 'type_text', name: 'type' },
            { data : 'actions', className : 'center' }
        ];

        $('select[name=agent_id]').change(function() {
            let data = {
                agent_id : $('select[name=agent_id]').val(),
            };
            $("#dynamic-table").data('dt_params', data).DataTable().ajax.reload();
        });

    </script>

    @include('admin.partials.datatable-init-ajax', ['ajax' => 'admin/get-products'])

    @include('admin.partials.chosen')

@endsection