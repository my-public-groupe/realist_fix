<tr class="">

    <td class="center">
        {{ $id }}
    </td>

    <td>
        <a href="admin/{{ $table }}/{{ $id }}/edit" class="modalbox" title="Редактирование: {{ $name }}"> {{ $name }}</a>
    </td>

    <td class="center">
        <div class="action-buttons">
            <a href="javascript:void(0);" class="{{ $enabled ? 'visible' : 'unvisible' }}  ajax-visibility" data-id="{{ $id }}">
                <i class="ace-icon fa fa-eye bigger-130"></i>
            </a>
        </div>
    </td>

    <td class="center">
        <div class="hidden-sm hidden-xs btn-group">

            <a href="admin/{{ $table }}/{{ $id }}/edit" class="btn btn-xs btn-info" title="Редактирование: {{ $name }}">
                <i class="ace-icon fa fa-pencil bigger-120"></i>
            </a>

            <a class="btn btn-xs btn-danger ajax-delete" data-id="{{ $id }}">
                <i class="ace-icon fa fa-trash-o bigger-120"></i>
            </a>

        </div>
    </td>

</tr>