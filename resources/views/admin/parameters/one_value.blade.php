<li class="ui-state-default">
    <span class="ui-icon ui-icon-arrowthick-2-n-s"></span>
    <input type="hidden" name="values[id][]" value="{{ $item->id ?? 0 }}" />
    <input type="text" name="values[ru][]" class="lang-ru" placeholder="значение" value="{{ $item->value ?? '' }}" />
    <input type="text" name="values[ro][]" class="lang-ro" placeholder="значение ro" value="{{ $item->value_ro ?? '' }}" />
    <input type="text" name="values[en][]" class="lang-en" placeholder="значение en" value="{{ $item->value_en ?? '' }}" />
    <i class="ace-icon fa fa-times-circle delete"></i>
</li>