@extends('admin.body')
@section('title', 'Список характеристик')

@section('centerbox')
    <div class="page-header">
        <h1> Администрирование <small><i class="ace-icon fa fa-angle-double-right"></i> Характеристики </small> </h1>
    </div>

    <div class="row">
        <div class="col-xs-12">
            <a class="btn  btn-success" href="{{ route('admin.parameters.create') }}">
                <i class="ace-icon fa fa-plus-square-o  bigger-120"></i>
                Создать
            </a>
            <div class="clearfix"><div class="pull-right tableTools-container"></div></div>
            <div class="table-header"> Список характеристик </div>

            <table id="dynamic-table" class="table table-striped table-bordered table-hover dataTable">
                <thead>
                <tr>
                    <th align="center">ID</th>
                    <th align="center">Наименование</th>
                    <th align="center"><i class="fa fa-cogs"></i></th>
                </tr>
                </thead>
                <tbody bgcolor="white"></tbody>
            </table>
        </div><!-- /.col-xs-12 -->
    </div><!-- /.row -->
@stop

@section('scripts')

    <script>
        var columns = [
            { data : 'id' },
            { data : 'name' },
            { data : 'actions', className : 'center' }
        ];
    </script>

    @include('admin.partials.datatable-init-ajax', ['ajax' => 'admin/get-parameters'])

@endsection