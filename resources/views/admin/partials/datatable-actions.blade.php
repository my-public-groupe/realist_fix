<div class="action-buttons" data-id="{{ $item->id }}" data-model="{{ get_class($item) }}">
    <a href="{{ route('admin.' . $item->getTable() . '.edit', $item->id) }}"><i class="ace-icon fa fa-pencil bigger-130"></i></a>
    @if (isset($item->enabled))
        <a href="javascript:void(0);" class="{{ $item->enabled ? 'visible' : 'unvisible' }} visibility-btn">
            <i class="ace-icon fa fa-eye bigger-130"></i>
        </a>
    @endif
    <a href="javascript:void(0);" class="red delete-btn"><i class="ace-icon fa fa-trash-o bigger-130"></i></a>
</div>