{!! HTML::script('/js/ckeditor/ckeditor.js') !!}

<script>
    $(document).ready(function(){
        var FORM_ID = '{{ $form_id ?? '' }}';
        if (FORM_ID != "") FORM_ID = "#" + FORM_ID + " ";
        $(FORM_ID + '.ckeditor').each(function(){
            CKEDITOR.replace($(this).attr('id'), {
                filebrowserImageBrowseUrl:  '/filemanager?type=Images',
                filebrowserBrowseUrl:       '/filemanager?type=Files',
                extraAllowedContent: 'ul(ul-half)',
                //extraPlugins: 'youtube'
            });
        });


        CKEDITOR.plugins.addExternal( 'youtube', '/js/ckeditor/plugins/youtube/', 'plugin.js' );

    });
</script>