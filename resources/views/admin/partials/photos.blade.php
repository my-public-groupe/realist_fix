<!-- PARAMS
    table       = model or uniq name
    table_id    = id of table
    thumbs      = array of thumbs paths (optional)
    div_id      = id of tab panel (optional)
    width       = width of final photo (optional)
    height      = height of final photo (optional)
    class       = css class of the panel (optional)
    with_watermark = (boolean) if need watermark selector (optional)
    crop        = array(XXXX, YYYY), XXXX-width, YYYY-height for cropping (optional)
-->

<div class="tab-pane {{ isset($class) ? $class : 0 }}" id="{{ $div_id ?? 'photos' }}" >
    <div class="col-sm-12 text-left">
        @if(isset($label_text))
            <div class="alert alert-info">
                {!! $label_text !!}
            </div>
        @endif
        @if (isset($with_watermark) && $with_watermark)
            <label>
                <input id="with_watermark" class="ace" type="checkbox" checked>
                <span class="lbl"> Водяной знак </span>
            </label>
            @if($table == 'products')
                <label style="margin-left: 50px;">Добавить лейбл</label>
                <select name="photo_label">
                    <option value="0"> --- Выберите значение ---</option>
                    <option value="1" @if(isset($data) && $data->photo_label == 1)selected @endif>Best choice</option>
                    <option value="2" @if(isset($data) && $data->photo_label == 2)selected @endif>Special offer</option>
                    <option value="3" @if(isset($data) && $data->photo_label == 3)selected @endif>Hot price</option>
                    <option value="4" @if(isset($data) && $data->photo_label == 4)selected @endif>Sold</option>
                    <option value="5" @if(isset($data) && $data->photo_label == 5)selected @endif>Rented</option>
                    <option value="6" @if(isset($data) && $data->photo_label == 6)selected @endif>Reserved</option>
                    <option value="7" @if(isset($data) && $data->photo_label == 7)selected @endif>Promo</option>
                </select>
            @endif
        @endif
    </div>

    <div id="queue"></div>

    <input class="file_upload" name="file_upload" type="file" multiple="true" id="uploadifive_{{ $div_id ?? 'photos' }}"/>

    <ul class="ace-thumbnails clearfix photos">

    </ul>
</div>

{{ HTML::script('ace/assets/js/uploadifive/jquery.uploadifive.min.js') }}
{{ HTML::style('ace/assets/js/uploadifive/uploadifive.css') }}

{{ HTML::style('https://cdnjs.cloudflare.com/ajax/libs/cropperjs/0.8.1/cropper.min.css') }}
{{ HTML::script('https://cdnjs.cloudflare.com/ajax/libs/cropperjs/0.8.1/cropper.min.js') }}

{!! HTML::script('ace/assets/js/jquery-ui.js') !!}

<script>
    $(document).ready(function(){
        var PHOTOS_DIV = "#{{ $div_id ?? 'photos' }}";

        var ratio = @if(isset($crop)) {{ $crop[0] / $crop[1] }} @else 1 @endif;

        PhotosObj{{ $div_id ?? 'photos' }} = new PhotosClass(PHOTOS_DIV, ratio);

        PhotosObj{{ $div_id ?? 'photos' }}.GetAjaxPhotos("{{ $table }}", "{{ $table_id }}");

        $(function() {
            $(PHOTOS_DIV).find('.file_upload').uploadifive({
                'buttonText'	   : "Добавить файлы",
                'buttonClass'	   : "btn btn-yellow",
                'height'		   : "50",
                'width'			   : "150",
                'auto'             : true,
                'removeCompleted'  : true,
                'simUploadLimit'   : 1,
                'onUpload' : function() {
                    var checked = $(PHOTOS_DIV).find('input#with_watermark').is( ":checked" );
                    $(PHOTOS_DIV).find('.file_upload').data('uploadifive').settings.formData =
                        {
                            @if(isset($table_id))table_id:  "{{ $table_id }}",              @endif
                            @if(isset($thumbs))thumbs:      "{{ implode(",", $thumbs) }}",  @endif
                            @if(isset($width)) width:       "{{ $width }}",                 @endif
                            table:	"{{ $table }}",
                            @if(isset($height))height:      "{{ $height }}",                @endif
                            '_token': "{{ csrf_token() }}",
                            with_watermark : checked
                        }
                },
                //'queueID'          : 'queue',
                'uploadScript'     : 'photos/upload',
                    'onUploadComplete' : function(file, data) {
                        try{
                            var response = JSON.parse(data);
                        }catch(e){
                            toastr.error("Ошибка загрузки фотографии.");
                            return;
                        }
                        if (response.success=="false"){
                            toastr.error(response.data);
                            return;
                        }

                        // add file to the list
                        PhotosObj{{ $div_id ?? 'photos' }}.ShowPhoto(response.data);
                    }
            });
        });

        initCropModalFormAjaxSubmit(PHOTOS_DIV);

    });

    function PhotosClass(div, ratio){

        this.PHOTOS_DIV = div;

        this.ratio = ratio;

        var that = this;

        this.ChangeDBSort = function(a, b){
            var _asort = a.attr("sort");
            var _bsort = b.attr("sort");
            a.attr("sort",_bsort);
            b.attr("sort",_asort);

            _a_id = a.find('input').val();
            _b_id = b.find('input').val();
            $.get("photos/changesort",
                    {
                        a_id:_a_id,
                        asort:_asort,
                        b_id:_b_id,
                        bsort:_bsort
                    },
                    function(){
                        //toastr.success('DONE');
                    }
            );
        };

        this.GetAjaxPhotos = function(table, table_id){
            $.get(
                    "photos/getphotos",
                    {
                        "table":	 table,
                        "table_id":  table_id
                    },
                    function (response) {
                        if (response.success=="false"){
                            toastr.error(response.data);
                            return;
                        }

                        $.each(response.data, function(i, item) {
                            that.ShowPhoto(item);
                        });
                    },
                    "json"
            );
        },

        this.ShowPhoto = function(data) {
            var photos_div      = this.PHOTOS_DIV;
            var $li             = $('<li sort="'+data.sort+'"></li>');
            var $img_href       = $('<a href="'+ data.path + data.filename + '" title="' + data.filename + '"></a>').fancybox();
            var $img            = $('<img width="160" height="160" src="'+ data.path + 'thumbs/' + data.filename + '" />');
            var $input          = $('<input type="hidden" name="photos[]" value ="' + data.id + '" />');
            var $tools          = $('<div class="tools"></div>');
            var $double_left    = $('<a href="javascript:void(0);" title="В начало" class="double_left"><i class="ace-icon fa fa-angle-double-left"></i></a>')
                                    .click(function(){
                                        var $curr  = $(this).closest('li');
                                        var $prev  = $curr.prev();
                                        var $first = $curr.parent().children().first();
                                        $first.before($curr);
                                        $prev.after($first);
                                        that.ChangeDBSort($curr, $first);
                                    });
            var $left           = $('<a href="javascript:void(0);" title="Левее" class="left"><i class="ace-icon fa fa-angle-left"></i></a>')
                                    .click(function(){
                                        var $curr = $(this).closest('li');
                                        var $prev = $(this).closest('li').prev();
                                        $($curr.after($prev));
                                        that.ChangeDBSort($curr, $prev);
                                    });
            var $delete         = $('<a href="javascript:void(0);" title="Удалить" data-id="' + data.id + '"><i class="ace-icon fa fa-trash-o"></i></a>')
                                    .click(function(){
                                        var id = $(this).data('id');
                                        if (confirm('Вы уверены?')) {
                                            $(this).parent().parent().remove();
                                            $.get("photos/delete/" + id  /* ,  {'id': id} */);
                                        }
                                    });

            var $right          = $('<a href="javascript:void(0);" title="Правее" class="right"><i class="ace-icon fa fa-angle-right"></i></a>')
                                    .click(function(){
                                        var $curr = $(this).closest('li');
                                        var $next = $(this).closest('li').next();
                                        $($curr.before($next));
                                        that.ChangeDBSort($curr, $next);
                                    });
            var $double_right   = $('<a href="javascript:void(0);" title="В конец" class="double_right"><i class="ace-icon fa fa-angle-double-right "></i></a>')
                                    .click(function(){
                                        var $curr  = $(this).closest('li');
                                        var $next  = $curr.next();
                                        var $last  = $curr.parent().children().last();
                                        $last.after($curr);
                                        $next.before($last);
                                        that.ChangeDBSort($curr, $last);
                                    });

            //var $rotate          = $('<a href="javascript:void();" title="Повернуть" class="rotate"><i class="ace-icon fa fa-repeat fa-1"></i></a>');

            var $crop = $('<a href="javascript:void(0);" title="Обрезать изображение" class="crop-emahe"><i class="ace-icon fa fa-crop"></i></a>')
                                .click(() => {
                                   cropImage($crop, this.ratio)
                                });

            $(this.PHOTOS_DIV).find('ul.photos')
                    .append($li.append($img_href.append($img))
                            .append($input)
                            .append($tools
                                    .append($double_left)
                                    .append($left)
                                    .append($delete)
                                    .append($right)
                                    .append($double_right)
                                    //.append($rotate)
                                    .append($crop)
                            )
                    ).sortable();
        };
    }

    function cropImage($btn, ratio) {
        var image = document.getElementById('image');
        var fullPath = $btn.closest('li').find('img').attr('src');
        var filename = fullPath.replace(/^.*[\\\/]/, '')
        image.src = '{{ config('photos.images_url') }}' + filename;
        $("input#photo_id").val($btn.closest('li').find('input').val());

        var $modal = $('#cropModal');

        $modal.modal('show');

        $modal.off('shown.bs.modal').on('shown.bs.modal', function (e) {
            cropper = new Cropper(image, {
                aspectRatio: ratio,
                viewMode: 1,
                crop: function(e) {
                    $("input#crop_x").val(parseInt(e.detail.x));
                    $("input#crop_y").val(parseInt(e.detail.y));
                    $("input#crop_width").val(parseInt(e.detail.width));
                    $("input#crop_height").val(parseInt(e.detail.height));
                }
            });
        });

        $modal.off('hide.bs.modal').on('hide.bs.modal', function (e) {
            cropper.destroy();
        });
    }

    function initCropModalFormAjaxSubmit(PHOTOS_DIV) {
        $("#cropModal .btn-primary").unbind().click(function (e) {
            e.preventDefault();
            var photo_id = $("input#photo_id").val();
            $.post('{{ url('photos/crop') }}',
                {
                    'photo_id' : photo_id,
                    'crop_x' : $("input#crop_x").val(),
                    'crop_y' : $("input#crop_y").val(),
                    'crop_width' : $("input#crop_width").val(),
                    'crop_height' : $("input#crop_height").val(),
                    '_token': "{{ csrf_token() }}",
                },
                function(response) {
                    var $img = $(PHOTOS_DIV + ' :input[value="' + photo_id + '"]').closest('li').find('img');
                    $img.attr('src', $img.attr('src') + '?' + (new Date).getTime());
                    toastr.success('Сохранено');
                    $("#cropModal").modal('hide');
                });
        });
    }

</script>

<!-- Modal -->
<div class="modal fade" id="cropModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Обрезать фото</h4>
            </div>
            <div class="modal-body">
                <div class="main">
                    <div class="row">
                        <div class="col-sm-11">
                            <img src="" id="image" style="max-width: 100%;" />
                            <input type="hidden" id="crop_x" />
                            <input type="hidden" id="crop_y" />
                            <input type="hidden" id="crop_width" />
                            <input type="hidden" id="crop_height" />
                            <input type="hidden" id="photo_id" />
                        </div><!-- /.col-sm-12 -->
                    </div><!-- /.row -->
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary">Сохранить</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->


