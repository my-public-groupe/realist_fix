<div class="tab-pane {{ $class ?? '' }}" id="description{{ $form_id ?? '' }}">

    <div class="tabbable tabs-left">

        <ul id="myTab" class="nav nav-tabs">
            <li class="active">
                <a href="#descRu" data-toggle="tab">Описание (ru)</a>
            </li>
            <li class="lang-ro">
                <a href="#descRo" data-toggle="tab">Описание (ro)</a>
            </li>
            <li class="lang-en">
                <a href="#descEn" data-toggle="tab">Описание (en)</a>
            </li>
        </ul>

        <div class="tab-content">

            <div class="tab-pane in active" id="descRu">
                {{ Form::textarea('description[ru]', $data->description ?? old('description.ru'), array('class' => 'ckeditor', 'id' => 'editor')) }}
            </div>
            <div class="tab-pane lang-ro" id="descRo">
                {{ Form::textarea('description[ro]', $data->description_ro ?? old('description.ro'), array('class' => 'ckeditor', 'id' => 'editor_ro')) }}
            </div>
            <div class="tab-pane lang-en" id="descEn">
                {{ Form::textarea('description[en]', $data->description_en ?? old('description.en'), array('class' => 'ckeditor', 'id' => 'editor_en')) }}
            </div>

        </div>

    </div>
</div>


@section('scripts')

    @include('admin.partials.ckeditor')

@append