{!! HTML::style('ace/assets/css/chosen.css') !!}
{!! HTML::script('ace/assets/js/chosen.jquery.min.js') !!}
<script>
    if ($("#chosencat").css('display') != 'none') $("#chosencat").chosen({search_contains: true, placeholder_text_multiple:"Начните вводить название",placeholder_text_single:"Начните вводить название", no_results_text:"Не найдено ничего" });
    $(".chosencat").each(function(){
        if ($(this).css('display') != 'none')
            $(this).chosen({search_contains: true, width: '100%', placeholder_text_multiple:"Начните вводить название",placeholder_text_single:"Начните вводить название", no_results_text:"Не найдено ничего" });
    });
</script>