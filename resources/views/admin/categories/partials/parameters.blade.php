<div class="tab-pane" id="parameters">
    <div class="row">
        <div class="col-sm-12">
            <div class="form-group">
                {{ Form::label('parameter', 'Выберите параметры', ['class'=>'col-sm-4 control-label no-padding-right showTip L6']) }}
                <div class="col-sm-4 showTip L6">
                    {{ Form::select('parameter', $parameters, 0, ['class' => 'form-control col-sm-11 col-xs-12', 'id' => 'parameter']) }}
                </div>
                <div class="col-sm-4">
                    <a href="javascript:addItem();" class="btn btn-sm btn-info showTip L6">
                        <i class="ace-icon fa fa-plus-circle bigger-110"></i>
                        Добавить
                    </a>
                </div>
            </div>
        </div><!-- /.col-sm-6 -->
    </div>

    <div class="row">
        <div class="col-sm-12">
            <ul id="sortable">
                @if (isset($category_parameters))
                    @foreach($category_parameters as $item)
                        <li class="ui-state-default">
                            <span class="ui-icon ui-icon-arrowthick-2-n-s"></span>
                            <input type="hidden" name="parameters[]" value="{{ $item->id }}" />
                            {{ $item->name }}
                            <i class="ace-icon fa fa-times-circle delete"></i>
                        </li>
                    @endforeach
                @endif
            </ul>
        </div>
    </div>
</div>

@section('styles')

    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

    <style>
        #sortable {
            border: 1px solid #eee;
            min-height: 20px;
            list-style-type: none;
            margin: 0;
            padding: 5px 0 0 0;
            margin-right: 10px;
            width: 100%;
        }

        #sortable li {
            margin: 0 5px 5px 5px;
            padding: 5px;
            border: 1px solid #ccc;
            background-color: #cce2c1;
        }

    </style>
@endsection

@section('scripts')

    {!! HTML::script('ace/assets/js/jquery-ui.js') !!}

    <script>
        $(document).ready(function(){

            $('#sortable').sortable();

            if ($('select[name=type]').val() == 1) {
                $('.tabbable').removeClass('hide');
            }

            initDelete();
        });

        $('select[name=type]').on('change', function() {
            if ( $('select[name=type]').val() == 1 ){
                $('.tabbable').removeClass('hide');
            }else{
                $('.tabbable').addClass('hide');
            }
        });

        function addItem() {
            var value   = $('select[name=parameter]').val();
            //проверяем, если уже добавили
            if ($('#sortable li input[value="' + value + '"]').length > 0) {
                toastr.error("Уже добавлен");
                return;
            }

            $li         = $('<li class="ui-state-default"></li>');
            $spansort   = $('<span class="ui-icon ui-icon-arrowthick-2-n-s"></span>');
            $input     = $('<input type="hidden" name="parameters[]" value="' + value + '" />');
            $spantext   = $('select[name=parameter] option:selected').text() + '  ';
            $delete     = $('<i class="ace-icon fa fa-times-circle delete"></i>');

            $li.append($spansort).append($input).append($spantext).append($delete);
            $('#sortable').append($li);

            initDelete();
        }

        function initDelete() {
            $('#sortable li i.delete').unbind( "click" ).click(function() {

                if (! confirm("Параметр будет удален для всех товаров категории!!!")) {
                    return false;
                }

                var parameter_id = $(this).closest('li').find('input[name="parameters[]"]').val();

                $this = $(this);

                $.get("{{ URL::to('admin/json/remove-parameter') }}",
                    {
                        'table_id': '{{ $data->id ?? 0 }}',
                        'parameters_id': parameter_id,
                        'table': 'categories'
                    },
                    function(response) {
                        if (response.success == "false") {
                            toastr.error(response.data);
                            return false;
                        }
                        if (response.success == "true") {
                            $this.closest('li').remove();
                            toastr.success("Удалено");
                            return true;
                        }
                        toastr.error(response);
                    }
                );
            });
        }
    </script>

@append
