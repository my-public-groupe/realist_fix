@extends('admin.body')
@section('title', 'Категория')


@section('centerbox')
    <div class="page-header">
        <h1> <a href="{{ URL::to('admin/categories') }}">Категории</a> <small><i class="ace-icon fa fa-angle-double-right"></i> Редактирование категории </small> </h1>
    </div>

    @if(!isset($data))
    {{ Form::open(['url' => 'admin/categories', 'class' => 'form-horizontal']) }}
    @else
    {{ Form::open(['url' => 'admin/categories/' . $data->id, 'method' => 'put', 'class' => 'form-horizontal']) }}
    @endif

    <div class="row">
        <div class="col-sm-6">
            <div class="form-group">
                {{ Form::label('name[ru]', 'Название (ru)', ['class'=>'col-sm-3 control-label no-padding-right']) }}
                <div class="col-sm-9">
                    {{ Form::text('name[ru]', (isset($data->name) ? $data->name : old('name.ru')), array('class' => 'col-sm-11 col-xs-12 name_ru')) }}
                </div>
            </div>
            <div class="form-group lang-ro" >
                {{ Form::label('name[ro]', 'Название (ro)', ['class'=>'col-sm-3 control-label no-padding-right']) }}
                <div class="col-sm-9">
                    {{ Form::text('name[ro]', (isset($data->name_ro) ? $data->name_ro : old('name.ro')), array('class' => 'col-sm-11 col-xs-12')) }}
                </div>
            </div>
            <div class="form-group lang-en">
                {{ Form::label('name[en]', 'Название (en)', ['class'=>'col-sm-3 control-label no-padding-right']) }}
                <div class="col-sm-9">
                    {{ Form::text('name[en]', (isset($data->name_en) ? $data->name_en : old('name.en')), array('class' => 'col-sm-11 col-xs-12')) }}
                </div>
            </div>
            <div class="form-group">
                {{ Form::label('slug', 'URL', ['class' => 'col-sm-3 control-label no-padding-right']) }}
                <div class="col-sm-9">
                    {{ Form::text('slug', (isset($data->slug) ? $data->slug : old('slug')), array('class' => 'col-sm-11 col-xs-12')) }}
                </div>
            </div>
        </div><!-- /.col-sm-6 -->
        <div class="col-xs-6">
            <div class="form-group">
                <div class="col-xs-12">
                    <div class="tabbable">
                        <ul id="myTab1" class="nav nav-tabs padding-12 tab-color-blue background-blue">

                            <li class="active">
                                <a href="#short_ru" data-toggle="tab" aria-expanded="true">RU</a>
                            </li>

                            <li class="lang-ro">
                                <a href="#short_ro" data-toggle="tab" aria-expanded="false">RO</a>
                            </li>

                            <li class="lang-en">
                                <a href="#short_en" data-toggle="tab" aria-expanded="false">EN</a>
                            </li>

                            <div class="center"> <span class="label label-xlg label-purple">Значение</span></div>
                        </ul>

                        <div class="tab-content">
                            <div class="tab-pane in active" id="short_ru">
                                {{ Form::textarea('description_short[ru]', $data->description_short ?? old('description_short.ru'), array('style'=>'width:100%', 'rows'=>'3')) }}
                            </div>
                            <div class="tab-pane lang-ro" id="short_ro">
                                {{ Form::textarea('description_short[ro]', $data->description_short_ro ?? old('description_short.ro'), array('style'=>'width:100%', 'rows'=>'3')) }}
                            </div>
                            <div class="tab-pane lang-en" id="short_en">
                                {{ Form::textarea('description_short[en]', $data->description_short_en ?? old('description_short.en'), array('style'=>'width:100%', 'rows'=>'3')) }}
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div><!-- /.row -->

    <div class="tabbable">
        <ul id="myTab4" class="nav nav-tabs padding-12 tab-color-blue background-blue">
            <li class="active">
                <a href="#photos" data-toggle="tab">Фото</a>
            </li>
            <li>
                <a href="#parameters" data-toggle="tab">Характеристики</a>
            </li>
            <li>
                <a href="#meta" data-toggle="tab">META</a>
            </li>
        </ul>
    </div>

    <div class="tab-content">

        @include('admin.partials.photos', ['table' => 'categories', 'table_id' => isset($data->id) ? $data->id : 0, 'class' => 'active'] )

        @include('admin.categories.partials.parameters')

        @include('admin.partials.meta')

    </div>



    <div class="form-actions">
        {{ Form::submit('Сохранить', array('class' => 'btn btn-success')) }}
    </div>

{{ Form::close() }}
@endsection

@section('scripts')

    @include('admin.partials.slug', ['input_name'=>'name[ru]'])

@append