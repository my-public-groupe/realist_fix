@extends('admin.body')
@section('title', 'Справочники')


@section('centerbox')
    <div class="page-header">
        <h1> <a href="{{ route('admin.lists.index', ['id' => $parent_id ?? 0]) }}">Страницы и контент</a><small><i class="ace-icon fa fa-angle-double-right"></i></small> </h1>
    </div>

    @if(!isset($data))
        {{ Form::open(['url' => 'admin/lists', 'class' => 'form-horizontal']) }}
    @else
        {{ Form::open(['url' => 'admin/lists/' . $data->id, 'method' => 'put', 'class' => 'form-horizontal']) }}
    @endif

    <div class="form-actions">
        <div class="row center">
            <div class="col-sm-2">
                <button id="submit_button1" type="submit" class="btn  btn-success btn-block btn-responsive" ><i class="ace-icon fa fa-floppy-o  bigger-120"></i> Сохранить </button>
            </div>
            <div class="col-sm-4">
                <div class="profile-contact-info">
                    <div class="profile-links align-left">

                        @if (isset($data))
                            <div class="btn btn-link">
                                <i class="ace-icon fa fa- bigger-120 green"></i>
                                ID: {{ $data->id }}
                            </div>

                            <div class="btn btn-link">
                                <i class="ace-icon fa fa-calendar bigger-120 green"></i>
                                Изменен: {{ $data->updated_at }}
                            </div>
                        @endif
                    </div>
                </div>
            </div>

        </div><!-- /.row -->
    </div><!-- /.form-actions -->

    <div class="row">
        <div class="col-sm-6">
            <div class="form-group">
                {{ Form::label('name[ru]', 'Заголовок (ru)', ['class'=>'col-sm-3 control-label no-padding-right']) }}
                <div class="col-sm-9">
                    {{ Form::text('name[ru]', $data->name ?? old('name.ru'), array('class' => 'col-sm-11 col-xs-12 name_ru')) }}
                </div>
            </div>
            <div class="form-group lang-ro" >
                {{ Form::label('name[ro]', 'Заголовок (ro)', ['class'=>'col-sm-3 control-label no-padding-right']) }}
                <div class="col-sm-9">
                    {{ Form::text('name[ro]', $data->name_ro ?? old('name.ro'), array('class' => 'col-sm-11 col-xs-12')) }}
                </div>
            </div>
            <div class="form-group lang-en">
                {{ Form::label('name[en]', 'Заголовок (en)', ['class'=>'col-sm-3 control-label no-padding-right']) }}
                <div class="col-sm-9">
                    {{ Form::text('name[en]', $data->name_en ?? old('name.en'), array('class' => 'col-sm-11 col-xs-12')) }}
                </div>
            </div>

            <div class="form-group">
                {{ Form::label('parent', 'Родитель', ['class'=>'col-sm-3 control-label no-padding-right']) }}
                <div class="col-sm-9">
                    @if(isset($parents))
                        @if(isset($parent_id))
                            {{ Form::select('parent_id', array("0" => "Нет родителя") + $parents, $parent_id, ['class' => 'col-sm-11 col-xs-12']) }}
                        @else
                            {{ Form::select('parent_id', array("0" => "Нет родителя") + $parents, '', ['class'=>'col-sm-11 col-xs-12']) }}
                        @endif
                    @endif
                </div>
            </div>

            <div class="form-group">
                {{ Form::label('slug', 'Key', ['class'=>'col-sm-3 control-label no-padding-right']) }}
                <div class="col-sm-9">
                    <input name="slug" type="text" value="{{ (isset($data->slug) ? $data->slug : old('slug')) }}" id="slug" class="col-sm-11 col-xs-12" @if(isset($data)) readonly @endif />
                </div>
            </div>

        </div><!-- /.col-sm-6 -->

        <div class="col-sm-6">
            <div class="form-group hide">
                <label for="mydate" class="col-sm-3 control-label no-padding-right"> Дата:</label>
                <div class="col-sm-5">
                    <div class="input-group">
                        <input type="date" name="date" class="form-control date-picker"
                               data-date-format="yyyy-mm-dd"
                               value="{{ (isset($data->created_at) ? date('Y-m-d', strtotime($data->created_at)) : old('date', date('Y-m-d'))) }}" />
                        <span class="input-group-addon">
                            <i class="fa fa-calendar bigger-110"></i>
                        </span>
                    </div>
                </div>
            </div>
        </div><!-- /.col-sm-6 -->
        <div class="col-xs-6">
            <div class="form-group">
                <div class="col-xs-12">
                    <div class="tabbable">
                        <ul id="myTab1" class="nav nav-tabs padding-12 tab-color-blue background-blue">

                            <li class="active">
                                <a href="#short_ru" data-toggle="tab" aria-expanded="true">RU</a>
                            </li>

                            <li class="lang-ro">
                                <a href="#short_ro" data-toggle="tab" aria-expanded="false">RO</a>
                            </li>

                            <li class="lang-en">
                                <a href="#short_en" data-toggle="tab" aria-expanded="false">EN</a>
                            </li>

                            <div class="center"> <span class="label label-xlg label-purple">Значение</span></div>
                        </ul>

                        <div class="tab-content">
                            <div class="tab-pane in active" id="short_ru">
                                {{ Form::textarea('description_short[ru]', $data->description_short ?? old('description_short.ru'), array('style'=>'width:100%', 'rows'=>'3')) }}
                            </div>
                            <div class="tab-pane lang-ro" id="short_ro">
                                {{ Form::textarea('description_short[ro]', $data->description_short_ro ?? old('description_short.ro'), array('style'=>'width:100%', 'rows'=>'3')) }}
                            </div>
                            <div class="tab-pane lang-en" id="short_en">
                                {{ Form::textarea('description_short[en]', $data->description_short_en ?? old('description_short.en'), array('style'=>'width:100%', 'rows'=>'3')) }}
                            </div>
                        </div>

                    </div>
                </div>
            </div>

            <div class="form-group">
                {{ Form::label('sort', 'Сортировка', ['class'=>'col-sm-3 control-label no-padding-right']) }}
                <div class="col-sm-9">
                    <input name="sort" type="number" value="{{ $data->sort ?? 0 }}" id="sort" class="col-sm-11 col-xs-12"/>
                </div>
            </div>
        </div>
    </div><!-- /.row -->
    <div class="tabbable">
        <ul id="myTab4" class="nav nav-tabs padding-12 tab-color-blue background-blue">
            <li class="active">
                <a href="#ru" data-toggle="tab">Описание</a>
            </li>

            <li>
                <a href="#photos" data-toggle="tab">Фото</a>
            </li>

            <li>
                <a href="#meta" data-toggle="tab">META</a>
            </li>

        </ul>


    </div>

    <div class="tab-content">
        <div class="tab-pane active" id="ru">

            <div class="tabbable  tabs-left">

                <ul id="myTab" class="nav nav-tabs">
                    <li class="active">
                        <a href="#descRu" data-toggle="tab">Описание (ru)</a>
                    </li>
                    <li class="lang-ro">
                        <a href="#descRo" data-toggle="tab">Описание (ro)</a>
                    </li>
                    <li class="lang-en">
                        <a href="#descEn" data-toggle="tab">Описание (en)</a>
                    </li>
                </ul>

                <div class="tab-content">
                    <div class="tab-pane in active" id="descRu">
                        {{ Form::textarea('description[ru]', $data->description ?? old('description.ru'), array('class' => 'ckeditor', 'id' => 'description')) }}
                    </div>
                    <div class="tab-pane lang-ro" id="descRo">
                        {{ Form::textarea('description[ro]', $data->description_ro ?? old('description.ro'), array('class' => 'ckeditor', 'id' => 'description_ro')) }}
                    </div>
                    <div class="tab-pane lang-en" id="descEn">
                        {{ Form::textarea('description[en]', $data->description_en ?? old('description.en'), array('class' => 'ckeditor', 'id' => 'description_en')) }}
                    </div>

                </div>

            </div>
        </div>

        @include('admin.partials.photos', ['table' => 'lists', 'table_id' => isset($data->id) ? $data->id : 0] )

        @include('admin.partials.meta')

    </div>

    <div class="form-actions">
        {{ Form::submit('Сохранить', array('class' => 'btn btn-success')) }}
    </div>

    {{ Form::close() }}
@endsection

@section('scripts')

    @include('admin.partials.ckeditor')

    @include('admin.partials.datepicker')

    @include('admin.partials.slug',['input_name'=>'name[ru]'])

@endsection
