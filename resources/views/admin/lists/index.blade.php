@extends('admin.body')
@section('title', 'Страницы и контент')
@section('centerbox')
    <div class="page-header">
        <h1> <a href="{{ route('admin.lists.index') }}">Страницы и контент</a>
            <small>
                @if(isset($parent))
                    @if (isset($parent->parent))
                         <i class="ace-icon fa fa-angle-double-right"></i>
                         <a href="{{ route('admin.lists.index', ['id' => $parent->parent->id]) }}">{{ $parent->parent->name }}</a>
                    @endif
                    <i class="ace-icon fa fa-angle-double-right"></i>
                    {{ $parent->name }}
                @endif
            </small>
        </h1>
    </div>

    <div class="row">
        <div class="col-xs-12">
            <a class="btn  btn-success" href="{{ URL::to('admin/' . $model . '/create?id='. $id ) }}">
                <i class="ace-icon fa fa-plus-square-o  bigger-120"></i>
                Создать
            </a>

            @yield('listbuttons')

            <div class="clearfix"><div class="pull-right tableTools-container"></div></div>
            <div class="table-header"> </div>

            <table id="dynamic-table" class="table table-striped table-bordered table-hover dataTable">
                <thead>
                <tr>
                    <th class="center">
                        <label class="pos-rel"><input type="checkbox" class="ace" /><span class="lbl"></span></label>
                    </th>
                    <th align="center">ID</th>
                    <th align="center">Наименование</th>
                    <th align="center">Key</th>
                    <th align="center"><i class="ace-icon fa fa-eye-slash bigger-130"></i></th>
                    <th align="center"><i class="menu-icon fa fa-cogs"></i> </th>
                </tr>
                </thead>
                <tbody bgcolor="white">
                @foreach($data as $d)
                    <tr class="">
                        <td class="center">
                            <label class="pos-rel"><input type="checkbox" class="ace" /><span class="lbl"></span></label>
                        </td>
                        <td align="right">
                            {{ $d->id }}
                        </td>

                        <td align="left">
                            @if (count($d->children) > 0)
                                <a title="Редактирование" href="{{ URL::to('admin/'.$model.'?id='.$d->id) }}">{{ $d->name }} >></a>
                            @else
                                <a title="Редактирование" href="{{ URL::to('admin/'.$model.'/'.$d->id.'/edit') }}">{{ $d->name }}</a>
                            @endif
                        </td>
                        <td align="center">
                           {{ $d->slug }}
                        </td>
                        <td align="center">
                            <div class="action-buttons">
                                <a href="javascript:void(0);" class="{{ $d->enabled ? 'visible' : 'unvisible' }}" data-id="{{ $d->id }}" data-model="{{ $model }}">
                                    <i class="ace-icon fa fa-eye bigger-130"></i>
                                </a>
                            </div>
                        </td>
                        <td align="center">
                            <div class="action-buttons">
                                <a href="{{ URL::to('admin/'.$model.'/'.$d->id.'/edit') }}" class="yellow"><i class="ace-icon fa fa-pencil bigger-130"></i></a>
                                {{ Form::open(array('url' => 'admin/' . $model . '/' . $d->id, 'class' => 'pull-right')) }}
                                {{ Form::hidden('_method', 'DELETE') }}
                                {{ Form::button('<i class="ace-icon fa fa-trash-o bigger-130"></i>', ['type' => 'submit', 'class' => 'red deletebutton']) }}
                                {{ Form::close() }}
                            </div>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div><!-- /.col-xs-12 -->
    </div><!-- /.row -->
@endsection

@section('styles')
    {{HTML::style('ace/assets/css/dataTables.responsive.css')}}
@endsection

@section('scripts')

    @include('admin.partials.datatable-init')

    @include('admin.partials.visibility')

    <script>
        $(document).ready(function(){
            $('.category-select').change(function(){
                location.href="admin/{{ $model }}?id=" + $( this ).val();
            });
        });
    </script>

@endsection

