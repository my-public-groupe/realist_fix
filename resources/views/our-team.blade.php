@extends('body')
@section('centerbox')
<section class="teams single-page">
        <div class="bread-crumbs">
            <div class="container">
                <span>@lang('common.youre_here'): <a href="{{route('index')}}">@lang('common.main')</a> / </span>
                <span class="active">@lang('common.our_agents')</span>
            </div>
        </div>
        <div class="container">
            <div class="row">
                    <div class="about-us-title col-md-12">
                       @lang('common.our_agents')
                    </div>
            </div>
            <div class="row">
                @foreach($agents as $agent)
                    @include('partials.one-employee')
                @endforeach
            </div>
            @include('partials.pagination')
        </div>
    </section>

    @include('partials.order-call-block')
    @include('partials.news-block')
@endsection