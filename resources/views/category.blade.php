@extends('body')

@section('centerbox')


    <h1>CATEGORY PAGE: {{ $category->name }}</h1>

    <ol>
        @foreach ($products as $item)
            <li><a href="{{ route('get-product', [$category->slug, $item->slug]) }}">{{ $item->name }}</a> {{ $item->mainphoto() }}</li>
        @endforeach
    </ol>

    {{ $products->links() }}


    <h2>Filters</h2>

    <ul>
        @foreach ($category->parameters as $parameter)

            <li>
                {{ $parameter->name }} [@if (isset($parameter->params['active'])) active @else not active @endif]

                @if ($parameter->type == \App\Models\Parameter::TYPE_INPUT)
                    [INPUTS]
                @endif

                @if ($parameter->type == \App\Models\Parameter::TYPE_SELECT)
                    <ol>
                        @foreach ($parameter->values as $value)
                            <li>{{ $value->value }}</li>
                        @endforeach
                    </ol>
                @endif
            </li>

        @endforeach
    </ul>
@endsection



