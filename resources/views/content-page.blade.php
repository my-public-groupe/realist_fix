@extends('body')
@section('centerbox')

    <div class="breadcrumbs">
        <div class="x-container">
            <div class="x-row">
                <div class="x-col">
                    <a href="{{ route('index') }}" class="breadcrumbs__link text--small link">@lang('common.main')</a>
                    <img src="/img/icons/chevron-right.svg" alt="chevron" class="breadcrumb__chevron">
                    <a href="javascript:void(0)" class="breadcrumbs__current-link text--medium-font link">Page</a>

                </div>
            </div>
        </div>
    </div>

<div class="content-page">
    <div class="content-page__container">
        <div class="content-page__row">





            <div class="content-page__col">
                <div class="content-page__img-row">
                    <div class="x-col">
                        <img src="/img/cat/1.jpg" alt="">
                    </div>
                </div>
            </div>




            <div class="content-page__col">


                <h1>Header 1</h1>
                <h2>Header 2</h2>
                <h3>Header 3</h3>



                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Asperiores ipsam magnam nostrum quasi rem. Aliquid cumque fugiat inventore ipsa laboriosam nesciunt nisi possimus repellendus reprehenderit repudiandae saepe sapiente tempore, voluptatem!</p>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Asperiores ipsam magnam nostrum quasi rem. Aliquid cumque fugiat inventore ipsa laboriosam nesciunt nisi possimus repellendus reprehenderit repudiandae saepe sapiente tempore, voluptatem!</p>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Asperiores ipsam magnam nostrum quasi rem. Aliquid cumque fugiat inventore ipsa laboriosam nesciunt nisi possimus repellendus reprehenderit repudiandae saepe sapiente tempore, voluptatem!</p>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Asperiores ipsam magnam nostrum quasi rem. Aliquid cumque fugiat inventore ipsa laboriosam nesciunt nisi possimus repellendus reprehenderit repudiandae saepe sapiente tempore, voluptatem!</p>

                <p>Text <a href="#">Link</a> </p>

                <ul>
                    <li>
                        Unordered item
                    </li>
                    <li>
                        Unordered item <a href="#">Link</a>
                    </li>
                    <li>
                        Unordered item
                    </li>
                    <li>
                        Unordered item
                    </li>
                    <li>
                        Unordered item
                    </li>
                    <li>
                        Unordered item
                    </li>
                </ul>

                <ol>
                    <li>Ordered item</li>
                    <li>Ordered item</li>
                    <li>Ordered item</li>
                    <li>Ordered item</li>
                    <li>Ordered item</li>
                    <li>Ordered item</li>
                    <li>Ordered item</li>
                    <li>Ordered item</li>
                </ol>



                <table>
                    <tr>
                        <th>Firstname </th>
                        <th>Lastname</th>
                        <th>Age</th>
                    </tr>
                    <tr>
                        <td>Jill</td>
                        <td>Smith</td>
                        <td>50</td>
                    </tr>
                    <tr>
                        <td>Eve</td>
                        <td>Jackson</td>
                        <td>94</td>
                    </tr>
                    <tr>
                        <td>John</td>
                        <td>Doe</td>
                        <td>80</td>
                    </tr>
                </table>


            </div>




        </div>
    </div>
</div>

@endsection