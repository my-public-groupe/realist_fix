<!doctype html>
<html lang="{{app()->getLocale()}}" prefix="og: http://ogp.me/ns#">

<head>
    <base href="/">
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=2.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <link rel="apple-touch-icon" sizes="180x180" href="/favicons/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/favicons/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/favicons/favicon-16x16.png">
    <link rel="manifest" href="/favicons/site.webmanifest">
    <link rel="mask-icon" href="/favicons/safari-pinned-tab.svg" color="#5bbad5">
    <meta name="msapplication-TileColor" content="#0000ff">
    <meta name="theme-color" content="#ffffff">

    @if(env('APP_ENV') == 'local')
        <meta name="robots" content="noindex,nofollow">
    @endif

    <title>@yield('meta_title', trans('common.meta_title'))</title>

    <meta property="og:url"           content="{{url()->current()}}" />
    <meta property="og:type"          content="website" />
    <meta property="og:title"         content="@yield('meta_title', trans('common.meta_title'))" />
    <meta property="og:description"   content="@yield('meta_description', trans('common.meta_description'))" />
    <meta property="og:image"         itemprop="image" content="@yield('og_image', url('/img/big-logo.png'))" />

    <meta name="keywords" content="@yield('meta_keywords', trans('common.meta_keywords'))"/>
    <meta name="description" content="@yield('meta_description', trans('common.meta_description'))"/>
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <meta name="twitter:card" content="@yield('meta_description', trans('common.meta_description'))" />
    <meta name="twitter:site" content="{{url()->current()}}" />
    <meta name="twitter:title" content="@yield('meta_title', trans('common.meta_title'))" />
    <meta name="twitter:description" content="@yield('meta_description', trans('common.meta_description'))" />
    <meta name="twitter:image" content="@yield('twitter_image', url('/favicons/android-chrome-512x512.png'))" />

    <link href='https://fonts.googleapis.com/css?family=Roboto:400,100,300,700' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

    {{-- app.css со старого сайта, нужен для некоторых страниц --}}
    @if(!in_array(Route::currentRouteName(), ['index', 'rent-category', 'sale-category', 'product', 'search']))
        <link rel="stylesheet" href="css/app.css">
    @endif

    <link rel="stylesheet" href="css/style.css?v={{ filemtime(public_path('css/style.css')) }}">
    <link rel="stylesheet" href="css/common.css?v={{ filemtime(public_path('css/common.css')) }}">
    <link rel="stylesheet" href="css/adaptive.css?v={{ filemtime(public_path('css/adaptive.css')) }}">
    <link rel="stylesheet" href="css/custom.css?v={{ filemtime(public_path('css/custom.css')) }}">
    <link rel="stylesheet" href="css/features.css?v={{ filemtime(public_path('css/features.css')) }}">

    <!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.5.1/jquery.min.js"></script> -->
    <link rel="stylesheet" type="text/css" href="js/carousel/jcarousel.responsive.css?v=2023">
    <script src="https://apps.elfsight.com/p/platform.js"; defer></script>
    <script>
        function scrollFunction() {
            const element = document.getElementById("body")
            const bodyRect = document.body.getBoundingClientRect()
            const elemRect = element.getBoundingClientRect()
            const needScrollToMyDivInPX = elemRect.top - bodyRect.top

            window.scrollTo({
                top: needScrollToMyDivInPX,
                behavior: 'smooth',
            })
        }
    </script>

    <style>
        .lazyload {
            opacity: 0;
        }

        .lazyloading {
            background: url('images/svg/loader.svg') no-repeat center;
            background-size: contain;
            opacity: 1;
            transition: opacity 300ms;
        }
    </style>

    @yield('styles')

    @if(env('APP_ENV') == 'production')
    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
                new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
            j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
            'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
        })(window,document,'script','dataLayer','GTM-NG6VCWL');</script>
    <!-- End Google Tag Manager -->
    @endif

    <script>
        window.lazySizesConfig = window.lazySizesConfig || {};
        window.lazySizesConfig.loadMode = 1;
    </script>
    <script src="js/lazysizes.min.js"></script>

    <script>
        document.addEventListener('lazybeforeunveil', function(e){
            let bg = e.target.getAttribute('data-bg');
            if(bg){
                e.target.style.backgroundImage = 'url(' + bg.replace(/\s/g, "%20") + ')';
            }
        });
    </script>
</head>

<body id="body">

@if(env('APP_ENV') == 'production')
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-NG6VCWL"
                  height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
@endif

<div class="custom-modal-content success-modal">
    <div class="form-container form-modal has-bg">
        <img src="images/modal-logo.png" alt="" class="modal-logo">
        <div class="close-btn">
            <span class="close-text">@lang('common.close')</span>
            <img src="images/svg/icon-close.svg" alt="">
        </div>

        <div class="modal-order-call-container">
            <div class="order-call-title">

            </div>
        </div>
    </div>


    <!--<div class="form-success form-modal">
        <div class="form-container ">
            <div class="close-btn">
                <span class="close-text">Закрыть</span>
                <img src="images/svg/icon-close.svg" alt="">
            </div>

            <div class="modal-order-call-container">
                <div class="order-call-title">
                    Ваша заявка успешна оформлена! <br />
                    Скоро вам перезвонят.
                </div>
                <div class="modal-order-call-form">

                    <button>Запросить звонок</button>

                </div>

            </div>
        </div>
    </div>-->
</div>

<!-- mobile call -->
<div class="custom-modal-content mobile-call-modal">
    <div class="form-container form-modal has-bg ">
        <img src="images/modal-logo.png" alt="" class="modal-logo">
        <div class="close-btn">
            <span class="close-text">@lang('common.close')</span>
            <img src="images/svg/icon-close.svg" alt="">
        </div>

        <div class="modal-order-call-container">
            <div class="order-call-title">
                @lang('common.leave_number')
            </div>
            <form id="mobile-call-form">
                {{csrf_field()}}
                <div class="modal-order-call-form">
                    <input type="text" name="name" placeholder="@lang('common.name')" required/>
                    <input type="text" name="phone" placeholder="@lang('common.phone')" required/>
                    <div class="modal-order-call-note">
                        {!! trans('common.personal_details_allow', ['link' => route('privacy-policy')]) !!}
                    </div>
                    <button type="submit">@lang('common.call')</button>
                </div>
            </form>

        </div>
    </div>
</div>

<div id="topBtn" onclick="scrollFunction()"></div>

<section class="mobile-phone-container">
    <a href="tel:@lang('common.call_center_num')" class="contact-phone">
        <img src="images/call-mobile.png" alt="">@lang('common.call_center_num')</a>
</section>

<div class="search hide">
    <form class="search__form" action="{{ route('search') }}">
        <div class="form-element">
            <button class="search__close" onclick="toggleSearch()"></button>
            <input type="text" name="searchword" placeholder="@lang('common.search_placeholder')" required>
        </div>

        <button type="submit" class="button button-blue">
            @lang('common.search')
        </button>
    </form>
</div>

<div id="app">
    {{-- call center --}}
    @include('partials.call-center')

    @include('partials.header')

    @yield('centerbox')

    @include('partials.footer')
</div>

<script>window.lang = '{{app()->getLocale()}}'</script>
<script>window.is_mobile = @json(\App\Http\Controllers\Controller::isMobile())</script>
<script>window.has_webp_support = @json(\App\Http\Controllers\Controller::hasWebp())</script>
<script src="{{ route('assets.lang') }}"></script>
<script src="{{mix('js/app.js')}}"></script>

<script src="js/jquery.min.js?v=111122"></script>
<script type="text/javascript" src="js/carousel/jquery.jcarousel.min.js"></script>
<script type="text/javascript" src="js/carousel/jcarousel.responsive.js?v={{ filemtime(public_path('js/carousel/jcarousel.responsive.js')) }}"></script>

<script src="js/common.js?v={{ filemtime(public_path('js/common.js')) }}"></script>
<script src="js/bootstrap.min.js?v={{ filemtime(public_path('js/bootstrap.min.js')) }}"></script>
<script src="js/main.js?v={{ filemtime(public_path('js/main.js')) }}"></script>

<script>
    $(function (){
        setTimeout(function (){
            let eapps_google_reviews = $('*[data-app="eapps-google-reviews"]');
            if(eapps_google_reviews.length){
                eapps_google_reviews.find('div').eq(5).text('{{ trans('common.what_customers_say') }}');
            }
        }, 1000);
    });
</script>

<script type="application/ld+json">
{
  "@context": "http://schema.org",
  "@type": "LocalBusiness",
  "url": "{{ route('index') }}",
  "logo": "{{ route('index') }}/images/logo.svg",
  "name": "{{ trans('common.meta_title') }}",
  "image": "{{ route('index') }}/images/logo.svg",
  "description": "{{ trans('common.meta_description') }}",
  "telephone": "{{ trans('common.call_center_num') }}",
  "email": "{{ trans('common.site_email') }}",
  "address" : {
    "@type": "PostalAddress",
    "streetAddress": "{{ trans('common.office_chisinau_address') }}",
    "addressLocality": "Chisinau",
    "telephone": "{{ trans('common.call_center_num') }}",
    "addressRegion": "Chisinau",
    "postalCode": "2009",
    "addressCountry" : {
      "@type": "Country",
      "name": "Moldova"
    }
  },
  "openingHours": "Mo,Tu,We,Th,Fr 08:30-18:00",
  "openingHours": "Sa 09:00-14:00",
  "hasMap": "https://www.google.com/maps/place/Realist+Estate+Agency/@47.020836,28.8229262,15z/data=!4m6!3m5!1s0x40c97ddb56228699:0xd3b066ae0873ff77!8m2!3d47.020836!4d28.8229262!16s%2Fg%2F11jhhf13cv",
  "geo": {
    "@type": "GeoCoordinates",
    "latitude": "47.020836",
    "longitude": "28.8229262"
  }
}
</script>

<script type="application/ld+json">
{
  "@context": "http://schema.org",
  "@type": "WebSite",
  "url": "{{ route('index') }}",
  "name": "Realist",
  "alternateName": "Realist Estate Agency",
  "potentialAction": {
    "@type": "SearchAction",
    "target": "{{ route('index') }}/search?searchword={search_term_string}",
    "query-input": "required name=search_term_string"
  }
}
</script>

@yield('scripts')

</body>
</html>