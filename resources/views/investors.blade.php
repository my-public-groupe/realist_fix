@extends('body')
@section('centerbox')
    <section class="investors single-page">
        <div class="bread-crumbs">
            <div class="container">
                <span>@lang('common.youre_here'): <a href="{{route('index')}}">@lang('common.main')</a> /</span>
                <span class="active">@lang('common.for_investors')</span>
            </div>
        </div>
        <div class="container">
            <div class="row">


                <div class="x-col investors__main-col">
                    <div class="investors__item">
                        <div class="investors__item-container">
                            <div class="investors__item-row">
                                <div class="investors__item-small-section">
                                    <div class="investors__imgs-wrap"><img src="images/investr_01_b.jpg"
                                                                           alt="building" class="investors__item-img">
                                        <img
                                                src="images/realist-figure-blue.svg" alt="figure"
                                                class="investors__figure-img"></div>
                                </div>
                                <div class="investors__item-big-section">
                                    <h2 class="title title--blue title--new-big investors__title-big">@lang('common.for_investors')</h2>
                                    <h3 class="title investors__title">@lang('common.investors_title1')</h3>
                                    <p class="text text--medium-font text--extra-big mb-60">@lang('common.investors_text1')</p>
                                    <h3 class="title investors__title">@lang('common.investors_title2')</h3>
                                    <div class="investors__btns"><a href="tel:@lang('common.investor_phone')"
                                                                    class="icon-btn icon-btn--blue icon-btn--white-icon icon-btn--big">
                                            <svg
                                                    xmlns="http://www.w3.org/2000/svg" width="16.974" height="17.006"
                                                    viewBox="0 0 16.974 17.006">
                                                <path id="phone"
                                                      d="M17.585,13.608v2.334a1.556,1.556,0,0,1-1.7,1.556,15.4,15.4,0,0,1-6.714-2.388,15.171,15.171,0,0,1-4.668-4.668A15.4,15.4,0,0,1,2.118,3.7,1.556,1.556,0,0,1,3.666,2H6A1.556,1.556,0,0,1,7.556,3.338,9.99,9.99,0,0,0,8.1,5.524a1.556,1.556,0,0,1-.35,1.642l-.988.988a12.448,12.448,0,0,0,4.668,4.668l.988-.988a1.556,1.556,0,0,1,1.642-.35,9.99,9.99,0,0,0,2.186.545A1.556,1.556,0,0,1,17.585,13.608Z"
                                                      transform="translate(-1.361 -1.25)" fill="none" stroke="#000"
                                                      stroke-linecap="round" stroke-linejoin="round"
                                                      stroke-width="1.5"></path>
                                            </svg>
                                            <span>@lang('common.investor_phone')</span></a> <a
                                                href="mailto:@lang('common.investor_email')"
                                                class="icon-btn icon-btn--blue icon-btn--white-icon icon-btn--big">
                                            <svg
                                                    xmlns="http://www.w3.org/2000/svg" width="18.45" height="14.588"
                                                    viewBox="0 0 18.45 14.588">
                                                <g id="mail" transform="translate(-0.955 -3.25)">
                                                    <path id="Path_3661" data-name="Path 3661"
                                                          d="M3.636,4H16.724A1.641,1.641,0,0,1,18.36,5.636v9.816a1.641,1.641,0,0,1-1.636,1.636H3.636A1.641,1.641,0,0,1,2,15.452V5.636A1.641,1.641,0,0,1,3.636,4Z"
                                                          fill="none" stroke="#000" stroke-linecap="round"
                                                          stroke-linejoin="round" stroke-width="1.5"></path>
                                                    <path id="Path_3662" data-name="Path 3662"
                                                          d="M18.36,6l-8.18,5.726L2,6" transform="translate(0 -0.364)"
                                                          fill="none" stroke="#000" stroke-linecap="round"
                                                          stroke-linejoin="round" stroke-width="1.5">
                                                    </path>
                                                </g>
                                            </svg>
                                            <span>@lang('common.investor_email')</span></a></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="investors__item investors__item--small-mb">
                        <div class="investors__item-container">
                            <div class="investors__item-row">
                                <div class="investors__item-small-section">
                                    <div class="investors__imgs-wrap"><img src="images/investr_02.jpg"
                                                                           alt="building" class="investors__item-img">
                                        <img
                                                src="images/realist-figure-blue.svg" alt="figure"
                                                class="investors__figure-img investors__figure-img--small"></div>
                                </div>
                                <div class="investors__item-big-section">
                                    <h3 class="title investors__title">@lang('common.investors_title3')</h3>
                                    <p class="text text--big">@lang('common.investors_text2')</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="investors__item ">
                        <div class="investors__item-container">
                            <div class="investors__item-row investors__item-row--reverse">
                                <div class="investors__item-medium-section">
                                    <h3 class="title investors__title">@lang('common.investors_title4')</h3>
                                    <p class="text text--big">@lang('common.investors_text3')</p>
                                </div>
                                <div class="investors__item-medium-section">
                                    <div class="investors__imgs-wrap investors__imgs-wrap--to-top"><img
                                                src="images/investr_03.jpg" alt="building" class="investors__item-img">
                                        <img src="images/realist-figure-blue.svg" alt="figure"
                                             class="investors__figure-img investors__figure-img--small"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="investors__item">
                        <div class="investors__item-container">
                            <div class="investors__item-row">
                                <div class="investors__item-small-section">
                                    <div class="investors__imgs-wrap"><img src="images/investr_04.jpg"
                                                                           alt="building" class="investors__item-img">
                                        <img
                                                src="images/realist-figure-blue.svg" alt="figure"
                                                class="investors__figure-img investors__figure-img--small"></div>
                                </div>
                                <div class="investors__item-big-section">
                                    <h3 class="title investors__title investors__title--small-mb">@lang('common.investors_title5')</h3>
                                    <p class="text text--big mb-30">
                                        @lang('common.investors_text4')
                                    </p>
                                    <h3 class="title investors__title investors__title--small-mb">@lang('common.investors_title6')</h3>
                                    <p class="text text--big">@lang('common.investors_text5')</p>
                                    <p class="text text--big mb-30">@lang('common.investors_text5_1')</p>
                                    <p class="text text--big">
                                        @lang('common.investors_text6_1') <a href="{{route('experts')}}" class="link">@lang('common.investors_text6_2')</a> @lang('common.investors_text6_3')
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </section>
@endsection