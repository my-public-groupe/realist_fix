@extends('body')
@section('centerbox')
    <div class="bread-crumbs">
        <div class="container">
            <span>@lang('common.youre_here'): <a href="{{route('index')}}">@lang('common.main')</a> / </span>
            <span class="active">@lang('common.search')</span>
        </div>
    </div>

    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <objects-list-search
                        searchword="{{ request('searchword') }}"
                        search_objects_url="{{route('get-search-results')}}">
                </objects-list-search>
            </div>
        </div>
    </div>
@endsection