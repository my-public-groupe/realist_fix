@extends('body')
@section('centerbox')

    <div class="breadcrumbs">
        <div class="x-container">
            <div class="x-row">
                <div class="x-col">
                    <a href="{{ route('index') }}" class="breadcrumbs__link text--small link">@lang('common.main')</a>
                    <img src="/img/icons/chevron-right.svg" alt="chevron" class="breadcrumb__chevron">
                    <a href="javascript:void(0)" class="breadcrumbs__current-link text--medium-font link">@lang('common.team')</a>
                </div>
            </div>
        </div>
    </div>

    <div class="team">
        <div class="team__img-section" style="display: none;">
            <div class="x-container">
                <div class="x-row x-row--centered">
                    <div class="x-col">
                        <div class="team__big-img">
                            <img src="/img/team/team_contacts.jpg" srcset="/img/team/team_contacts@2x.jpg" alt="">
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <section class="team__text-content">
            <div class="x-container">
                <div class="x-row x-row--centered">
                    <div class="x-col">
                        <h2 class="title title--blue title--new-big investors__title--small-mb">@lang('common.team')</h2>
                        <p class="team__main-text text main-big-text">
                            @lang('common.team_main_text')
                        </p>
                    </div>
                </div>
            </div>
        </section>



        <div class="team__members">

            <div class="x-container">

                <div class="team__members-out-row">
                    <div class="team__members-out-col">

                        <div class="team__members-inside-container">
                            <ul class="team__members-inside-row">

                                @foreach($agents as $agent)


                                    <li class="team__members-item team__members-item--dark">
                                        <div class="team__members-item__inside">
                                            <div class="team__members-item-content">
                                                <a href="{{ route('get-agent', $agent->id) }}" class="team__members-tem-img-wrap" style="background-image: url('/uploaded/{{ $agent->mainphoto() }}')">
                                                    <img src="/uploaded/{{ $agent->mainphoto() }}" alt="{{ $agent->name }}">
                                                </a>

                                                <div class="team__members-item-text text--smaller">
                                                    <b class="team__member-name text ">{{ $agent->name }}</b>
                                                    @if(isset($agent->params['function']))
                                                        <b class="team__member-function">{{ $agent->params['function'] ?? '' }}</b>
                                                    @endif
                                                    <i class="team__member-phone">
                                                        <a href="tel:{{ $agent->phone }}">{{ $agent->phone }}</a>
                                                    </i>
                                                    @if(isset($agent->params['email']))
                                                        <a href="mailto:{{ $agent->params['email']}}" class="team__member-email">{{ $agent->params['email']}}</a>
                                                    @endif



                                                </div>
                                            </div>
                                        </div>

                                        <a href="{{ route('get-agent', $agent->id) }}" class="team__member-add-link"></a>
                                    </li>

                                @endforeach


                                {{--Join element--}}
                                <li class="team__members-item">
                                    <a class="team__join-element" href="{{ route('careers') }}">
                                        <p class="text text--extra-big">@lang('common.want_to_work')</p>
                                        <span href="javascript:void(0)" class="button button--blue" >@lang('common.join_to_team')</span>
                                    </a>
                                </li>

                            </ul>
                        </div>



                    </div>
                </div>

                <ul class="team__members-list">

                </ul>


            </div>
        </div>


    </div>


@endsection