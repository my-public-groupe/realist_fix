@extends('body')

@section('centerbox')
    <section class="news single-page">
        <div class="bread-crumbs">
            <div class="container">
                <span>@lang('common.youre_here'): <a href="{{route('index')}}">@lang('common.main')</a> /</span>
                <span class="active">@lang('common.news')</span>
            </div>
        </div>
        <div class="container">
            <div class="section-title">
                <div class="text"> @lang('common.news')</div>
            </div>
            <div class="row">
                @foreach($news as $n)
                    <div class="col-md-4">
                        <a href="{{route('get-one-news', $n->slug)}}" class="news-item">
                            <div class="news-date">
                                {{$n->created_at->format('d.m.Y')}}
                            </div>
                            <div class="news-preview" style="background-image: url('uploaded/{{$n->mainphoto()}}')"></div>
                            <div class="news-title">
                                {{$n->name}}
                            </div>
                        </a>
                    </div>
                @endforeach
            </div>

            {{$news->links()}}
        </div>
    </section>
    @include('partials.consulting-block')
@endsection