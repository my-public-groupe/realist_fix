@extends('body')
@section('meta_title', $data->name?? null)
@section('meta_keywords', $data->meta->meta_keywords ?? null)
@section('meta_description', $data->meta->meta_description ?? null)
@section('og_image', url('/uploaded/' . $data->mainphoto()))
@section('twitter_image', url('/uploaded/' . $data->mainphoto()))
@section('centerbox')
    <section class="news single-page">
        <div class="bread-crumbs">
            <div class="container">
                <span>@lang('common.youre_here'): <a href="{{route('index')}}">@lang('common.main')</a> /</span>
                <span><a href="{{route('news')}}">@lang('common.news')</a> /</span>
                <span class="active">{{$data->name}}</span>
            </div>
        </div>
        <div class="container">

            <div class="row">
                @if($data->mainphoto(1) != 'no_photo.png')
                    <div class="img-preview" style="background-image: url('uploaded/{{$data->mainphoto(1)}}')">
                        <div class="post-date">{{$data->created_at->format('d.m.Y')}}</div>
                    </div>
                @endif

                <div class="one-article-text">
                    <div class="section-title">
                        <div class="text"> {{ $data->name }}</div>
                    </div>
                    <div class="section-description">
                        {!! $data->description !!}
                    </div>
                </div>
            </div>
        </div>
    </section>
    @include('partials.news-block')
    @include('partials.consulting-block')
@endsection